# -*- coding:utf-8 -*-

import os
import sys
proj_path = '/home/matsui-k/projects/d-rcn/'
sys.path.append('/home/matsui-k/projects/d-rcn')
input_L = int(sys.argv[1])
output_L = int(sys.argv[2])
i_gpu = int(sys.argv[3])
save_dir = os.getcwd()

if i_gpu == 3:
    os.environ['THEANO_FLAGS'] = 'device=gpu'
else:
    os.environ['THEANO_FLAGS'] = 'device=gpu{}'.format(i_gpu)
os.path.exists(save_dir)

import time

import numpy as np
import scipy.io as sio

from scripts.core import TN_utils, blind_utils, optimizers, compling_multiorder, models_positive_const
reload(TN_utils)
reload(models_positive_const)
reload(compling_multiorder)

os.system('nvidia-smi')

# In[5]:
input_tensors, output_tensors = blind_utils.load_blind(input_L, output_L, folder_name='in8out8')
input_tensors, n_input_tensors_list = blind_utils.multi_order_mix(input_tensors,
                                                                  max_j_input=input_L, max_j_output=output_L)
fodf = blind_utils.load_reconst_fodf('in8out8')


# In[6]:
N = 100000
train_input, train_output, test_input, test_output, input_coef = blind_utils.pre_blind_pc(input_tensors, fodf, N=N)


# In[8]:
nn = models_positive_const.TwoLayerAfterTC(
    j_input_list=range(0, input_L+1, 2), j_output_list=range(0, input_L+1, 2),
    n_input_tensors_list=n_input_tensors_list,
    n_hidden=10, func_key_list=['tanh', 'tanh'], error_func_key='l1')

t1 = time.clock()
results = models_positive_const.compile(train_input, train_output, test_input, test_output,
                                      model=nn, optimizer=optimizers.Adam())
compile_time = time.clock() - t1
print 'compile_time', compile_time

f_train, f_training_error, f_test_error, f_output, param_list = results

# train loop parameter
batch_size = 1000
n_epochs = 301
interval = 10

# train loop
N = train_input[0].shape[0]
n_batchs = N // batch_size
index_list = np.asarray(np.random.permutation(range(N)), dtype=np.int32)
N_test = test_input[0].shape[0]
test_index_list = np.asarray(range(N_test), dtype=np.int32)

training_error_array = np.zeros((n_epochs // interval + 2,))
test_error_array = np.zeros((n_epochs // interval + 2,))

# error without training
i_batch = 0
training_error = f_training_error(i_batch, index_list, batch_size)[0]
training_error_array[0] = training_error
test_error = f_test_error(i_batch, test_index_list, batch_size)[0]
test_error_array[0] = test_error


start_time = time.clock()
for i_epoch in xrange(n_epochs):
    index_list = np.random.permutation(index_list)
    test_index_list = np.random.permutation(test_index_list)

    for i_batch in xrange(n_batchs):
        f_train(i_batch, index_list, batch_size)

    if i_epoch % interval == 0:
        training_error = f_training_error(i_batch, index_list, batch_size)[0]
        training_error_array[i_epoch // interval + 1] = training_error
        test_error = f_test_error(0, test_index_list, batch_size)[0]
        test_error_array[i_epoch // interval + 1] = test_error
        if np.isnan(training_error) or np.isnan(test_error):
            break
        pred = TN_utils.predict_original_feature_pc(f_output, [tensor.transpose(0, 2, 1) for tensor in input_tensors],
                                          input_coef, n_dir=fodf.shape[1])

        print i_epoch, training_error, test_error, (time.clock() - start_time) // 60

train_time = time.clock() - start_time

sio.savemat(save_dir+'/pred'+str(output_L)+'.mat', {'pred':pred})
import gzip, cPickle
with gzip.open(save_dir+'/summary'+str(output_L)+'.pkl.gz', 'wb') as f:
    cPickle.dump(zip(['training_error_array', 'test_error_array'],
                [training_error_array, test_error_array]),
                 f)

save_list = ['compile_time', 'train_time', 'input_L', 'output_L', 'training_error_array', 'test_error_array']
str_list = [n + '\t{' + n + '}\n' for n in save_list]
write_list = [s.format(**locals()) for s in str_list]
with open(save_dir+'/summary'+str(output_L)+'.txt', 'w') as f:
    f.writelines(write_list)