import os
import sys
proj_path = '/home/matsui-k/projects/d-rcn/'
sys.path.append('/home/matsui-k/projects/d-rcn')
input_L = int(sys.argv[1])
output_L = int(sys.argv[2])
i_gpu = int(sys.argv[3])
save_dir = os.getcwd()

if i_gpu == 3:
    os.environ['THEANO_FLAGS'] = 'device=gpu'
else:
    os.environ['THEANO_FLAGS'] = 'device=gpu{}'.format(i_gpu)
os.path.exists(save_dir)

import time

import numpy as np
import scipy.io as sio

from scripts.core import TN_utils, blind_utils, optimizers, compling_multiorder, models_multi_order
reload(TN_utils)
reload(models_multi_order)
reload(compling_multiorder)

os.system('nvidia-smi')

# In[5]:
input_tensors, output_tensors = blind_utils.load_blind(input_L=8, output_L=8, folder_name='in8out8')

# In[6]:
N = 100000
train_input, train_output, test_input, test_output, input_coef, output_coef = blind_utils.pre_blind(input_tensors, output_tensors, N=N)


# In[8]:
n_hidden_tensors = 100
func_key_list = ['relu', 'relu', 'linear']
nn = models_multi_order.Rcn3layer_multioreder(
        j_input_list=range(0, input_L+1, 2), j_output=output_L,
        n_input_tensors_list=[1 for i in range(0, input_L+1, 2)],
        n_hidden_tensors=n_hidden_tensors, func_key_list=func_key_list)

i_output = range(0, output_L+1, 2).index(output_L)

t1 = time.clock()
results = compling_multiorder.compile(train_input, train_output[i_output], test_input, test_output[i_output],
                                      model=nn, make_updates=optimizers.adam)
compile_time = time.clock() - t1
print 'compile_time', compile_time

f_train, f_training_error, f_test_error, f_output, param_list = results

# adam parameter
alpha = 0.001
beta1 = 0.9
beta2 = 0.999
eps = 10e-8
t = 0.
lr_decay = 0.9

# train loop parameter
batch_size = 1000
n_epochs = 301
interval = 10

# train loop
N = train_input[0].shape[0]
n_batchs = N // batch_size
index_list = np.asarray(np.random.permutation(range(N)), dtype=np.int32)
N_test = test_input[0].shape[0]
test_index_list = np.asarray(range(N_test), dtype=np.int32)

training_error_array = np.zeros((n_epochs // interval + 2,))
test_error_array = np.zeros((n_epochs // interval + 2,))

# error without training
i_batch = 0
training_error = f_training_error(i_batch, index_list, batch_size)[0]
training_error_array[0] = training_error
test_error = f_test_error(i_batch, test_index_list, batch_size)[0]
test_error_array[0] = test_error


start_time = time.clock()
for i_epoch in xrange(n_epochs):
    index_list = np.random.permutation(index_list)
    test_index_list = np.random.permutation(test_index_list)

    for i_batch in xrange(n_batchs):
        t += 1.
        f_train(i_batch, index_list, batch_size, alpha, beta1, beta2, eps, t)
        
    if i_epoch % interval == 0:
        training_error = f_training_error(i_batch, index_list, batch_size)[0]
        training_error_array[i_epoch // interval + 1] = training_error
        test_error = f_test_error(0, test_index_list, batch_size)[0]
        test_error_array[i_epoch // interval + 1] = test_error

        alpha *= lr_decay
        print i_epoch, training_error, test_error, (time.clock() - start_time) // 60

train_time = time.clock() - start_time

# In[15]:
pred = TN_utils.predict_original_feature_multiorder(f_output, [tens[:, np.newaxis, :] for tens in input_tensors],
                                            np.array(input_coef)[:, np.newaxis], 2*(2*output_L+1))

sio.savemat(save_dir+'/pred'+str(output_L)+'.mat', {'pred':pred})

import gzip, cPickle
with gzip.open(save_dir+'/summary'+str(output_L)+'.pkl.gz', 'wb') as f:
    cPickle.dump(zip(['n_hidden_tensors', 'func_key_list', 'N', 'batch_size',
                'lr_decay', 'training_error_array', 'test_error_array',
                'compile_time', 'train_time', 'output_coef'],
                [n_hidden_tensors, func_key_list, N, batch_size,
                 lr_decay, training_error_array, test_error_array,
                 compile_time, train_time, output_coef]),
                 f)
