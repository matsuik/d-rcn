"""

"""

import sys
proj_path = '/home/matsui-k/projects/d-rcn/'
sys.path.append('/home/matsui-k/projects/d-rcn')
input_L = int(sys.argv[1])
output_L = int(sys.argv[2])
i_gpu = int(sys.argv[3])
save_dir = proj_path+'results/blind/sigm_2_100_0920'


import os
os.environ['THEANO_FLAGS'] = 'device=gpu{}'.format(i_gpu)
os.path.exists(save_dir)

import time

import numpy as np
import scipy.io as sio

from scripts.core import TN_utils, optimizers, compling_multiorder, models_multi_order, blocks
reload(TN_utils)
reload(models_multi_order)
reload(compling_multiorder)
reload(blocks)

# In[5]:
input_tensors, output_tensors = TN_utils.load_blind(input_L, output_L=8, folder_name='in8out8')

# In[6]:
N = 100000
train_input, train_output, test_input, test_output, input_coef = TN_utils.pre_blind_tanh(input_tensors, output_tensors, N=N)


# In[8]:

n_hidden_tensors = 100
func_key_list = ['relu', '0.5*sigmoid+0.5']
nn = models_multi_order.Rcn2layer_multioreder(
        j_input_list=range(0, input_L+1, 2), j_output=output_L, n_input_tensors_list=[1 for i in range(0, input_L+1, 2)],
        n_hidden_tensors=n_hidden_tensors, func_key_list=func_key_list)

i_output = range(0, input_L+1, 2).index(output_L)

t1 = time.clock()
results = compling_multiorder.compile(train_input, train_output[i_output], test_input, test_output[i_output],
                                      model=nn, make_updates=optimizers.adam)
compile_time = time.clock() - t1

f_train, f_training_error, f_test_error, f_output, param_list = results


# In[13]:

batch_size = 1000
N = train_input[0].shape[0]
n_batchs = N // batch_size
index_list = range(N)
index_list = np.asarray(np.random.permutation(index_list), dtype=np.int32)

N_test = test_input[0].shape[0]
test_index_list = np.asarray(range(N_test), dtype=np.int32)

n_epochs = 101
interval = 10
training_error_array = np.zeros((n_epochs // interval + 2,))
test_error_array = np.zeros((n_epochs // interval + 2,))

alpha = 0.001
beta1 = 0.9
beta2 = 0.999
eps = 10e-8
t = 0.

beta = 0.01

i_batch = 0
training_error = f_training_error(i_batch, index_list, batch_size, beta, t)[0]
training_error_array[0] = training_error
test_error = f_test_error(i_batch, test_index_list, batch_size)[0]
test_error_array[0] = test_error

lr_decay = 0.95
start_time = time.clock()
for i_epoch in xrange(n_epochs):
    index_list = np.random.permutation(index_list)
    test_index_list = np.random.permutation(test_index_list)

    for i_batch in xrange(n_batchs):
        t = t + 1.
        f_train(i_batch, index_list, batch_size, alpha, beta1, beta2, eps, t, beta, t)
        
    if i_epoch % interval == 0:
        training_error = f_training_error(i_batch, index_list, batch_size, beta, t)[0]
        training_error_array[i_epoch // interval + 1] = training_error
        test_error = f_test_error(0, test_index_list, batch_size)[0]
        test_error_array[i_epoch // interval + 1] = test_error

        alpha = alpha * lr_decay
        print i_epoch, training_error, test_error
train_time = time.clock() - start_time

# In[15]:
pred = TN_utils.predict_original_feature_multiorder(f_output, [tens[:, np.newaxis, :] for tens in input_tensors],
                                            np.array(input_coef)[:, np.newaxis], 2*(2*output_L+1))

sio.savemat(save_dir+'/pred'+str(output_L)+'.mat', {'pred':pred})

import gzip, cPickle
with gzip.open(save_dir+'/summary'+str(output_L)+'.pkl.gz', 'wb') as f:
    cPickle.dump(zip(['n_hidden_tensors', 'func_key_list', 'N', 'batch_size',
                'lr_decay', 'training_error_array', 'test_error_array',
                'compile_time', 'train_time'],
                [n_hidden_tensors, func_key_list, N, batch_size,
                 lr_decay, training_error_array, test_error_array,
                 compile_time, train_time]),
                 f)
