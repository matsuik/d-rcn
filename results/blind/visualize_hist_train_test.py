
# coding: utf-8
import os, sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import gzip, cPickle
import scipy.io as sio

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('save_dir', type=str)
parser.add_argument('l_list', nargs='+')
parser.add_argument('--weight', action='store_true', default=False)
args = parser.parse_args()
save_dir = args.save_dir
L_list = args.l_list

os.chdir(save_dir)

pred_list = []
for L in L_list:
    ms = sio.loadmat('pred'+str(L)+'.mat')
    pred_list.append(ms['pred'])

for L, pred in zip(L_list, pred_list):
    print pred.shape
    plt.figure()
    plt.hist(np.linalg.norm(pred, axis=1), bins=100)
    plt.title('hist'+str(L))
    plt.savefig('hist'+str(L)+'.png')

summary_list = []
for L in L_list:
    with gzip.open('summary'+str(L)+'.pkl.gz', 'rb') as f:
        summary_list.append(cPickle.load(f))

for L, summary in zip(L_list, summary_list):
    plt.figure()
    plt.plot(dict(summary)['training_error_array'])
    plt.plot(dict(summary)['test_error_array'])
    plt.savefig('errors'+str(L)+'.png')

if args.weight:
    npz = np.load('params'+str(L)+'.npz')
    for name, array in zip(npz['arr_1'], npz['arr_0']):
        plt.figure()
        plt.hist(array.flatten(), bins=100)
        plt.savefig('param'+str(L)+name+'.png')

see_norm = False
if see_norm:
    with open('norms'+str(L)+'.npz', 'r') as f:
        params = np.load(f)
        for i, param in enumerate(params['arr_0']):
            plt.figure()
            plt.hist(param.flatten(), bins=100)
            print 'norms'+str(L)+str(i)+'.png'
            plt.savefig('norms'+str(L)+str(i)+'.png')





