i_kng=$2
filename=$1
input_max_L=$3
output_L=$4
i_gpu=$5

ssh kng0${i_kng} "cd ~/projects/d-rcn/results/blind/${filename} && source ~/setpath_theano.sh && nohup python worker.py ${input_max_L} ${output_L} ${i_gpu}" 