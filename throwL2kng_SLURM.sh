filename=$1
i_kng=$2
input_max_L=$3
output_L=$4
i_gpu=$5

ssh kng0${i_kng} "cd ~/projects/d-rcn/results/blind/${filename} && nohup srun -c 2 --mem 20000 --nodelist=kng0${i_kng} --gres=gpu:1 -t 1440 python worker.py ${input_max_L} ${output_L} 3" 
