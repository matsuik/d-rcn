
img = imread('../data/line_detection/neuron01.png');
img=single(img)/255;
load ../results/line_detection/neuron1_gt1_train.mat
figure;TN_plot(img, gt1_train, 1, 2);

mask = TN_mask(img, 2, 0.05);
pad2 = zeros(size(gt1_train));
pad(:, mask) = gt1_train(:, mask);
figure;TN_plot(img, pad, 1, 2);


img = imread('../data/line_detection/neuron02.png');
img=single(img)/255;
load ../results/line_detection/neuron2_gt1_train.mat
figure;TN_plot(img, gt1_train, 1, 2);

img = imread('../data/line_detection/neuron03.png');
img=single(img)/255;
load ../results/line_detection/neuron3_gt1_train.mat
figure;TN_plot(img, gt1_train, 1, 2);

