function TN_plot(img,V,skip,order,rescale)

    clf;
    %imshow(img);
    imagesc(img);
    colormap gray
    hold on
    
    imgsz=size(img);
    if nargin<3
        skip=2;
    end;
    if nargin<5
        rescale=1;
    end;
    [X Y] = ndgrid(1:skip:(imgsz(1)),1:skip:(imgsz(2)));
    
    scaling=3/max(sqrt(sum(V(:,:).^2,1)));
    
    U=V(:,1:skip:end,1:skip:end);
    
    
    if (order==1)
        quiver([Y(:)],[X(:)],scaling*[U(2,:).'],scaling*[U(1,:).'],'r','AutoScale','off');
    else
        tmp=(U(1,:)+1i*U(2,:)).';

        tmp=abs(tmp).*(tmp./abs(tmp)).^(1/rescale);
        
        X2=[];
        Y2=[];
        U1=[];
        U2=[];
        rot=exp((1)*1i*2*pi/order);
        for o=1:order
            X2=[X2(:);X(:)];
            Y2=[Y2(:);Y(:)];
            U1=[U1(:);real(tmp(:))];
            U2=[U2(:);imag(tmp(:))];
            tmp=rot*tmp;
        end;
        quiver(Y2(:).',X2(:).',scaling*U2(:).',scaling*U1(:).','c','AutoScale','off');
    end;
    