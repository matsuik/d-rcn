function dmat=distmat(b,a)
  dmat=(-2.*(a*b'))+repmat(sum(a.^2,2),1,size(b,1))+(repmat(sum(b.^2,2)',size(a,1),1));