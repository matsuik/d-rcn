function [featureImg, M, a] = TN_b(img, products, M)
            % featureImg: shape(1)*shape(2)x60 complex double
            shape=size(img);
            scales=[1:5];
            max_order= M.max_order;
            epsilon=0.01;

            for s=1:numel(scales)
                %optionally: local standard normalization factor
                std_dev=TN_std(img,scales(s)*1.5,'inv',false);
                
                % the Gausian filtered image
                img_smoothed=TN_smooth(img,scales(s),'normalize',true);
                
                
                
                a{s}=zeros([max_order,shape]);
                
                % first order complex derivative
                a{s}(1,:,:)=TN_complex_derivative(img_smoothed);
                
                % remaining derivatives
                for o=2:max_order
                    a{s}(o,:,:)=TN_complex_derivative(squeeze(a{s}(o-1,:,:)));
                end;
                
                % normalizing with respect to local intensity variation
                for o=1:max_order
                    a{s}(o,:,:)=squeeze(a{s}(o,:,:))./(std_dev+epsilon);
                end;
            end;
            
            num_tensors_per_scale=(size(products,1)); 
            featureImg=zeros([prod(shape),num_tensors_per_scale*numel(scales)]);
                count=1;
                for s=1:numel(scales)
                    for p=1:size(products,1)
                        if (products(p,2)==0)
                            featureImg(:,count)=a{s}(2,:);
                        elseif (products(p,4)==0)
                                if (products(p,end)>0)
                                    featureImg(:,count)=a{s}(products(p,1),:).*a{s}(products(p,2),:);
                                else
                                    featureImg(:,count)=a{s}(products(p,1),:).*conj(a{s}(products(p,2),:));
                                end;
                        else
                            switch products(p,end)
                                case 1
                                    featureImg(:,count)=a{s}(products(p,1),:).*conj(a{s}(products(p,2),:)).*conj(a{s}(products(p,3),:));
                                case 2
                                    featureImg(:,count)=a{s}(products(p,1),:).*a{s}(products(p,2),:).*a{s}(products(p,3),:);
                                case 3
                                    featureImg(:,count)=a{s}(products(p,1),:).*a{s}(products(p,2),:).*conj(a{s}(products(p,3),:));
                            end;
                        end;
                        count=count+1;
                    end;
                end;
                
                M.shape = shape;
                M.scales = scales;
                M.epsilon = 0.01;