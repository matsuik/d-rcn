function index = TN_mask(img, scale, thresh)
    % scaleは5くらい
    % threshは0.25とか
    std_dev=TN_std(img,scale,'inv',false);
    index = squeeze(abs(TN_smooth(img,scale)./(std_dev+0.1)))>thresh;