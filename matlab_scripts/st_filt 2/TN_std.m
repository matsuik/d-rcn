% computes the local intenisty standard deviation 
function std_dev=TN_std(Img,sigman,varargin)



for k = 1:2:length(varargin),
    eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

classid=class(Img);

    
    img_mean=TN_smooth(Img,sigman,'normalize',true);
    img_mean2=TN_smooth(Img.^2,sigman,'normalize',true);

    
  
    tmp=img_mean2-img_mean.^2;
    
    std_dev=cast(real((sqrt(tmp.*(tmp>0)))),classid);
    

   


