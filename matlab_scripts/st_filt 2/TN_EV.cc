#include <math.h>
#include "mex.h"
#include <vector>
#include <complex>
#include <cmath>
#include <omp.h>
#include <sstream>
#include <cstddef>
#include <vector>



#define EPSILON 0.00000000000001

#ifdef _OCTAVE_
	  #include "lapacke.h"
	#else
	    #include "lapack.h"
	#endif    
	#include "string.h"


	bool eig_2x2_symm(const double & xx,
			  const double & xy,
			  const double & yy,
			  double * Evec, 
			  double * Eval,
			  bool sort=true
			) 
	{
	  double A[]={xx,xy,
			xy,yy};      
	  char JOBZ='V';  
	  char UPLO='U';
	  
	  
	    std::ptrdiff_t N=2;
	    std::ptrdiff_t ldh=N;
	    std::ptrdiff_t lwork=3*N-1;
	    std::ptrdiff_t info;
	    
	    double work[lwork];
	  
	    dsyev(  
		&JOBZ,
		&UPLO,
		&N,
		A,
		&ldh,
		Eval,
		work,
		&lwork,
		&info
		      );
	    
	    
	  memcpy(Evec, A,sizeof(double)*4);
	  if (sort)
	  {
if (std::abs(Eval[1])>std::abs(Eval[0]))
	      {
		std::swap(Eval[1],Eval[0]); 
		std::swap(Evec[2],Evec[0]);
		std::swap(Evec[3],Evec[1]);
	      }
	  }   
	  
	  return (info==0);

	}

	bool eig_2x2_symm(const float & xx,
			  const float & xy,
			  const float & yy,
			  float * Evec, 
			  float * Eval,
			  bool sort=true) 
	{
	  
	    
	  char JOBZ='V';  
	  char UPLO='U';
	//     float D[N];
	//     float E[N-1];
	    float A[]={xx,xy,
			xy,yy};       
			
	  
	  
	  
	    std::ptrdiff_t N=2;
	    std::ptrdiff_t ldh=N;
	    std::ptrdiff_t lwork=3*N-1;
	    std::ptrdiff_t info;
	    float work[lwork];

		ssyev( &JOBZ,
		      &UPLO,
		      &N,
		      A,
		      &ldh,
		      Eval,
		      work,
		      &lwork,
		      &info
		);
	
	  
	  memcpy(Evec, A,sizeof(float)*4);
	    if (sort)
	 	  {
	  
	      if (std::abs(Eval[1])>std::abs(Eval[0]))
	      {
		std::swap(Eval[1],Eval[0]); 
		std::swap(Evec[2],Evec[0]);
		std::swap(Evec[3],Evec[1]);
	      }
	  
	  }   
	  
	  return (info==0);

	  
	}



template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

  
    const mxArray *SalTensor;
    SalTensor = prhs[0];       
    const int numdim = mxGetNumberOfDimensions(SalTensor);
    const int *dims = mxGetDimensions(SalTensor);

//     printf("input dims : %d\n",numdim);
if (numdim==3)
{
    int shape[3];  
    shape[0] = dims[2];
    shape[1] = dims[1];    
    T *saltensor = (T*) mxGetData(SalTensor);
    if (dims[0]!=3)
      mexErrMsgTxt("error: first dim must be 6\n");
    
    int ndims[3];
    ndims[0]=4;
    ndims[1]=dims[1];
    ndims[2]=dims[2];
    plhs[0] = mxCreateNumericArray(3,ndims,mxGetClassID(SalTensor),mxREAL);
    T *eigen_vec = (T*) mxGetData(plhs[0]);

    ndims[0]=2;
    ndims[1]=dims[1];
    ndims[2]=dims[2];
    plhs[1] = mxCreateNumericArray(3,ndims,mxGetClassID(SalTensor),mxREAL);
    T *eigen_val = (T*) mxGetData(plhs[1]);
    

    
    
    bool sort=true;
    

    std::size_t numvoxel=shape[0]*shape[1];
    
    
    for (std::size_t idx= 0; idx < numvoxel; idx++)    
    {
	eig_2x2_symm(	saltensor[0],
			saltensor[2],
			saltensor[1],
			eigen_vec, 
			eigen_val);       

	  
 	  saltensor+=3;
 	  eigen_val+=2;
 	  eigen_vec+=4;
  }

		
}
}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

  if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
   _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
  else
    if (mxGetClassID(prhs[0])==mxSINGLE_CLASS)
    _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
      else 
	mexErrMsgTxt("error: unsupported data type\n");
  
}