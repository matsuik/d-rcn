function V = TN_filter(img)
            
  shape=size(img);
  scales=[1:5];

  %initializing a 3D array for the votes (magnitues)
  %per scale [scales x image width x image height]
  F=zeros([numel(scales),shape]);

  %initializing a 3D array for the voting directions
  %per scale [scales x 2 x image width x image height]
  Votes=zeros([numel(scales),2,shape]);


  %computing filter response for each scale
  for s=1:numel(scales)
      %optionally:  computing a local normalization 
      %factor to account for local contrast variations
      std_dev=TN_std(img,scales(s)*1.5,'inv',false);
      
      %do the Gaussian smoothing
      img_smoothed=TN_smooth(img,scales(s),'normalize',true);
      
      %compute the Hessian matrix field [dxx,dyy,dxy](I_sigma)
      H=TN_derivatives(img_smoothed);

      %substract the trace
      Lap=sum(H(1,:)+H(2,:),1);
      H(1,:)=H(1,:)-Lap(1,:);
      H(2,:)=H(2,:)-Lap(1,:);

      %compute the eigenvectors and eigenvalues
      [Evec,Eval]=TN_EV(H);
      
      %store them in the voting field 
      Votes(s,:,:,:)=Evec(1:2,:,:,:);

      %compute the filter magnitude
      response=scales(s)^2*squeeze(Eval(1,:,:));
      
      %account for local contrast variation
      %and (optinally) we remove negative responses
      %(dark ridges instead of bright ridges)
      F(s,:,:)=squeeze(Eval(1,:,:)).*(1./(std_dev+0.01)).*(response>0);
  end;

  %v: get the maximum response over scale per voxel
  %indx: the index (scale index) of the maximum
  [v,indx]=max(F(:,:,:),[],1);

  %get the orientations corresponding to the maximum response
  V = zeros([2,shape]);
  for s=1:numel(scales)
      Mask=squeeze((s==indx));
      V(1,:,:) = squeeze(V(1,:,:)) + (squeeze(Votes(s,1,:,:)).*Mask);
      V(2,:,:) = squeeze(V(2,:,:)) + (squeeze(Votes(s,2,:,:)).*Mask);
  end;

  %maximum response * maximum direction 
  %Note: to save memroy,
  %we don't compute the tensor v (n n^T), we only return v n
  V = V.*reshape(repmat(v(:,:)./(sqrt(sum(V(:,:).^2,1))+eps),2,1),size(V));