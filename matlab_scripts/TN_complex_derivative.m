
function a=TN_complex_derivative(a)

    %note that arrays in matlab are in column-major order:
    %complex dimension (Y) is here from left-to-right in this matrix
    complex_derivatives=...
               [0      , 0  ,(-1)/8,   0,      0;
                0      , 0  ,     1,   0,      0;
                (-1i)/8, 1i ,     0, -1i, (1i)/8;
                0      , 0  ,    -1,   0,      0;
                0      , 0  , (1)/8,   0,      0;];
       
       
    filter_mode='replicate';       
    a=imfilter(a,(complex_derivatives),filter_mode);       
    
    
    
        