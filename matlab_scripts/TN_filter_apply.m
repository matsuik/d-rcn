function V = TN_filter_apply(featureImg, M)
    shape = M.shape;
    W = M.W;

    Vo2=reshape(((featureImg(:,:))*W),shape);
    Vo1=sqrt(Vo2);
    V=zeros([2,shape]);
    V(1,:,:)=real(Vo1);
    V(2,:,:)=imag(Vo1);