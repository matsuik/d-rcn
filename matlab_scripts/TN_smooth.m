% Gaussian filtering

function imgs=TN_smooth(img,sigma,varargin)  

        normalize=true;

        for k = 1:2:length(varargin),
                eval(sprintf('%s=varargin{k+1};',varargin{k}));
        end;
        
        classname=class(img);
        dim=2;
        
        imagereal=isreal(img);

        shape=size(img);
        img=padarray(img,ceil(shape+3*sigma),'symmetric','post');


        imgsz=size(img);

        [X Y] = ndgrid(0:(imgsz(1)-1),0:(imgsz(2)-1));
        X = X - ceil(imgsz(1)/2);
        Y = Y - ceil(imgsz(2)/2);
        R2 = (X.^2/(2*sigma^2) + Y.^2/(2*sigma^2));
        clear X Y;
    
        gaussin = (fftshift(exp(-R2)));
        if normalize
            gaussin=gaussin/cast(sqrt(((2*pi)^dim)*sigma.^4),classname);
        end;
        
        gaussin_ft= fftn(gaussin);
        clear gaussin;

        if imagereal
            img=ifftn(fftn(img).*gaussin_ft,'symmetric');    
        else
            img=ifftn(fftn(img).*gaussin_ft);    
        end;

        imgs=img(1:shape(1),1:shape(2));

