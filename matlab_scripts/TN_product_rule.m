function [products, M] = TN_product_rule()
            
            max_order = 5;
            
            %we now create third order tensors from the derivative tensors
            %by conbining up to two tensors
            order1=false;
            order2=false;
            order3=true;
            products=[];

            if order1
                products=[products;[2,0,0,0]];
            end;
            
            
            if order2            
                        
                        for o1=1:max_order
                            for o2=1:max_order
                                if (o1+o2)==2
                                    products=[products;[o1,o2,+1,0]];
                                end;
                                if (o1-o2)==2
                                    products=[products;[o1,o2,-1,0]];
                                end;
                            end;
                        end;

 
            end;
            if order3
                         products3=[];
                        for o1=1:max_order
                            for o2=1:max_order
                                for o3=1:max_order
                                    if ((o1-o2)>-1)&&((o1-o2-o3)==2)
                                        products3=[products3;[o1,o2,o3,1]];
                                    end;
                                    if (o1>=o2) && (o2>=o3) &&((o1+o2+o3)==2)
                                        products3=[products3;[o1,o2,o3,2]];
                                    end;
                                    if (o1+o2-o3)==2
                                        products3=[products3;[o1,o2,o3,3]];
                                    end;
                                end;
                            end;
                        end;

                        D=(distmat(sort(products3(:,1:3),2),sort(products3(:,1:3),2)));
                        [C,ia,ic]=unique(sort(products3(:,1:3),2),'rows');
                        products=[products;products3(ia,:)];
                        
            end;
            
            M.max_order = max_order;