%%
%bten=zeros([3,3,128]);for a=1:128,bten(:,:,a)=dirs.bDir{128}(:,a)*dirs.bDir{128}(:,a)';end;

% ten [3 x 3 x N]
% b = 1000 = "1"
% nt = 1000 verschiedene diff parameter gesa,plet
% D1 parallel in axon
% D2 parallel out of axon
% D3 parallel out of axon orthogonal
% 
% standart
% D1=2.2
% D2=1.5
% D3=0.5 

% V axon [0, 1]
% Vw intra and extra water V+Vw = 1

% ndir = anzahl richtung



function data = genDWIsignalCrossings(ten,varargin)

    if 0
        %%
        dirs=load('bDirs.mat');
        bdirs=dirs.bDir{128};
        bdirs=[bdirs,-bdirs];
        
        nrealdirs=size(bdirs,2)/2;
        
        bvalue=1000;
        bten=zeros([3,3,2*nrealdirs+1]);
        for a=1:2*nrealdirs,bten(:,:,a+1)=bdirs(:,a)*bdirs(:,a)';end;
        bten=bvalue*bten;
        
        
        
        CH=convhull(bdirs');
        figure(1);
        trisurf(CH,bdirs(1,:),bdirs(2,:),bdirs(3,:));
        axis equal
        
        
        indx=1;
        data = genDWIsignalCrossings(bten);
        
        %%
        figure(2);
        clf
        hold on
        
        %ind3=find(max(data.signal(2:end,1,:),[],1)>1.5);
        
        for indx=1:5
            subplot(1,5,indx);
            
             hold on
               ind=randi(size(data.signal,2));
               ind2=randi(size(data.signal,3));
               
               %ind2=ind3(indx);
               
             title([num2str(ind),'  ',num2str(ind2)])
            s=data.signal(2:end,ind,ind2);
            snorm=data.signal(1,ind,ind2);
            s=s./repmat(snorm,size(s,1),1);
            %min(s)
            bdirs_w=(bdirs.*repmat(s',3,1));
            %bdirs_w=[bdirs_w,-bdirs_w];
            CH=convhull(bdirs_w');

            %trisurf(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:));
            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','none');
            
            %subplot(2,5,indx+5);
            
            w1=data.W(ind);
            w2=1-w1;
            plot3(w1*[data.n1(1,ind),-data.n1(1,ind)],w1*[data.n1(2,ind),-data.n1(2,ind)],w1*[data.n1(3,ind),-data.n1(3,ind)],'linewidth',5)
            plot3(w2*[data.n2(1,ind),-data.n2(1,ind)],w2*[data.n2(2,ind),-data.n2(2,ind)],w2*[data.n2(3,ind),-data.n2(3,ind)],'linewidth',5)
            axis equal
            axis(1.5*[-1 1 -1 1 -1 1])
        end;
        
          %%
        figure(2);
        clf
        hold on
        for indx=1:5
            ind=randi(size(data.signal,1));
             ind2=randi(size(data.signal,3));
            subplot(1,5,indx);
             hold on
            %ind=rand(size(data.signal,2));
            s=data.signal(ind,2:end,ind2).';
            bdirs_w=(bdirs.*repmat(s',3,1));
            bdirs_w=[bdirs_w,-bdirs_w];
            CH=convhull(bdirs_w');

            %trisurf(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:));
            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','none');
            
            %subplot(2,5,indx+5);
            
            w1=1;
            plot3(w1*[data.n(1,ind),-data.n(1,ind)],w1*[data.n(2,ind),-data.n(2,ind)],w1*[data.n(3,ind),-data.n(3,ind)],'linewidth',5)
            axis equal
             axis(1.5*[-1 1 -1 1 -1 1])
        end;
    end;
    
    
    % main code
    
    
    ten = ten/1000;
    nt = 1000; %n_sample of configuation
    ndir = 100; %n_sample of 
    nz = 0.02; %noise

    csf = 0.5; % water ratio
    
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    D1 = 0.2+rand(nt,1)*2.2;
    D2 = 0.2+rand(nt,1)*1.5;
    D3 = 0.2+rand(nt,1)*0.5;    
%     D1 = 0.2+rand(nt,1)*2.8;
%     D2 = 0.2+rand(nt,1)*2.8;
%     D3 = 0.2+rand(nt,1)*1.3;



    V = rand(nt,1)*1; % water in axon
    Vw = (1-V).*rand(nt,1)*csf; % water out of axon

    idx = find(abs(D1-(D2+2*D3))<1.5);

    D1 = D1(idx)';
    D2 = D2(idx)';
    D3 = D3(idx)';
    Vi = V(idx)';
    Vf = Vw(idx)'; % parallel to fiber
    Ve = 1-V(idx)'-Vw(idx)'; % orthogonal to fiber
    

    

    
    %% sample fiber direction
    n = randn(3,ndir);
    n = n ./ repmat(sqrt(sum(n.^2)),[3 1]); %normalization
    
    n2 = randn(3,ndir);
    n2 = n2 ./ repmat(sqrt(sum(n2.^2)),[3 1]); %normalization
    
    
    %% sample dispersion
    A = 0.1*(rand(1,size(D1,2)).^2) ;


    %% generate signal 
    S1 = genSigd(ten,n,D1,D2,D3,Vi,Ve,Vf,A); 
    S2 = genSigd(ten,n2,D1,D2,D3,Vi,Ve,Vf,A); 
    
    data.shape_org=size(S1);
    
    shape=size(S1);
    shape=shape([2,1,3]);
    S1=(reshape(permute(S1,[2,1,3]),shape));
    S2=(reshape(permute(S2,[2,1,3]),shape));
    
    
    %w=1;
    W=rand([1,size(S1,2)]);
    w=(repmat(W,size(S1,1),1));
    w=(reshape(repmat(w(:),1,size(S1,3)),shape));
    %w=reshape(repmat(rand([1,size(S1,1)]),1,size(S1,2)*size(S1,3)),shape);
    %w(:)=1;
    S=S1.*w+S2.*(1-w);
    S = abs(S+ nz*(randn(size(S))+1i*randn(size(S))));
    
    
    
    
    
 
    data.signal = S;
    data.n1= n;
    data.n2= n2;
    data.w=w;
    data.W=W;
    
    data.D1 = D1;
    data.D2 = D2;
    data.D3 = D3;
    data.Vi = Vi;
    data.Ve = Ve;
    data.Vf = Vf;




% generate shell signal
function S = genSig(qxx,b,D1,D2,D3,Vi,Ve,Vf,A)
   
S = SHDisp(qxx,b,D1,A).*repmat(permute(Vi,[1 3 2]),[size(qxx,1) size(qxx,2) 1]) + ...
    SHDisp(qxx,b,D2-D3,A).*repmat(permute(Ve,[1 3 2]),[size(qxx,1) size(qxx,2) 1]).* ...
     repmat(exp(-repmat(permute(D3,[1 3 2]),[1 size(qxx,2) 1]).*repmat(b',[1 1 length(D3)])),[size(qxx,1) 1 1]) + ...
     repmat(permute(Vf,[1 3 2]),[size(qxx,1) size(qxx,2) 1]).* ...
     repmat(exp(-repmat(permute(0*D3+3,[1 3 2]),[1 size(qxx,2) 1]).*repmat(b',[1 1 length(D3)])),[size(qxx,1) 1 1]);
 
function S = genSigd(ten,n,D1,D2,D3,Vi,Ve,Vf,A)
b = squeeze(ten(1,1,:) + ten(2,2,:) + ten(3,3,:));
qxx =    (n(1,:).^2)' * squeeze(ten(1,1,:))' + ...
         (n(2,:).^2)' * squeeze(ten(2,2,:))' + ...
         (n(3,:).^2)' * squeeze(ten(3,3,:))' + ...
       + 2*(n(1,:).*n(2,:))' * squeeze(ten(1,2,:))' + ... 
       + 2*(n(1,:).*n(3,:))' * squeeze(ten(1,3,:))' + ... 
       + 2*(n(3,:).*n(2,:))' * squeeze(ten(3,2,:))' ;   
S = genSig(qxx,b,D1,D2,D3,Vi,Ve,Vf,A);




% generate dispersed stick
function S = SHDisp(qxx,b,D,lam)

    lmax = 50;
    L = (0:1:lmax);
    f = exp(-lam'*(L.*(L+1)));
    f =f(:,1:2:end);
    buni = unique(round(b*10));
    
    t = 0:0.001:1; 
    
    p = 0.5*(myleg(lmax,t')+myleg(lmax,-t')) .* repmat(sqrt((2*L+1)),[size(t,2) 1]) /sqrt(length(t));
    p = p(:,1:2:end);
    S = ones(size(qxx,1),length(b),length(lam));
    for k = 2:length(buni),
        bD = D*buni(k)/10;
        idx = round(b*10)==buni(k);
        %P = ((exp(-bD'*t.^2)*p).*f)*p';
        P = ((exp(-bD'*t.^2)*p).*f)*pinv(p);
        
        it = sqrt(qxx(:,idx)/buni(k)*10); it(it>1) = 1; it(it<0) = 0;

        %r = interp1(t,real(P)',real(it));
        r = interp1(t,real(P)',real(it));

        % itx = floor(it*(length(t)-1))+1;
        % r = P(:,itx)';
        %r = reshape(r,[size(S,1),sum(idx),size(S,3)]);
        
        S(:,idx,:) = r;    
    end;

function p = myleg(n,x);


    if n == 0,
        p = x*0+1;
        return;
    end
    if n == 1
        p = [x*0+1 x];
        return;
    end;
    p = zeros(size(x,1),n+1);
    p(:,1:2) = [x*0+1 x];
    for k = 2:n,
        p(:,k+1) = ((2*k-1)*x.*p(:,k) - (k-1)*p(:,k-1))/k;    
    end;
