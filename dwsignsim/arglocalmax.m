%bdirs represent whole sphere, pred has shape (N, n_dirs), k=7
%visualize=bool
function [indices_first_second_for_bdirs, knn_index] = arglocalmax(bdirs, pred, ratio, k, visualize)
    pred = pred'; % (N, n_dirs) to (n_dirs, N)
    % row i is a list of indeces of knns of the i-th direction
    knn_index = knnsearch(bdirs', bdirs', 'K', k);

    if visualize
        CH=convhull(bdirs');
        trisurf(CH,bdirs(1,:),bdirs(2,:),bdirs(3,:));
        axis equal
        hold on
        for i=randi([1, 256], 1, 5),
            scatter3(bdirs(1, knn_index(i, :)), bdirs(2, knn_index(i, :)), bdirs(3, knn_index(i, :)), 'filled')
        end
    end

    
    n_dirs = size(pred, 1)
    N = size(pred, 2)
    indices_first_second_for_bdirs = cell(1, N);
    for i_pred=1:N,
        s = pred(:, i_pred);

        local_max_index_list = []; % list of indeces of local max directions
        for i=1:256,
            [~, argmax] = max(s(knn_index(i, :)));
            if 1==argmax,
                local_max_index_list = [local_max_index_list, i];
            end;
        end;
        [~, indices_sorted] = sortrows(s(local_max_index_list, :));
        flipped = flip(indices_sorted);
        i_max = flipped(1);
        valid_peak_list = [];
        for i_lm=flipped(1:2:size(flipped, 1))'
            if s(local_max_index_list(i_lm)) > ratio * s(local_max_index_list(i_max))
                valid_peak_list = [valid_peak_list, i_lm];
            end
        end
        indices_first_second_for_bdirs{i_pred} = local_max_index_list(valid_peak_list);
    end

    if visualize
        i_pred = randi(N);
        s = pred(:, i_pred);
        bdirs_w=(bdirs.*repmat(s',3,1));
        bdirs_w_first_second = bdirs_w(:, indices_first_second_for_bdirs{i_pred});

        CH=convhull(bdirs');

        figure();
        trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
        axis equal
        hold on 
        % for i=local_max_index_list,
        %     scatter3(bdirs_w(1, i), bdirs_w(2, i), bdirs_w(3, i), 'filled')
        % end;



        scatter3(bdirs_w_first_second(1,:), bdirs_w_first_second(2,:),bdirs_w_first_second(3,:), 'm', 'filled')
        scatter3(-bdirs_w_first_second(1,:), -bdirs_w_first_second(2,:),-bdirs_w_first_second(3,:), 'm', 'filled')
    end
end

