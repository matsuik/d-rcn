%%

% loading gradient directions
dirs=load('bDirs.mat');

% we choose 128 directions
bdirs=dirs.bDir{128};

% creating the whole sphere
%bdirs=[bdirs];
bdirs=[bdirs,-bdirs];
nrealdirs=size(bdirs,2)/2;

% we set the bvalue to 1000 and create the b-tensors
bvalue=1000;
bten=zeros([3,3,2*nrealdirs+1]);
for a=1:2*nrealdirs,
    bten(:,:,a+1)=bdirs(:,a)*bdirs(:,a)';
end;

bten=bvalue*bten;


% plot the sphere discretization
CH=convhull(bdirs');
trisurf(CH,bdirs(1,:),bdirs(2,:),bdirs(3,:));
axis equal

%%
% generating the signal
%data = genDWIsignalCrossings(bten,'ndir',1000,'nt',1000);
data = genDWIsignalCrossings(bten,'ndir',1000,'nt',1000);


% scale invariance: deviding the signal by the b0 image
signal=data.signal(2:end,:)./((repmat(data.signal(1,:),size(data.signal,1)-1,1)));

    
alldirs=bdirs;


%% fodf directions

input_L = 8;
output_L = 12;
%order for FODF
L=output_L;

signal_sh = discrete2sh(signal,alldirs, input_L);

% shcoeffients of  delta impuls in n1 direction
n1_sh=n2delta(data.n1,L);
fodf_n1=(reshape(repmat(n1_sh(:),1,size(data.signal,3)),[size(n1_sh,1),size(data.signal,2),size(data.signal,3)]));

% shcoeffients of  delta impuls in n2 direction
n2_sh=n2delta(data.n2,L);
fodf_n2=(reshape(repmat(n2_sh(:),1,size(data.signal,3)),[size(n2_sh,1),size(data.signal,2),size(data.signal,3)]));

% weighted sum = fodf
w1=(data.w(1:size(fodf_n1,1),:,:));
fodf_sh=fodf_n1.*w1+fodf_n2.*(1-w1);

data_base.input=signal_sh;
data_base.output=fodf_sh(:,:);


    Mcontra_signal = getSHMatrix(input_L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
    % reconstruction of signal (harmonic domain -> spatial domain)
    signal_rec=reshape(real((Mcontra_signal.'*signal_sh(:,:))),[size(Mcontra_signal,2),size(fodf_sh,2),size(fodf_sh,3)]);
       
    Mcontra_fodf = getSHMatrix(L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
    % reconstruction of fodf (harmonic domain -> spatial domain)
    fodf=reshape(real((Mcontra_fodf .'*fodf_sh(:,:))),[size(Mcontra_fodf,2),size(fodf_sh,2),size(fodf_sh,3)]);
    
    

%%

        figure(3);
        subplot(1,2,1);
        hist(rad2deg(acos(abs(sum(data.n1.*data.n2,1)))))
        title('crossing degree');
        
        subplot(1,2,2);
        hist(data.w(1,:,1))
        title('crossing ratio');

        figure(2);
        colormap default
        clf
        hold on
        for indx=1:5

            ind=randi(size(data.signal,2));
            ind2=randi(size(data.signal,3));
            
            %% signal + ground truth
            subplot(3,5,indx);
            hold on
            title([num2str(ind),'  ',num2str(ind2)])
            s=data.signal(2:end,ind,ind2);
            snorm=data.signal(1,ind,ind2);
            s=s./repmat(snorm,size(s,1),1);
            
            bdirs_w=(bdirs.*repmat(s',3,1));
            CH=convhull(bdirs');
            
            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','none','FaceVertexCData',s(:));
            
            w1=data.W(ind);
            w2=1-w1;
            plot3(w1*[data.n1(1,ind),-data.n1(1,ind)],w1*[data.n1(2,ind),-data.n1(2,ind)],w1*[data.n1(3,ind),-data.n1(3,ind)],'linewidth',5)
            plot3(w2*[data.n2(1,ind),-data.n2(1,ind)],w2*[data.n2(2,ind),-data.n2(2,ind)],w2*[data.n2(3,ind),-data.n2(3,ind)],'linewidth',5)
            axis equal
            axis(1.5*[-1 1 -1 1 -1 1])
            
            
            %% reconstructed signal (raw -> sh -> raw')
            subplot(3,5,indx+5);
            hold on
            title('reconstruction (signal)')
            s=signal_rec(:,ind,ind2);
            bdirs_w=(bdirs.*repmat(s',3,1));
            CH=convhull(bdirs');

            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','black');
            axis equal
            axis(1.5*[-1 1 -1 1 -1 1])

            
            %% reconstructed gt (raw -> sh -> raw')
            subplot(3,5,indx+10);
            hold on
            title(['reconstruction (directions)  ',num2str(dot(data.n1(:,ind),data.n2(:,ind)))])
            s=8*fodf(:,ind,ind2);
            
            bdirs_w=(bdirs.*repmat(s',3,1));
            CH=convhull(bdirs');

            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
            axis equal
            axis(1*[-1 1 -1 1 -1 1])            
        end;
        