function ofield = discrete2sh(ndata,dir, L, varargin)

type = 'STA_OFIELD_EVEN';
storage = 'STA_FIELD_STORAGE_C';
pseudoinverse = true;
pseudoinverse = false;
for k = 1:2:numel(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;


if length(size(dir)) == 3,
    for k = 1:size(dir,3),
        [V D] = eigs(dir(:,:,k),1);
        bDir(:,k) = V(:,1);
    end;
else
    bDir = dir;
end;

M= getSHMatrix(L,bDir,type,storage,0);



if pseudoinverse,
    ndata = reshape(pinv(M')*ndata(:,:),[size(M,1) size(ndata,2) size(ndata,3) size(ndata,4)]) ;
else,
    ndata = reshape(M*ndata(:,:),[size(M,1) size(ndata,2) size(ndata,3) size(ndata,4)]) ;
end;

ofield =ndata;


