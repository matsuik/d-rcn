% loading gradient directions
dirs=load('bDirs.mat');

% we choose 128 directions
bdirs=dirs.bDir{128};

% creating the whole sphere
%bdirs=[bdirs];
bdirs=[bdirs,-bdirs];

folder = 'pc/initial_1119';
load(['../results/blind/', folder, '/pred4.mat']);

t = cputime;
[indices_first_second_for_bdirs, knn_index] = arglocalmax(bdirs, pred(1:1000, :), 0.2, 7, true);
e = t - cputime