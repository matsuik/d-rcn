%%

% loading gradient directions
dirs=load('bDirs.mat');

% we choose 128 directions
bdirs=dirs.bDir{128};

% creating the whole sphere
%bdirs=[bdirs];
bdirs=[bdirs,-bdirs];


% row i is a list of indeces of knns of the i-th direction
knn_index = knnsearch(bdirs', bdirs', 'K', 7);

% plot the sphere discretization
CH=convhull(bdirs');
trisurf(CH,bdirs(1,:),bdirs(2,:),bdirs(3,:));
axis equal
hold on
for i=randi([1, 256], 1, 5),
    scatter3(bdirs(1, knn_index(i, :)), bdirs(2, knn_index(i, :)), bdirs(3, knn_index(i, :)), 'filled')
end

folder = 'pc/initial_1119';
load(['../results/blind/', folder, '/pred4.mat']);
pred = pred';

%% 

ratio = 0.2;
a = 0;
    i_pred = randi(100);
    t = cputime;
    s = pred(:, i_pred);

    local_max_index_list = []; % list of indeces of local max directions
    for i=1:256,
        [v, argmax] = max(s(knn_index(i, :)));
        if 1==argmax,
            local_max_index_list = [local_max_index_list, i];
        end;
    end;
    [sorted, indices_sorted] = sortrows(s(local_max_index_list, :));
    flipped = flip(indices_sorted);
    i_max = flipped(1)
    valid_peak_list = [];
    for i_lm=flipped(1:2:size(flipped, 1))'
        i_lm
        if s(local_max_index_list(i_lm)) > ratio * s(local_max_index_list(i_max))
            'valid'
            i_lm
            valid_peak_list = [valid_peak_list, i_lm];
        end
    end
    
    a = a + cputime - t;

bdirs_w=(bdirs.*repmat(s',3,1));
local_max_index_list
valid_peak_list
bdirs_w_first_second = bdirs_w(:,local_max_index_list(valid_peak_list));
bdirs_w_local_max = bdirs_w(:,local_max_index_list);

CH=convhull(bdirs');

figure();
trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
axis equal
hold on 
% for i=local_max_index_list,
%     scatter3(bdirs_w(1, i), bdirs_w(2, i), bdirs_w(3, i), 'filled')
% end;

scatter3(bdirs_w_first_second(1,:), bdirs_w_first_second(2,:),bdirs_w_first_second(3,:), 'm', 'filled')
scatter3(-bdirs_w_first_second(1,:), -bdirs_w_first_second(2,:),-bdirs_w_first_second(3,:), 'm', 'filled')
scatter3(bdirs_w_local_max(1,:), bdirs_w_local_max(2,:),bdirs_w_local_max(3,:), 'b')
scatter3(-bdirs_w_local_max(1,:), -bdirs_w_local_max(2,:),-bdirs_w_local_max(3,:), 'b'  )

%% 

bdirs_w_valid = bdirs_w(:, valid_peak_list{i_pred}+1);
scatter3(bdirs_w_valid(1,:), bdirs_w_valid(2,:),bdirs_w_valid(3,:), 'r', 'filled')




%TODO
%i_local_max_sorted = argmax(argsout(v)(:4))
%bdirs(i_loacal_max_sorted)
%eliminate nonprominent dir
%which to compare to true dir or local max of recons_fodf
%should interpolate s?
