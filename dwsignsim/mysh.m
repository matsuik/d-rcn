function sh=mysh(n,l,m)

r=sqrt(sum(n.^2,1));

theta=acos(n(3,:)./r);
phi=atan2(n(2,:),n(1,:));



p = legendre(l,cos(theta),'sch');

if size(p,1)>1
    p(2:end,:)=p(2:end,:)/sqrt(2);
end;

if nargin<3
    if size(p,1)>1
        leg=[p(end:-1:2,:);p];
    else
        leg=p;
    end;
    m=repmat([-l:1:l]',1,size(n,2));
    sh=leg.*exp(1i*m.*repmat(phi,2*l+1,1)).*(-1).^(m); 
    if size(p,1)>1
        sign=mod(abs([-l:1:-1]),2)==1;
        sh(1:l,:)=(sh(1:l,:)).*(-1).^repmat(sign',1,size(n,2));
    end;
else
    leg=p(abs(m)+1);
    sh=leg.*exp(1i*m*phi);
end;



