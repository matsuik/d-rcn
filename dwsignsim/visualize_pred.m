%% make btens

% loading gradient directions
dirs=load('bDirs.mat');

% we choose 128 directions
bdirs=dirs.bDir{128};

% creating the whole sphere
%bdirs=[bdirs];
bdirs=[bdirs,-bdirs];
nrealdirs=size(bdirs,2)/2;

% we set the bvalue to 1000 and create the b-tensors
bvalue=1000;
bten=zeros([3,3,2*nrealdirs+1]);
for a=1:2*nrealdirs,
    bten(:,:,a+1)=bdirs(:,a)*bdirs(:,a)';
end;

bten=bvalue*bten;
alldirs=bdirs;

%%

input_L = 8;
Mcontra_signal = getSHMatrix(input_L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
output_L = 8;
Mcontra_fodf = getSHMatrix(output_L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);


folder = 'in8out12/initial_try_1004';
load ../data/blind/in8out8/data_base.mat
load(['../results/blind/', folder, '/pred.mat']);
pred = transpose(pred);

%%

        figure(2);
        colormap default
        clf
        hold ons
        n_samples = 5;
        for indx=1:n_samples

            ind=randi(size(pred,2));
            
            %% reconstructed hardi
            subplot(3,n_samples,indx);
            hold on
            title('input')
            
            s=real((Mcontra_fodf .'*data_base.input(:, ind)));
            bdirs_w=(bdirs.*repmat(s',3,1));
            CH=convhull(bdirs');

            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
            axis equal
            axis(1*[-1 1 -1 1 -1 1]) 
            %% reconstructed gt
            subplot(3,n_samples,indx+n_samples);
            hold on
            title('gt')
            
            s=8*real((Mcontra_fodf .'*data_base.output(:, ind)));
            bdirs_w=(bdirs.*repmat(s',3,1));
            CH=convhull(bdirs');

            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
            axis equal
            axis(1*[-1 1 -1 1 -1 1]) 
            %% reconstructed pred
            subplot(3,n_samples,indx+2*n_samples);
            hold on
            title('prediction')
            
            s=8*real((Mcontra_fodf .'*pred(:, ind)));
            bdirs_w=(bdirs.*repmat(s',3,1));
            CH=convhull(bdirs');

            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
            axis equal
            axis(1*[-1 1 -1 1 -1 1]) 
        end;