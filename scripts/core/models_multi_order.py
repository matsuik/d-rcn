# -*- coding:utf-8 -*-
from __future__ import division

import numpy as np
import theano
import theano.tensor as T
from collections import OrderedDict


from theano_utils import floatX
import blocks


class Model(object):
    def __init__(self, j_input_list, j_output, n_input_tensors_list, n_hidden_tensors, func_key_list):
        """

        Parameters
        ----------
        j_input_list : list of int
            インプットにつかうtensorの次数
        j_output : int
            アウトプットに使う次数
        n_input_tensors_list : list of int
            インプットのそれぞれの次数のテンソルの数
        n_hidden_tensors : int
            隠れ層のユニット数, 単純化のために何層目でも一緒にする
        func_key_list : list of str
            norm activationの層数と一緒にする

        Returns
        -------

        """
        assert len(j_input_list) == len(n_input_tensors_list)
        self.j_input_list = j_input_list
        self.j_output = j_output
        self.n_input_tensors_list = n_input_tensors_list
        self.n_hidden_tensors = n_hidden_tensors
        self.tensors_list = [T.tensor3() for i in j_input_list]
        self.t_mat = T.matrix("t")  # (batch, D)
        self.func_key_list = func_key_list
        self.param_list = []


class Rcn2layer_multioreder(Model):
    def __init__(self, j_input_list, j_output, n_input_tensors_list, n_hidden_tensors, func_key_list):
        super(Rcn2layer_multioreder, self).__init__(j_input_list, j_output, n_input_tensors_list, n_hidden_tensors,
                                                    func_key_list)
        assert len(self.func_key_list) == 2


    def make_graph_train(self):
        out1, self.tc_weight_list, self.tc_bias_list, n_couples =\
            blocks.multi_order_mix(self.tensors_list, self.j_input_list, self.j_output,
                                   self.n_input_tensors_list, self.n_hidden_tensors, is_train=True,
                                   func_key=self.func_key_list[0])

        if self.j_output in self.j_input_list:
            print "do ordinary forward"
            self.W1 = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(self.j_output)],
                                         self.n_hidden_tensors, name='W1')
            self.bias1 = blocks.make_bias(self.n_hidden_tensors, name='bias1')
            self.param_list.append(self.W1)
            self.param_list.append(self.bias1)

            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(self.j_output)],
                                            self.W1)
            h1na = blocks.activate_norm(h1sv, self.bias1, func_key=self.func_key_list[0])[0]
            out1 = T.concatenate([out1, h1na], axis=2)

            n_couples += 1

        self.W2 = blocks.make_weight(n_couples*self.n_hidden_tensors, 1, name='W2')
        self.bias2 = blocks.make_bias(1, name='bias2')

        h2 = blocks.superpose_vectors(out1, self.W2)
        out2, acted_norm2= blocks.activate_norm(h2, self.bias2, self.func_key_list[1])
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)

        self.param_list.extend(self.tc_weight_list)
        self.param_list.extend(self.tc_bias_list)
        self.param_list.append(self.W2)
        self.param_list.append(self.bias2)

        self.obj = mse
        self.train_error = mse


    def make_graph_test(self):
        out1 = blocks.multi_order_mix(self.tensors_list, self.j_input_list, self.j_output,
                                   self.n_input_tensors_list, self.n_hidden_tensors, is_train=False,
                                   func_key=self.func_key_list[0],
                                   weight_list=self.tc_weight_list, bias_list=self.tc_bias_list)

        if self.j_output in self.j_input_list:
            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(self.j_output)],
                                            self.W1)
            h1na = blocks.activate_norm(h1sv, self.bias1, func_key=self.func_key_list[0])[0]
            out1 = T.concatenate([out1, h1na], axis=2)

        h2 = blocks.superpose_vectors(out1, self.W2)
        out2, acted_norm2 = blocks.activate_norm(
            h2, self.bias2, self.func_key_list[1])

        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)

        self.test_error = mse
        self.test_out = out2


class ResRcn2layer_multioreder(Model):
    def __init__(self, j_input_list, j_output, n_input_tensors_list, n_hidden_tensors, func_key_list):
        super(ResRcn2layer_multioreder, self).__init__(j_input_list, j_output, n_input_tensors_list, n_hidden_tensors,
                                                    func_key_list)
        assert len(self.func_key_list) == 2


    def make_graph_train(self):
        out1, self.tc_weight_list, self.tc_bias_list, n_couples =\
            blocks.multi_order_mix(self.tensors_list, self.j_input_list, self.j_output,
                                   self.n_input_tensors_list, self.n_hidden_tensors, is_train=True,
                                   func_key=self.func_key_list[0])

        if self.j_output in self.j_input_list:
            print "ordinary forward"
            self.W1 = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(self.j_output)],
                                         self.n_hidden_tensors, name='W1')
            self.bias1 = blocks.make_bias(self.n_hidden_tensors, name='bias1')
            self.param_list.append(self.W1)
            self.param_list.append(self.bias1)

            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(self.j_output)],
                                            self.W1)
            h1na = blocks.activate_norm(h1sv, self.bias1, func_key=self.func_key_list[0])[0]
            out1 = T.concatenate([out1, h1na], axis=2)

            n_couples += 1

            print "direct forward skipping first layer"
            self.W_direct = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(self.j_output)],
                                         1, name='W_direct')
            self.bias_direct = blocks.make_bias(1, name='bias_direct')
            self.param_list.append(self.W_direct)
            self.param_list.append(self.bias_direct)
            h_direct = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(self.j_output)],
                                            self.W_direct)
            out_direct = blocks.activate_norm(h_direct, self.bias_direct, func_key=self.func_key_list[1])[0]



        self.W2 = blocks.make_weight(n_couples*self.n_hidden_tensors, 1, name='W2')
        self.bias2 = blocks.make_bias(1, name='bias2')

        h2 = blocks.superpose_vectors(out1, self.W2)
        out2, acted_norm2= blocks.activate_norm(h2, self.bias2, self.func_key_list[1])
        out2 = out2 + out_direct
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)

        self.param_list.extend(self.tc_weight_list)
        self.param_list.extend(self.tc_bias_list)
        self.param_list.append(self.W2)
        self.param_list.append(self.bias2)

        self.obj = mse
        self.train_error = mse


    def make_graph_test(self):
        out1 = blocks.multi_order_mix(self.tensors_list, self.j_input_list, self.j_output,
                                   self.n_input_tensors_list, self.n_hidden_tensors, is_train=False,
                                   func_key=self.func_key_list[0],
                                   weight_list=self.tc_weight_list, bias_list=self.tc_bias_list)

        if self.j_output in self.j_input_list:
            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(self.j_output)],
                                            self.W1)
            h1na = blocks.activate_norm(h1sv, self.bias1, func_key=self.func_key_list[0])[0]
            out1 = T.concatenate([out1, h1na], axis=2)

            h_direct = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(self.j_output)],
                                            self.W_direct)
            out_direct = blocks.activate_norm(h_direct, self.bias_direct, func_key=self.func_key_list[1])[0]

        h2 = blocks.superpose_vectors(out1, self.W2)
        out2, acted_norm2 = blocks.activate_norm(h2, self.bias2, self.func_key_list[1])
        out2 = out2 + out_direct
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)

        self.test_error = mse
        self.test_out = out2


class Rcn3layer_multioreder(Model):
    def __init__(self, j_input_list, j_output, n_input_tensors_list, n_hidden_tensors, func_key_list):
        super(Rcn3layer_multioreder, self).__init__(j_input_list, j_output, n_input_tensors_list, n_hidden_tensors,
                                                    func_key_list)
        assert len(self.func_key_list) == 3


    def make_graph_train(self):
        out1, self.tc_weight_list, self.tc_bias_list, n_couples =\
            blocks.multi_order_mix(self.tensors_list, self.j_input_list, self.j_output,
                                   self.n_input_tensors_list, self.n_hidden_tensors, is_train=True,
                                   func_key=self.func_key_list[0])

        if self.j_output in self.j_input_list:
            print "do ordinary forward"
            self.W1 = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(self.j_output)],
                                         self.n_hidden_tensors, name='W1')
            self.bias1 = blocks.make_bias(self.n_hidden_tensors, name='bias1')
            self.param_list.append(self.W1)
            self.param_list.append(self.bias1)

            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(self.j_output)],
                                            self.W1)
            h1na = blocks.activate_norm(h1sv, self.bias1, func_key=self.func_key_list[0])[0]
            out1 = T.concatenate([out1, h1na], axis=2)

            n_couples += 1

        self.W2 = blocks.make_weight(n_couples*self.n_hidden_tensors, self.n_hidden_tensors, name='W2')
        self.bias2 = blocks.make_bias(self.n_hidden_tensors, name='bias2')

        h2 = blocks.superpose_vectors(out1, self.W2)
        out2, acted_norm2= blocks.activate_norm(h2, self.bias2, self.func_key_list[1])

        self.W3 = blocks.make_weight(self.n_hidden_tensors, 1, name='W3')
        self.bias3 = blocks.make_bias(1, name='bias3')

        h3 = blocks.superpose_vectors(out2, self.W3)
        out3, acted_norm3= blocks.activate_norm(h3, self.bias3, self.func_key_list[2])

        mse = T.mean((out3 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)

        self.param_list.extend(self.tc_weight_list)
        self.param_list.extend(self.tc_bias_list)
        self.param_list.append(self.W2)
        self.param_list.append(self.bias2)
        self.param_list.append(self.W3)
        self.param_list.append(self.bias3)

        self.obj = mse
        self.train_error = mse


    def make_graph_test(self):
        out1 = blocks.multi_order_mix(self.tensors_list, self.j_input_list, self.j_output,
                                   self.n_input_tensors_list, self.n_hidden_tensors, is_train=False,
                                   func_key=self.func_key_list[0],
                                   weight_list=self.tc_weight_list, bias_list=self.tc_bias_list)

        norm_list =[]
        if self.j_output in self.j_input_list:
            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(self.j_output)],
                                            self.W1)
            h1na, acted_norm1 = blocks.activate_norm(h1sv, self.bias1, func_key=self.func_key_list[0])
            norm_list.append(acted_norm1)
            out1 = T.concatenate([out1, h1na], axis=2)

        h2 = blocks.superpose_vectors(out1, self.W2)
        out2, acted_norm2 = blocks.activate_norm(h2, self.bias2, self.func_key_list[1])
        norm_list.append(acted_norm2)

        h3 = blocks.superpose_vectors(out2, self.W3)
        out3, acted_norm3 = blocks.activate_norm(h3, self.bias3, self.func_key_list[2])
        norm_list.append(acted_norm3)

        mse = T.mean((out3 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)

        self.test_error = mse
        self.test_out = out3
