# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 19:55:31 2016

@author: matsuik
"""


from __future__ import division

import numpy as np
import theano
import theano.tensor as T

from theano_utils import floatX
import conv_blocks as blocks


def make_weight_bias(n_input_tensors, n_output_tensors):
    w_mat = theano.shared(
                floatX(0.01 * np.random.normal(size=(n_output_tensors, n_input_tensors, 1, 1))),
                name="w_mat",
                borrow=False)
    bias_vec = theano.shared(
                    floatX(0.01*np.ones(shape=(n_output_tensors,))),
                    name="bias_vec",
                    borrow=False)
    return w_mat, bias_vec


class Model(object):
    def __init__(self):
        self.x_t3 = T.tensor4("x")  
        self.t_mat = T.tensor4("t")
        

class Rcn1layer(Model):
    def __init__(self, n_input_tensors, func_key):
        super(Rcn1layer, self).__init__()
        self.n_input_tensors = n_input_tensors
        self.func_key = func_key
    def make_graph(self):
        w_mat, bias_vec = make_weight_bias(self.n_input_tensors, 1)
        param_list = [w_mat, bias_vec]
            
        h1 = blocks.superpose_vectors(self.x_t3, w_mat)
        out1, acted_norm1 = blocks.activate_norm(h1, bias_vec, func_key=self.func_key)
        
        mse = T.mean((out1 - self.t_mat) ** 2)
        
        # l2regularizeしてないので obj = mse
        return self.x_t3, self.t_mat, mse, mse, param_list, out1
    
    
class Rcn2layer(Model):
    def __init__(self, n_input_tensors, n_hidden_tensors, func_key_list):
        super(Rcn2layer, self).__init__()
        self.n_input_tensors = n_input_tensors
        self.n_hidden_tensors = n_hidden_tensors
        self.func_key_list = func_key_list
    def make_graph(self):
        param_list = []
        
        w1_mat, bias1_vec = make_weight_bias(self.n_input_tensors, self.n_hidden_tensors)
        param_list.extend([w1_mat, bias1_vec])
        
        h1 = blocks.superpose_vectors(self.x_t3, w1_mat)
        out1, acted_norm1 = blocks.activate_norm(h1, bias1_vec, self.func_key_list[0])
    
        w2_mat, bias2_vec = make_weight_bias(self.n_hidden_tensors, 1)
        param_list.extend([w2_mat, bias2_vec])        
            
        h2 = blocks.superpose_vectors(out1, w2_mat)
        out2, acted_norm2 = blocks.activate_norm(h2, bias2_vec, self.func_key_list[1])
        
        mse = T.mean((out2 - self.t_mat) ** 2)
        
        # l2regularizeしてないので obj = mse
        return self.x_t3, self.t_mat, mse, mse, param_list, out2
    