# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 19:16:45 2016

@author: matsuik
"""
import theano.tensor as T

def get_act_func(keyword):
    """
    Args
    ----
    keyword : str
        "relu", "retanh", "sigm"
        
    Returns
    -------
    elementwise scalar f: TensorVariable -> TensorVariable
    """
    
    assert keyword in ["relu", "retanh", "sigmoid"], "keyword is not valid."
    
    if keyword == "relu":
        return lambda x: T.switch(x > 0, x, 0)
    elif keyword == "retanh":
        return lambda x: T.switch(x > 0, T.tanh(x), 0)
    elif keyword == "sigmoid":
        return T.nnet.sigmoid
        


def superpose_vectors(x_t4, w_t4):
    """
    superpose D-dimansion vectors with scalars
    
    Args
    ----
    x_t4 : TensorVariable 4d tensor
        (batch_size, n_input_tensors, D, 1)
    w_t4 : TensorVariable 4d tensor
        (n_ouput_tensors, n_input_tensors, 1, 1)
    
    Returns
    -------
    theano.tensor.var.TensorVariable
        (batch_size, n_ouput_tensors, D, 1)
    
    """
    
    # sum_k x_ijk w_pk -> y_ijp
    return T.nnet.conv2d(x_t4, w_t4)


def activate_norm(x_t4, bias_vec, func_key, eps=1e-5):
    """
    elementwise_scalar_func(||x||+b) * x / (||x|| + eps)
    
    Args
    ----
    x_t4 : theano.tensor.var.TensorVariable
        (batch_size, n_tensors, D, 1)
    bias_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    func_key : str
        see theano_utils.get_act_func
    eps : float
        small number prevents zerodivision
        
    Returns
    -------
    output_t4 : theano_utils.get_act_func
        (batch_size, n_tensors, D, 1)
    acted_norm : theano_utils.get_act_func
        (batch_size, n_tensors)
        This should be obseved.
    
    """
    
    act_func = get_act_func(func_key)  
    
    norm = x_t4.norm(L=2, axis=2) # (batch_size, n_tensors, 1)
    activation = norm + bias_vec.dimshuffle(0, "x")  # (batch_size, n_tensors, 1)
    acted_norm = act_func(activation)  # (batch_size, n_tensors, 1)
    ouput_t4 = acted_norm.dimshuffle(0, 1, "x", 2) * x_t4\
                / (norm.dimshuffle(0, 1, "x", 2) + eps)
    return ouput_t4, acted_norm
    