# -*- coding: utf-8 -*-
"""
Created on Tue Apr  5 15:29:29 2016

@author: matsuik
"""
from __future__ import division

import numpy as np
import theano
import theano.tensor as T
from collections import OrderedDict

from theano_utils import floatX
import blocks

def _make_weight_bias(n_input_tensors, n_output_tensors, layer_number, bias_scale):
    w_mat = theano.shared(
                floatX(0.01 * np.random.normal(size=(n_output_tensors, n_input_tensors))),
                name="w{}".format(layer_number),
                borrow=False)
    bias_vec = theano.shared(
                    floatX(bias_scale*np.ones(shape=(n_output_tensors,))),
                    name="bias{}".format(layer_number),
                    borrow=False)
    return w_mat, bias_vec


class Ann2layer(object):
    def __init__(self, n_tensors_list, func_key_list, bias_scale, l2_reg):
        self.x_t3 = T.matrix("x")  # (batch_size, n_tensors)
        self.t_mat = T.vector("t")  # (batch,)
        self.n_tensors_list = n_tensors_list
        self.func_key_list = func_key_list
        self.l2_reg = l2_reg        
        
        self.w1, self.b1 = \
            _make_weight_bias(self.n_tensors_list[0],
                              self.n_tensors_list[1],
                              layer_number=1,
                              bias_scale=bias_scale)
        
        self.w2, self.b2 =\
            _make_weight_bias(self.n_tensors_list[1],
                              1,
                              layer_number=2,
                              bias_scale=bias_scale)
                        
        self.param_l = [self.w1, self.b1,
                        self.w2, self.b2]
                        
    def make_graph_train(self):
        beta = T.scalar("beta")
        timestep = T.scalar("t")
        model_param_l = [beta, timestep]
        model_updates = OrderedDict()
        
        h1 = T.dot(self.x_t3, self.w1.T) + self.b1 # (batch_size, n1)
        out1 = blocks.get_act_func(self.func_key_list[0])(h1)
        
        h2 = T.dot(out1, self.w2.T) + self.b2 # (batch_size, 1)
        out2 = blocks.get_act_func(self.func_key_list[1])(h2)
        
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 'x')) ** 2)
        obj = mse + self.l2_reg * (T.sum(self.w1**2) + T.sum(self.w2**2))
        
        return self, obj, mse, model_updates, model_param_l
        
        
    def make_graph_test(self):
        
        h1 = T.dot(self.x_t3, self.w1.T) + self.b1 # (batch_size, n1)
        out1 = blocks.get_act_func(self.func_key_list[0])(h1)
        
        h2 = T.dot(out1, self.w2.T) + self.b2 # (batch_size, n2)
        out2 = blocks.get_act_func(self.func_key_list[1])(h2)
        
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 'x')) ** 2)
        return mse, out2
