# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 21:55:27 2016

@author: matsuik

Optimizersの子クラスはcompilingに渡される
目的関数と、パラメータをもらって、updateの規則をつくる
learning_rateはdecayするためにtheano.functionに渡せるようにTensorVariableにしておく。

"""
from __future__ import division
from collections import OrderedDict
import theano
import theano.tensor as T

from theano_utils import floatX, zero_shared


class Sgd(object):
    def __init__(self, lr=0.001, lr_decay=1.):
        self.lr = lr
        self.lr_decay = lr_decay

    def make_updates(self, loss, param_list):
        lr = theano.shared(floatX(self.lr))
        gparam_list = [T.grad(loss, param) for param in param_list]

        updates = OrderedDict()
        for param, gparam in zip(param_list, gparam_list):
            updates[param] = param - lr * gparam

        updates[lr] = lr * self.lr_decay

        return updates

class Adam(object):
    """
    Recommended default settings are
    α = 0.001, β1 = 0.9, β2 = 0.999 and  eps= 10e−8.
    t is timestep from 1.
    """
    def __init__(self, alpha=0.001, beta1=0.9, beta2=0.999, eps=10e-8, lr_decay=1.):
        self.alpha = alpha
        self.beta1 = beta1
        self.beta2 = beta2
        self.eps = eps
        self.lr_decay = lr_decay

    def make_updates(self, loss, param_list):

        alpha = theano.shared(floatX(self.alpha))
        t = theano.shared(floatX(1.))

        gparam_list = [T.grad(loss, p) for p in param_list]
        first_moment_list = [zero_shared(p.shape.eval()) for p in param_list]
        second_moment_list = [zero_shared(p.shape.eval()) for p in param_list]

        updates = OrderedDict()
        for param, gparam, first_moment, second_moment \
                in zip(param_list, gparam_list, first_moment_list, second_moment_list):
            m = self.beta1*first_moment + (1.-self.beta1)*gparam
            v = self.beta2*second_moment + (1.-self.beta2)*gparam*gparam
            m_hat = m / (1.-self.beta1**t)
            v_hat = v / (1.-self.beta2**t)
            updates[param] = param - alpha*m_hat / (T.sqrt(v_hat)+self.eps)
            updates[first_moment] = m
            updates[second_moment] = v

        updates[alpha] = alpha * self.lr_decay
        updates[t] = t + 1.
        return updates