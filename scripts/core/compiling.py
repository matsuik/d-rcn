# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 14:55:03 2016

@author: matsuik
"""
from __future__ import division

import theano
import theano.tensor as T

from theano_utils import floatX
from collections import OrderedDict


def share_data_sets(feature_vec, gt_vec, test_feature_vec, test_gt_vec):
    s_input = theano.shared(floatX(feature_vec),
                            "feature_vec", borrow=True)
    s_target = theano.shared(floatX(gt_vec),
                             "gt2", borrow=True)
    s_test_input = theano.shared(floatX(test_feature_vec),
                                 "test_feature_vec", borrow=True)
    s_test_target = theano.shared(floatX(test_gt_vec),
                                  "test_gt_vec", borrow=True)
    return s_input, s_target, s_test_input, s_test_target



def compile_bn(data_set, model, make_updates):
    """
    データをsharedにして、modelとoptimizerを使ってcomputational graphを作って、
    コンパイルする。
    
    Parameters
    -----------
    data_set : list of numpy.ndarray
        feature_vec : ndarray
            (n_pixels, D, n_tensors)
        gt_vec : ndarray
            (n_pixels, D)
        test_feature_vec, test_gt_vec
    model : models.Rcn1layerとか
    optimizer : optimizers.SGDとか
    
    """

    s_input, s_target, s_test_input, s_test_target = share_data_sets(*data_set)
        
    nn, obj, train_mse, model_updates, model_param_l = model.make_graph_train
    test_mse, test_out = model.make_graph_test()
    
    updates, opt_param_list = make_updates(loss=obj, param_list=nn.param_l)

    i_batch = T.iscalar("i_batch")
    index_list = T.ivector("index_list")
    batch_size = T.iscalar("batch_size")
    
    od = OrderedDict()
    for k, e in updates.items() + model_updates.items():
        od[k] = e

    f_train = theano.function(
        inputs=[i_batch, index_list, batch_size]+opt_param_list+model_param_l,
        updates=od,
        givens=[(nn.x_t3, s_input[index_list[i_batch*batch_size: i_batch*batch_size + batch_size]]),
                (nn.t_mat, s_target[index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])],
        on_unused_input='warn')
                
    f_training_error = theano.function(
        inputs=[i_batch, index_list, batch_size]+model_param_l,
        outputs=[train_mse],
        givens=[(nn.x_t3, s_input[index_list[i_batch*batch_size: i_batch*batch_size + batch_size]]),
                (nn.t_mat, s_target[index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])],
        on_unused_input='warn')
                
    f_test_error = theano.function(
        inputs=[i_batch, index_list, batch_size],
        outputs=[test_mse],
        givens=[(nn.x_t3, s_test_input[index_list[i_batch*batch_size: i_batch*batch_size + batch_size]]),
                (nn.t_mat, s_test_target[index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])])
                
    f_output = theano.function(
        inputs=[nn.x_t3],
        outputs=[test_out])

    result = [f_train, f_training_error, f_test_error, f_output, s_input,
              s_target, s_test_input, s_test_target, nn.param_l]
    return result