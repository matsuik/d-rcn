# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 14:55:03 2016

@author: matsuik
"""
from __future__ import division

import theano
import theano.tensor as T

from theano_utils import floatX
from collections import OrderedDict


def share_data(data):
    return theano.shared(floatX(data), borrow=True)


def compile(train_feture_vec_list, train_gt_vec, test_feature_vec_list, test_gt_vec, model, optimizer,
            see_norm=False):
    """
    データをsharedにして、modelとoptimizerを使ってcomputational graphを作って、
    コンパイルする。

    Parameters
    -----------
    train_feture_vec_list : list of numpy.ndarray
        list of (N, dim, n_tensors)
    train_gt_vec : numpy.ndarray
        (N, dim)
    test_feature_vec_list, test_gt_vec
    model : models.Rcn1layerとか
    optimizer : optimizers.SGDとか

    """

    s_train_feature_vec_list = [share_data(data) for data in train_feture_vec_list]
    s_train_gt_vec = share_data(train_gt_vec)
    s_test_feature_vec_list = [share_data(data) for data in test_feature_vec_list]
    s_test_gt_vec = share_data(test_gt_vec)

    model.make_graph_train()
    model.make_graph_test()

    updates = optimizer.make_updates(loss=model.obj, param_list=model.param_list)

    i_batch = T.iscalar("i_batch")
    index_list = T.ivector("index_list")
    batch_size = T.iscalar("batch_size")

    train_givens = [(tensor, s_train_feature_vec_list[i][index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])
                for i, tensor in enumerate(model.tensors_list)] +\
                   [(model.t_mat, s_train_gt_vec[index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])]
    test_givens = [(tensor, s_test_feature_vec_list[i][index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])
                for i, tensor in enumerate(model.tensors_list)] +\
                  [(model.t_mat, s_test_gt_vec[index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])]

    f_train = theano.function(
        inputs=[i_batch, index_list, batch_size],
        updates=updates,
        givens=train_givens,
        on_unused_input='warn')

    f_training_error = theano.function(
        inputs=[i_batch, index_list, batch_size],
        outputs=[model.train_error],
        givens=train_givens,
        on_unused_input='warn')

    f_test_error = theano.function(
        inputs=[i_batch, index_list, batch_size],
        outputs=[model.test_error],
        givens=test_givens,
        on_unused_input='warn'
        )

    f_output = theano.function(
        inputs=model.tensors_list,
        on_unused_input='warn',
        outputs=[model.test_out])

    result = [f_train, f_training_error, f_test_error, f_output, model.param_list]
    return result
