# -*- coding: utf-8 -*-
from __future__ import division

import numpy as np
import scipy.io as sio
from scipy.linalg import svdvals
proj_path = '/home/matsui-k/projects/d-rcn/'



def load_neuron(i_neuron=1, only_feature=False):
    """
    load data/featureImg.mat, data/index.mat

    Parameters
    ----------
    i_neuron : int in {1, 2, 3}

    Returns
    -------
    feature_vec : ndarray
        (n_pixeles, n_tensors, dim)
    GT_vec : ndarray
        (n_pixeles, dim)
    mask_vec : ndarray
        (n_pixeles, ), dtype='bool'
    im_shape : tuple
    """

    assert i_neuron in [1, 2, 3]

    matlab_struct = sio.loadmat(
        proj_path + 'data/line_detection/featureImg{}.mat'.format(str(i_neuron)))

    feature_comp_vec = matlab_struct['featureImg']
    feature_vec = np.zeros((feature_comp_vec.shape[0], feature_comp_vec.shape[1], 2))
    feature_vec[:, :, 0] = np.real(feature_comp_vec[:, :])
    feature_vec[:, :, 1] = np.imag(feature_comp_vec[:, :])

    if only_feature:
        return feature_vec

    ms = sio.loadmat(proj_path + 'data/line_detection/mask{}_sigma=2_thresh=0dot05.mat'.format(str(i_neuron)))

    GT_comp_img = matlab_struct['imgGT'].T
    mask_img = ms['mask'].T
    mask_img = np.asarray(mask_img, dtype="bool")

    GT_comp_vec = GT_comp_img.flatten()
    mask_vec = mask_img.flatten()

    GT_vec = np.zeros((GT_comp_vec.shape[0], 2))
    GT_vec[:, 0] = np.real(GT_comp_vec)
    GT_vec[:, 1] = np.imag(GT_comp_vec)

    print "feature_vec.shape", feature_vec.shape
    print "GT_vec.shape", GT_vec.shape
    print "mask_vec.shape", mask_vec.shape

    return feature_vec, GT_vec, mask_vec, mask_img.shape


def load_featureImg_r(number):
    """
    load "../data/featureImgr" + str(number) + ".mat"

    Parameters
    ----------
    number : int

    Returns
    -------
    feature_vec : ndarray
        (n_pixeles, n_tensors, dim)
    """
    matlab_struct = sio.loadmat('../data/featureImgr' + str(number) + '.mat')
    feature_comp_vec = matlab_struct['f']

    # 複素数からベクトルへの変換
    feature_vec = np.zeros((feature_comp_vec.shape[0], feature_comp_vec.shape[1], 2))
    feature_vec[:, :, 0] = np.real(feature_comp_vec[:, :])
    feature_vec[:, :, 1] = np.imag(feature_comp_vec[:, :])

    return feature_vec


def load_toytreefeature_mat():
    """
    load data/toytreefeature.mat

    Parameters
    ----------
    None

    Returns
    -------
    feature_vec : ndarray
        (n_pixeles, n_tensors, dim)
    GT_vec : ndarray
        (n_pixeles, dim)
    mask_vec : ndarray
        (n_pixeles, ), dtype='bool'
    """

    matlab_struct = sio.loadmat(proj_path + 'data/line_detection/toytreefeature.mat')

    feature_comp_vec = matlab_struct['featureImg']
    GT_comp_img = matlab_struct['imgGT'].T
    mask_img = matlab_struct['mask'].T
    mask_img = np.asarray(mask_img, dtype="bool")

    feature_comp_vec = feature_comp_vec.reshape((356, 256, 60))[:256].reshape((256 * 256, 60))
    GT_comp_vec = GT_comp_img[:256].flatten()
    mask_vec = mask_img[:256].flatten()

    # 複素数からベクトルへの変換
    feature_vec = np.zeros((feature_comp_vec.shape[0], feature_comp_vec.shape[1], 2))
    feature_vec[:, :, 0] = np.real(feature_comp_vec[:, :])
    feature_vec[:, :, 1] = np.imag(feature_comp_vec[:, :])

    GT_vec = np.zeros((GT_comp_vec.shape[0], 2))
    GT_vec[:, 0] = np.real(GT_comp_vec)
    GT_vec[:, 1] = np.imag(GT_comp_vec)

    print "feature_vec.shape", feature_vec.shape
    print "GT_vec.shape", GT_vec.shape
    print "mask_vec.shape", mask_vec.shape

    return feature_vec, GT_vec, mask_vec


def gt2vec_to_gt1img(gt2_v, img_shape):

    """
    for line detection
    Convert gt2_v into gt1_img for TN_plot's vtrain.
    一回複素数に戻して、tensor orderを変更してから、またベクトルに戻す。

    Parameters
    ----------
    gt2_v : ndarray
        (n_pixels, dim)
    img_shape : tuple of int
        shape of the return image in python
    Returns
    -------
    gt1_img : ndarray
        (dim, img_shape[1], img_shape[2]) for TN_plot.m's V

    """

    gt2_comp = gt2_v[:, 0] + 1j * gt2_v[:, 1]

    norm = np.absolute(gt2_comp)
    gt1_comp = norm * np.sqrt(gt2_comp / (norm + np.finfo(np.float64).eps))

    gt1_v = np.zeros(gt2_v.shape)
    gt1_v[:, 0] = np.real(gt1_comp)
    gt1_v[:, 1] = np.imag(gt1_comp)

    gt1_img = gt1_v.reshape((img_shape[0], img_shape[1], 2))

    # for matlab's 'Fortrun' reshaping
    gt1_img = gt1_img.transpose((2, 1, 0))

    return gt1_img


def mse_prec_reca(pred, gt):
    """
    Args
    ----
    pred : np.ndarray
        (N, dim) output from f_output
    gt : np.ndarray
        (N, dim)

    Returns
    --------
    mse, precision, recall : list of float
    """
    precision_mask = np.linalg.norm(pred, axis=1).nonzero()[0]
    recall_mask = np.linalg.norm(gt, axis=1).nonzero()[0]

    errors = (pred - gt) ** 2

    mse = np.mean(errors)
    precision = np.mean(errors[precision_mask])
    recall = np.mean(errors[recall_mask])
    return [mse, precision, recall]


def clcl_cn_inv(mat):
    """
    Calculate inverse conditional number of 2D matrix.

    Param
    --------
    mat : ndarray
        (2, 2)

    Ret
    ----
    cn_inv : float
    """
    sv = svdvals(mat)
    cn_inv = sv[1] / sv[0]

    return cn_inv


def normalize_max_feature_vec(feature_vec_train):
    """
    Parameters
    ----------
    feature_vec : ndarray
        (n_pixeles, n_tensors, dim)

    Returns
    -------
    normed : ndarray
        (n_pixeles, n_tensors, dim)
    norm_coef : ndarray
        (n_tensors, )
    """

    eps = 1e-8
    norm_coef = np.max(np.linalg.norm(feature_vec_train, axis=2), axis=0)
    normed_train = feature_vec_train / (norm_coef[:, np.newaxis] + eps)
    return normed_train, norm_coef


def pad_transpose_pred(pred, mask_vec, gt_norm_coef, vol_shape, pad):
    """
    Parameters
    ----------
    pred : numpy.ndarray
        has shape # of mask_vec True + (2*(2l+1), )
    mask_vec : numpy.ndarray
        (n_voxels, )
    gt_norm_coef : float
    vol_shape : numpy.ndarray
        (x, y, z)
    pad : None or numpy.ndarray
        if None: zero padding
        let pad=gt_vec ifyw where its shape (n_voxels, )

    Returns
    -------

    """

    if pad == None:
        pad = np.zeros((mask_vec.shape[0], pred.shape[1])) # (n_voxels, order)
    if mask_vec.nonzero()[0].shape[0] == pred.shape[0]:
        pad[mask_vec] = pred *  gt_norm_coef
    else:
        pad[mask_vec] = pred[mask_vec] *  gt_norm_coef
    retval = pad.reshape(vol_shape+(pred.shape[1], )).transpose((3, 0, 1, 2))
    print retval.shape
    return retval