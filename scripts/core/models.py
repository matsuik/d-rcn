# -*- coding: utf-8 -*-
"""
feedforward graphをつくる。
モデルの構成(層数、ユニット数, 活性化関数), 
目的関数の定義(誤差関数、l2 regularisationなどのペナルティ)などを定義する。

パラメータの初期値も決定する。

Modelの子クラスのインスタンスはcompiling.compile_graphに渡される。

"""

from __future__ import division

import numpy as np
import theano
import theano.tensor as T
from collections import OrderedDict

from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

from theano_utils import floatX
import blocks


def _make_weight_bias(n_input_tensors, n_output_tensors, layer_number, bias_scale):
    w_mat = theano.shared(
                floatX(0.01 * np.random.normal(size=(n_output_tensors, n_input_tensors))),
                name="w{}".format(layer_number),
                borrow=False)
    bias_vec = theano.shared(
                    floatX(bias_scale*np.ones(shape=(n_output_tensors,))),
                    name="bias{}".format(layer_number),
                    borrow=False)
    return w_mat, bias_vec


class Model(object):
    def __init__(self, n_tensors_list, func_key_list, l2_reg, drop_list):
        self.x_t3 = T.tensor3("x")  # (batch_size, D, n_tensors)
        self.t_mat = T.matrix("t")  # (batch, D)
        self.n_tensors_list = n_tensors_list
        self.func_key_list = func_key_list
        self.l2_reg = l2_reg
        self.drop_list = drop_list
        self.srng = RandomStreams(seed=np.random.randint(1999))
        
        
class Rcn2layer_wo_bn(Model):
    def __init__(self, n_tensors_list, func_key_list, l2_reg, drop_list, gamma_scale, bias_scale):
        assert len(n_tensors_list) == 2
        assert len(func_key_list) == 2
        assert len(drop_list) == 2
        assert isinstance(func_key_list[0], str)
        super(Rcn2layer_wo_bn, self).__init__(n_tensors_list, func_key_list, l2_reg, drop_list)
        
        self.w1, self.b1 = \
            _make_weight_bias(self.n_tensors_list[0],
                              self.n_tensors_list[1],
                              layer_number=1,
                              bias_scale=bias_scale)
        
        self.w2, self.b2 =\
            _make_weight_bias(self.n_tensors_list[1],
                              1,
                              layer_number=2,
                              bias_scale=bias_scale)
                              
        self.param_l = [self.w1, self.b1,
                        self.w2, self.b2]

    def make_graph_train(self):
        beta = T.scalar("beta")
        timestep = T.scalar("t")
        model_param_l = [beta, timestep]
        model_updates = OrderedDict()
        
        input_layer = blocks.dropout(self.x_t3, p=self.drop_list[0],
                                     n_tensors=self.n_tensors_list[0],
                                     srng=self.srng)
        
        h1 = blocks.superpose_vectors(input_layer, self.w1)
        out1, acted_norm1 = blocks.activate_norm(
            h1, self.b1, self.func_key_list[0])
        
        out1 = blocks.dropout(out1, p=self.drop_list[1],
                              n_tensors=self.n_tensors_list[1],
                              srng=self.srng)
              
        h2 = blocks.superpose_vectors(out1, self.w2)
        out2, acted_norm2= blocks.activate_norm(h2, self.b2, self.func_key_list[1])
        
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)
        obj = mse + self.l2_reg * (T.sum(self.w1**2) + T.sum(self.w2**2))
        
        return self, obj, mse, model_updates, model_param_l
        
        
    def make_graph_test(self):
        h1 = blocks.superpose_vectors(self.x_t3, self.w1)
        out1, acted_norm1 = blocks.activate_norm(
            h1, self.b1, self.func_key_list[0])
            
        h2 = blocks.superpose_vectors(out1, self.w2)
        out2, acted_norm2 = blocks.activate_norm(
            h2, self.b2, self.func_key_list[1])
        
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)
        
        return mse, out2

class Rcn2layer_with_tensor_coupling(Model):
    def __init__(self, n_tensors_list, func_key_list, l2_reg, drop_list, gamma_scale, bias_scale):
        assert len(n_tensors_list) == 2
        assert len(func_key_list) == 2
        assert len(drop_list) == 2
        assert isinstance(func_key_list[0], str)
        super(Rcn2layer_with_tensor_coupling, self).__init__(n_tensors_list, func_key_list, l2_reg, drop_list)


        self.w1, self.b1 = \
            _make_weight_bias(self.n_tensors_list[0],
                              self.n_tensors_list[1],
                              layer_number=1,
                              bias_scale=bias_scale)

        self.w11, self.b11 = \
            _make_weight_bias(self.n_tensors_list[0],
                              self.n_tensors_list[1],
                              layer_number=1,
                              bias_scale=bias_scale)
        self.w12, self.b12 = \
            _make_weight_bias(self.n_tensors_list[0],
                              self.n_tensors_list[1],
                              layer_number=1,
                              bias_scale=bias_scale)


        self.w2, self.b2 =\
            _make_weight_bias(2*self.n_tensors_list[1],
                              1,
                              layer_number=2,
                              bias_scale=bias_scale)

        self.param_l = [self.w1, self.b1,
                        self.w11, self.b11,
                        self.w12,
                        self.w2, self.b2]


    def make_graph_train(self):
        beta = T.scalar("beta")
        timestep = T.scalar("t")
        model_param_l = [beta, timestep]
        model_updates = OrderedDict()

        input_layer = blocks.dropout(self.x_t3, p=self.drop_list[0],
                                     n_tensors=self.n_tensors_list[0],
                                     srng=self.srng)

        h1_sv = blocks.superpose_vectors(input_layer, self.w1)
        out1_sv, acted_norm1 = blocks.activate_norm(
            h1_sv, self.b1, self.func_key_list[0])

        h11 = blocks.superpose_vectors(input_layer, self.w11)
        h12 = blocks.superpose_vectors(input_layer, self.w12)
        h1_tc = blocks.tensor_coupling(h11, h12, j=2, j1=2, j2=2)
        out1_tc, acted_norm1 = blocks.activate_norm(
            h1_tc, self.b11, self.func_key_list[0])

        out1 = T.concatenate([out1_sv, out1_tc], axis=2)

        out1 = blocks.dropout(out1, p=self.drop_list[1],
                              n_tensors=self.n_tensors_list[1],
                                srng=self.srng)

        h2 = blocks.superpose_vectors(out1, self.w2)
        out2, acted_norm2= blocks.activate_norm(h2, self.b2, self.func_key_list[1])

        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)
        obj = mse + self.l2_reg * (T.sum(self.w11**2) + (T.sum(self.w12**2)) + T.sum(self.w2**2))

        return self, obj, mse, model_updates, model_param_l


    def make_graph_test(self):

        h1_sv = blocks.superpose_vectors(self.x_t3, self.w1)
        out1_sv, acted_norm1 = blocks.activate_norm(
            h1_sv, self.b1, self.func_key_list[0])

        h11 = blocks.superpose_vectors(self.x_t3, self.w11)
        h12 = blocks.superpose_vectors(self.x_t3, self.w12)
        h1_tc = blocks.tensor_coupling(h11, h12, j=2, j1=2, j2=2)
        out1_tc, acted_norm1 = blocks.activate_norm(
            h1_tc, self.b11, self.func_key_list[0])

        out1 = T.concatenate([out1_sv, out1_tc], axis=2)

        h2 = blocks.superpose_vectors(out1, self.w2)
        out2, acted_norm2 = blocks.activate_norm(
            h2, self.b2, self.func_key_list[1])

        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)

        return mse, out2


class Rcn2layer_only_tensor_coupling(Model):
    def __init__(self, n_tensors_list, func_key_list, l2_reg, drop_list, gamma_scale, bias_scale):
        assert len(n_tensors_list) == 2
        assert len(func_key_list) == 2
        assert len(drop_list) == 2
        assert isinstance(func_key_list[0], str)
        super(Rcn2layer_only_tensor_coupling, self).__init__(n_tensors_list, func_key_list, l2_reg, drop_list)

        self.w11, self.b11 = \
            _make_weight_bias(self.n_tensors_list[0],
                              self.n_tensors_list[1],
                              layer_number=1,
                              bias_scale=bias_scale)
        self.w12, self.b12 = \
            _make_weight_bias(self.n_tensors_list[0],
                              self.n_tensors_list[1],
                              layer_number=1,
                              bias_scale=bias_scale)


        self.w2, self.b2 =\
            _make_weight_bias(self.n_tensors_list[1],
                              1,
                              layer_number=2,
                              bias_scale=bias_scale)

        self.param_l = [self.w11, self.b11,
                        self.w12,
                        self.w2, self.b2]


    def make_graph_train(self):
        beta = T.scalar("beta")
        timestep = T.scalar("t")
        model_param_l = [beta, timestep]
        model_updates = OrderedDict()

        input_layer = blocks.dropout(self.x_t3, p=self.drop_list[0],
                                     n_tensors=self.n_tensors_list[0],
                                     srng=self.srng)

        h11 = blocks.superpose_vectors(input_layer, self.w11)
        h12 = blocks.superpose_vectors(input_layer, self.w12)
        h1 = blocks.tensor_coupling(h11, h12, j=2, j1=2, j2=2)
        out1, acted_norm1 = blocks.activate_norm(
            h1, self.b11, self.func_key_list[0])

        out1 = blocks.dropout(out1, p=self.drop_list[1],
                              n_tensors=self.n_tensors_list[1],
                                srng=self.srng)

        h2 = blocks.superpose_vectors(out1, self.w2)
        out2, acted_norm2= blocks.activate_norm(h2, self.b2, self.func_key_list[1])

        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)
        obj = mse + self.l2_reg * (T.sum(self.w11**2) + (T.sum(self.w12**2)) + T.sum(self.w2**2))

        return self, obj, mse, model_updates, model_param_l


    def make_graph_test(self):
        h11 = blocks.superpose_vectors(self.x_t3, self.w11)
        h12 = blocks.superpose_vectors(self.x_t3, self.w12)
        h1 = blocks.tensor_coupling(h11, h12, j=2, j1=2, j2=2)
        out1, acted_norm1 = blocks.activate_norm(
            h1, self.b11, self.func_key_list[0])

        h2 = blocks.superpose_vectors(out1, self.w2)
        out2, acted_norm2 = blocks.activate_norm(
            h2, self.b2, self.func_key_list[1])

        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)

        return mse, out2


class Rcn2layer_hidden_bn(Model):
    def __init__(self, n_tensors_list, func_key_list, l2_reg, drop_list, gamma_scale, bias_scale):
        assert len(n_tensors_list) == 2
        assert len(func_key_list) == 2
        assert len(drop_list) == 2
        assert isinstance(func_key_list[0], str)
        super(Rcn2layer_hidden_bn, self).__init__(n_tensors_list, func_key_list, l2_reg, drop_list)
        
        self.w1, self.b1 = \
            _make_weight_bias(self.n_tensors_list[0],
                              self.n_tensors_list[1],
                              layer_number=1,
                              bias_scale=bias_scale)
                              
        self.gamma1 = theano.shared(
            floatX(np.abs(gamma_scale*np.random.normal(size=(self.n_tensors_list[1],)))),
            name="gamma1",
            borrow=False)
        
        self.var1 = theano.shared(
            floatX(np.zeros(self.n_tensors_list[1])),
            name="var1",
            borrow=False)
        
        self.w2, self.b2 =\
            _make_weight_bias(self.n_tensors_list[1],
                              1,
                              layer_number=2,
                              bias_scale=bias_scale)
                              
        self.param_l = [self.w1, self.b1, self.gamma1, 
                        self.w2, self.b2]
    
    
    def make_graph_train(self):
        beta = T.scalar("beta")
        timestep = T.scalar("t")
        model_param_l = [beta, timestep]
        model_updates = OrderedDict()
        
        input_layer = blocks.dropout(self.x_t3, p=self.drop_list[0],
                                     n_tensors=self.n_tensors_list[0],
                                     srng=self.srng)
        
        h1 = blocks.superpose_vectors(input_layer, self.w1)
        out1, acted_norm1, model_updates = blocks.activate_norm_bn_train(
            h1, self.gamma1, self.b1, beta, timestep, self.var1,
            self.func_key_list[0], model_updates)
        
        out1 = blocks.dropout(out1, p=self.drop_list[1],
                              n_tensors=self.n_tensors_list[1],
                                srng=self.srng)
              
        h2 = blocks.superpose_vectors(out1, self.w2)
        out2, acted_norm2 = blocks.activate_norm(
            h2, self.b2, self.func_key_list[1])
        
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)
        obj = mse + self.l2_reg * (T.sum(self.w1**2) + T.sum(self.w2**2))
        
        return self, obj, mse, model_updates, model_param_l
        
        
    def make_graph_test(self):
        h1 = blocks.superpose_vectors(self.x_t3, self.w1)
        out1, acted_norm1 = blocks.activate_norm_bn_test(
            h1, self.gamma1, self.b1, self.var1, self.func_key_list[0])
            
        h2 = blocks.superpose_vectors(out1, self.w2)
        out2, acted_norm2 = blocks.activate_norm(
            h2, self.b2, self.func_key_list[1])
        
        
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)
        
        return mse, out2
        


class Rcn2layer_all_bn(Model):
    def __init__(self, n_tensors_list, func_key_list, l2_reg, drop_list, gamma_scale, bias_scale):
        assert len(n_tensors_list) == 2
        assert len(func_key_list) == 2
        assert len(drop_list) == 2
        assert isinstance(func_key_list[0], str)
        super(Rcn2layer_all_bn, self).__init__(n_tensors_list, func_key_list, l2_reg, drop_list)
        
        self.w1, self.b1 = \
            _make_weight_bias(self.n_tensors_list[0],
                              self.n_tensors_list[1],
                              layer_number=1,
                              bias_scale=bias_scale)
                              
        self.gamma1 = theano.shared(
            floatX(np.abs(gamma_scale*np.random.normal(size=(self.n_tensors_list[1],)))),
            name="gamma1",
            borrow=False)
        
        self.var1 = theano.shared(
            floatX(np.zeros(self.n_tensors_list[1])),
            name="var1",
            borrow=False)
        
        self.w2, self.b2 =\
            _make_weight_bias(self.n_tensors_list[1],
                              1,
                              layer_number=2,
                              bias_scale=bias_scale)
                              
        self.gamma2 = theano.shared(
            floatX(np.array(gamma_scale)),
            name="gamma2",
            borrow=False)
        
        self.var2 = theano.shared(
            floatX(np.zeros(1)),
            name="var2",
            borrow=False)
                              
        self.param_l = [self.w1, self.b1, self.gamma1, 
                        self.w2, self.b2, self.gamma2]
    
    
    def make_graph_train(self):
        beta = T.scalar("beta")
        timestep = T.scalar("t")
        model_param_l = [beta, timestep]
        model_updates = OrderedDict()
        
        input_layer = blocks.dropout(self.x_t3, p=self.drop_list[0],
                                     n_tensors=self.n_tensors_list[0],
                                     srng=self.srng)
        
        h1 = blocks.superpose_vectors(input_layer, self.w1)
        out1, acted_norm1, model_updates = blocks.activate_norm_bn_train(
            h1, self.gamma1, self.b1, beta, timestep, self.var1,
            self.func_key_list[0], model_updates)
        
        out1 = blocks.dropout(out1, p=self.drop_list[1],
                              n_tensors=self.n_tensors_list[1],
                                srng=self.srng)
              
        h2 = blocks.superpose_vectors(out1, self.w2)
        out2, acted_norm2, model_updates = blocks.activate_norm_bn_train(
            h2, self.gamma2, self.b2, beta, timestep, self.var2,
            self.func_key_list[1], model_updates)
        
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)
        obj = mse + self.l2_reg * (T.sum(self.w1**2) + T.sum(self.w2**2))
        
        return self, obj, mse, model_updates, model_param_l
        
        
    def make_graph_test(self):
        h1 = blocks.superpose_vectors(self.x_t3, self.w1)
        out1, acted_norm1 = blocks.activate_norm_bn_test(
            h1, self.gamma1, self.b1, self.var1, self.func_key_list[0])
            
        h2 = blocks.superpose_vectors(out1, self.w2)
        out2, acted_norm2 = blocks.activate_norm_bn_test(
            h2, self.gamma2, self.b2, self.var2, self.func_key_list[1])
        
        
        mse = T.mean((out2 - self.t_mat.dimshuffle(0, 1, "x")) ** 2)
        
        return mse, out2
        
        
