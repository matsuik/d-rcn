# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 20:35:18 2016

@author: matsuik
"""

import numpy as np
import theano

def floatX(array_):
    """
    Args
    ----
    array_: np.ndarray
    
    Returns
    -------
    np.ndarray
    """
    
    return np.asarray(array_, dtype=theano.config.floatX)
    
def zero_shared(shape):
    """
    Args
    ----
    shape : TensorVariable
        from TensorVariable.shape.eval()
    """
    return theano.shared(floatX(np.zeros(shape)))

