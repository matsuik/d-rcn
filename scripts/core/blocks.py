# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 20:59:19 2016

@author: matsuik

models.pyにsuperpose_vectorsやnorm_activationなどの低レベルの関数を提供

"""

import numpy as np
import TN_utils
import models
import theano
import theano.tensor as T


def make_weight(n_input_tensors, n_output_tensors, model=None, name=None, scale=0.01):
    """
    Parameters
    ----------
    n_input_tensors
    n_output_tensors
    scale

    Returns
    -------
    w_mat : theano shared variable
        (n_output_tensors, n_input_tensors)
    """
    w_mat = theano.shared(
                TN_utils.floatX(scale * np.random.normal(size=(n_output_tensors, n_input_tensors))),
                name=name,
                borrow=False)
    if model:
        model.param_list.append(w_mat)

    return w_mat


def make_bias(n_output_tensors, model=None, name=None, scale=0.):
    """
    Parameters
    ----------
    n_output_tensors
    scale

    Returns
    -------
    bias_vec : theano shared variable
        (n_output_tensors, )
    """
    bias_vec = theano.shared(
                    TN_utils.floatX(scale*np.ones(shape=(n_output_tensors,))),
                    name=name,
                    borrow=False)
    if model:
        model.param_list.append(bias_vec)

    return bias_vec


def get_act_func(keyword):
    """
    Args
    ----
    keyword : str
        "relu", "retanh", "sigm", "l-retanh"
        
    Returns
    -------
    elementswise scalar f: TensorVariable -> TensorVariable
    """

    if keyword == "linear":
        a = lambda x: x
    elif keyword == "sigmoid":
        a = T.nnet.sigmoid
    elif keyword == "tanh":
        a = T.tanh
    elif keyword == "relu":
        a = lambda x: T.switch(x > 0, x, 0)
    elif keyword == "l-relu":
        a = lambda x: T.switch(x > 0, x, 0.1*x)
    elif keyword == "elu":
        a = lambda x: T.switch(x > 0, x, T.exp(x)-1.)

    elif keyword == 'softmax':
        a = T.nnet.softmax

    elif keyword == "retanh":
        a = lambda x: T.switch(x > 0, T.tanh(x), 0)
    elif keyword == "exp-linear":
        a = lambda x: T.switch(x > 0, x + 1., T.exp(x))
    elif keyword == "l-retanh":
        a = lambda x: T.switch(x > 0, T.tanh(x), 0.01*x) 

    elif keyword == "super-tanh":
        a = lambda x: (T.tanh(x-1.)+1.)/2.
    elif keyword == '0.5*sigmoid+0.5':
        a = lambda x: 0.5*T.nnet.sigmoid(x) + 0.5
    else:
        raise
        
    return a


def get_error_func(metoric_name):
    if metoric_name == 'l2':
        def error_func(pred, gt):
            return T.mean((pred - gt)**2)
    elif metoric_name == 'l1':
        def error_func(pred, gt):
            return T.mean(abs(pred - gt))
    elif metoric_name == 'cross_entropy':
        error_func = T.nnet.categorical_crossentropy
    elif metoric_name == 'KL':
        def error_func(pred, gt):
            return T.sum(-pred*T.log(gt/pred))
    else:
        raise
    return error_func


def get_reg_func(metoric_name):
    if metoric_name == 'l2':
        def func(pred):
            return T.mean((pred)**2)
    elif metoric_name == 'l1':
        def func(pred):
            return T.mean(abs(pred))
    elif metoric_name == 'entropy':
        def func(pred):
            return T.sum(pred*T.log(pred))
    else:
        raise
    return func


def superpose_vectors(x_t3, w_mat):
    """
    superpose D-dimansion vectors with scalars
    
    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_input_tensors)
    w_mat : theano.tensor.var.TensorVariable
        (n_ouput_tensors, n_input_tensors)
    
    Returns
    -------
    theano.tensor.var.TensorVariable
        (batch_size, D, n_ouput_tensors)
    """
    
    # sum_k x_ijk w_pk -> y_ijp
    return T.tensordot(x_t3, w_mat, axes=(2, 1))


def activate_norm(x_t3, bias_vec, func_key="relu", eps=1e-8):
    """
    elementwise_scalar_func(||x||+b) * x / (||x|| + eps)
    
    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_tensors)
    bias_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    func_key : str
        see theano_utils.get_act_func
    eps : float
        small number prevents zerodivision
        
    Returns
    -------
    output_t3 : a node of computational graph
        (batch_size, D, n_tensors)
    acted_norm : a node of computaional graph
        (batch_size, n_tensors)
        これじゃなくて activationを見るべき
    
    """
    
    act_func = get_act_func(func_key)  
    
    norm = x_t3.norm(L=2, axis=1)  # (batch_size, n_tensors)
    activation = norm + bias_vec  # (batch_size, n_tensors)
    acted_norm = act_func(activation)  # (batch_size, n_tensors)
    ouput_t3 = acted_norm.dimshuffle(0, "x", 1) * x_t3 / (norm.dimshuffle(0, "x", 1) + eps)
    return ouput_t3, acted_norm


def dropout(x_t3, n_tensors, p, srng):
    """


    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_tensors)
    n_tensors : int
    p : float
    srng : RandomStreams

    Returns
    -------
    symbolic tensor (batch_size, D, n_tensors)
    """

    mask = srng.binomial(n=1, p=p, size=(x_t3.shape[0], x_t3.shape[2]))
    return x_t3 * T.cast(mask/p, theano.config.floatX).dimshuffle(0, "x", 1)


def tensor_coupling(x1_t3, x2_t3, j, j1, j2):
    """

    Parameters
    ----------
    x1_t3 : TensorVariable
        (N, dim1, n_tensors)
    x2_t3 : TensorVariable
        (N, dim2, n_tensors)
    j : int
        the order of the output tensors
    j1 : int
        the order of x1_t3
    j2 : int
        the order of x2_t3

    Returns
    -------
    out : TensorVariable
        (N, dim_out, n_tensors)
    """
    def c_mul(y1, y2):
        """x : (N, 2, n_tensors)"""
        return T.stacklists([y1[:, 0]*y2[:, 0] - y1[:, 1]*y2[:, 1],
                             y1[:, 0]*y2[:, 1] + y1[:, 1]*y2[:, 0]]).transpose(1, 0, 2)

    cg_table = TN_utils.cg_table(j, j1, j2)
    tensor = []
    for m in range(-j, j+1):
        tmp = 0
        for m1, m2 in [(m1, m2) for m1 in range(-j1, j1+1) for m2 in range(-j2, j2+1) if m == m1+m2]:
            tmp += cg_table[m, m1, m2]*c_mul(x1_t3[:, 2*(m1+j1) : 2*(m1+j1)+2], x2_t3[:, 2*(m2+j2) : 2*(m2+j2)+2])
        tensor.append(tmp)
    out = T.concatenate(tensor, axis=1)
    return out


def multi_order_mix(tensor_list, j_input_list, j_output, n_input_tensors_list, n_output_tensors,
                    is_train, func_key, weight_list=None, bias_list=None):
    """

    Parameters
    ----------
    tensor_list : list of TensorVariable
        a Tensorbariable has shape (N, dim, n_tensors)
    j_input_list : list of int
    j_output : int
    n_input_tensors_list : list of int
    n_output_tensors : int
    is_train : bool
    func_key : str
    weight_list : list of SharedVariable
    bias_list : list of SharedVariable

    Returns
    -------

    """
    assert len(tensor_list) == len(j_input_list) == len(n_input_tensors_list)
    coupling_list = [(l1, l2) for l1 in j_input_list for l2 in j_input_list
                     if abs(l1-l2) <= j_output <= l1+l2 and l1 <= l2]
    print coupling_list

    output_list = []
    if is_train:
        weight_list = []
        bias_list = []
        for coupling in coupling_list:
            j1 = coupling[0]
            j2 = coupling[1]
            index_j1 = j_input_list.index(j1)
            index_j2 = j_input_list.index(j2)

            W1 = make_weight(n_input_tensors_list[index_j1], n_output_tensors,
                             name='W_{}_{}'.format(coupling[0], coupling))
            W2 = make_weight(n_input_tensors_list[index_j2], n_output_tensors,
                             name='W_{}_{}'.format(coupling[1], coupling))
            weight_list.append(W1)
            weight_list.append(W2)
            bias = make_bias(n_output_tensors, name='bias_{}'.format(coupling))
            bias_list.append(bias)

            h1 = superpose_vectors(tensor_list[index_j1], W1)
            h2 = superpose_vectors(tensor_list[index_j2], W2)

            h_tc = tensor_coupling(h1, h2,  j_output, j1, j2)

            h_na = activate_norm(h_tc, bias, func_key)[0]

            output_list.append(h_na)

        return T.concatenate(output_list, axis=2), weight_list, bias_list, len(coupling_list)

    else:
        for i, coupling in enumerate(coupling_list):
            j1 = coupling[0]
            j2 = coupling[1]
            index_j1 = j_input_list.index(j1)
            index_j2 = j_input_list.index(j2)

            W1 = weight_list[2*i]
            W2 = weight_list[2*i + 1]
            bias = bias_list[i]

            h1 = superpose_vectors(tensor_list[index_j1], W1)
            h2 = superpose_vectors(tensor_list[index_j2], W2)

            h_tc = tensor_coupling(h1, h2,  j_output, j1, j2)

            h_na = activate_norm(h_tc, bias, func_key)[0]

            output_list.append(h_na)

        return T.concatenate(output_list, axis=2)

def simple_multi_order_mix(tensor_list, j_input_list, j_output):
    """

    Parameters
    ----------
    tensor_list : list of TensorVariable
        a Tensorbariable has shape (N, dim, 1)
    j_input_list : list of int
    j_output : int

    Returns
    -------
    TensorVariable
        (N, dim, n_tensors)

    """
    assert len(tensor_list) == len(j_input_list)
    coupling_list = [(l1, l2) for l1 in j_input_list for l2 in j_input_list
                     if abs(l1-l2) <= j_output <= l1+l2 and l1 <= l2]
    print coupling_list

    output_list = []
    for coupling in coupling_list:
        j1 = coupling[0]
        j2 = coupling[1]
        index_j1 = j_input_list.index(j1)
        index_j2 = j_input_list.index(j2)

        h_tc = tensor_coupling(tensor_list[index_j1], tensor_list[index_j2],  j_output, j1, j2)
        output_list.append(h_tc)

    return T.concatenate(output_list, axis=2)


### batch nomalization
def activate_norm_bn_train(x_t3, gamma_vec, bias_vec, beta, t, ac_var, func_key,
                           model_updates, eps=1e-5):
    """
    n = ||x||
    activation = (n - E[n]) / sqrt(V[n]+eps) 
    activate(gamma*n + b) * x / (n + eps)
    
    mean, varは指数的に最近のをみて推定
    
    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_tensors)
    gamma_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    bias_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    beta : symbolic scalar
        平均, 分散の推定につかう
    t : symbolic scalar
        timestep, 平均、分散の推定のバイアスを取るために使う(Adamぽく)
    ac_var : shared variable
        (n_tensors, ) 分散の推定値  
    func_key : str
        see theano_utils.get_act_func
    model_updates : OrderedDict
    eps : float
        small number prevents zerodivision
        
    Returns
    -------
    output_t3 : a node of computational graph
        (batch_size, D, n_tensors)
    acted_norm : a node of computaional graph
        (batch_size, n_tensors)
        これじゃなくて activationを見るべき
    
    """
    
    act_func = get_act_func(func_key)
    D = T.cast(x_t3.shape[1], theano.config.floatX)
    
    norm = x_t3.norm(L=2, axis=1)  # (batch_size, n_tensors)
    
    batch_var = T.mean(norm**2, axis=0) # (n_tensors,)
    var = beta*batch_var + (1.-beta)*ac_var
    var_hat = var / (1. - beta**t)
    
    activation = norm / T.sqrt(var_hat+eps) * gamma_vec + bias_vec  # (batch_size, n_tensors)
    acted_norm = act_func(activation/D)  # (batch_size, n_tensors)
    ouput_t3 = acted_norm.dimshuffle(0, "x", 1) * x_t3\
                / (norm.dimshuffle(0, "x", 1) + eps)
                
    model_updates[ac_var] = var
    
    return ouput_t3, acted_norm, model_updates

def activate_norm_bn_test(x_t3, gamma_vec, bias_vec, ac_var, func_key, eps=1e-5):
    """
    elementwise_scalar_func(||x||+b) * x / (||x|| + eps)
    
    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_tensors)
    gamma_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    bias_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    func_key : str
        see theano_utils.get_act_func
    eps : float
        small number prevents zerodivision
        
    Returns
    -------
    output_t3 : a node of computational graph
        (batch_size, D, n_tensors)
    acted_norm : a node of computaional graph
        (batch_size, n_tensors)
        これじゃなくて activationを見るべき
    
    """
    
    act_func = get_act_func(func_key) 
    D = T.cast(x_t3.shape[1], theano.config.floatX)
    
    norm = x_t3.norm(L=2, axis=1)  # (batch_size, n_tensors)
    
    activation = norm / T.sqrt(ac_var+eps) * gamma_vec + bias_vec  # (batch_size, n_tensors)
    acted_norm = act_func(activation/D)  # (batch_size, n_tensors)
    ouput_t3 = acted_norm.dimshuffle(0, "x", 1) * x_t3\
                / (norm.dimshuffle(0, "x", 1) + eps)
    return ouput_t3, acted_norm
    
    
def activate_norm_bn_train_ordinary(x_t3, gamma_vec, bias_vec, beta, t, 
                                    ac_mean,ac_var, func_key, eps=1e-8):
    """
    n = ||x||
    activation = (n - E[n]) / sqrt(V[n]+eps) 
    activate(gamma*n + b) * x / (n + eps)
    
    mean, varは指数的に最近のをみて推定
    
    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_tensors)
    gamma_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    bias_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    beta : symbolic scalar
        平均, 分散の推定につかう
    t : symbolic scalar
        timestep, 平均、分散の推定のバイアスを取るために使う(Adamぽく)
    ac_mean : shared variable
        (n_tensors,) 平均の推定値
    ac_var : shared variable
        (n_tensors, ) 分散の推定値  
    func_key : str
        see theano_utils.get_act_func
    eps : float
        small number prevents zerodivision
        
    Returns
    -------
    output_t3 : a node of computational graph
        (batch_size, D, n_tensors)
    acted_norm : a node of computaional graph
        (batch_size, n_tensors)
        これじゃなくて activationを見るべき
    
    """
    
    act_func = get_act_func(func_key)  
    
    norm = x_t3.norm(L=2, axis=1)  # (batch_size, n_tensors)
    
    batch_mean = T.mean(norm, axis=0)
    batch_var = T.var(norm, axis=0) # (n_tensors,)
    mean = beta*batch_mean + (1.-beta)*ac_mean
    var = beta*batch_var + (1.-beta)*ac_var
    mean_hat = mean / (1. - beta**t)
    var_hat = var / (1. - beta**t)
    
    activation = (norm - mean_hat) / T.sqrt(var_hat+eps) * gamma_vec + bias_vec  # (batch_size, n_tensors)
    acted_norm = act_func(activation)  # (batch_size, n_tensors)
    ouput_t3 = acted_norm.dimshuffle(0, "x", 1) * x_t3\
                / (norm.dimshuffle(0, "x", 1) + eps)
                
    model_updates = {}
    model_updates[ac_mean] = mean
    model_updates[ac_var] = var
    
    return ouput_t3, acted_norm, model_updates

def activate_norm_bn_test_ordinary(x_t3, gamma_vec, bias_vec, ac_mean, ac_var, func_key, eps=1e-8):
    """
    elementwise_scalar_func(||x||+b) * x / (||x|| + eps)
    
    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_tensors)
    gamma_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    bias_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    ac_mean : shared variable
        (n_tensors,) 平均の推定値
    ac_var : shared variable
        (n_tensors, ) 分散の推定値  
    func_key : str
        see theano_utils.get_act_func
    eps : float
        small number prevents zerodivision
        
    Returns
    -------
    output_t3 : a node of computational graph
        (batch_size, D, n_tensors)
    acted_norm : a node of computaional graph
        (batch_size, n_tensors)
        これじゃなくて activationを見るべき
    
    """
    
    act_func = get_act_func(func_key)  
    
    norm = x_t3.norm(L=2, axis=1)  # (batch_size, n_tensors)
    
    activation = (norm - ac_mean) / T.sqrt(ac_var+eps) * gamma_vec + bias_vec  # (batch_size, n_tensors)
    acted_norm = act_func(activation)  # (batch_size, n_tensors)
    ouput_t3 = acted_norm.dimshuffle(0, "x", 1) * x_t3\
                / (norm.dimshuffle(0, "x", 1) + eps)
    return ouput_t3, acted_norm
    
    

