# -*- coding: utf-8 -*-
from __future__ import division

import os, sys
import cPickle

import numpy as np
import scipy.io as sio
import theano
import theano.tensor as T
from scipy.spatial import ConvexHull
proj_path = '/home/matsui-k/projects/d-rcn/'

from TN_utils import floatX, normalize_vec, normalize_ex2_feature_vec, comp2vec, vec2comp, make_index
import blocks

def printf(string):
    print string
    sys.stdout.flush()


def load_blind(input_L, output_L, folder_name, load_output=False):
    """
    matlab structからloadして、lごとにsplitして、複素数を実数に直す

    Parameters
    ----------
    input_L : int
    output_L : int
        呼ぼうとしてるデータの最大L
    folder_name : str
        path to d-rcn/data/blind/str

    Returns
    -------
    input_list : list of ndarray
        0, 2, 4, 6, ...
        (N, dim=2*(2*l+1))
    output_list : list of ndarray
        0, 2, 4, 6, ...
        (N, dim=2*(2*l+1))

    """
    printf('loding data...')
    def separete(tensor, index_list):
        return [tensor[index[0]:index[1]] for index in index_list]

    def c2v(tens_list):
        v_list = []
        for tens in tens_list:
            tmp = np.squeeze(comp2vec(tens.T[:, :, np.newaxis]))
            v_list.append(tmp)
            print tmp.shape
        return v_list

    input_index = make_index(input_L)
    printf(input_index)
    ms_input = sio.loadmat(proj_path + 'data/blind/'+folder_name+'/input.mat')
    input = ms_input['input']
    input_list = separete(input, input_index)
    input_list = c2v(input_list)

    if not load_output:
        printf('data is loaded.')
        
        return input_list
    else:
        output_index = make_index(output_L)
        printf(output_index)
        ms_output = sio.loadmat(proj_path + 'data/blind/'+folder_name+'/output.mat')
        output = ms_output['output']
        output_list = separete(output, output_index)
        output_list = c2v(output_list)

        printf( 'data is loaded.')
        

        return input_list, output_list


def load_reconst_fodf(folder_name):
    """

    Parameters
    ----------
    folder_name

    Returns
    -------
    folf : numpy.ndarray
        shape (N, n_dirs)
    """
    ms = sio.loadmat(proj_path + 'data/blind/'+folder_name+'/reconstructed_fodf.mat')
    return floatX(ms['s'].T)


def pre_blind(input_tensors, output_tensors, N, train_valid_ratio=0.8):
    """

    permutateして、trainとtestに分けて、正規化
    inputに使う方は (N, dim, n_tensor)にする必要があるので(N, dim, 1)とする。

    outputもふつうに正規化する。

    Parameters
    ----------
    input_tensors : list of numpy.ndarray
    output_tensors : list of numpy.ndarray
    N : int
    train_valid_ratio : float

    Returns
    -------
    list of ndarray:
        l-th element has shape (N, dim, 1)
    list of ndarray:
        l-th element has shape (N, dim)
    list of ndarray:
        test_input (N, dim, 1)
    list of ndarray:
        test_output (N, dim)
    input_coef : list of float
    output_coef : list of float

    """
    n_train = int(N*train_valid_ratio)
    n_test = int(N*(1.-train_valid_ratio ))
    np.random.seed(1999)
    permu_index = np.random.permutation(range(input_tensors[0].shape[0]))

    def normalize(tens_list):
        train_normed_list = []
        test_normed_list = []
        coef_list = []
        for tens in tens_list:
            train_normed, coef = normalize_ex2_feature_vec(tens[permu_index][:n_train])
            train_normed_list.append(train_normed)
            coef_list.append(coef)
            test_normed = normalize_vec(tens[permu_index][n_train:n_train+n_test], coef)
            test_normed_list.append(test_normed)
        return train_normed_list, test_normed_list, coef_list

    # outputも正規化してる。
    train_input, test_input, input_coef = normalize(input_tensors)
    train_output, test_output, output_coef = normalize(output_tensors)

    def newaxise(tens_list):
        return [tens[:, :, np.newaxis] for tens in tens_list]

    return newaxise(train_input), train_output, newaxise(test_input), test_output, input_coef, output_coef


def pre_blind_tanh(input_tensors, output_tensors, N, train_valid_ratio=0.8):
    """
    permutateして、trainとtestに分けて、正規化
    inputに使う方は (N, dim, n_tensor)にする必要があるので(N, dim, 1)とする。

    outputを正規化せずに, RCNのouput activationに '0.5*sigmoid+0.5'を使うとき

    Parameters
    ----------
    input_tensors : list of numpy.ndarray
    output_tensors : list of numpy.ndarray
    N : int
    train_valid_ratio : float

    Returns
    -------
    list of ndarray:
        l-th element has shape (N, dim, 1)
    list of ndarray:
        l-th element has shape (N, dim)
    list of ndarray:
        test_input (N, dim, 1)
    list of ndarray:
        test_output (N, dim)
    input_coef : list of float


    """
    n_train = int(N*train_valid_ratio)
    n_test = int(N*(1.-train_valid_ratio ))
    np.random.seed(1999)
    permu_index = np.random.permutation(range(input_tensors[0].shape[0]))
    def normalize(tens_list):
        train_normed_list = []
        test_normed_list = []
        coef_list = []
        for tens in tens_list:
            train_normed, coef = normalize_ex2_feature_vec(tens[permu_index][:n_train])
            train_normed_list.append(train_normed)
            coef_list.append(coef)
            test_normed = normalize_vec(tens[permu_index][n_train:n_train+n_test], coef)
            test_normed_list.append(test_normed)

        return train_normed_list, test_normed_list, coef_list

    train_input, test_input, input_coef = normalize(input_tensors)

    # outputは正規化しない
    train_output = []
    test_output = []
    for tens in output_tensors:
        train_output.append(tens[permu_index][:n_train])
        test_output.append(tens[permu_index][n_train:n_train+n_test])

    def newaxise(tens_list):
        return [tens[:, :, np.newaxis] for tens in tens_list]
    return newaxise(train_input), train_output, newaxise(test_input), test_output, input_coef


def pre_blind_pc(input_tensors, gt_vec, N, train_valid_ratio=0.8):
    """
    inputは正規化してる, gtは0以下のところを0にしちゃってる

    Parameters
    ----------
    input_tensors : list of numpy.ndarray
        (N_all, dim) or (N_all, dim, n_tensors)
    gt_vec : numpy.ndarray
        (N_all, n_dir)
    N:#samples to use
    train_valid_ratio

    Returns
    -------
    list of ndarray:
        l-th element has shape (N, dim, n_tensors)
    ndarray:
        (N, n_dir)
    list of ndarray:
        l-th element has shape (N, dim, n_tensors)
    ndarray:
        (N, n_dir)
    input_coef : list of float

    """
    n_train = int(N*train_valid_ratio)
    n_test = int(N*(1.-train_valid_ratio ))
    permu_index = np.random.permutation(range(input_tensors[0].shape[0]))
    def normalize(tens_list):
        """

        Parameters
        ----------
        tens_list : list of numpy.ndarray
            a array has shape (N_all, n_tensors, dim) for normalize_ex2_feature_vec
        Returns
        -------
        list of numpy.ndarray
            a array has shape (N_all, dim, n_tensors)

        """
        train_normed_list = []
        test_normed_list = []
        coef_list = []
        for tens in tens_list:
            train_normed, coef = normalize_ex2_feature_vec(tens[permu_index][:n_train])
            printf( train_normed.shape)
            train_normed_list.append(train_normed.transpose(0, 2, 1))
            coef_list.append(coef)
            test_normed = normalize_vec(tens[permu_index][n_train:n_train+n_test], coef)
            test_normed_list.append(test_normed.transpose(0, 2, 1))
            printf( test_normed.shape)

        return train_normed_list, test_normed_list, coef_list


    if input_tensors[0].ndim == 2: # (N_all, dim) to (N_all, 1, dim)
        input_tensors = [tensor[:, np.newaxis, :] for tensor in input_tensors]
        printf( input_tensors[1].shape)
    elif input_tensors[0].ndim == 3: # (N_all, dim, n_tensors) to (N_all, n_tensors, dim)
        input_tensors = [tensor.transpose(0, 2, 1) for tensor in input_tensors]
        printf( input_tensors[1].shape)

    train_input, test_input, input_coef = normalize(input_tensors)

    gt_vec[gt_vec < 0.] = 0.
    gt_vec = gt_vec / np.sum(gt_vec, axis=1)[:, np.newaxis]

    train_output = gt_vec[permu_index][:n_train]
    test_output = gt_vec[permu_index][n_train:n_train+n_test]

    return train_input, train_output, test_input, test_output, input_coef


def post_process_blind(tens_list, output_coef):
    """
        tens_listのひとつ目の要素はL=2
        output_coefのひとつ目の要素はL=0
    """
    tens0 = np.concatenate([np.ones((tens_list[0].shape[0], 1)), np.zeros((tens_list[0].shape[0], 1))], axis=1)
    tens_list.insert(0, tens0)
    a = [vec2comp(tens) for tens in tens_list]
    return np.concatenate([tens * coef for tens, coef in zip(a, output_coef)], axis=1)


def post_process_blind_tanh(tens_list):
    """
        tens_listのひとつ目の要素はL=2
        output_coefのひとつ目の要素はL=0
    """
    tens0 = np.concatenate([np.ones((tens_list[0].shape[0], 1)), np.zeros((tens_list[0].shape[0], 1))], axis=1)
    tens_list.insert(0, tens0)
    a = [vec2comp(tens) for tens in tens_list]
    return np.concatenate([tens for tens in a], axis=1)


def multi_order_mix(np_tensor_list, max_j_input, max_j_output):
    """

    Parameters
    ----------
    np_tensor_list : list of numpy.ndarray
        a ndarray has shape (N, dim_l)
    max_j_input : int
    max_j_output : int

    Returns
    -------
    list of numpy.ndarray
        a ndarray has shape (N, dim_l, n_tensors_l)
    list of int

    """
    printf( 'mixing tensors...')
    sys.setrecursionlimit(50000) # for pickle
    j_input_list = range(0, max_j_input+1, 2)
    j_output_list = range(0, max_j_output+1, 2)
    tensor_list = [T.tensor3() for _ in range(len(np_tensor_list))]
    out_list = []
    for j in j_output_list:
        printf( 'mixing '+str(j_input_list)+'_'+str(j))
        cache_path = os.path.join(os.path.dirname(__file__), 'theano_function_cache',
                                  'multi_order_mix_'+str(j_input_list)+'_'+str(j)+'.pkl')
        if os.path.exists(cache_path):
            printf( 'loding a cached function...')
            with open(cache_path, 'rb') as f:
                func = cPickle.load(f)
        else:
            printf( 'compiling ' + 'multi_order_mix_'+str(j_input_list)+'_'+str(j))
            y = blocks.simple_multi_order_mix(tensor_list, j_input_list, j_output=j)
            func = theano.function(inputs=tensor_list, outputs=[y])
            with open(cache_path, 'wb') as f:
                cPickle.dump(func, f, protocol=cPickle.HIGHEST_PROTOCOL)
        out = func(*[tensor[:, :, np.newaxis] for tensor in np_tensor_list])[0]
        out = np.concatenate([out, np_tensor_list[j_output_list.index(j)][:, :, np.newaxis]], axis=2)
        out_list.append(out)
    printf('tensors are mixed.')
    return out_list, [tensor.shape[2] for tensor in out_list]


def localmax(pred, ratio=0.2):
    """

    Parameters
    ----------
    pred : numpy.ndarray
        (N, n_dir)

    Returns
    -------
    peak_value_list_list : list of list
    peak_dir_list_list : list of list


    """
    ms = sio.loadmat('/home/matsui-k/projects/d-rcn/dwsignsim/bdirs_whole_258.mat')
    bdirs = ms['bdirs']
    assert pred.shape[1] == bdirs.shape[1]

    # convexhullでedgeで繋がってるvertexをneighborとする, nnの数は自分も含めて6か7になる
    ch = ConvexHull(bdirs.T)
    knn_indices = []
    for i_dir in range(256):
        nn_indices_list = []
        for simplex in ch.simplices:
            if i_dir in simplex:
                nn_indices_list.extend(list(simplex))
        knn_indices.append(np.unique(nn_indices_list))

    peak_value_list_list = []
    peak_dir_list_list = []
    for s in pred:
        # nnの中でlocal maxなら候補にする
        local_max_indices_for_bdirs = []
        for i_dir in range(s.shape[0]):
            if i_dir == knn_indices[i_dir][np.argmax(s[knn_indices[i_dir]])]:
                local_max_indices_for_bdirs.append(i_dir)
        indices_sorted_for_local_max = np.argsort(s[local_max_indices_for_bdirs])[::-2] # ソートしてから後ろから1ッコ飛ばしで

        # global maxのratio倍のやつだけ採用する
        max_index = indices_sorted_for_local_max[0]
        valid_peak_indices = []
        for i_peak in indices_sorted_for_local_max:
            if s[local_max_indices_for_bdirs[i_peak]] > s[local_max_indices_for_bdirs[max_index]] * ratio:
                valid_peak_indices.append(local_max_indices_for_bdirs[i_peak])

        # 2字形式で近似して、eigenvectorを取ることでmaxを求める
        peak_value_list = []
        peak_dir_list = []
        for i_peak in valid_peak_indices:
            nn_index = knn_indices[i_peak]
            nn_bdirs = bdirs[:, nn_index]
            # x.shape (dim=6, #nn=6 or 7)
            x = np.array([nn_bdirs[0]**2, nn_bdirs[1]**2, nn_bdirs[2]**2, nn_bdirs[0]*nn_bdirs[1], nn_bdirs[1]*nn_bdirs[2], nn_bdirs[2]*nn_bdirs[0]])
            # y.shape (#nn=6 or 7, )
            y = s[nn_index]
            w = np.dot(np.linalg.pinv(x.T), y) # w.shape (dim=6, )
            H = np.array([[w[0], w[3]/2, w[5]/2], [w[3]/2, w[1], w[4]/2], [w[5]/2, w[4]/2, w[2]]])
            e_val, e_vec = np.linalg.eigh(H)
            peak_value_list.append(e_val[-1])
            peak_dir_list.append(e_vec[:, -1])

        peak_dir_list_list.append(peak_dir_list)
        peak_value_list_list.append(peak_value_list)

    return peak_value_list_list, peak_dir_list_list
