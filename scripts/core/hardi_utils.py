# -*- coding: utf-8 -*-
from __future__ import division

import numpy as np
import scipy.io as sio

from TN_utils import normalize_vec, normalize_ex2_feature_vec, permu_date_set

proj_path = '/home/matsui-k/projects/d-rcn/'

def load_hardi(i_order, i_indiv=0, feature='invivo', gt='exvivo', mask_thresh=0.5, only_feature=False):
    """
    Parameters
    ----------
    i_order : int in [0, 1, 2] indexes [0, 2, 4]
    i_indiv : int in [0, 1] indexes [1506, 1507]
    feature : 'invivo' or 'exvivo'
    gt : 'invivo' or 'exvivo'

    Returns
    -------
    feature_vec : np.ndarray has shape (n_voxels, n_tensors, dim)
    gt_vec : np.ndarray has shape (n_voxels, dim)
    mask : np.ndarray has shape (n_voxels, ), dtype='bool'
    """
    assert i_order in [0, 1, 2]
    assert i_indiv in [0, 1]

    indiv = ['1506', '1507'][i_indiv]
    feature_path = proj_path+'data/hardi_in_ex/20160407/'+indiv+'_'+feature+'_hardi.mat'
    feature_d = sio.loadmat(feature_path)
    tensor_features = feature_d['tensor_features']
    # the last is row data
    feature_vol = tensor_features[0, i_order].transpose(2, 3, 4, 1, 0)[:, :, :, :, :]
    vol_shape=feature_vol.shape[:3]
    feature_vec = feature_vol.reshape(np.prod(vol_shape), feature_vol.shape[3], feature_vol.shape[4])
    if only_feature:
        print feature_vec.shape
        return feature_vec

    gt_path = proj_path+'data/hardi_in_ex/20160407/'+indiv+'_'+gt+'_hardi.mat'
    gt_d = sio.loadmat(gt_path)
    tensors_gt = gt_d['tensor_features']
    # take only the last
    gt_vol = tensors_gt[0, i_order].transpose(2, 3, 4, 1, 0)[:, :, :, -1, :]
    gt_vec = gt_vol.reshape(np.prod(vol_shape), feature_vol.shape[4])

    mask_vol = np.logical_or(feature_d['wm'] > mask_thresh, gt_d['wm'] > mask_thresh)
    mask_vec = mask_vol.flatten()
    vol_shape = mask_vol.shape

    print feature_vec.shape, gt_vec.shape, mask_vec.shape
    return feature_vec, gt_vec, mask_vec, vol_shape


def preprocess_hardi(feature_vec, gt_vec, mask_vec, split_data=True, train_sample_ratio=0.8, permu_index=None):
    """
    Parameters
    ----------
    feature_vec : numpy.ndarray has shape (n_voxels, n_tensors, dim)
    gt_vec : numpy.ndarray has shape (n_voxels, dim)
    mask : numpy.ndarray has shape (n_voxels, ), dtype='bool'

    Returns
    --------
    list of np.ndarray : [fe_train, gt_train, fe_test, gt_test]
        fe have shape (N, D, n_tensors), gt have shape (N, D)
    norm_coef : np.ndarray
        (n_tensors, )
        ほかのneuronのfeaureImgを正規化するのに必要
    """

    m_feature_vec = feature_vec[mask_vec]
    m_gt_vec = gt_vec[mask_vec]

    if not split_data:
        # 全部トレーニングに使う。過学習させてみる
        nm_feature_vec, norm_coef = normalize_ex2_feature_vec(m_feature_vec)
        nmT_feature_vec = nm_feature_vec.transpose(0, 2, 1)
        nm_gt_vec, gt_norm_coef = normalize_ex2_feature_vec(m_gt_vec)
        return [nmT_feature_vec, nm_gt_vec] * 2, norm_coef, gt_norm_coef

    if permu_index is None:
        pm_feature_vec, pm_gt_vec = permu_date_set(m_feature_vec, m_gt_vec)
    else:
        pm_feature_vec = m_feature_vec[permu_index]
        pm_gt_vec = m_gt_vec[permu_index]

    # トレーニングサンプルとテストデータの数決める
    N = pm_feature_vec.shape[0]
    n_train_samples = int(N*train_sample_ratio)

    # 分ける
    pm_f_train = pm_feature_vec[:n_train_samples]
    pm_f_test = pm_feature_vec[n_train_samples:]
    pm_gt_train = pm_gt_vec[:n_train_samples]
    pm_gt_test = pm_gt_vec[n_train_samples:]

    # 正規化
    pmn_f_train, norm_coef = normalize_ex2_feature_vec(pm_f_train)
    pmn_f_test = normalize_vec(pm_f_test, norm_coef)

    pmn_gt_train, gt_norm_coef = normalize_ex2_feature_vec(pm_gt_train)
    pmn_gt_test = normalize_vec(pm_gt_test, gt_norm_coef)

    pmnT_f_train = pmn_f_train.transpose(0, 2, 1)
    pmnT_f_test = pmn_f_test.transpose(0, 2, 1)

    data_set = [pmnT_f_train, pmn_gt_train, pmnT_f_test, pmn_gt_test]
    for data in data_set:
        print data.shape
    return data_set, norm_coef, gt_norm_coef


def vec2vol(pred_vec, vol_shape):
    """

    Parameters
    ----------
    pred_vec : numpy.ndarray
        (N, dim)
    vol_shape : tuple of int

    Returns
    -------
    pred_vol : numpy.ndarray
        (dim, X, Y, Z) for MATLAB

    """
    return pred_vec.reshape(vol_shape+(pred_vec.shape[1],)).transpose(3, 0, 1, 2)
