# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 17:34:48 2016

@author: matsuik

"""

from __future__ import division

import numpy as np
import scipy.io as sio
import theano
from sympy.physics.quantum.cg import CG
from sympy.physics.quantum.spin import Rotation
from sympy import pi

proj_path = '/home/matsui-k/projects/d-rcn/'


# theano utils
def floatX(array_):
    """
    Args
    ----
    array_: np.ndarray
    
    Returns
    -------
    np.ndarray
    """

    return np.asarray(array_, dtype=theano.config.floatX)


def zero_shared(shape):
    """
    Args
    ----
    shape : TensorVariable
        from TensorVariable.shape.eval()
    """
    return theano.shared(floatX(np.zeros(shape)))


def normalize_ex2_feature_vec(feature_vec_train):
    """
    Parameters
    ----------
    feature_vec : ndarray
        (n_pixeles, n_tensors, dim) or (n_pixels, dim)
        
    Returns
    -------
    normed : ndarray
        (n_pixeles, n_tensors, dim) or (n_pixels, dim)
    norm_coef : ndarray or float
        (n_tensors, )
    """

    dim_index = len(feature_vec_train.shape) - 1

    eps = 1e-8
    norm_coef = np.sqrt(np.mean(np.linalg.norm(feature_vec_train, axis=dim_index) ** 2, axis=0))

    if dim_index == 2:
        normed_train = feature_vec_train / (norm_coef[:, np.newaxis] + eps)
    if dim_index == 1:
        normed_train = feature_vec_train / (norm_coef + eps)
    return normed_train, norm_coef


def normalize_vec(vec, norm_coef):
    """
    Parameters
    ----------
    feature_vec : ndarray or float
        (n_pixeles, n_tensors, dim) or (n_pixels, dim)
        
    Returns
    -------
    normed : ndarray
        (n_pixeles, n_tensors, dim) or (n_pixels, dim)
    """
    if len(vec.shape) == 3:
        normed = vec / (norm_coef[:, np.newaxis] + 1e-8)
    if len(vec.shape) == 2:
        normed = vec / (norm_coef + 1e-8)
    return normed


def predict_processed_feature(f_output, feature_vec):
    """
    Args
    ----
    f_output : theano compiled fuction
        from np.ndarray (N, dim, n_tensors) np.float32 
        to one length list of np.ndarray (N, dim, 1) np.float32
    feature_vec : np.ndarray
        (N, dim, n_tensors) normed, transposed
        
    Returns
    -------
    np.ndarray : (N, dim)
    """

    return np.squeeze(f_output(floatX(feature_vec))[0])


def predict_original_feature(f_output, feature_vec, norm_coef, batch_size=1024):
    """
    Args
    ----
    f_output : theano compiled fuction
        from np.ndarray (N, dim, n_tensors) np.float32 
        to one length list of np.ndarray (N, dim, 1) np.float32
    feature_vec : np.ndarray
        (N, n_tensors, dim) from load_neuron()
    norm_coef : numpy.ndarray
        (n_tensors,)
    batch_size : int
        
    Returns
    -------
    np.ndarray : (N, dim)
    """

    processed = normalize_vec(feature_vec, norm_coef).transpose(0, 2, 1)
    ret_mat = floatX(np.zeros((processed.shape[0], processed.shape[1])))
    n_batch = processed.shape[0] // batch_size
    for i_batch in range(n_batch):
        index = i_batch * batch_size
        # f_output returns (N, dim, n_tensors=1)
        ret_mat[index: index + batch_size] = f_output(floatX(processed[index: index + batch_size]))[0][:, :, 0]
    if not n_batch * batch_size == processed.shape[0]:
        ret_mat[n_batch * batch_size:] = f_output(floatX(processed[n_batch * batch_size:]))[0][:, :, 0]
    return ret_mat


def predict_original_feature_multiorder(f_output, feature_vec_list, norm_coef_list, out_dim, batch_size=1024):
    """
    正規化をもとに戻して, 一気に計算するとメモリに乗らないので分けてoutputして、concatenate

    Args
    ----
    f_output : theano compiled fuction
        from a few np.ndarray-s shape=(N, dim, n_tensors) dtype=np.float32
        to one length list of np.ndarray shape=(N, dim, 1) dtype=np.float32
    feature_vec_list : list of np.ndarray
        (N, n_tensors, dim)
    norm_coef_list : list of numpy.ndarray
        (n_tensors,)
    out_dim : int
    batch_size : int

    Returns
    -------
    np.ndarray : (N, dim)
    """

    processed_list = []
    for feature_vec, norm_coef in zip(feature_vec_list, norm_coef_list):
        processed_list.append(normalize_vec(feature_vec, norm_coef).transpose(0, 2, 1))
    ret_mat = floatX(np.zeros((processed_list[0].shape[0], out_dim)))
    n_batch = processed_list[0].shape[0] // batch_size
    for i_batch in range(n_batch):
        index = i_batch * batch_size
        # f_output returns (N, dim, n_tensors=1)
        ret_mat[index: index + batch_size] = f_output(
            *[floatX(processed[index: index + batch_size]) for processed in processed_list])[0][:, :, 0]
    if not n_batch * batch_size == processed_list[0].shape[0]:
        ret_mat[n_batch * batch_size:] = \
        f_output(*[floatX(processed[n_batch * batch_size:]) for processed in processed_list])[0][:, :, 0]
    return ret_mat


def predict_original_feature_pc(f_output, feature_vec_list, norm_coef_list, n_dir, batch_size=1024):
    """
    正規化をもとに戻して, 一気に計算するとメモリに乗らないので分けてoutputして、concatenate

    Args
    ----
    f_output : theano compiled fuction
        from a few np.ndarray-s shape=(N, dim, n_tensors) dtype=np.float32
        to one length list of np.ndarray shape=(N, n_dir) dtype=np.float32
    feature_vec_list : list of np.ndarray
        (N, n_tensors, dim)
    norm_coef_list : list of numpy.ndarray
        (n_tensors,)
    n_dir : int
        HARDI信号の離散化方向の数
    batch_size : int

    Returns
    -------
    np.ndarray : (N, n_dir)
    """

    processed_list = []
    for feature_vec, norm_coef in zip(feature_vec_list, norm_coef_list):
        processed_list.append(normalize_vec(feature_vec, norm_coef).transpose(0, 2, 1))
    ret_mat = floatX(np.zeros((processed_list[0].shape[0], n_dir)))
    n_batch = processed_list[0].shape[0] // batch_size
    for i_batch in range(n_batch):
        index = i_batch * batch_size
        # f_output returns (N, n_dir)
        ret_mat[index: index + batch_size] = f_output(
            *[floatX(processed[index: index + batch_size]) for processed in processed_list])[0]
    if not n_batch * batch_size == processed_list[0].shape[0]:
        ret_mat[n_batch * batch_size:] = \
        f_output(*[floatX(processed[n_batch * batch_size:]) for processed in processed_list])[0]
    return ret_mat


def pred_act_original_feature_multiorder(f_output, feature_vec_list, norm_coef_list, out_dim, batch_size=1024):
    """
    正規化をもとに戻して, 一気に計算するとメモリに乗らないので分けてoutputして、concatenate

    Args
    ----
    f_output : theano compiled fuction
        from a few np.ndarray-s shape=(N, dim, n_tensors) dtype=np.float32
        to one length list of np.ndarray shape=(N, dim, 1) dtype=np.float32
    feature_vec_list : list of np.ndarray
        (N, n_tensors, dim)
    norm_coef_list : list of numpy.ndarray
        (n_tensors,)
    out_dim : int
    batch_size : int

    Returns
    -------
    np.ndarray : (N, dim)
    """

    processed_list = []
    for feature_vec, norm_coef in zip(feature_vec_list, norm_coef_list):
        processed_list.append(normalize_vec(feature_vec, norm_coef).transpose(0, 2, 1))
    ret_mat = floatX(np.zeros((processed_list[0].shape[0], out_dim)))
    n_batch = processed_list[0].shape[0] // batch_size
    norm_list = []
    for i_batch in range(n_batch):
        index = i_batch * batch_size
        # f_output returns (N, dim, n_tensors=1)
        output = f_output(*[floatX(processed[index: index + batch_size]) for processed in processed_list])
        ret_mat[index: index + batch_size] = output[0][:, :, 0]
        norm_list.append(output[1:])
    if not n_batch * batch_size == processed_list[0].shape[0]:
        output = f_output(*[floatX(processed[n_batch * batch_size:]) for processed in processed_list])
        ret_mat[n_batch * batch_size:] = output[0][:, :, 0]
        norm_list.append(output[1:])

    concat_norm_list = []
    for i in range(len(norm_list[0])):
        concat_norm_list.append(np.concatenate([onebatch[i] for onebatch in norm_list], axis=0))
    return ret_mat, concat_norm_list


def permu_date_set(feature_vec, gt_vec):
    """
    Args
    -----
    feature_vec : ndarray
        (N, ,,,)
    gt_vec : ndarray
        (N, ,,,)
    
    Reterns
    --------
    p_feature_vec, p_gt_vec

    Returns
    -------


    """
    assert feature_vec.shape[0] == gt_vec.shape[0]
    index = np.random.permutation(range(feature_vec.shape[0]))
    p_feature_vec = feature_vec[index]
    p_gt_vec = gt_vec[index]
    return p_feature_vec, p_gt_vec


def vec2comp(x_float):
    """

    Parameters
    ----------
    x_float: numpy.ndarray
        (N, dim, n_tensors)

    Returns
    -------
        (N, dim/2, n_tensors), dtype=complex

    """
    return x_float[:, ::2] + 1j * x_float[:, 1::2]


def comp2vec(x_comp):
    """

    Parameters
    ----------
    x: numpy.ndarray
        (N, dim/2, n_tensors) dtype=complex

    Returns
    -------
    x: numpy.ndarray
        (N, dim, n_tensors) dtype=float

    """
    x_float = floatX(np.zeros((x_comp.shape[0], 2 * x_comp.shape[1], x_comp.shape[2])))
    x_float[:, ::2] = np.real(x_comp)
    x_float[:, 1::2] = np.imag(x_comp)
    return x_float


# for spherical tensors
def make_index(L):
    """

    Parameters
    ----------
    L: int

    Returns
    -------
    list of tuple of two int

    >>make_index(4)
    [(0, 1), (1, 6), (6, 15)]

    """
    index_list = []
    start = 0
    for step in [2 * l + 1 for l in range(0, L + 1, 2)]:
        index_list.append((start, start + step))
        start += step
    return index_list


def load_Mreconstruct(max_L):
    """
    Parameters
    ----------
    max_L : int

    Returns
    -------
    pad : numpy.ndarray
        (n_dir, sum(2*(2*L_array+1)))
        [[R, -I, R, -I, ...],
         [R, -I, R, -I, ...],,,

    """
    ms = sio.loadmat('/home/matsui-k/projects/d-rcn/data/blind/Mcontra_fodfL=12.mat')
    M = ms['Mcontra_fodf']
    M_t = M[:make_index(max_L)[-1][1]].T
    print 'M_t.shape', M_t.shape
    pad = np.zeros((M_t.shape[0], M_t.shape[1] * 2))
    pad[:, ::2] = np.real(M_t)
    pad[:, 1::2] = -np.imag(M_t)
    print 'pad.shape', pad.shape

    return floatX(pad)


def clebsch_Gordan_coef(j, m, j1, m1, j2, m2):
    """
    see https://en.wikipedia.org/wiki/Table_of_Clebsch%E2%80%93Gordan_coefficients
    """
    assert -j <= m <= j
    assert -j1 <= m1 <= j1
    assert -j2 <= m2 <= j2
    assert abs(j1 - j2) <= j <= j1 + j2

    # CG returns 0 against the invalid args.
    return float(CG(j1, m1, j2, m2, j, m).doit())


def cg_table(j, j1, j2):
    """

    Parameters
    ----------
    j
    j1
    j2

    Returns
    -------
    table : numpy.ndarray
        shape=(2*J+1, 2*j1+1, 2*j2+1)

    """
    table = np.zeros((2 * j + 1, 2 * j1 + 1, 2 * j2 + 1))
    for m in range(-j, j + 1):
        for m1 in range(-j1, j1 + 1):
            for m2 in range(-j2, j2 + 1):
                table[m, m1, m2] = clebsch_Gordan_coef(j, m, j1, m1, j2, m2)
    return floatX(table)


def wignerD(j, alpha=pi / 2., beta=pi / 2., gamma=pi / 2.):
    """

    Parameters
    ----------
    j
    alpha
    beta
    gamma

    Returns
    -------
    wigner D matrix

    """
    A = np.zeros((2 * j + 1, 2 * j + 1), dtype=complex)
    for m in range(-j, j + 1):
        for mp in range(-j, j + 1):
            A[m + j, mp + j] = Rotation.D(j, m, mp, alpha, beta, gamma).doit()
    return A


def rotate_tensor(x_float, D):
    """
    Parameters
    ----------
    x_float : numpy.ndarray
        (N, dim, n_tensors)
    D : wignerD matrix

    Returns
    -------

    """
    x_comp = vec2comp(x_float)
    return floatX(comp2vec(np.tensordot(np.conj(D), x_comp, axes=(1, 1)).transpose(1, 0, 2)))
