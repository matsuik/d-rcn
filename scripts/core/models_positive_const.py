# -*- coding: utf-8 -*-
from __future__ import division


import theano
import theano.tensor as T

from theano_utils import floatX
import blocks, TN_utils




class Model(object):
    def __init__(self, j_input_list=[], j_output_list=[], n_input_tensors_list=[], n_hidden=2, func_key_list=[],
                 error_func_key='l2', reg_lambda=0., reg_func_key='l2', pc_funk_key='l-relu'):
        self.j_input_list = j_input_list
        self.j_output_list = j_output_list

        self.error_func = get_metric(error_func_key)
        self.reg_func = get_metric(reg_func_key)
        self.pc_func_key = pc_funk_key
        self.reg_lambda = floatX(reg_lambda)
        self.n_input_tensors_list = n_input_tensors_list
        self.n_hidden = n_hidden
        self.func_key_list = func_key_list

        self.tensors_list = [T.tensor3() for _ in j_input_list]
        self.t_mat = T.matrix("t")  # (batchsize, n_dir)
        self.param_list = []
        self.M = TN_utils.load_Mreconstruct(max_L=self.j_output_list[-1]) #(n_dir, dim_concat), L=0もはいってる

    def make_graph_head(self):
        self.out_list = []

    def concatenate(self):
        out_concat = T.concatenate(self.out_list, axis=1)[:, :, 0] # (N, dim_concat, 1) to (N, dim_concat)
        pred_reconst = T.dot(out_concat, self.M.T) # (N, n_dirs)
        pred = blocks.get_act_func(self.pc_func_key)(pred_reconst)

        error = self.error_func(pred, self.t_mat)
        obj = error + self.reg_lambda * self.reg_func(pred)

        self.obj = obj
        self.train_error = error
        self.train_out = pred

    def make_graph_train(self):
        self.make_graph_head()
        self.concatenate()

    def make_graph_test(self):
        pass


class ScalarProducts(Model):
    def __init__(self, j_input_list, j_output_list,
                 error_func_key='l2', reg_lambda=0., reg_func_key='l2', pc_funk_key='l-relu'):
        l = locals()
        del l['self']
        super(ScalarProducts, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for i, tensor in enumerate(self.tensors_list):
            w = TN_utils.zero_shared(1)
            self.param_list.append(w)

            h = w * tensor
            out_list.append(h)
        self.out_list = out_list


class OneLayer(object):
    def __init__(self, j_input_list, j_output_list, n_input_tensors_list, func_key_list):
        assert len(j_input_list) == len(n_input_tensors_list)
        assert len(func_key_list) == 1
        self.j_input_list = j_input_list
        self.j_output_list = j_output_list
        self.n_input_tensors_list = n_input_tensors_list
        self.func_key_list = func_key_list

        self.tensors_list = [T.tensor3() for _ in j_input_list]
        self.t_mat = T.matrix("t")  # (batchsize, n_dir)

        self.param_list = []

        self.M = TN_utils.load_Mreconstruct(max_L=self.j_output_list[-1]) #(n_dir, dim_concat), L=0もはいってる

        self.print_list = []

    def make_graph_train(self):
        out_list = []
        for j_output in self.j_output_list:
            weight = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                             1, name='W{}'.format(j_output))
            self.param_list.append(weight)

            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], weight)
            out_list.append(h1sv)


        out_concat = T.concatenate(out_list, axis=1)[:, :, 0] # (N, dim_concat, 1) to (N, dim_concat)
        pred_reconst = T.dot(out_concat, self.M.T) # (N, n_dirs)
        pred = blocks.get_act_func('l-relu')(pred_reconst)

        mse = T.mean((pred - self.t_mat)**2)

        self.obj = mse
        self.train_error = mse
        self.train_out = pred

    def make_graph_test(self):
        pass


class NonlinearOneLayer(object):
    def __init__(self, j_input_list, j_output_list, n_input_tensors_list, func_key_list):
        assert len(j_input_list) == len(n_input_tensors_list)
        assert len(func_key_list) == 1
        self.j_input_list = j_input_list
        self.j_output_list = j_output_list
        self.n_input_tensors_list = n_input_tensors_list
        self.func_key_list = func_key_list

        self.tensors_list = [T.tensor3() for _ in j_input_list]
        self.t_mat = T.matrix("t")  # (batchsize, n_dir)

        self.param_list = []

        self.M = TN_utils.load_Mreconstruct(max_L=self.j_output_list[-1]) #(n_dir, dim_concat), L=0もはいってる

        self.print_list = []

    def make_graph_train(self):
        out_list = []
        for j_output in self.j_output_list:
            weight = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                             1, name='W{}'.format(j_output))
            self.param_list.append(weight)

            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], weight)

            bias = blocks.make_bias(1, name='bias{}'.format(j_output))
            self.param_list.append(bias)

            h1na = blocks.activate_norm(h1sv, bias, func_key=self.func_key_list[0])[0]

            w_scalar = TN_utils.zero_shared(1)
            self.param_list.append(w_scalar)

            out_list.append(h1na * w_scalar)

        out_concat = T.concatenate(out_list, axis=1)[:, :, 0] # (N, dim_concat, 1) to (N, dim_concat)
        pred_reconst = T.dot(out_concat, self.M.T) # (N, n_dirs)
        pred = blocks.get_act_func('l-relu')(pred_reconst)

        mse = T.mean((pred - self.t_mat)**2)

        self.obj = mse
        self.train_error = mse
        self.train_out = pred

    def make_graph_test(self):
        pass


class TwoLayerAfterTC(Model):
    def __init__(self, j_input_list, j_output_list, n_input_tensors_list, n_hidden, func_key_list,
                 reg_lambda=0., reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu'):
        assert len(j_input_list) == len(n_input_tensors_list)
        assert len(func_key_list) == 2
        l = locals()
        del l['self']
        super(TwoLayerAfterTC, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                             self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            b = blocks.make_bias(self.n_hidden, self, name='bias1{}'.format(j_output))
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]

            w = blocks.make_weight(self.n_hidden, 1, self)
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[1])[0]

            w_scalar = TN_utils.zero_shared(1)
            self.param_list.append(w_scalar)

            out_list.append(h * w_scalar)
        self.out_list = out_list


# obsolete
class Rcn2layer_pc(object):
    def __init__(self, j_input_list, j_output_list, n_input_tensors_list, n_hidden_tensors, func_key_list):
        assert len(j_input_list) == len(n_input_tensors_list)
        self.j_input_list = j_input_list
        self.j_output_list = j_output_list

        self.tensors_list = [T.tensor3() for _ in j_input_list]
        self.t_mat = T.matrix("t")  # (batchsize, n_dir)

        self.n_input_tensors_list = n_input_tensors_list
        self.n_hidden_tensors = n_hidden_tensors

        self.func_key_list = func_key_list
        self.param_list = []
        self.param_dict_list = []
        self.M = TN_utils.load_Mreconstruct(max_L=self.j_output_list[-1]) #(n_dir, dim_concat), L=0もはいってる

        self.print_list = []

    def make_graph_train(self):
        out2_list = []
        for j_output in self.j_output_list:
            param_dict = {}
            out1, self.tc_weight_list, self.tc_bias_list, n_couples =\
                blocks.multi_order_mix(self.tensors_list, self.j_input_list, j_output,
                                       self.n_input_tensors_list, self.n_hidden_tensors, is_train=True,
                                       func_key=self.func_key_list[0])
            param_dict['tc_weight_list'] = self.tc_weight_list
            param_dict['tc_bias_list'] = self.tc_bias_list
            self.param_list.extend(self.tc_weight_list)
            self.param_list.extend(self.tc_bias_list)

            if j_output in self.j_input_list:
                self.W1 = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                             self.n_hidden_tensors, name='W1')
                self.bias1 = blocks.make_bias(self.n_hidden_tensors, name='bias1')
                param_dict['W1'] = self.W1
                param_dict['bias1'] = self.bias1
                self.param_list.append(self.W1)
                self.param_list.append(self.bias1)

                h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)],
                                                self.W1)
                h1na = blocks.activate_norm(h1sv, self.bias1, func_key=self.func_key_list[0])[0]
                out1 = T.concatenate([out1, h1na], axis=2)

                n_couples += 1

            self.W2 = blocks.make_weight(n_couples*self.n_hidden_tensors, 1, name='W2')
            self.bias2 = blocks.make_bias(1, name='bias2')
            param_dict['W2'] = self.W2
            param_dict['bias2'] = self.bias2
            self.param_list.append(self.W2)
            self.param_list.append(self.bias2)

            self.param_dict_list.append(param_dict)

            h2 = blocks.superpose_vectors(out1, self.W2)
            # out2.shape = (N, dim, n_tensors=1)
            out2, acted_norm2= blocks.activate_norm(h2, self.bias2, self.func_key_list[1])
            out2_list.append(out2)


        #self.print_list.append(theano.printing.Print('out2_list[-1].shape')(out2_list[-1].shape))
        #self.print_list.append(theano.printing.Print('concatenated_out2_list.shape')(T.concatenate(out2_list, axis=1).shape))
        out2_concat = T.concatenate(out2_list, axis=1)[:, :, 0] # (N, dim_concat, 1) to (N, dim_concat)
        #self.print_list.append(theano.printing.Print('out2_concat.shape')(out2_concat.shape))



        pred_reconst = T.dot(out2_concat, self.M.T) # (N, n_dirs)
        pred = blocks.get_act_func('l-relu')(pred_reconst)

        mse = T.mean((pred - self.t_mat)**2)


        self.obj = mse
        self.train_error = mse
        self.train_out = pred


    def make_graph_test(self):
        out2_list = []
        for j_output, param_dict in zip(self.j_output_list, self.param_dict_list):
            out1 = blocks.multi_order_mix(self.tensors_list, self.j_input_list, j_output,
                                       self.n_input_tensors_list, self.n_hidden_tensors, is_train=False,
                                       func_key=self.func_key_list[0],
                                        weight_list=param_dict['tc_weight_list'], bias_list=param_dict['tc_bias_list'])

            if j_output in self.j_input_list:
                h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)],
                                                param_dict['W1'])
                h1na = blocks.activate_norm(h1sv, param_dict['bias1'], func_key=self.func_key_list[0])[0]
                out1 = T.concatenate([out1, h1na], axis=2)

            h2 = blocks.superpose_vectors(out1, param_dict['W2'])
            # out2.shape = (N, dim, n_tensors=1)
            out2, acted_norm2 = blocks.activate_norm(h2, param_dict['bias2'], self.func_key_list[1])
            out2_list.append(out2)

        out2_concat = T.concatenate(out2_list, axis=1)[:, :, 0] # (N, dim_concat, 1) squeezed to (N, dim_concat)

        pred_reconst = T.dot(out2_concat, self.M) # (N, n_dirs)
        pred = blocks.get_act_func('l-relu')(pred_reconst)

        mse = T.mean((pred - self.t_mat)**2)

        self.test_error = mse
        self.test_out = pred


def share_data(data):
    return theano.shared(floatX(data), borrow=True)


def compile(train_feature_vec_list, train_gt_vec, test_feature_vec_list, test_gt_vec, model, optimizer):
    """
    データをsharedにして、modelとoptimizerを使ってcomputational graphを作って、
    コンパイルする。

    Parameters
    -----------
    train_feture_vec_list : list of numpy.ndarray
        list of (N, dim, n_tensors)
    train_gt_vec : numpy.ndarray
        (N, n_dir)
    test_feature_vec_list, test_gt_vec
    model : models.Rcn1layerとか
    optimizer : optimizers.SGDとか

    """

    s_train_feature_vec_list = [share_data(data) for data in train_feature_vec_list]
    s_train_gt_vec = share_data(train_gt_vec)
    s_test_feature_vec_list = [share_data(data) for data in test_feature_vec_list]
    s_test_gt_vec = share_data(test_gt_vec)

    model.make_graph_train()

    updates = optimizer.make_updates(loss=model.obj, param_list=model.param_list)

    i_batch = T.iscalar("i_batch")
    index_list = T.ivector("index_list")
    batch_size = T.iscalar("batch_size")
    train_givens = [(tensor, s_train_feature_vec_list[i][index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])
                for i, tensor in enumerate(model.tensors_list)] +\
                   [(model.t_mat, s_train_gt_vec[index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])]
    test_givens = [(tensor, s_test_feature_vec_list[i][index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])
                for i, tensor in enumerate(model.tensors_list)] +\
                  [(model.t_mat, s_test_gt_vec[index_list[i_batch*batch_size: i_batch*batch_size + batch_size]])]

    f_train = theano.function(
            inputs=[i_batch, index_list, batch_size],
            updates=updates,
            givens=train_givens,
            on_unused_input='warn')

    f_training_error = theano.function(
        inputs=[i_batch, index_list, batch_size],
        outputs=[model.train_error],
        givens=train_givens,
        on_unused_input='warn')

    # modelは一緒なのでgivesだけ変えてる
    f_test_error = theano.function(
        inputs=[i_batch, index_list, batch_size],
        outputs=[model.train_error],
        givens=test_givens
        )

    # modelは一緒なのでtrainのoutつかってる
    # test_out.shape (N, n_dir)
    f_output = theano.function(
        inputs=model.tensors_list,
        outputs=[model.train_out]
    )

    result = [f_train, f_training_error, f_test_error, f_output, model.param_list]
    return result


