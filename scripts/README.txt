test_toytree.ipynbを見ればだいたいわかる

blocks.py
    models.pyにsuperpose_vectorsやnorm_activationなどの低レベルの関数を提供

models.py
    feedforward graphをつくる。
    モデルの構成(層数、ユニット数, 活性化関数),
    目的関数の定義(誤差関数、l2 regularizatoinなどのペナルティ)などを定義する。

    パラメータの初期値も決定する。

    Modelの子クラスのインスタンスはcompiling.compile_graphに渡される。

optimizers.py
    Optimizersの子クラスはcompilingに渡される
    目的関数と、パラメータをもらって、updateの規則をつくる
    learning_rateはdecayするためにtheano.functionに渡せるように
    TensorVariableにしておく。

compiling.py
    データをsharedにして、modelとoptimizerを使ってcomputational graphを作って、
    コンパイルする。

theano_utils.py
    theano.config.floatXとかsharedをつくるのとか

TN_utils.py
    データのロードとか前処理

rcn2layer_toytree.py
    mauiのGPUで動くかの検証用 動いた。

ex_test_tensordot.ipynb
    blocks.pyなどのテスト
    
test_theano_utils.ipynb
    theano_utilsのテスト
    
HARDIdata.ipynb
    exvivoのspherical harmonics expansion coefficient のdataの説明


conv_blocks.py
conv_models.py
test_conv.py
    T.tensordotをT.nnet.conv2dで置き換えてる
    CPUでは微妙な性能, GPUでやるべき
