import os
from datetime import datetime
input_L = 4
output_L_list = [2, 4]
node_gpu_list = [(3, 1), (4, 1), (5, 0), (5, 1)]
timestump = str(datetime.now()).split('.')[0].replace(' ', '')
save_dir = '/home/matsui-k/projects/d-rcn/results/blind/'+'in'+str(input_L)+'out'+str(output_L_list[-1])+timestump
os.makedirs(save_dir)


for output_L, node_gpu in zip(output_L_list, node_gpu_list):
    cmd = "ssh kng0{} 'python ~/projects/d-rcn/scripts/experience/blind/worker.py {} {} {} {}'"\
          .format(node_gpu[0], input_L, output_L, node_gpu[1], save_dir)
os.system(cmd)