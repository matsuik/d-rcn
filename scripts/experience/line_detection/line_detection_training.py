import sys
proj_path = '/home/matsui-k/projects/d-rcn/'
sys.path.append(proj_path)

import numpy as np
from PIL import Image
from scripts.core import TN_utils, models, compiling, optimizers

img = np.array(Image.open(proj_path+'data/line_detection/neuron01.png'))
img = np.asarray(img.T/255., dtype=np.float32)
fe_vec, gt_vec, mask_vec, im_shape = TN_utils.load_neuron(1)
data_set, norm_coef, gt_norm_coef = TN_utils.preprocess(fe_vec*(img.flatten()[:, np.newaxis, np.newaxis]), gt_vec, mask_vec,
                                                        split_data=False)



pred_train = TN_utils.predict_processed_feature(f_output, data[0])
train_errors = TN_utils.mse_prec_reca(pred_train, data[1])

import scipy.io as sio
for i_neuron in range(3):
    pred_all = TN_utils.predict_original_feature(
        f_output, TN_utils.load_neuron(i_neuron+1, only_feature=True), norm_coef)
    gt1_img = TN_utils.gt2vec_to_gt1img(pred_all, img_shape=(512, 512))
    sio.savemat(proj_path + 
        "results/line_detection/neuron{}_gt1_train.mat".format(str(i_neuron+1)),
        {"gt1_train": gt1_img})
