# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 13:14:45 2016

@author: matsuik
"""

from __future__ import division
import sys
import time
import gzip
import cPickle
import os

import numpy as np

import TN_utils
import compiling
import models
import optimizers


with open("../data/1506_inv_hardi_b2.npz", "r") as f:
    feature = np.load(f)['arr_0']
with open("../data/1506_ex_hardi_a2.npz", "r") as f:
    gt = np.load(f)['arr_0']
with open("../data/1507_inv_hardi_b2.npz", "r") as f:
    feature_test = np.load(f)['arr_0']
with open("../data/1507_ex_hardi_a2.npz", "r") as f:
    gt_test = np.load(f)['arr_0']
    
feature, gt = TN_utils.permu_date_set(feature, gt)
feature_test, gt_test = TN_utils.permu_date_set(feature_test, gt_test)

feature_train = feature[:50000]
gt_train = gt[:50000]
feature_valid = feature[50000:60000]
gt_valid = gt[50000:60000]
feature_test = feature_test[:10000]
gt_test = gt_test[:10000]

gt_norm_coef = np.max(np.linalg.norm(gt_train, axis=1))
gt_train = gt_train / gt_norm_coef
gt_valid = gt_valid / gt_norm_coef
gt_test = gt_test / gt_norm_coef

feature_train, norm_coef = TN_utils.normalize_max_feature_vec(feature_train)
feature_valid = TN_utils.normalize_vec(feature_valid, norm_coef)
feature_test = TN_utils.normalize_vec(feature_test, norm_coef)

# (batch_size, n_tensors, D) (batch_size, D, n_tensors)
feature_train = feature_train.transpose(0, 2, 1)
feature_valid = feature_valid.transpose(0, 2, 1)
feature_test = feature_test.transpose(0, 2, 1)

n_hidden = 100
batch_size = 10500
act_key = ['relu', 'relu']
l2_reg = 1.
drop_list = [1., 1.]
gamma_scale = 10**-3
bias_scale = 10**np.random.uniform(-2, -0.5)
alpha = 10**np.random.uniform(-4, -1)

dire = '../results/hiddenbn_Apr4/'
if not os.path.exists(dire):
    os.mkdir(dire)

with gzip.open(dire + 'params{}.pkl.gz'.format(str(sys.argv[1])), "w") as f:
    cPickle.dump([bias_scale, alpha] , f)

rcn_2layer = models.Rcn2layer_hidden_bn(n_tensors_list=[feature_train.shape[2], n_hidden], 
                            func_key_list=act_key,
                             l2_reg=l2_reg,
                            drop_list=drop_list,
                            gamma_scale=gamma_scale,
                            bias_scale=bias_scale)
                            
result = compiling.compile_bn(
    feature_vec=feature_train, gt_vec=gt_train,
    test_feature_vec=feature_test, test_gt_vec=gt_test,
    model=rcn_2layer,
    make_updates=optimizers.adam)
f_train, f_training_error, f_test_error, f_output, s_input, s_target, s_test_input, s_test_target, param_list= result 


N = s_input.get_value(borrow=True).shape[0]
batch_size = batch_size
n_batchs = N // batch_size - 1
index_list = range(N)
index_list = np.asarray(np.random.permutation(index_list), dtype=np.int32)

N_test = 10000
test_index_list = np.asarray(range(N_test), dtype=np.int32)

n_epochs = 51
interval = 10
training_error_array = np.zeros((n_epochs // interval + 2,))
test_error_array = np.zeros((n_epochs // interval + 2,))

alpha = 0.001
beta1 = 0.9
beta2 = 0.999
eps = 10e-8
t = 0.

beta = 0.01

start = time.time()

i_batch = 0
training_error = f_training_error(i_batch, index_list, batch_size, beta, t)[0]
training_error_array[0] = training_error
test_error = f_test_error(i_batch, test_index_list, batch_size)[0]
test_error_array[0] = test_error

try:
    for i_epoch in xrange(n_epochs):
        index_list = np.random.permutation(index_list)
        
        for i_batch in xrange(n_batchs):
            t = t + 1.
            f_train(i_batch, index_list, batch_size, alpha, beta1, beta2, eps, t, beta, t)
            
        if i_epoch % interval == 0:
            training_error = f_training_error(i_batch, index_list, batch_size, beta, t)[0]
            training_error_array[i_epoch // interval + 1] = training_error
            test_error = f_test_error(0, test_index_list, N_test)[0]
            test_error_array[i_epoch // interval + 1] = test_error
        
            alpha = alpha ** 0.95
            n_batchs = N // batch_size - 1
	    
        
finally:       
    spending = time.time() - start
    pred_train = np.squeeze(f_output(feature_train)[0])
    pred_test = np.squeeze(f_output(feature_test)[0])
    
    precision_mask_train = np.linalg.norm(pred_train, axis=1).nonzero()[0]
    precision_mask_test = np.linalg.norm(pred_test, axis=1).nonzero()[0]
    
    recall_mask_train = np.linalg.norm(gt_train, axis=1).nonzero()[0]
    recall_mask_test = np.linalg.norm(gt_test, axis=1).nonzero()[0]
    
    errors_N_train = (pred_train - gt_train)**2
    errors_N_test = (pred_test - gt_test)**2
    
    mse_train = np.mean(errors_N_train)
    mse_test = np.mean(errors_N_test)
    
    precision_train = np.mean(errors_N_train[precision_mask_train])
    precision_test = np.mean(errors_N_test[precision_mask_test])
    
    recall_train = np.mean(errors_N_train[recall_mask_train])
    recall_test = np.mean(errors_N_test[recall_mask_test])
    with gzip.open(dire + 'errors{}.pkl.gz'.format(str(sys.argv[1])), "w") as f:
        cPickle.dump([training_error_array, test_error_array, spending,
                      mse_train, mse_test, precision_train, precision_test,
                      recall_train, recall_test], f)
