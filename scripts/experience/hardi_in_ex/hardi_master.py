# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 11:40:10 2016

@author: matsuik
"""

import os
import sys
import itertools
_list=["maui25", "maui26", "maui27", "maui28"]
nodes=itertools.cycle(_list)

filename = sys.argv[1]
n_samples = int(sys.argv[2])

for i in range(n_samples):
    # cudaのpathをあたえてあげる, fullpathが必要, "と'は交互になってればOK
    # gpus=1だとまちっぱなしになる、maui25-28のプロセッサ数が12なので, ppn=６にしとけばmemory allocationも大丈夫そう
    # ppn=1だとgpuのこと考えずに仕事振りまくってsharedを作るときに爆る
    cmd = "echo 'cd ~/projects/d-rcn/scripts/; source ~/.bashrc; \
    python " + filename + " {}' | qsub -l nodes={}:ppn=4".format(i, nodes.next())
    os.system(cmd)
