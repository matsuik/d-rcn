# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 12:24:53 2016

@author: matsuik
"""
from __future__ import division
import time
import os
import sys
sys.path.append('/home/matsui-k/projects/d-rcn')
os.environ['THEANO_FLAGS'] = 'device=gpu1'

import numpy as np
from scripts.core import TN_utils, models, compiling, optimizers

fe_vec, gt_vec, mask_vec, vol_shape = TN_utils.load_hardi(1)
data_set, norm_coef, gt_norm_coef = TN_utils.preprocess(fe_vec, gt_vec, mask_vec, split_data=False)

rcn_2layer = models.Rcn2layer_hidden_bn(
    n_tensors_list=[fe_vec.shape[1], 100],
    func_key_list=["relu", "relu"],
    l2_reg=0.,
    gamma_scale=10**-2,
    drop_list=[1., 1.],
    bias_scale=1.0)
                            
result = compiling.compile_bn(
    data_set=data_set,
    model=rcn_2layer,
    make_updates=optimizers.adam)
f_train, f_training_error, f_test_error, f_output, s_input, s_target, s_test_input, s_test_target, param_list= result

batch_size = 10500
N = s_input.get_value(borrow=True).shape[0]
batch_size = batch_size
n_batchs = N // batch_size
index_list = range(N)
index_list = np.asarray(np.random.permutation(index_list), dtype=np.int32)

N_test = data_set[2].shape[0]
test_index_list = np.asarray(range(N_test), dtype=np.int32)

n_epochs = 101
interval = 10
training_error_array = np.zeros((n_epochs // interval + 2,))
test_error_array = np.zeros((n_epochs // interval + 2,))

alpha = 0.001
beta1 = 0.9
beta2 = 0.999
eps = 10e-8
t = 0.

beta = 0.01

start = time.time()

i_batch = 0
training_error = f_training_error(i_batch, index_list, batch_size, beta, t)[0]
training_error_array[0] = training_error
test_error = f_test_error(i_batch, test_index_list, batch_size)[0]
test_error_array[0] = test_error

try:
    for i_epoch in xrange(n_epochs):
        index_list = np.random.permutation(index_list)
        test_index_list = np.random.permutation(test_index_list)
        
        for i_batch in xrange(n_batchs):
            t = t + 1.
            f_train(i_batch, index_list, batch_size, alpha, beta1, beta2, eps, t, beta, t)
            
        if i_epoch % interval == 0:
            training_error = f_training_error(i_batch, index_list, batch_size, beta, t)[0]
            training_error_array[i_epoch // interval + 1] = training_error
            test_error = f_test_error(0, test_index_list, 10000)[0]
            test_error_array[i_epoch // interval + 1] = test_error
        
            alpha **= 0.95
	    print training_error, test_error
finally:
    pass

pred_train = np.squeeze(f_output(data_set[0])[0])
pred_test = np.squeeze(f_output(data_set[2])[0])
