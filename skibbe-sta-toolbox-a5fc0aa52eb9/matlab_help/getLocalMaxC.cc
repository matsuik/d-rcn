#include "mex.h"

#include <unistd.h>
#include <complex>
#include <map>
#include <gsl/gsl_blas.h>
#include "tred33.h"


#define _SUPPORT_MATLAB_ 
#include "sta_mex_helpfunc.h"
#include "sta_omp_threads.h"

/*
 mex getLocalMaxC.cpp 
*/




using namespace std;

template<typename T>
void blas_matrix_mult(
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
  
}

using namespace std;

template<>
void blas_matrix_mult<double>(
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
    double one[2] = {1,0};
    double zero[2] = {0,0};
    cblas_zgemm (CblasColMajor,
		 CblasTrans,
		 CblasNoTrans,
		 ndir,
		 lp_n,
		 stride,
		 (void*) one,
		 mout,
		 stride,
		 pin,
		 stride,
		 (void*) zero,
		 tmp,
		 ndir);           
}

template<>
void blas_matrix_mult<float>(
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
    float one[2] = {1,0};
    float zero[2] = {0,0};
    cblas_cgemm (CblasColMajor,
		 CblasTrans,
		 CblasNoTrans,
		 ndir,
		 lp_n,
		 stride,
		 (void*) one,
		 mout,
		 stride,
		 pin,
		 stride,
		 (void*) zero,
		 tmp,
		 ndir);           
}


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    
    
    const mxArray *ST1;
    ST1 = prhs[0];       
    const mwSize *dimsST1 = mxGetDimensions(ST1);
    const mwSize numdimST1 = mxGetNumberOfDimensions(ST1);
    if (numdimST1!=5)  mexErrMsgTxt("error: Array dimension for the first argument must be 5 (2x2L+1xXxYxZ)\n"); 
    std::complex<T> *pin = (std::complex<T> *) (mxGetData(ST1));
  
    const mxArray *BDir = prhs[1];     
    T *bdir = (T *) (mxGetData(BDir));
    long int ndir = mxGetN(BDir);
    
    
    const mxArray *Neighbors= prhs[2];     
    double *neighs = (double *) (mxGetData(Neighbors));
    int numneighs = mxGetM(Neighbors);
    int* Nidx = (int*) malloc(sizeof(int)*numneighs*ndir);
    for (int k = 0; k < numneighs*ndir;k++)
        Nidx[k] = (int) (neighs[k] -1);     
    
    if (ndir != mxGetN(Neighbors))
    {
        mexPrintf("dimension mismatch 1\n");
	free(Nidx);
        return;
    }
    
    const mxArray *Mout = prhs[3];     
    T *mout = (T *) (mxGetData(Mout));
    const mwSize *dimsMout = mxGetDimensions(Mout);
    if (ndir != dimsMout[2])
    {
        mexPrintf("dimension mismatch 2\n");
        return;
    }
   
   
    if (dimsST1[1] != dimsMout[1])
    {
        mexPrintf("dimension mismatch 3\n");
        return;
    }
    
    const mxArray *LocPS = prhs[4];     
    T *locPS = (T *) (mxGetData(LocPS));
    int interp = 1;
    if (locPS == 0)
        interp = 0;
         
    const mxArray *Params = prhs[5];     
    T *params = (T *) (mxGetData(Params));
    
    std::size_t nummax = (int) params[0];
    int sym = (int) params[1];

    
    std::size_t shape[3];    
    for(int i =2;i <numdimST1;i++)
    {
    	shape[4-i]=dimsST1[i];
    }
    
       
    mwSize outdim[4];
    outdim[0] = 3*nummax;
    outdim[1] = dimsST1[2];
    outdim[2] = dimsST1[3];
    outdim[3] = dimsST1[4];
    std::size_t num_elements=outdim[0] *outdim[1] *outdim[2] *outdim[3];
    
    plhs[0] = mxCreateNumericArray(4,outdim,mxGetClassID(ST1),mxREAL);
    T *result = (T *) mxGetData(plhs[0]);


    std::size_t sz = dimsST1[2]*dimsST1[3]*dimsST1[4];
 
    long int stride = dimsST1[1];
    
 
   

     long int lp_n = 1;
 


//     map<T,int> locmax;
//     T tmp[2*ndir];
//     T tmpV[9];
//     T tmpV2[6];
//     T *V[3];
//     V[0] = &tmpV[0];
//     V[1] = &tmpV[3];
//     V[2] = &tmpV[6];
//     T Gm[numneighs];
//     T dm[3];
//     T em[3];
//     int szpsmat = 6*(numneighs+1);
    
    printf("%d\n",hanalysis::get_numCPUs()); 
     
     
    #pragma omp parallel for num_threads(hanalysis::get_numCPUs())
    for (std::size_t k = 0; k < sz; k++)
    {
        map<T,int> locmax;
	T tmp[2*ndir];
	T tmpV[9];
	T tmpV2[6];
	T *V[3];
	V[0] = &tmpV[0];
	V[1] = &tmpV[3];
	V[2] = &tmpV[6];
	T Gm[numneighs];
	T dm[3];
	T em[3];
	int szpsmat = 6*(numneighs+1);
       
         
        //cblas_zgemm (CblasColMajor, CblasTrans, CblasNoTrans, ndir,lp_n,stride, (void*) one, mout, stride,(void*) &( pin[k*stride] ),stride, (void*) zero,tmp,ndir);           
	
	blas_matrix_mult<T>(
		      ndir,
		      lp_n,
		      stride,
		      mout,
		     (void*) &( pin[k*stride] ),
		      tmp);
	
	
	
      
        //T max = -9999.0;
        //int maxidx = -1;
               
        locmax.clear();
        
        int uplim = ndir;
        if (sym)
            uplim = uplim/2;
        
        for(int m = 0; m < uplim;m++)
        {
              bool lm = true;
              for (int j = 0; j < numneighs;j++)
              { 
                   if (tmp[2*Nidx[j+numneighs*m]] > tmp[2*m])
                   {
                       lm = false;
                       break;
                   }
              }
              if (lm)
              {
                  locmax[tmp[2*m]] = m;
              }            
        }
        
      
        std::size_t cnt = 0;
        
        for (typename map<T,int>::iterator it = locmax.end(); it != locmax.begin();)
        {           
            it--;
            if (cnt== nummax)
                break;
            int idx = it->second;
            T val = it->first;
            
            if (interp)
            {
                                   
                  for (int j = 0; j < numneighs;j++)
                  { 
                       Gm[j] = tmp[2*Nidx[j+numneighs*idx]];
                  }
                  Gm[numneighs] = tmp[2*idx];
                  for (int i = 0; i < 6;i++)
                  {
                      tmpV2[i] = 0;
                      for (int j = 0; j < numneighs+1;j++)                         
                          tmpV2[i] += locPS[6*j+i+idx*szpsmat]*Gm[j];
                  }
                  V[0][0] = tmpV2[0];
                  V[1][1] = tmpV2[1];
                  V[2][2] = tmpV2[2];
                  V[0][1] = tmpV2[3];
                  V[0][2] = tmpV2[4];
                  V[1][2] = tmpV2[5];
                  V[1][0] = tmpV2[3];
                  V[2][0] = tmpV2[4];
                  V[2][1] = tmpV2[5];

                  tred2<T>(V, dm, em);
                  tql2<T>(V, dm, em);
                  
                  int sg = ((V[0][2]*bdir[3*idx] +  V[1][2]*bdir[3*idx+1] + V[2][2]*bdir[3*idx+2])>0)?1:-1;

		  //sta_assert(nummax*3*k + 3*cnt +2<num_elements);
                  result[nummax*3*k + 3*cnt]    =  V[0][2]*dm[2]*sg;
                  result[nummax*3*k + 3*cnt +1] =  V[1][2]*dm[2]*sg;
                  result[nummax*3*k + 3*cnt +2] =  V[2][2]*dm[2]*sg;
            }
            else
            {     
	        //sta_assert(nummax*3*k + 3*cnt +2<num_elements);
                result[nummax*3*k + 3*cnt]    =  bdir[3*idx]*val;
                result[nummax*3*k + 3*cnt +1] =  bdir[3*idx+1]*val;
                result[nummax*3*k + 3*cnt +2] =  bdir[3*idx+2]*val;
            }
                cnt++;
        }
    
        
    }
    
    free(Nidx);
     
}

void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{    
  if (nrhs>4)
    {
    if (mxGetClassID(prhs[0])==mxDOUBLE_CLASS)
        _mexFunction<double> (nlhs, plhs,nrhs, prhs);
    else
        _mexFunction<float> (nlhs, plhs,nrhs, prhs);
    }
  else 
    mexErrMsgTxt("error: unsupported data type\n");
}

