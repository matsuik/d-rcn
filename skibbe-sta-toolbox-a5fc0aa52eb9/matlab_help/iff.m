


function result = iff(condition,trueResult,falseResult)
  error(nargchk(3,3,nargin));
  if condition
    result = trueResult;
  else
    result = falseResult;
  end




