function f = sta_solidangleMult(f1,f2,L)

L1 = f1.L;
L2 = f2.L;


isodd1 = false;
if strcmpi(f1.type,'STA_OFIELD_FULL'),
    start1 = 0;
    step1 = 1;
elseif strcmpi(f1.type,'STA_OFIELD_EVEN'),
    start1 = 0;
    step1 = 2;
elseif strcmpi(f1.type,'STA_OFIELD_ODD'),
    start1 = 1;
    step1 = 2;
    isodd1 = true;
elseif strcmpi(f1.type,'STA_OFIELD_SINGLE'),
    start1 = L1;
    step1 = 1;
    isodd1 = mod(L1,2)==1;
else,
    warning('not supported !!');
    return;
end;

isodd2 = false;
if strcmpi(f2.type,'STA_OFIELD_FULL'),
    start2 = 0;
    step2 = 1;
elseif strcmpi(f2.type,'STA_OFIELD_EVEN'),
    start2 = 0;
    step2 = 2;
elseif strcmpi(f2.type,'STA_OFIELD_ODD'),
    start2 = 1;
    step2 = 2;
    isodd2 = true;
elseif strcmpi(f2.type,'STA_OFIELD_SINGLE'),
    start2 = L2;
    step2 = 1;
    isodd2 = mod(L2,2)==1;
else,
    warning('not supported !!');
    return;
end;

if strcmpi(f1.type,'STA_OFIELD_FULL') | strcmpi(f2.type,'STA_OFIELD_FULL'), 
    outstep = 1;
    outtype = 'STA_OFIELD_FULL';
else
    outstep = 2;
    outtype = 'STA_OFIELD_EVEN';
end;
outstart = 0;

if (strcmpi(f1.type,'STA_OFIELD_EVEN') & isodd2 ) | ...
   (strcmpi(f2.type,'STA_OFIELD_EVEN') & isodd1 ),   
    outstart = 1;
    outtype = 'STA_OFIELD_ODD';
end;


if not(exist('L')),
    L = L1+L2;
end;


for l = outstart:outstep:L,
    F{l+1} = 0;
    for j1 = start1:step1:L1,
        for j2 = start2:step2:L2,
            if mod(j1+j2+l,2) == 0 & abs(j1-j2) <= l & j1+j2 >= l,
                product =  f1.get(j1).prod(f2.get(j2),l) * ClebschGordan(j1,j2,l,0,0,0)* (2*j1+1)*(2*j2+1)/(2*l+1);
                F{l+1} = F{l+1} + product.data;
            end;
        end;
    end;
end;
data = cat(2,F{1+(outstart:outstep:L)});
f = stafield(stafieldStruct(data,outtype,f1.storage));




