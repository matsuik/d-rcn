
function i=LtoIndex(L,step,realsym)

if realsym == 0,
    if step == 1,
        i = L^2;
    elseif step == 2,
        i = (L-1)*L/2;
    end;    
elseif realsym == 1,
    if step == 1,
        i = L*(L+1)/2;
    elseif step == 2,
        i = (L/2)^2;
    end;
end;