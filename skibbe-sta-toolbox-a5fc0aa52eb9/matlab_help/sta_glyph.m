function sta_glyph(ofield,varargin)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   sta_glyph(ofield,paramname,paramvalue,....)
%
%   plots the stafield ofield in glyph-style,
%   parameters are 
%
%   gamma,scval,meanval -- the vlaue of the glyph radius/color is 
%             computed according to scval*(v-meanval)^gamma. For scval=0
%             scval is set to 1/max(v(:))
%   init_view -- view initially displayed (1-x, 2-y, 3-z)
%   init_slice -- slice number initially displayed
%   background -- background image to be displayed
%   mask -- only for on-voxels glyphs are displayed
%
%






gamma = 1;
scval = 0;
meanval = 0;
init_view = 1;

background = 'max';
mask = [];
mask_thres = 0.001;

for k = 1:2:length(varargin),
    %if not(exist(varargin{k})),
    %    display(['invalid parameter: ' varargin{k}]);
    %else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    %end;
end;

if ~exist('init_slice','var')
    init_slice = round(ofield.shape(init_view)/2);
end;

if strcmp(ofield.type,'STA_OFIELD_FULL'),
    step = 1;
elseif strcmp(ofield.type,'STA_OFIELD_EVEN'),
    step = 2;
elseif strcmp(ofield.type,'STA_OFIELD_ODD'),
    step = 2;
elseif strcmp(ofield.type,'STA_OFIELD_SINGLE'),
    if ofield.L == 1,
        ofield.type = 'STA_OFIELD_ODD';
        step = 2;
    elseif ofield.L == 0,
        stackview(squeeze(ofield.data(1,1,:,:,:)),@imagesc);
        return;
    else
        %ofield.type = 'STA_OFIELD_EVEN';
    end    
end;



alldirs =  load('symS2distributions.mat');
bDir = alldirs.dirs128;
bDir = [bDir -bDir];

%updirs = load('dirs1024.mat');
%bDir = updirs.dirs1024;

%[X Y Z] = sphere(50);
%bDir = [X(:) Y(:) Z(:)]';


M=getSHMatrix(ofield.L,bDir,ofield.type,ofield.storage,1);


data = real(reshape(M.'*squeeze(ofield.data(1,:,:)+i*ofield.data(2,:,:)),[size(M,2) ofield.shape]));


if strcmp(background,'energy'),
    background = sqrt(squeeze(sum(data.^2)));
elseif strcmp(background,'mean'),
    background = (squeeze(sum(data)));
elseif strcmp(background,'max'),
    background = squeeze(max(data));
end;

if isempty(mask),    
    maxdata = max(abs(data(:)));
    mask = background>mask_thres*maxdata;
else
    %maxdata = max(abs(data(mask(:)>0)));
    maxdata =max(max((data(:,mask(:)))));
end;

if scval == 0,    
    scval = 1/maxdata;
end;


stackviewGLYPH(data,bDir,background,mask,[meanval scval gamma],[init_view init_slice]);


      