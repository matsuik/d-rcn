function r = sta_getComponentOffset(field_storage,field_type,L)

    if strcmp(field_storage,'STA_FIELD_STORAGE_C'),
        if strcmp(field_type,'STA_OFIELD_SINGLE')
            r = 0;
            return
        elseif strcmp(field_type,'STA_OFIELD_FULL')
            r = (L)*(L);
            return;
        elseif strcmp(field_type,'STA_OFIELD_ODD')    
            r = ((L-1)*L)/2;
            return;
        elseif strcmp(field_type,'STA_OFIELD_EVEN')       
            r = ((L-1)*L)/2;
            return;
        else
            warning('unsupported')
            r = -1;
            return;
        end;
    else
        if strcmp(field_type,'STA_OFIELD_SINGLE')
            r = 0;
            return
        elseif strcmp(field_type,'STA_OFIELD_FULL')
            r = (L*(L+1))/2;
            return;
        elseif strcmp(field_type,'STA_OFIELD_ODD')    
            r =  ((L+1)*(L+3))/4-L-1;
            return;
        elseif strcmp(field_type,'STA_OFIELD_EVEN')       
            r = ((L+2)*(L+2))/4-L-1;
            return;
        else
            warning('unsupported')
            r = -1;
            return;
        end;
    end;