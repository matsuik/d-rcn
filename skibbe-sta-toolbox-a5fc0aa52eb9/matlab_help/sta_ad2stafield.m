function ofield = sta_ad2stafield(data,dir,varargin)

L = 4;
type = 'STA_OFIELD_EVEN';
storage = 'STA_FIELD_STORAGE_R';
pseudoinverse = false;

for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;


if length(size(dir)) == 3,
    for k = 1:size(dir,3),
        [V D] = eigs(dir(:,:,k),1);
        bDir(:,k) = V(:,1);
    end;
else
    bDir = dir;
end;

M = getSHMatrix(L,bDir,type,storage,0);

ndata = permute(data,[4 1 2 3]);
if pseudoinverse,
    ndata = reshape(pinv(M')*ndata(:,:),[size(M,1) size(ndata,2) size(ndata,3) size(ndata,4)]) ;
else,
    ndata = reshape(M*ndata(:,:),[size(M,1) size(ndata,2) size(ndata,3) size(ndata,4)]) ;
end;
ndata = cat(1,reshape(real(ndata),[1 size(ndata)]),reshape(imag(ndata),[1 size(ndata)]));

ofield = stafieldStruct(ndata,type,storage);


