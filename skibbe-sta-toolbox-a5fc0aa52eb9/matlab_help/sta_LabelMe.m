function varargout = sta_LabelMe(varargin)
% STA_LABELME MATLAB code for sta_LabelMe.fig
%      STA_LABELME, by itself, creates a new STA_LABELME or raises the existing
%      singleton*.
%
%      H = STA_LABELME returns the handle to a new STA_LABELME or the
%      handle to
%      the existing singleton*.
%
%      STA_LABELME('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STA_LABELME.M with the given input arguments.
%
%      STA_LABELME('Property','Value',...) creates a new STA_LABELME or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sta_LabelMe_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sta_LabelMe_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sta_LabelMe

% Last Modified by GUIDE v2.5 30-Mar-2012 17:05:40

%GuiName=mfilename;
%for k = 1:2:length(varargin),
%        if strcmp(varargin{k},'ID')
%        GuiName=varargin{k+1};    
%       end;
%end;


% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @sta_LabelMe_OpeningFcn, ...
                   'gui_OutputFcn',  @sta_LabelMe_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before sta_LabelMe is made visible.
function sta_LabelMe_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sta_LabelMe (see VARARGIN)

% Choose default command line output for sta_LabelMe
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes sta_LabelMe wait for user response (see UIRESUME)
% uiwait(handles.MainFigure);


Image=zeros(2,2,2);
labelIndx=[];
labelSub=[];
labelNames=[];
% @(x,parent)imagesc(x,'Parent',parent,[0,0.1])
%sta_LabelMe('Image',{tmp,tmp2,tmp3},'showfunc',@(x,parent)imagesc(x,'Parent',parent,[0,1]),'ccolor','r')
%sta_LabelMe('Image',tmp,'showfunc',@(x,parent)imagesc(x,'Parent',parent,[0,1]),'color','www','labelSub',landmarks2.subindx)


set(handles.SelectedLabel,'String',num2str(-1)); 
set(handles.LabelList,'String','');
set(handles.LabelList,'Value',1);
%set(handles.SelectedLabel,'String',num2str(1)); 

showfunc=@(x,parent)imagesc(x,'Parent',parent);
Colors='grm';
color=Colors;
for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

if ~iscell(Image)
    Image={Image};
end;

Colors(1:numel(color))=color;
MyData.crosscolor=Colors;

MyData.showfunc=showfunc;
for a=1:numel(Image),
    if numel(Image)>1
        Image{a}=Image{a}-min(Image{a}(:));
        Image{a}=Image{a}./max(Image{a}(:));
    end;
    MyData.Image(a).data=Image{a};
end;
shape=size(MyData.Image(1).data);

MyData.slice=ceil(shape/2);
MyData.shape=shape;
MyData.axes=[handles.axesX,handles.axesY,handles.axesZ];
MyData.start_handles=handles;
MyData.isScrolling=0;
MyData.isZooming=0;
MyData.zoom_level=2;
MyData.isShift=0;
%MyData.Zoom=[1,1,1];
MyData.lastUpdate=uint64(0);
MyData.LandmarkMenue=handles.EditLandmarks;

MyData.landmark_filename='';


% %MyData.labelIndx=sub2ind(shape,17,17,17);%randi([1,prod(shape)],1,20);
% MyData.labelIndx=randi([1,prod(shape)],1,20);
% %MyData.labelIndx=sub2ind(shape,17,27,37);
% MyData.labelSub=zeros([3,numel(MyData.labelIndx)]);
% [X,Y,Z]=ind2sub(shape,MyData.labelIndx);
% MyData.labelSub(1,:)=X;
% MyData.labelSub(2,:)=Y;
% MyData.labelSub(3,:)=Z;

MyData.selected_label=-1;
if exist('landmarks'),
    MyData.labelSub=landmarks.subindx';
    MyData.labelIndx=landmarks.indx;
    labelNames=landmarks.Names;
    
else
    if numel(labelIndx)>0
        MyData.labelIndx=labelIndx;
        MyData.labelSub=zeros([3,numel(MyData.labelIndx)]);
        [X,Y,Z]=ind2sub(shape,MyData.labelIndx);
        MyData.labelSub(1,:)=X;
        MyData.labelSub(2,:)=Y;
        MyData.labelSub(3,:)=Z; 
    elseif numel(labelSub)>0
        MyData.labelSub=labelSub';
        MyData.labelIndx=sub2ind(shape,labelSub(:,1),labelSub(:,2),labelSub(:,3));
    else
       MyData.labelSub=[];
       MyData.labelIndx=[];
    end;
end;


for Radius=1:5,
[MyData.circle(Radius).x,MyData.circle(Radius).y,z] = cylinder(6-Radius,200);
end;







% Zspace=(shape(3)/5);
% ticks=[shape(3):-Zspace:1];
% ticks_list={};
% for a=1:numel(ticks)
%     ticks_list{a}=num2str(ticks(a),'% 1.1f');
% end;    
% set(handles.axesX,'XTickLabel',ticks_list);

drawData(MyData,handles.axesX,'dim',1,'slice',MyData.slice(1));
drawData(MyData,handles.axesY,'dim',2,'slice',MyData.slice(2));
drawData(MyData,handles.axesZ,'dim',3,'slice',MyData.slice(3));

xlabel(handles.axesX,'-Z');
ylabel(handles.axesX,'Y');

xlabel(handles.axesZ,'X');
ylabel(handles.axesZ,'Y');

xlabel(handles.axesY,'X');
ylabel(handles.axesY,'Z');



%handle = uicontextmenu('PropertyName',PropertyValue,...) 

colormap(handles.axesX,'gray');



num_labels=size(MyData.labelSub,2);
if numel(MyData.labelSub)>0
    if num_labels>200
        display('too many labels!?');
        
    end;
    
   

    label_text={};
    for a=1:num_labels,
          if (numel(labelNames))>=1
            MyData.labelNames(a).name=labelNames(a).name;
          else
              MyData.labelNames(a).name='';
          end;
          label_text{a}=getLabelTxt(a,MyData.labelSub(:,a)',MyData.labelNames(a).name);
    end;

    set(handles.LabelList,'String',label_text);
else
    set(handles.LabelList,'String','');
end;

    

set(handles.MainFigure,'UserData',MyData);


function ltxt=getLabelTxt(a,position,labelname)

%     if a<10
%         nstr=['00',num2str(a)];
%     elseif a<100
%         nstr=['0',num2str(a)];
%     else
%         nstr=[num2str(a)];
%     end;

    %nstr=num2str(a,'%03.10f');
    nstr=num2str(a,'%0.3d');
    
    if numel(labelname)>0
        %ltxt=[nstr,'  [',num2str(position),'], (',labelname,')'];
        ltxt=[nstr,'  (',labelname,')'];
    else
        %ltxt=[nstr,'  [',num2str(position),']'];
        ltxt=[nstr,'  [',num2str(position,'%0.2f %0.2f %0.2f'),']'];
    end;



% --- Outputs from this function are returned to the command line.
function varargout = sta_LabelMe_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function drawData(Data,hAxis,varargin)
dim=1;
mode=0;
for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

cla(hAxis);

slice=Data.slice(dim);
%shape=size(Data.Image(1).data);
%shape2D=shape;
%shape2D(dim)=[];
for a=1:numel(Data.Image),
    switch mode
        case 0
            switch dim
                case 1
                    %img=permute(squeeze(Data.Image(1).data(slice,:,:)),[2,1]);
                    img{a}=(squeeze(Data.Image(a).data(slice,:,end:-1:1)));
                case 2
                    %img=squeeze(Data.Image(1).data(:,slice,end:-1:1));
                    img{a}=permute(squeeze(Data.Image(a).data(:,slice,:)),[2,1]);
                case 3
                    %img=squeeze(Data.Image(1).data(:,:,slice));
                    img{a}=permute(squeeze(Data.Image(a).data(:,:,slice)),[2,1]);
            end;
    end;
end;    
aratio=[(size(img{1})./max(size(img{1}))),1];
aratio([1,2])=aratio([2,1]);
shape=size(img{1});

set(hAxis,'NextPlot','replacechildren');

if numel(Data.Image)==1
    himage=Data.showfunc(img{1},hAxis);
else
    if (numel(Data.Image)>3);
        error('only up to 3 images!');
    end;
        
    RGB=zeros([shape,3]);
    for a=1:numel(Data.Image),
        RGB(:,:,a)=img{a};
    end;
    himage=Data.showfunc(RGB,hAxis);
end;

set(himage,'uicontextmenu',Data.LandmarkMenue);


%,'Parent',hAxis

%himage=Data.showfunc(img);
%set(himage,'Parent',hAxis);
set(himage,'ButtonDownFcn',{get(hAxis,'ButtonDownFcn')});

%zoom(hAxis, 5); 
%zoom(2); 

if (Data.isZooming && (Data.zoom_level~=1))
    zoom_level=Data.zoom_level;
    tmp=[max(shape),max(shape)]/(zoom_level*2);
    switch dim
        case 1
            %center=[shape(2)-Data.slice(3),Data.slice(2)];
            center=[1+shape(2)-Data.slice(3),Data.slice(2)];
        case 2
            center=[Data.slice(1),Data.slice(3)];
        case 3
            center=[Data.slice(1),Data.slice(2)];
    end;
    set(hAxis,'XLim',[center(1)-tmp(1),center(1)+tmp(1)]);
    set(hAxis,'YLim',[center(2)-tmp(2),center(2)+tmp(2)]);
else
%    set(hAxis,'XLim',[1,shape(2)]);
%    set(hAxis,'YLim',[1,shape(1)]);
    set(hAxis,'XLim',[0.5,shape(2)+0.5]);
    set(hAxis,'YLim',[0.5,shape(1)+0.5]);
end;




set(hAxis,'PlotBoxAspectRatio',aratio);
set(hAxis,'PlotBoxAspectRatioMode','manual');

set(hAxis,'DataAspectRatio',[1,1,1]);
set(hAxis,'DataAspectRatioMode','manual');


set(hAxis,'NextPlot','add');

switch dim
    case 1
%         plot(hAxis,[shape(2),shape(2)]-[Data.slice(3),Data.slice(3)],[1,shape(1)],'k','LineWidth',3);
%         plot(hAxis,[1,shape(2)],[Data.slice(2),Data.slice(2)],'k','LineWidth',3);   
%         plot(hAxis,[shape(2),shape(2)]-[Data.slice(3),Data.slice(3)],[1,shape(1)],'w');
%         plot(hAxis,[1,shape(2)],[Data.slice(2),Data.slice(2)],'w');   
        %plot(hAxis,[shape(2),shape(2)]-[Data.slice(3),Data.slice(3)],[1,shape(1)],'w','LineWidth',3);
        %plot(hAxis,[shape(2),shape(2)]-[Data.slice(3),Data.slice(3)],[1+Data.slice(2)+5,shape(1)],'r','LineWidth',3);
        %plot(hAxis,[shape(2),shape(2)]-[Data.slice(3),Data.slice(3)],[1,Data.slice(2)-5],'r','LineWidth',3);
        %plot(hAxis,[1,shape(2)-Data.slice(3)-5],[Data.slice(2),Data.slice(2)],'r','LineWidth',3);   
        %plot(hAxis,[shape(2)-Data.slice(3)+5,shape(2)],[Data.slice(2),Data.slice(2)],'r','LineWidth',3);   
        
        plot(hAxis,1+[shape(2),shape(2)]-[Data.slice(3),Data.slice(3)],[0.5,shape(1)+0.5],Data.crosscolor(1));
        plot(hAxis,[0.5,shape(2)+0.5],[Data.slice(2),Data.slice(2)],Data.crosscolor(1));   
        
        if numel(Data.labelSub)>0 && get(Data.start_handles.showlabels,'Value')
            scale=ceil(abs(Data.labelSub(1,:)-Data.slice(1)));
            selection=scale<5;
            if sum(selection)>0
                pos=Data.labelSub([2,3],selection);
                pos(2,:)=1+shape(2)-pos(2,:);
                %pos(1,:)=shape(1,:)-pos(1,:);
                plotCircles(Data,pos,scale(selection),hAxis);
            end;
        end;
    case 2        
        plot(hAxis,[Data.slice(1),Data.slice(1)],[0.5,shape(1)+0.5],Data.crosscolor(1));
        plot(hAxis,[0.5,shape(2)+0.5],[Data.slice(3),Data.slice(3)],Data.crosscolor(1));           
        
        if numel(Data.labelSub)>0 && get(Data.start_handles.showlabels,'Value')
            scale=ceil(abs(Data.labelSub(2,:)-Data.slice(2)));
            selection=scale<5;
            if sum(selection)>0
                plotCircles(Data,Data.labelSub([3,1],selection),scale(selection),hAxis);
            end;
        end;
    case 3        
        plot(hAxis,[Data.slice(1),Data.slice(1)],[0.5,shape(1)+0.5],Data.crosscolor(1));
        plot(hAxis,[0.5,shape(2)+0.5],[Data.slice(2),Data.slice(2)],Data.crosscolor(1));          
        
        if numel(Data.labelSub)>0 && get(Data.start_handles.showlabels,'Value')
            scale=ceil(abs(Data.labelSub(3,:)-Data.slice(3)));
            selection=scale<5;
            if sum(selection)>0
                plotCircles(Data,Data.labelSub([2,1],selection),scale(selection),hAxis);
            end;
        end;
end;
    
legend(hAxis,'hide');

%set(hAxis,'Box','on');

hlines = findall(hAxis,'Type','line');
for line = 1:length(hlines)
    set(hlines(line),'uicontextmenu',Data.LandmarkMenue)
end



if hAxis==Data.axes(1)
    set(Data.start_handles.edit1,'String',['[',num2str(Data.slice),']']);
    if numel(Data.Image)>1
        vl='[ ';
        for a=1:numel(Data.Image),
            value=Data.Image(a).data(Data.slice(1),Data.slice(2),Data.slice(3));
            if a==1,
                vl=[vl,num2str(value,'%.2f')];
            else
                vl=[vl,' , ',num2str(value,'%.2f')];
            end;
        end;
        vl=[vl,' ]'];
    else
        value=Data.Image(1).data(Data.slice(1),Data.slice(2),Data.slice(3));
        vl=num2str(value);
    end;
    set(Data.start_handles.edit2,'String',vl);
end;


function plotCircles(Data,landmark_pos,scale,handle)

        
        for b=1:numel(landmark_pos(1,:)),
            s=scale(b)+1;
            x=Data.circle(s).x;
            y=Data.circle(s).y;
            py=landmark_pos(2,b);
            px=landmark_pos(1,b);
            plot(handle,x(1,:)+py,y(1,:)+px,Data.crosscolor(2),'LineWidth',1)
            if s==1,
                plot(handle,[py,py],[px-3,px+3],Data.crosscolor(3));
                plot(handle,[py+3,py-3],[px,px],Data.crosscolor(3));
                %x=Data.circle(end).x;
                %y=Data.circle(end).y;
                %plot(x(1,:)+py,y(1,:)+px,'g','LineWidth',1)
            end,
        end;    




% --- Executes on scroll wheel click while the figure is in focus.
function MainFigure_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)

dim=-1;





    if (myHitTest(handles.axesX))
        %fprintf('wheel X\n');
        dim=1;
    elseif (myHitTest(handles.axesY))
        %fprintf('wheel Y\n');
        dim=2;
    elseif (myHitTest(handles.axesZ))
        %fprintf('wheel Z\n');
        dim=3;
    end;

    if dim~=-1,
        Data=get(handles.MainFigure,'UserData');
        Data.selected_label=-1;
        set(handles.SelectedLabel,'String',num2str(Data.selected_label));        
        if (Data.isShift==0)
            switch dim
                case 1
                    Data.slice(dim)=Data.slice(dim)+eventdata.VerticalScrollCount;
                case 2
                    Data.slice(dim)=Data.slice(dim)-eventdata.VerticalScrollCount;
                case 3
                    Data.slice(dim)=Data.slice(dim)+eventdata.VerticalScrollCount;
            end;
            Data.slice(Data.slice<1)=1;
            Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);
            set(handles.MainFigure,'UserData',[]);
            set(handles.MainFigure,'UserData',Data);
            harray=[handles.axesX,handles.axesY,handles.axesZ];
            %drawData(Data,harray(dim),'dim',dim,'slice',Data.slice(dim));

            drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
            drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
            drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));
            
        else
%             switch dim
%                 case 1
%                     Data.Zoom(1)=Data.Zoom(1)+eventdata.VerticalScrollCount;
%                 case 2
%                     Data.Zoom(2)=Data.Zoom(2)+eventdata.VerticalScrollCount;
%                 case 3
%                     Data.Zoom(3)=Data.Zoom(3)+eventdata.VerticalScrollCount;
%             end;
%             Data.Zoom(Data.Zoom<1)=1;
%             Data.Zoom(Data.Zoom>5)=5;
%             Data.Zoom
%             set(handles.MainFigure,'UserData',Data);
%             harray=[handles.axesX,handles.axesY,handles.axesZ];
%             drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
%             drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
%             drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));            
        end;
    end;


%get(handles.MainFigure, 'CurrentCharacter')
%mouse_key=get(gcf,'SelectionType')
%if strcmp(mouse_key,'extend')
%     if (myHitTest(handles.axesX))
%         Data=get(handles.MainFigure,'UserData');
%         Data.isScrolling=1;
%         set(handles.MainFigure,'UserData',Data);
%     elseif (myHitTest(handles.axesY))
%         Data=get(handles.MainFigure,'UserData');
%         Data.isScrolling=2;
%         set(handles.MainFigure,'UserData',Data);
%     elseif (myHitTest(handles.axesZ))
%         Data=get(handles.MainFigure,'UserData');
%         Data.isScrolling=3;
%         set(handles.MainFigure,'UserData',Data);
%     end;    
%    if (MyData.isZooming)
%    end;
%end;

function [inside,X,Y,faspect]=myHitTest(hObject)
inside=true;


if hObject==0, 
    inside=false;
    X=0;
    Y=0;
    faspect=[1,1];
    return;  
end


oldUnits = get(0,'units');
set(0,'units','pixels');
%rect=get(hObject,'Position');
rect=getpixelposition(hObject);

fig = get(0,'PointerWindow');
mpos = get(0,'PointerLocation');
set(0,'units',oldUnits);

if fig==0, 
    inside=false;
    X=0;
    Y=0;
    faspect=[1,1];
    return;  
end

figPos = getpixelposition(fig);
x = (mpos(1)-figPos(1));
y = (mpos(2)-figPos(2));

inside=true;
% if (x<rect(1)) || (x>rect(1)+rect(3)) ||...
%    (y<rect(2)) || (y>rect(2)+rect(4))    
%     inside=false;
% end;
if (x<rect(1)) || (x>rect(1)+rect(3)) ||...
   (y<rect(2)) || (y>rect(2)+rect(4))    
    inside=false;
end;



if nargout>1
    faspect=[rect(3),rect(4)];
    %faspect=faspect./min(faspect);
    
    
    X=(x-rect(1))/rect(3);
    Y=(y-rect(2))/rect(4);
end;


% --- Executes on mouse press over axes background.
function axesZ_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axesZ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on mouse press over axes background.
function axesX_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axesX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over axes background.
function axesY_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axesY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on MainFigure or any of its controls.
function MainFigure_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)




% --- Executes on mouse motion over figure - except title and menu.
function MainFigure_WindowButtonMotionFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%fprintf('%d %d %d %d\n',ceil(get(handles.MainFigure,'Position')));

if hObject==0,
    return;
end;
Data=get(handles.MainFigure,'UserData');
%Data.isZooming=0;
%if abs(double(tic)-double(Data.lastUpdate))<100000
if abs(double(tic)-double(Data.lastUpdate))<70000
    return;
end;
Data.lastUpdate=tic;
set(handles.MainFigure,'UserData',[]);
set(handles.MainFigure,'UserData',Data);        


%if strcmp(mouse_key,'normal'),
mouse_key=get(gcf,'SelectionType');
% 
% if strcmp(mouse_key,'extend')
%     if (MyData.isZooming)
%     
%     end;if strcmp(mouse_key,'extend')
%     return;
% end;



if   (Data.isScrolling~=0) && strcmp(mouse_key,'normal'),
    Data.selected_label=-1;
    set(handles.SelectedLabel,'String',num2str(Data.selected_label));
    Data.isScrolling=0;
    [inside,X,Y,fsapect]=myHitTest(handles.axesX);
    if inside
            aspect_box=fsapect(1)/fsapect(2);
            aspect_img=Data.shape(3)/Data.shape(2);
       
            if aspect_box<aspect_img,
                X=1-(0.5+X-0.5);
                fact=aspect_img/aspect_box;
                Y=(0.5+fact*(Y-0.5));
            else
                fact=aspect_box/aspect_img;
                X=1-(0.5+fact*(X-0.5));
                Y=(0.5+(Y-0.5));
            end;
            
            Data.slice([2,3])=ceil([Y,X].*Data.shape([2,3]));
    else
        [inside,X,Y,fsapect]=myHitTest(handles.axesY);
        if inside
            
            aspect_box=fsapect(1)/fsapect(2);
            aspect_img=Data.shape(1)/Data.shape(3);
       
            if aspect_box<aspect_img,
                X=(0.5+X-0.5);
                fact=aspect_img/aspect_box;
                Y=(0.5+fact*(Y-0.5));
            else
                fact=aspect_box/aspect_img;
                X=(0.5+fact*(X-0.5));
                Y=(0.5+(Y-0.5));
            end;
            Data.slice([1,3])=ceil([X,Y].*Data.shape([1,3]));
        else    
            [inside,X,Y,fsapect]=myHitTest(handles.axesZ);
            if inside
            aspect_box=fsapect(1)/fsapect(2);
            aspect_img=Data.shape(1)/Data.shape(2);
       
            if aspect_box<aspect_img,
                X=(0.5+X-0.5);
                fact=aspect_img/aspect_box;
                Y=(0.5+fact*(Y-0.5));
            else
                fact=aspect_box/aspect_img;
                X=(0.5+fact*(X-0.5));
                Y=(0.5+(Y-0.5));
            end;                
                Data.slice([1,2])=ceil([X,Y].*Data.shape([1,2]));
            end;
        end;
    end;
    
    
    if inside
            Data.slice(Data.slice<1)=1;
            Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);        
            Data.isScrolling=1;
            Data.selected_label=-1;
            set(handles.SelectedLabel,'String',num2str(Data.selected_label));
            set(handles.MainFigure,'UserData',[]);
            set(handles.MainFigure,'UserData',Data);
            harray=[handles.axesX,handles.axesY,handles.axesZ];
            drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
            drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
            drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));                
    else
        Data.isScrolling=0;
        %fprintf('out\n');
        set(handles.MainFigure,'UserData',Data);        
    end;
else
    
    
end;

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function MainFigure_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

mouse_key=get(gcf,'SelectionType');

if strcmp(mouse_key,'normal'),
    if (myHitTest(handles.axesX))
        Data=get(handles.MainFigure,'UserData');
        Data.isZooming=0;
        Data.isScrolling=1;
        Data.selected_label=-1;
        Data.lastUpdate=0;
        set(handles.SelectedLabel,'String',num2str(Data.selected_label));
        set(handles.MainFigure,'UserData',Data);
        MainFigure_WindowButtonMotionFcn(hObject, eventdata, handles);
        %harray=[handles.axesX,handles.axesY,handles.axesZ];
        %drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
        %drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
        %drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));             
    elseif (myHitTest(handles.axesY))
        Data=get(handles.MainFigure,'UserData');
        Data.isZooming=0;
        Data.isScrolling=2;
        Data.selected_label=-1;
        Data.lastUpdate=0;
        set(handles.SelectedLabel,'String',num2str(Data.selected_label));
        set(handles.MainFigure,'UserData',Data);
        MainFigure_WindowButtonMotionFcn(hObject, eventdata, handles);
        %harray=[handles.axesX,handles.axesY,handles.axesZ];
        %drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
        %drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
        %drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));         
    elseif (myHitTest(handles.axesZ))
        Data=get(handles.MainFigure,'UserData');
        Data.isZooming=0;
        Data.isScrolling=3;
        Data.selected_label=-1;
        Data.lastUpdate=0;
        set(handles.SelectedLabel,'String',num2str(Data.selected_label));
        set(handles.MainFigure,'UserData',Data);
        MainFigure_WindowButtonMotionFcn(hObject, eventdata, handles);
        %harray=[handles.axesX,handles.axesY,handles.axesZ];
        %drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
        %drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
        %drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));         
    end;
end;


% if strcmp(mouse_key,'alt'),
%          Data=get(handles.MainFigure,'UserData');
%          Data.isZooming=0;
%          harray=[handles.axesX,handles.axesY,handles.axesZ];
%          %drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
%          drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
%          %drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));       
% end;







% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function MainFigure_WindowButtonUpFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');
Data.isScrolling=0;
%Data.isZooming=0;

set(handles.MainFigure,'UserData',Data);



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end






% --- Executes on selection change in LabelList.
function LabelList_Callback(hObject, eventdata, handles)
% hObject    handle to LabelList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns LabelList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from LabelList

%contents = cellstr(get(hObject,'String')) 
%contents{get(hObject,'Value')}

selected_label=get(handles.LabelList,'Value');

if numel(selected_label)>0
    Data=get(handles.MainFigure,'UserData');
    Data.slice=round(Data.labelSub(:,selected_label)');
    Data.selected_label=selected_label;
    harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     
    set(handles.MainFigure,'UserData',Data);
    set(handles.SelectedLabel,'String',num2str(selected_label));
end;


% --- Executes during object creation, after setting all properties.
function LabelList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LabelList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Ym.
function Ym_Callback(hObject, eventdata, handles)
% hObject    handle to Ym (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Data=get(handles.MainFigure,'UserData');

if Data.selected_label==-1    
    Data.slice(2)=Data.slice(2)-1;
    Data.slice(Data.slice<1)=1;
    Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
else
    %Data.slice=Data.labelSub(:,Data.selected_label)'; 
    %Data.slice(2)=Data.slice(2)-1;
    %Data.slice(Data.slice<1)=1;
    %Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
    %Data.labelSub(:,Data.selected_label)=Data.slice';        
    
    position=Data.labelSub(:,Data.selected_label)';
    contents = cellstr(get(handles.popupmenu3,'String')) ;
    move_pixels=str2double(contents{get(handles.popupmenu3,'Value')});    
    position(2)=position(2)-move_pixels;
    position(position<1)=1;
    position(position>Data.shape)=Data.shape(position>Data.shape); 
    Data.slice=round(position);
    Data.labelSub(:,Data.selected_label)=position';     
end;
    harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     
    set(handles.MainFigure,'UserData',Data);
if Data.selected_label~=-1    
    LabelTxt = cellstr(get(handles.LabelList,'String'));
    LabelTxt{Data.selected_label}=getLabelTxt(Data.selected_label,Data.labelSub(:,Data.selected_label)',Data.labelNames(Data.selected_label).name);
    set(handles.LabelList,'String',LabelTxt);
end;



% --- Executes on button press in Xp.
function Xp_Callback(hObject, eventdata, handles)
% hObject    handle to Xp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');
if Data.selected_label==-1
    Data.slice(1)=Data.slice(1)+1;
    Data.slice(Data.slice<1)=1;
    Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
else
    %Data.slice=Data.labelSub(:,Data.selected_label)';
    %Data.slice(1)=Data.slice(1)+1;
    %Data.slice(Data.slice<1)=1;
    %Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
    %Data.labelSub(:,Data.selected_label)=Data.slice';        
    
    position=Data.labelSub(:,Data.selected_label)';
    contents = cellstr(get(handles.popupmenu3,'String')) ;
    move_pixels=str2double(contents{get(handles.popupmenu3,'Value')});    
    position(1)=position(1)+move_pixels;
    position(position<1)=1;
    position(position>Data.shape)=Data.shape(position>Data.shape); 
    Data.slice=round(position);
    Data.labelSub(:,Data.selected_label)=position'; 
end;
    harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     
    set(handles.MainFigure,'UserData',Data);
if Data.selected_label~=-1        
    LabelTxt = cellstr(get(handles.LabelList,'String'));
    LabelTxt{Data.selected_label}=getLabelTxt(Data.selected_label,Data.labelSub(:,Data.selected_label)',Data.labelNames(Data.selected_label).name);
    set(handles.LabelList,'String',LabelTxt);
end;


% --- Executes on button press in Xm.
function Xm_Callback(hObject, eventdata, handles)
% hObject    handle to Xm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


Data=get(handles.MainFigure,'UserData');
if Data.selected_label==-1
    Data.slice(1)=Data.slice(1)-1;
    Data.slice(Data.slice<1)=1;
    Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
    
else
    %Data.slice=Data.labelSub(:,Data.selected_label)';
    %Data.slice(1)=Data.slice(1)-1;
    %Data.slice(Data.slice<1)=1;
    %Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
    %Data.labelSub(:,Data.selected_label)=Data.slice';        
    position=Data.labelSub(:,Data.selected_label)';
    contents = cellstr(get(handles.popupmenu3,'String')) ;
    move_pixels=str2double(contents{get(handles.popupmenu3,'Value')});    
    position(1)=position(1)-move_pixels;
    position(position<1)=1;
    position(position>Data.shape)=Data.shape(position>Data.shape); 
    Data.slice=round(position);
    Data.labelSub(:,Data.selected_label)=position';     
end;
    harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     
    set(handles.MainFigure,'UserData',Data);
if Data.selected_label~=-1        
    LabelTxt = cellstr(get(handles.LabelList,'String'));
    LabelTxt{Data.selected_label}=getLabelTxt(Data.selected_label,Data.labelSub(:,Data.selected_label)',Data.labelNames(Data.selected_label).name);
    set(handles.LabelList,'String',LabelTxt);
end;

% --- Executes on button press in Yp.
function Yp_Callback(hObject, eventdata, handles)
% hObject    handle to Yp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');
if Data.selected_label==-1
    Data.slice(2)=Data.slice(2)+1;
    Data.slice(Data.slice<1)=1;
    Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
else
    position=Data.labelSub(:,Data.selected_label)';
    contents = cellstr(get(handles.popupmenu3,'String')) ;
    move_pixels=str2double(contents{get(handles.popupmenu3,'Value')});    
    position(2)=position(2)+move_pixels;
    position(position<1)=1;
    position(position>Data.shape)=Data.shape(position>Data.shape); 
    Data.slice=round(position);
    Data.labelSub(:,Data.selected_label)=position'; 
    %Data.slice=Data.labelSub(:,Data.selected_label)';
    %Data.slice(2)=Data.slice(2)+1;
    %Data.slice(Data.slice<1)=1;
    %Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
    %Data.labelSub(:,Data.selected_label)=Data.slice';        
    
end;

    harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     
    set(handles.MainFigure,'UserData',Data);
if Data.selected_label~=-1        
    LabelTxt = cellstr(get(handles.LabelList,'String'));
    LabelTxt{Data.selected_label}=getLabelTxt(Data.selected_label,Data.labelSub(:,Data.selected_label)',Data.labelNames(Data.selected_label).name);
    set(handles.LabelList,'String',LabelTxt);    
end;

% --- Executes on button press in Zm.
function Zm_Callback(hObject, eventdata, handles)
% hObject    handle to Zm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');
if Data.selected_label==-1
    Data.slice(3)=Data.slice(3)-1;
    Data.slice(Data.slice<1)=1;
    Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
else
    %Data.slice=Data.labelSub(:,Data.selected_label)';
    %Data.slice(3)=Data.slice(3)-1;
    %Data.slice(Data.slice<1)=1;
    %Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
    %Data.labelSub(:,Data.selected_label)=Data.slice';    
    
    position=Data.labelSub(:,Data.selected_label)';
    contents = cellstr(get(handles.popupmenu3,'String')) ;
    move_pixels=str2double(contents{get(handles.popupmenu3,'Value')});    
    position(3)=position(3)-move_pixels;
    position(position<1)=1;
    position(position>Data.shape)=Data.shape(position>Data.shape); 
    Data.slice=round(position);
    Data.labelSub(:,Data.selected_label)=position';     
    
    
end;

    harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     
    set(handles.MainFigure,'UserData',Data);
if Data.selected_label~=-1        
    LabelTxt = cellstr(get(handles.LabelList,'String'));
    LabelTxt{Data.selected_label}=getLabelTxt(Data.selected_label,Data.labelSub(:,Data.selected_label)',Data.labelNames(Data.selected_label).name);
    set(handles.LabelList,'String',LabelTxt);    
end;

% --- Executes on button press in Zp.
function Zp_Callback(hObject, eventdata, handles)
% hObject    handle to Zp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');
if Data.selected_label==-1
    Data.slice(3)=Data.slice(3)+1;
    Data.slice(Data.slice<1)=1;
    Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
else
    %Data.slice=Data.labelSub(:,Data.selected_label)';
    %Data.slice(3)=Data.slice(3)+1;
    %Data.slice(Data.slice<1)=1;
    %Data.slice(Data.slice>Data.shape)=Data.shape(Data.slice>Data.shape);      
    %Data.labelSub(:,Data.selected_label)=Data.slice';        
    
    position=Data.labelSub(:,Data.selected_label)';
    contents = cellstr(get(handles.popupmenu3,'String')) ;
    move_pixels=str2double(contents{get(handles.popupmenu3,'Value')});    
    position(3)=position(3)+move_pixels;
    position(position<1)=1;
    position(position>Data.shape)=Data.shape(position>Data.shape); 
    Data.slice=round(position);
    Data.labelSub(:,Data.selected_label)=position';     
end;
    harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     
    set(handles.MainFigure,'UserData',Data);
if Data.selected_label~=-1       
    LabelTxt = cellstr(get(handles.LabelList,'String'));
    LabelTxt{Data.selected_label}=getLabelTxt(Data.selected_label,Data.labelSub(:,Data.selected_label)',Data.labelNames(Data.selected_label).name);
    set(handles.LabelList,'String',LabelTxt);    
end;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function EditLandmarks_Callback(hObject, eventdata, handles)
% hObject    handle to EditLandmarks (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');
Data.isScrolling=1;
Data.lastUpdate=0;
set(handles.MainFigure,'UserData',Data);
if not(Data.isZooming)
    MainFigure_WindowButtonMotionFcn(hObject, eventdata, handles);
end;
Data=get(handles.MainFigure,'UserData');
Data.isScrolling=0;
set(handles.MainFigure,'UserData',Data);


% --------------------------------------------------------------------
function SelLandmark_Callback(hObject, eventdata, handles)
% hObject    handle to SelLandmark (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%curent_pos=Data.slice
Data=get(handles.MainFigure,'UserData');
%Data.isZooming=1;

lpos=Data.labelSub;
if numel(lpos)>0
    cpos=Data.slice;
    dmat=distmat(lpos',cpos);
    [v,closest_landmark]=min(dmat);
    
    selected_label=closest_landmark;

    Data.slice=round(Data.labelSub(:,selected_label)');
    Data.selected_label=selected_label;
    harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     
    set(handles.MainFigure,'UserData',Data);
    set(handles.SelectedLabel,'String',num2str(selected_label));
end;

% --------------------------------------------------------------------
function AddLandmark_Callback(hObject, eventdata, handles)
% hObject    handle to AddLandmark (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');
    
NewLabelPos=Data.slice;
num_labels=size(Data.labelSub,2);

Data.labelSub(:,num_labels+1)=Data.slice';
Data.labelIndx(num_labels+1)=sub2ind(Data.shape,Data.slice(1),Data.slice(2),Data.slice(3));
Data.labelNames(num_labels+1).name='';

    if num_labels+1>100
        error('oo many labels!');
    end;

    label_text={};
    for a=1:num_labels,
          label_text{a}=getLabelTxt(a,Data.labelSub(:,a)',Data.labelNames(a).name);
    end;
    set(handles.LabelList,'String',label_text);  
    set(handles.LabelList,'Value',numel(label_text));  
    
    Data.selected_label=num_labels+1;
    set(handles.MainFigure,'UserData',Data);
    
    label_text={};
    for a=1:num_labels+1,
          label_text{a}=getLabelTxt(a,Data.labelSub(:,a)',Data.labelNames(a).name);
    end;
    set(handles.LabelList,'String',label_text);  
    set(handles.LabelList,'Value',num_labels+1);      
    
    harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));         

    set(handles.SelectedLabel,'String',num2str(Data.selected_label));


% --------------------------------------------------------------------
function DelLandmark_Callback(hObject, eventdata, handles)
% hObject    handle to DelLandmark (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

SelLandmark_Callback(hObject, eventdata, handles);
Data.isZooming=0;


Data=get(handles.MainFigure,'UserData');
if Data.selected_label~=-1
    
    
    
    Data.labelSub(:,Data.selected_label)=[];
    Data.labelIndx(Data.selected_label)=[];
    Data.labelNames(Data.selected_label)=[];
    num_labels=size(Data.labelSub,2);

    if num_labels>100
        error('oo many labels!');
    end;

    label_text={};
    for a=1:num_labels,
          label_text{a}=getLabelTxt(a,Data.labelSub(:,a)',Data.labelNames(a).name);
    end;
    set(handles.LabelList,'String',label_text);  
    set(handles.LabelList,'Value',min(numel(label_text),Data.selected_label+1));  
    
    Data.selected_label=-1;
    
   set(handles.SelectedLabel,'String',num2str(Data.selected_label));
    
    set(handles.MainFigure,'UserData',Data);
    
    harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));         
end;

function dmat=distmat(b,a)
  %dmat=(-2.*(a'*b))+repmat(sum(a.^2),1,size(b,2))+repmat(sum(b.^2),size(a,1),1);
  dmat=(-2.*(a*b'))+repmat(sum(a.^2,2),1,size(b,1))+(repmat(sum(b.^2,2)',size(a,1),1));


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');

prompt = {'Enter var name for landmarks:'};
title = 'LM -> work space';
lines = 1;

if numel(Data.landmark_filename)==0
    proposed_varname=['landmark(1).data'];
    try
       existing_landmarks = evalin('base', 'landmark;');
       %if iscell(existing_landmarks)
           proposed_varname=['landmark(',num2str(numel(existing_landmarks))+1,').data'];
       %end;
    catch err

    end
else
    proposed_varname=Data.landmark_filename;
end;

def = {proposed_varname,};
answer = inputdlg(prompt, title, lines, def);
if numel(answer)>0
    Data.landmark_filename=answer{1};
    set(handles.MainFigure,'UserData',Data);
    landmarks.subindx=Data.labelSub';
    landmarks.indx=Data.labelIndx;
    landmarks.Names=Data.labelNames;
    

    assignin('base','tmp______',landmarks);
    evalin('base',[answer{1},'=','tmp______;']);
    evalin('base','clear tmp______;');
        %assignin('base', answer{1},landmarks);
    %evalin('base',[answer{1},'=','landmarks']);
    dmat=distmat(landmarks.subindx,landmarks.subindx);
    dmat=dmat+(max(dmat(:))+1)*eye(size(dmat));
    duplicate_lables=find(min(dmat)==0);
    if numel(duplicate_lables)>0
        fprintf('WARNING: labels sharing the same position(s):\n');
        for a=1:numel(duplicate_lables),
            fprintf('label : %d %s\n',duplicate_lables(a),landmarks.Names(duplicate_lables(a)).name);
        end;
    end;
    fprintf('saving landmarks in variable: %s\n',answer{1});
end;


% --------------------------------------------------------------------
function ListDel_Callback(hObject, eventdata, handles)
% hObject    handle to ListDel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


Data=get(handles.MainFigure,'UserData');
num_labels=size(Data.labelSub,2);
if num_labels<1
    return;
end;

LabelList_Callback(hObject, eventdata, handles);
DelLandmark_Callback(hObject, eventdata, handles);



% --------------------------------------------------------------------
function EditLandmarks2_Callback(hObject, eventdata, handles)
% hObject    handle to EditLandmarks2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in showlabels.
function showlabels_Callback(hObject, eventdata, handles)
% hObject    handle to showlabels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showlabels
Data=get(handles.MainFigure,'UserData');
 harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     


% --- Executes on key release with focus on MainFigure and none of its controls.
function MainFigure_KeyReleaseFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was released, in lower case
%	Character: character interpretation of the key(s) that was released
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) released
% handles    structure with handles and user data (see GUIDATA)


if strcmp(eventdata.Key,'shift')
    Data=get(handles.MainFigure,'UserData');   
    Data.isShift=0;
    set(handles.MainFigure,'UserData',Data);   
end;

% --- Executes on key press with focus on MainFigure and none of its controls.
function MainFigure_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to MainFigure (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

if strcmp(eventdata.Key,'shift')
    Data=get(handles.MainFigure,'UserData');   
    Data.isShift=1;
    set(handles.MainFigure,'UserData',Data);   
end;


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2
contents = cellstr(get(hObject,'String')) ;
Data=get(handles.MainFigure,'UserData');
Data.zoom_level=str2double(contents{get(hObject,'Value')});
set(handles.MainFigure,'UserData',Data);
 harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     



% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function SelLandmark2_Callback(hObject, eventdata, handles)
% hObject    handle to SelLandmark2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%curent_pos=Data.slice
Data=get(handles.MainFigure,'UserData');
Data.isZooming=1;
set(handles.MainFigure,'UserData',Data);
SelLandmark_Callback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function Zoom_Callback(hObject, eventdata, handles)
% hObject    handle to Zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');
Data.isZooming=1;
set(handles.MainFigure,'UserData',Data);
 harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     


% --------------------------------------------------------------------
function Unselect_Callback(hObject, eventdata, handles)
% hObject    handle to Unselect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Data=get(handles.MainFigure,'UserData');
Data.selected_label=-1;
set(handles.SelectedLabel,'String',num2str(Data.selected_label));
set(handles.MainFigure,'UserData',Data);
 harray=[handles.axesX,handles.axesY,handles.axesZ];
    drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
    drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
    drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));     


% --------------------------------------------------------------------
function RenameLabel_Callback(hObject, eventdata, handles)
% hObject    handle to RenameLabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');
num_labels=size(Data.labelSub,2);
if num_labels<1
    return;
end;


if Data.selected_label~=-1
    prompt = {'Name the landmark:'};
    title = ':-)';
    lines = 1;
    def = {Data.labelNames(Data.selected_label).name};
    answer = inputdlg(prompt, title, lines, def);
        if numel(answer)>0
            Data.labelNames(Data.selected_label).name=answer{1};

            label_text={};
            for a=1:num_labels,
                  label_text{a}=getLabelTxt(a,Data.labelSub(:,a)',Data.labelNames(a).name);
            end;
            set(handles.LabelList,'String',label_text);  
            %set(handles.LabelList,'Value',min(numel(label_text),Data.selected_label+1));  

            set(handles.MainFigure,'UserData',Data);

            harray=[handles.axesX,handles.axesY,handles.axesZ];
            drawData(Data,harray(1),'dim',1,'slice',Data.slice(1));
            drawData(Data,harray(2),'dim',2,'slice',Data.slice(2));
            drawData(Data,harray(3),'dim',3,'slice',Data.slice(3));         
        end;
end;


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Unselect_Callback(hObject, eventdata, handles);


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Data=get(handles.MainFigure,'UserData');
if (Data.selected_label~=-1)&&(Data.selected_label>1)
    tmp=Data.labelSub(:,Data.selected_label-1);
    Data.labelSub(:,Data.selected_label-1)=Data.labelSub(:,Data.selected_label);
    Data.labelSub(:,Data.selected_label)=tmp;
    
    tmp=Data.labelIndx(Data.selected_label-1);
    Data.labelIndx(Data.selected_label-1)=Data.labelIndx(Data.selected_label);
    Data.labelIndx(Data.selected_label)=tmp;
    
    tmp=Data.labelNames(Data.selected_label-1);
    Data.labelNames(Data.selected_label-1)=Data.labelNames(Data.selected_label);
    Data.labelNames(Data.selected_label)=tmp;   
    
    
    LabelTxt = cellstr(get(handles.LabelList,'String'));
    %tmp=LabelTxt{Data.selected_label-1};
    %LabelTxt{Data.selected_label-1}=LabelTxt{Data.selected_label};
    %LabelTxt{Data.selected_label}=tmp;
    
    LabelTxt{Data.selected_label-1}=getLabelTxt(Data.selected_label-1,Data.labelSub(:,Data.selected_label-1)',Data.labelNames(Data.selected_label-1).name);
    LabelTxt{Data.selected_label}=getLabelTxt(Data.selected_label,Data.labelSub(:,Data.selected_label)',Data.labelNames(Data.selected_label).name);

    
    set(handles.LabelList,'String',LabelTxt);
    set(handles.LabelList,'Value',Data.selected_label-1);
    
    Data.selected_label=Data.selected_label-1;
    set(handles.SelectedLabel,'String',num2str(Data.selected_label));            
    
    set(handles.MainFigure,'UserData',Data);
    
end;


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




Data=get(handles.MainFigure,'UserData');
if (Data.selected_label~=-1)&&(Data.selected_label<numel(Data.labelIndx))
    tmp=Data.labelSub(:,Data.selected_label+1);
    Data.labelSub(:,Data.selected_label+1)=Data.labelSub(:,Data.selected_label);
    Data.labelSub(:,Data.selected_label)=tmp;
    
    tmp=Data.labelIndx(Data.selected_label+1);
    Data.labelIndx(Data.selected_label+1)=Data.labelIndx(Data.selected_label);
    Data.labelIndx(Data.selected_label)=tmp;
    
    tmp=Data.labelNames(Data.selected_label+1);
    Data.labelNames(Data.selected_label+1)=Data.labelNames(Data.selected_label);
    Data.labelNames(Data.selected_label)=tmp;   
    
    
    LabelTxt = cellstr(get(handles.LabelList,'String'));
    %tmp=LabelTxt{Data.selected_label+1};
    
    LabelTxt{Data.selected_label+1}=getLabelTxt(Data.selected_label+1,Data.labelSub(:,Data.selected_label+1)',Data.labelNames(Data.selected_label+1).name);
    LabelTxt{Data.selected_label}=getLabelTxt(Data.selected_label,Data.labelSub(:,Data.selected_label)',Data.labelNames(Data.selected_label).name);
    
    %LabelTxt{Data.selected_label+1}=LabelTxt{Data.selected_label};
    %LabelTxt{Data.selected_label}=tmp;
    
    
    set(handles.LabelList,'String',LabelTxt);
    set(handles.LabelList,'Value',Data.selected_label+1);
    
    Data.selected_label=Data.selected_label+1;
    set(handles.SelectedLabel,'String',num2str(Data.selected_label));            
    
    set(handles.MainFigure,'UserData',Data);
    
end;


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over Yp.
function Yp_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to Yp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
