function [detections,Hdet,H]=sta_loacalmax(H,threshold)

SE=ones(3,3,3);
SE(2,2,2)=0;


if nargin<2
    threshold=min(H(:))+eps;
end

Hdet = (H > imdilate(H, SE));
detections_idx=find(Hdet(:));
[pX,pY,pZ]=ind2sub(size(H),detections_idx);
detections=[pX,pY,pZ,H(detections_idx)];
tindx=detections(:,end)>threshold;
detections=detections(tindx,:)';
[v,indx]=sort(detections(4,:));
detections=detections(:,indx);

Hdet=(H.*Hdet)>threshold;