function stafield_figure(ifield,varargin)

c=1;
m=0;
dim=3;

for k = 1:2:length(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

if ~exist('layer')
    layer=ceil(ifield.shape(dim)/2);
end;

m1=ifield.L-m+1;

switch (dim)
    case 1
        img=ifield.data(c,m1,layer,:,:);
        %imshow(squeeze(ifield.data(c,m1,layer,:,:)),[min(ifield.data(:)),max(ifield.data(:))],'InitialMagnification','fit');
    case 2
        img=ifield.data(c,m1,:,layer,:);
        %imshow(squeeze(ifield.data(c,m1,:,layer,:)),[min(ifield.data(:)),max(ifield.data(:))],'InitialMagnification','fit');        
    case 3
        img=ifield.data(c,m1,:,:,layer);
        %imshow(squeeze(ifield.data(c,m1,:,:,layer)),[min(ifield.data(:)),max(ifield.data(:))],'InitialMagnification','fit');
end
imshow(squeeze(img),[min(img(:))-eps,max(img(:))+eps],'InitialMagnification','fit');

if c==1
    title(['[rank: ',num2str(ifield.L),', real. of m=',num2str(m),']']);
else
    title(['[rank: ',num2str(ifield.L),', imag. of m=',num2str(m),']']);
end;