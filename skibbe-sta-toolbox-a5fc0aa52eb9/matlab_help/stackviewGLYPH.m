%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  data  - data array with size [numdirs w h d]
%  dirs - dirs of size [3 numdirs]
%  back - background to be displayed of size [w h d]
%  mask - mask of sz [w h d]
%  params - [mean scaling gamma] (see showODF below)
%  init (optional) - two component vector 
%                 init(1) = 1,2 or 3 (which cutplane to start with x,y,z ~ 1,2,3)
%                 init(2) = which slice to start with
%



function stackview(data,dirs,back,mask,params,init)


ds.signal = data;
ds.dirs = dirs;
ds.back = back;
ds.mask = mask;

f = gcf;
clf;


set(f,'WindowButtonDownFcn',{@mousepressed_Callback,ds});
set(f,'ToolBar','figure');

h = uicontrol('Style','slider','String','Surf','Position',[0,0,300,25],'Tag','theslider','Value',0.5,'Callback',{@resolutionslider_Callback,gcbo,[],[],ds,params});
botPanel = uibuttongroup('Units','pixels','Position',[400 0  200 30],'SelectionChangeFcn',{@radiob_Callback,gcbo,ds},'Tag','butgroup');
hX = uicontrol('Style','radiobutton','String','X','Position',[10,1,40,25],'Tag','1','Parent',botPanel);
hY = uicontrol('Style','radiobutton','String','Y','Position',[60,1,40,25],'Tag','2','Parent',botPanel);
hZ = uicontrol('Style','radiobutton','String','Z','Position',[110,1,40,25],'Tag','3','Parent',botPanel);


coord = uicontrol('Style','text','String','x:1','Tag','coordinfo','Position',[300,0,100,25]);

if exist('init'),
    if init(1) == 1,
        set(botPanel,'SelectedObject',hX);
        radiob_Callback(botPanel,[],[],ds,false);
    elseif init(1) == 2,
        set(botPanel,'SelectedObject',hY);
        radiob_Callback(botPanel,[],[],ds,false);
        
    elseif init(1) == 3,
        set(botPanel,'SelectedObject',hZ);
        radiob_Callback(botPanel,[],[],ds,false);

    end;
    if length(init) > 1,
        set(h,'Value',init(2));
    end;
else
    set(botPanel,'SelectedObject',hX);
    radiob_Callback(botPanel,[],[],ds,false);  
end;

showslice(ds,params);

% --------------------------------------------------------------------
function varargout = mousepressed_Callback(h, eventdata,varargin)
    ds = varargin{1};
%    sz = size(img);
    p = round(get(gca,'CurrentPoint')); p = p(1,1:2);
    hc = findobj(gcf,'Tag','haircross');
    delete(hc);
    rectangle('Position',[p-0.5 1 1],'Tag','haircross','edgecolor',[1 1 0]);

    showTS(p,ds);
    
function showTS(p,ds)
    f = gcf;
    pf = get(f,'Userdata');
    if isempty(pf) | not(ishandle(pf)),
        pf = figure('Toolbar','figure');
        set(f,'Userdata',pf);
    end;
    
    figure(f);
    slider = findobj(gcf,'Tag','theslider');
    pos = get(slider,'value');
    botpanel = findobj(gcf,'Tag','butgroup');
    radiob = get(botpanel,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    idxpos = round(pos);
    cinfo = findobj(gcf,'Tag','coordinfo');
    
    sz = size(ds.mask);
    
    figure(pf); 
   switch selection,
        case 3,
            if p(2) <= sz(1) & p(2) > 0 & p(1) <= sz(2) & p(1) > 0,
                myplot(squeeze(ds.signal(:,p(2),p(1),idxpos)),ds,3);
                title(sprintf('position (%i,%i,%i)',p(2),p(1),idxpos));
            end;
        case 2,
            if p(2) <= sz(1) & p(2) > 0 & p(1) <= sz(3) & p(1) > 0,            
                myplot(squeeze(ds.signal(:,p(2),idxpos,p(1))),ds,2);
                title(sprintf('position (%i,%i,%i)',p(2),idxpos,p(1)));

            end;
        case 1,
            if p(2) <= sz(2) & p(2) > 0 & p(1) <= sz(3) & p(1) > 0,            
                myplot(squeeze(ds.signal(:,idxpos,p(2),p(1))),ds,1);
                title(sprintf('position (%i,%i,%i)',idxpos,p(2),p(1)));

            end;
            
    end;    
      figure(f);
      
function myplot(f,ds,mode)
    clf; 
    [bDir K] = computeConvHull(ds.dirs',mode); 
    n = length(f);
    bDir = bDir(:,[2 1 3]);
    subplot(2,1,1);

    if sum(f>0) > 0,
        cmap = hot(128);    
        fpos = f.*(f>0);
        valDirPos = bDir.* (fpos*ones(1,3));       
        rgbfpos = cmap(round(127*fpos/max(fpos))+1,:);
        patch('Faces',K,'Vertices',valDirPos,'facecolor','interp','edgecolor','none','FaceVertexCData',rgbfpos);
    end;

    if sum(f<0) > 0,
        cmap = winter(128);
        fneg = -f.*(f<0);
        valDirNeg = bDir.* (fneg*ones(1,3));     
        rgbfneg = cmap(round(127*fneg/max(fneg))+1,:);   
        patch('Faces',K,'Vertices',valDirNeg,'facecolor','interp','edgecolor','none','FaceVertexCData',rgbfneg);
    end;
    
    axis equal; grid on;
    axis([-1 1 -1 1 -1 1]*max(abs(f)));
    view(0,90)
    
    str = sprintf('mean: %.2f  std: %.2f   norm: %.2f   GFA: %.2f   posratio: %.2f  minmax (%.2f,%.2f)',mean(f),std(f),norm(f),std(f)/norm(f), sum(f>0)/length(f),min(f),max(f));
    uicontrol('style','text','position',[0 0 650 20],'string',str,'BackgroundColor',[1 1 1]);
    
    subplot(2,1,2);
    phi = 0:0.05:(2*pi);
    [m idx] = (max((abs(bDir* [cos(phi) ; sin(phi) ; zeros(1,length(phi))]))));
    plot(phi,f(idx)); hold on;
    [m idx] = (max((abs(bDir* [cos(phi) ; zeros(1,length(phi));sin(phi) ]))));
    plot(phi,f(idx),'r');
    [m idx] = (max((abs(bDir* [zeros(1,length(phi)) ; cos(phi) ; sin(phi) ]))));
    plot(phi,f(idx),'g');

  %  axis([0 max(phi) 0 1]); hold off;
   
    grid on;
    return
% --------------------------------------------------------------------
function varargout = resolutionslider_Callback(h, eventdata, handles, varargin)
  
    img = varargin{3};
    showslice(img,varargin{4});
    
% --------------------------------------------------------------------
function varargout = radiob_Callback(h, eventdata, handles, varargin)
    data = varargin{1};
     
    radiob = get(h,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    
    
    mask = data.mask;
    init_slider(size(mask,selection));
 
    slider = findobj(gcf,'Tag','theslider');
    cb = get(slider,'Callback');
    
    if ~(numel(varargin)>1) 
        showslice(data,cb{6});
    end;
    
    
function init_slider(maxs)
    slider = findobj(gcf,'Tag','theslider');
    set(slider,'SliderStep',[1/maxs 0.2]);
    set(slider,'Max',maxs);
    set(slider,'Min',0.99);
    set(slider,'Value',round(maxs/2));

    
    
function showslice(ds,params);

   
    slider = findobj(gcf,'Tag','theslider');
    pos = get(slider,'value');
    botpanel = findobj(gcf,'Tag','butgroup');
    radiob = get(botpanel,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    idxpos = round(pos);
    cinfo = findobj(gcf,'Tag','coordinfo');
    
    backgr = ds.back;
   
    switch selection,
        case 3,
            showODF(3,ds,squeeze(ds.signal(:,:,:,idxpos)),squeeze(ds.mask(:,:,idxpos)),squeeze(backgr(:,:,idxpos)),params(1),params(2),params(3));
             set(cinfo,'String',sprintf('%i/%i',idxpos,size(ds.back,3)));    
         case 2,           
            showODF(2,ds,squeeze(ds.signal(:,:,idxpos,:)),squeeze(ds.mask(:,idxpos,:)),squeeze(backgr(:,idxpos,:)),params(1),params(2),params(3));
             set(cinfo,'String',sprintf('%i/%i',idxpos,size(ds.back,2)));    
        case 1,                       
            showODF(1,ds,squeeze(ds.signal(:,idxpos,:,:)),squeeze(ds.mask(idxpos,:,:)),squeeze(backgr(idxpos,:,:)),params(1),params(2),params(3));            
            set(cinfo,'String',sprintf('%i/%i',idxpos,size(ds.back,1)));    
    end;
 
  %  set(gca,'Position',[0 0 1 1])
 
  
function  [bDir K] = computeConvHull(bDir,mode)

K = convhulln(double(bDir));

switch mode,
    case 1,
        bDir = bDir(:,[2 3 1]);
        %points = single([Z(:)' ; X(:)' ; Y(:)']);
    case 2,
        bDir = bDir(:,[1 3 2]);
        %points = single([X(:)' ; Z(:)' ; Y(:)']);
    case 3,
        bDir = bDir;
       %points = single([X(:)' ; Y(:)' ; Z(:)']);
end;
    
function showODF(mode,ds,data,mask,back,meanval,scval,gamma)

delete(get(gca,'Children'))

[bDir K] = computeConvHull(ds.dirs',mode);

[Xs Ys] = ndgrid(1:size(mask,1)+1,1:size(mask,2)+1);

idx = find(mask>0);
data = data(:,idx);

[ix iy] = ind2sub(size(mask),idx);

allFaces = [];

bDir = bDir(:,[2 1 3]);

for k = 1:length(idx),
    

    
    C = double(data(:,k));
    C = C - meanval;    
    if (gamma ~= 1)  
        if (gamma >0) 
            C = sign(C).*abs(C).^gamma;
        end;
    end;
    if isnan(scval),
        C = C /max(C(:));
    else,
        C = scval*C;
    end;
    
    if (gamma < 0)  
            C = sign(C).*abs(C).^(-gamma);
    end;
    R = abs(C);



    valDir = 0.5*bDir.* (R*ones(1,3));
    valDir(:,1) = valDir(:,1)+iy(k);
    valDir(:,2) = valDir(:,2)+ix(k);
    valDir(:,3) = valDir(:,3) -1;

    allFaces{k} = K+length(C)*(k-1);
    allVerts{k} = valDir;
    allData{k} = C;
    
end;


toshow(:,:,1) = double(back / (0.001+max(back(:))))*0.8;
toshow(:,:,2) = toshow(:,:,1);
toshow(:,:,3) = toshow(:,:,1);

 %surface(Ys-0.5,Xs-0.5,Xs*0-1.5,toshow,'EdgeColor','none', 'FaceLighting', 'none', 'EdgeLighting', 'none','CDataMapping','scaled','VertexNormals',[-1 0 0],'NormalMode','manual')
 %surface(Ys-0.5,Xs-0.5,Xs*0-1.5,toshow,'EdgeColor','none', 'FaceLighting', 'none', 'EdgeLighting', 'none','CDataMapping','scaled','NormalMode','manual')
 surface(Ys-0.5,Xs-0.5,Xs*0-1.5,toshow,'EdgeColor','none', 'FaceLighting', 'none', 'EdgeLighting', 'none','CDataMapping','scaled')
            
           

if not(isempty(allFaces)),
    patch('Faces',cat(1,allFaces{:}),'Vertices',cat(1,allVerts{:}),'facecolor','interp','edgecolor','none','FaceVertexCData',cat(1,allData{:}),'CDataMapping','scaled');
end;
 
axis([1 size(toshow,2) 1 size(toshow,1)])
  
%   axis equal    
%axis vis3d
%axis off;
%axis([1 size(mask,1) 1 size(mask,2)]);
     
            