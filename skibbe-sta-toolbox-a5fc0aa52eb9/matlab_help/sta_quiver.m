function sta_quiver(ofield,varargin)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   sta_quiver(ofield,paramname,paramvalue,....)
%
%   plots the stafield ofield in quiver-style,
%   parameters are 
%        showfun -- function used for displying background
%        scale -- scaling a quivers (same as for original quiver parameter s)
%        nummax -- number of maximas displayed as quivs
%        nummin -- number of minias displayed as quivs
%        interp -- true, for accurate estimation if direction via quadform fit
%        linewidth -- width of quivers
%
%






showfun = @imagesc;
scale =0; 
nummax = 2;
nummin = 0;
interp = true;
linewidth = 1;
init_view = 1;
init_slice = [];

for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;



if strcmp(ofield.type,'STA_OFIELD_FULL'),
    step = 1;
elseif strcmp(ofield.type,'STA_OFIELD_EVEN'),
    step = 2;
elseif strcmp(ofield.type,'STA_OFIELD_ODD'),
    step = 2;
elseif strcmp(ofield.type,'STA_OFIELD_SINGLE'),
    if ofield.L == 1,
        ofield.type = 'STA_OFIELD_ODD';
        step = 2;
    else
        warning('unsupported');
        return;
    end    
end;

[arrowsmax SHmat bDir] = getLocalMax(ofield.data,ofield.L,nummax,interp,ofield.type,ofield.storage);
img = squeeze(sum(arrowsmax(1:3,:,:,:).^2));     
scfac = sqrt(1/max(sum(arrowsmax(:,:).^2)))*0.7;
arrowsmax = arrowsmax *scfac;
arrows = {arrowsmax};
if nummin > 0,
    arrowsmin = getLocalMax(-ofield.data,ofield.L,nummax,interp,ofield.type,ofield.storage);
    arrowsmin = arrowsmin * scfac;
    arrows = [arrows {arrowsmin} ];
end;


if  strcmp(ofield.type,'STA_OFIELD_FULL') |  strcmp(ofield.type,'STA_OFIELD_ODD'),
    quiverfun = @(x,y,col,mf) quiverfunasym(x,y,col,scale,linewidth,mf);
else,
    quiverfun = @(x,y,col,mf) quiverfunsym(x,y,col,scale,linewidth,mf);
end;
    


%f = figure('Position',[360,500,750,585]);    
f = gcf;
clf;

set(f,'WindowButtonDownFcn',{@mousepressed_Callback,ofield,conj(SHmat),bDir});

h = uicontrol('Style','slider','String','Surf','Position',[0,0,300,25],'Tag','theslider','Value',0.5,'Callback',{@resolutionslider_Callback,gcbo,[],[],{img,arrows},{showfun,quiverfun}});
botPanel = uibuttongroup('Units','pixels','Position',[400 0  200 30],'SelectionChangeFcn',{@radiob_Callback,gcbo,{img,arrows}},'Tag','butgroup');
hX = uicontrol('Style','radiobutton','String','X','Position',[10,1,40,25],'Tag','1','Parent',botPanel);
hY = uicontrol('Style','radiobutton','String','Y','Position',[60,1,40,25],'Tag','2','Parent',botPanel);
hZ = uicontrol('Style','radiobutton','String','Z','Position',[110,1,40,25],'Tag','3','Parent',botPanel);
coord = uicontrol('Style','text','String','x:1','Tag','coordinfo','Position',[300,0,100,25]);




if exist('init_view'),
    if init_view == 1,
        set(botPanel,'SelectedObject',hX);
        radiob_Callback(botPanel,[],[],{img,arrows});
    elseif init_view == 2,
        set(botPanel,'SelectedObject',hY);
        radiob_Callback(botPanel,[],[],{img,arrows});        
    elseif init_view == 3,
        set(botPanel,'SelectedObject',hZ);
        radiob_Callback(botPanel,[],[],{img,arrows});

    end;
    if not(isempty(init_slice)),
        set(h,'Value',init_slice);
    end;
else
    set(botPanel,'SelectedObject',hX);
    radiob_Callback(botPanel,[],[],{img,arrows});  
end;


showslice({img,arrows},showfun,quiverfun);






% --------------------------------------------------------------------
function varargout = resolutionslider_Callback(h, eventdata, handles, varargin)
  
    img = varargin{3};
    showslice(img,varargin{4}{1},varargin{4}{2});
    
% --------------------------------------------------------------------
function varargout = radiob_Callback(h, eventdata, handles, varargin)
    data = varargin{1};
     
    radiob = get(h,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    
    slider = findobj(gcf,'Tag','theslider');
    init_slider(size(data{1},selection));         
    cb = get(slider,'Callback');
        
    showslice(data,cb{6}{1},cb{6}{2});
    
function init_slider(maxs)
    slider = findobj(gcf,'Tag','theslider');
    set(slider,'SliderStep',[1/maxs 0.2]);
    set(slider,'Max',maxs);
    set(slider,'Min',0.99);
    set(slider,'Value',round(maxs/2));

    
    
    
function showslice(data,showfun,quiverfun);

    img = data{1};
    allarrows = data{2};

    slider = findobj(gcf,'Tag','theslider');
    pos = get(slider,'value');
    botpanel = findobj(gcf,'Tag','butgroup');
    radiob = get(botpanel,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    idxpos = round(pos);   
    cinfo = findobj(gcf,'Tag','coordinfo');

    q = size(img,4);
    switch selection,
        case 3,
            showfun(squeeze(img(:,:,idxpos,:))); 
            set(cinfo,'String',sprintf('%i/%i',idxpos,size(img,3)));    
        case 2,
            showfun(reshape(img(:,idxpos,:,:),[size(img,1) size(img,3) q])); 
            set(cinfo,'String',sprintf('%i/%i',idxpos,size(img,2)));    
        case 1,
            showfun(reshape(img(idxpos,:,:,:),[size(img,2) size(img,3) q]));
            set(cinfo,'String',sprintf('%i/%i',idxpos,size(img,1)));    
    end;
    hold on;
    arrows = allarrows{1};
    for k = (size(arrows,1)/3)-1:-1:0,
        switch selection,
            case 3,
                arrow_a = reshape(arrows(1+3*k,:,:,idxpos),[size(img,1) size(img,2) ]);
                arrow_b = reshape(arrows(2+3*k,:,:,idxpos),[size(img,1) size(img,2) ]);       
            case 2,
                arrow_a = reshape(arrows(1+3*k,:,idxpos,:),[size(img,1) size(img,3) ]);
                arrow_b = reshape(arrows(3+3*k,:,idxpos,:),[size(img,1) size(img,3) ]);       
            case 1,
                arrow_a = reshape(arrows(2+3*k,idxpos,:,:),[size(img,2) size(img,3) ]);
                arrow_b = reshape(arrows(3+3*k,idxpos,:,:),[size(img,2) size(img,3)]);       
        end;
        quiverfun(arrow_b,arrow_a,k+1,0); 
    end;
    if length(allarrows) > 1,
        arrows = allarrows{2};
        for k = (size(arrows,1)/3)-1:-1:0,
            switch selection,
                case 3,
                    arrow_a = reshape(arrows(1+3*k,:,:,idxpos),[size(img,1) size(img,2) ]);
                    arrow_b = reshape(arrows(2+3*k,:,:,idxpos),[size(img,1) size(img,2) ]);       
                case 2,
                    arrow_a = reshape(arrows(1+3*k,:,idxpos,:),[size(img,1) size(img,3) ]);
                    arrow_b = reshape(arrows(3+3*k,:,idxpos,:),[size(img,1) size(img,3) ]);       
                case 1,
                    arrow_a = reshape(arrows(2+3*k,idxpos,:,:),[size(img,2) size(img,3) ]);
                    arrow_b = reshape(arrows(3+3*k,idxpos,:,:),[size(img,2) size(img,3)]);       
            end;
            quiverfun(arrow_b,arrow_a,k+1,1); 
        end;
    end;
    
    colormap gray
    hold off;
    axis xy;
  
    
 function quiverfunsym(arrow_b,arrow_a,col,scale,linewidth,minflag),
     c = 'rgbymc';
     if not(minflag),
         quiver(arrow_b,arrow_a,scale,['' c(col)],'linewidth',linewidth);
         quiver(-arrow_b,-arrow_a,scale,['' c(col)],'linewidth',linewidth);
     else,
         quiver(arrow_b,arrow_a,scale,['x' c(col)],'linewidth',linewidth);
         quiver(-arrow_b,-arrow_a,scale,['x' c(col)],'linewidth',linewidth);         
     end;
  
 function quiverfunasym(arrow_b,arrow_a,col,scale,linewidth,minflag),
     c = 'rgbymc';
     if not(minflag),
         quiver(arrow_b,arrow_a,scale,['' c(col)],'linewidth',linewidth);      
     else,
         quiver(arrow_b,arrow_a,scale,['.' c(col)],'linewidth',linewidth);       
     end;
     
    
    
            
     
function varargout = mousepressed_Callback(h, eventdata,ofield,SHmat,bDir)
  

    p = round(get(gca,'CurrentPoint')); p = p(1,1:2);
    hc = findobj(gcf,'Tag','haircross');
    delete(hc);
    rectangle('Position',[p-0.5 1 1],'Tag','haircross','edgecolor',[1 1 0]);

    showTS(p,ofield,SHmat,bDir);  
    
            
    

function showTS(p,ofield,SHmat,bDir)
    f = gcf;
    pf = get(f,'Userdata');
    if isempty(pf) | not(ishandle(pf)),
        pf = figure('Toolbar','figure');
        set(f,'Userdata',pf);
    end;
    
    figure(f);
    slider = findobj(gcf,'Tag','theslider');
    pos = get(slider,'value');
    botpanel = findobj(gcf,'Tag','butgroup');
    radiob = get(botpanel,'SelectedObject');
    selection = str2num(get(radiob,'Tag'));    
    idxpos = round(pos);
    cinfo = findobj(gcf,'Tag','coordinfo');

    sz = ofield.shape;
    
    figure(pf); 
   switch selection,
        case 3,
            if p(2) <= sz(1) & p(2) > 0 & p(1) <= sz(2) & p(1) > 0,
                data = real(squeeze(ofield.data(1,:,p(2),p(1),idxpos)+i*ofield.data(2,:,p(2),p(1),idxpos))*SHmat);                
                myplot(data',bDir,3);
                title(sprintf('position (%i,%i,%i)',p(2),p(1),idxpos));
            end;
        case 2,
            if p(2) <= sz(1) & p(2) > 0 & p(1) <= sz(3) & p(1) > 0,      
                data = real(squeeze(ofield.data(1,:,p(2),idxpos,p(1))+i*ofield.data(2,:,p(2),idxpos,p(1)))*SHmat);
                myplot(data',bDir,2);               
                title(sprintf('position (%i,%i,%i)',p(2),idxpos,p(1)));
            end;
        case 1,
            if p(2) <= sz(2) & p(2) > 0 & p(1) <= sz(3) & p(1) > 0,   
                data = real(squeeze(ofield.data(1,:,idxpos,p(2),p(1))+i*ofield.data(2,:,idxpos,p(2),p(1)))*SHmat);
                myplot(data',bDir,1);             
                title(sprintf('position (%i,%i,%i)',idxpos,p(2),p(1)));
            end;
            
    end;    
      figure(f);
      
function myplot(f,dirs,mode)
    clf; 
    [bDir K] = computeConvHull(dirs',mode); 
    n = length(f);
    bDir = bDir(:,[2 1 3]); bDir(:,1) = -bDir(:,1);
    subplot(2,1,1);

    if sum(f>0) > 0,
        cmap = hot(128);    
        fpos = f.*(f>0);
        valDirPos = bDir.* (fpos*ones(1,3));       
        rgbfpos = cmap(round(127*fpos/max(fpos))+1,:);
        patch('Faces',K,'Vertices',valDirPos,'facecolor','interp','edgecolor','none','FaceVertexCData',rgbfpos);
    end;

    if sum(f<0) > 0,
        cmap = winter(128);
        fneg = -f.*(f<0);
        valDirNeg = bDir.* (fneg*ones(1,3));     
        rgbfneg = cmap(round(127*fneg/max(fneg))+1,:);   
        patch('Faces',K,'Vertices',valDirNeg,'facecolor','interp','edgecolor','none','FaceVertexCData',rgbfneg);
    end;
    
    axis equal; grid on;
    axis([-1 1 -1 1 -1 1]*max(abs(f)));
    view(0,90)
    
    str = sprintf('mean: %.2f  std: %.2f   norm: %.2f   GFA: %.2f   posratio: %.2f  minmax (%.2f,%.2f)',mean(f),std(f),norm(f),std(f)/norm(f), sum(f>0)/length(f),min(f),max(f));
    uicontrol('style','text','position',[0 0 650 20],'string',str,'BackgroundColor',[1 1 1]);
    
    subplot(2,1,2);
    phi = 0:0.05:(2*pi);
    [m idx] = (max((abs(bDir* [cos(phi) ; sin(phi) ; zeros(1,length(phi))]))));
    plot(phi,f(idx)); hold on;
    [m idx] = (max((abs(bDir* [cos(phi) ; zeros(1,length(phi));sin(phi) ]))));
    plot(phi,f(idx),'r');
    [m idx] = (max((abs(bDir* [zeros(1,length(phi)) ; cos(phi) ; sin(phi) ]))));
    plot(phi,f(idx),'g');

  %  axis([0 max(phi) 0 1]); hold off;
   
    grid on;
    return    
   
  
function  [bDir K] = computeConvHull(bDir,mode)

K = convhulln(double(bDir));

switch mode,
    case 1,
        bDir = bDir(:,[2 3 1]);
        %points = single([Z(:)' ; X(:)' ; Y(:)']);
    case 2,
        bDir = bDir(:,[1 3 2]);  bDir(:,1) = -bDir(:,1);
        %points = single([X(:)' ; Z(:)' ; Y(:)']);
    case 3,
        bDir = bDir;
       %points = single([X(:)' ; Y(:)' ; Z(:)']);
end; 
          




function result = iff(condition,trueResult,falseResult)
  error(nargchk(3,3,nargin));
  if condition
    result = trueResult;
  else
    result = falseResult;
  end

    
            