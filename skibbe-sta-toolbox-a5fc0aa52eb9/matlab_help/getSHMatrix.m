
    
function [M lidx]=getSHMatrix(L,samples,type,storage,contravariant,normalization) 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  
%
%


if nargin<6
    normalization='inner_product';
end;

start = 0;
step = 1;

if strcmp(type,'STA_OFIELD_FULL'),
    start = 0;
    step = 1;
elseif strcmp(type,'STA_OFIELD_EVEN'),
    start = 0;
    step = 2;
elseif strcmp(type,'STA_OFIELD_ODD'),
    start = 1;
    step = 2;
elseif strcmp(type,'STA_OFIELD_SINGLE'),
    start = L;
    step = 1;
else
    warning('unsupported type!');
    return;
end;

if strcmp(storage,'STA_FIELD_STORAGE_R'),
    realsym = 1;
elseif strcmp(storage,'STA_FIELD_STORAGE_C'),
    realsym = 0;
else
    warning('unsupported type!');
    return;
end;


index=1;
samples = double(samples);

for l=start:step:L,
    uplim = l;
    if realsym == 1,
        uplim = 0;
    end;
    for m=-l:uplim,
        for n=1:size(samples,2),
            if contravariant,
                switch normalization
                    case 'inner_product'
                        sh = (2*l+1)*conj(sta_sphericalHarmonic(l,m,samples(1,n),samples(2,n),samples(3,n))) / ( size(samples,2) );
                    case 'stensor'
                        sh = (2*l+1)*conj(sta_sphericalHarmonic(l,m,samples(1,n),samples(2,n),samples(3,n))) ;
                    case 'ntrace'
                        sh = (2*l+1)*conj(sta_sphericalHarmonic(l,m,samples(1,n),samples(2,n),samples(3,n))) ;    
                        
                end;
            else
                sh = sta_sphericalHarmonic(l,m,samples(1,n),samples(2,n),samples(3,n));
            end;
            
            switch normalization
            case 'inner_product'
                if realsym == 1 && m ~= 0,
                    if contravariant,                   
                        sh = sh*2;
                    else
                        sh = sh;
                    end;
                end;
            case 'ntrace'
                if realsym == 1 && m ~= 0,
                    if contravariant,                   
                        sh = sh*2;
                    else
                        sh = sh;
                    end;
                end;    
            end;

            M(index,n)=sh;
            lidx(index) = l;
        end;
        index=index+1;
    end;
end;    


return;