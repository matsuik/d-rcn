/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox for Matlab
 * 
 *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox for Matlab
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

#include "mex.h"
#include "stensor.h"



template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nlhs>1)
        mexErrMsgTxt("lhs!=1");
    if ((nrhs!=4)&&(nrhs!=5))
        mexErrMsgTxt("rhs=(l,m,theta,phi) or rhs=(l,m,x,y,z)\\n");

    T parameters[5];
    mxClassID cid=mxUNKNOWN_CLASS;


    for (int i=0;i<nrhs;i++)
    {
        const mxArray *ST1;
        ST1 = prhs[i];
        //const int *dimsST1 = mxGetDimensions(ST1);
        const int numdimST1 = mxGetNumberOfDimensions(ST1);
        if (numdimST1!=2)
            mexErrMsgTxt("error: Array dimension missmatch\n");
        T *pST1 = (T*) (mxGetData(ST1));
        parameters[i]=(T)(*pST1);
        cid=mxGetClassID(ST1);
    }


    if (nrhs==5)
    {
        double x=parameters[2];
        double y=parameters[3];
        double z=parameters[4];
        double r=std::sqrt(x*x+y*y+z*z);
        if (r<=0)
            mexErrMsgTxt("error: r==0\n");
        parameters[2] = acos(z/r);
        parameters[3] = atan2(y,x);
    }

    mwSize ndims=1;
    plhs[0] = mxCreateNumericArray(1,&ndims,cid,mxCOMPLEX);
    T *result_r = (T *) mxGetData(plhs[0]);
    T *result_i = (T *) mxGetImagData(plhs[0]);
    if (result_i==NULL)
      mexErrMsgTxt("error: no imaginary data has been allocated\n");
    if ((abs(parameters[1])<=parameters[0])&&
            (parameters[0]>=0))
    {
        int l=(int)std::floor(parameters[0]);
        int m=(int)std::floor(parameters[1]);
        std::complex<double>tmp=hanalysis::basis_SphericalHarmonicsSemiSchmidt(l,m,parameters[2],parameters[3]);
        *result_r=(T)tmp.real();
        *result_i=(T)tmp.imag();
        return ;
    } else
    {
        mexErrMsgTxt("error: not defined\n");
        *result_r=(T)0;
	*result_i=(T)0;
    }
}


void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
}



