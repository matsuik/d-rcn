function fullfield = sta_tofull(ofield),

st = ofield.storage;
ty = ofield.type;
L = ofield.L;
shape = ofield.shape;

if strcmp(ty,'STA_OFIELD_FULL')
    fullfield = ofield;
    return;
end;


fullfield = ofield;
fullfield.type = 'STA_OFIELD_FULL';
fullfield.data = zeros([2, sta_getComponentOffset(st,'STA_OFIELD_FULL',L+1) shape]);


if strcmp(ty,'STA_OFIELD_EVEN')
    for k = 0:2:ofield.L,
        fullfield.data(:,(sta_getComponentOffset(st,'STA_OFIELD_FULL',k)+1:sta_getComponentOffset(st,'STA_OFIELD_FULL',k+1)),:,:,:) ...
         = ofield.data(:,(sta_getComponentOffset(st,ty,k)+1:sta_getComponentOffset(st,ty,k+2)),:,:,:) ;
    end;
elseif strcmp(ty,'STA_OFIELD_ODD')
    for k = 1:2:ofield.L,
        fullfield.data(:,(sta_getComponentOffset(st,'STA_OFIELD_FULL',k)+1:sta_getComponentOffset(st,'STA_OFIELD_FULL',k+1)),:,:,:) ...
         = ofield.data(:,(sta_getComponentOffset(st,ty,k)+1:sta_getComponentOffset(st,ty,k+2)),:,:,:) ;
    end; 
else
    warning('unsupported');
    return;
end;
    