function f = sta_solidangleMult(f1,f2,L)

L1 = f1.L;
L2 = f2.L;

if strcmpi(f1.type,'STA_FIELD_STORAGE_C') | strcmpi(f2.type,'STA_FIELD_STORAGE_C'),
    warning('not supported');
    return;
end;


isodd1 = false;
if strcmpi(f1.type,'STA_OFIELD_FULL'),
    start1 = 0;
    step1 = 1;
elseif strcmpi(f1.type,'STA_OFIELD_EVEN'),
    start1 = 0;
    step1 = 2;
elseif strcmpi(f1.type,'STA_OFIELD_ODD'),
    start1 = 1;
    step1 = 2;
    isodd1 = true;
elseif strcmpi(f1.type,'STA_OFIELD_SINGLE'),
    start1 = L1;
    step1 = 1;
    isodd1 = mod(L1,2)==1;
else,
    warning('not supported !!');
    return;
end;

isodd2 = false;
if strcmpi(f2.type,'STA_OFIELD_FULL'),
    start2 = 0;
    step2 = 1;
elseif strcmpi(f2.type,'STA_OFIELD_EVEN'),
    start2 = 0;
    step2 = 2;
elseif strcmpi(f2.type,'STA_OFIELD_ODD'),
    start2 = 1;
    step2 = 2;
    isodd2 = true;
elseif strcmpi(f2.type,'STA_OFIELD_SINGLE'),
    start2 = L2;
    step2 = 1;
    isodd2 = mod(L2,2)==1;
else,
    warning('not supported !!');
    return;
end;

if strcmpi(f1.type,'STA_OFIELD_FULL') | strcmpi(f2.type,'STA_OFIELD_FULL'), 
    outstep = 1;
    outtype = 'STA_OFIELD_FULL';
else
    outstep = 2;
    outtype = 'STA_OFIELD_EVEN';
end;
outstart = 0;

if (strcmpi(f1.type,'STA_OFIELD_EVEN') & isodd2 ) | ...
   (strcmpi(f2.type,'STA_OFIELD_EVEN') & isodd1 ),   
    outstart = 1;
    outtype = 'STA_OFIELD_ODD';
end;


if not(exist('L')),
    L = L1+L2;
end;


alldirs =  load('symS2distributions.mat');

bDir = alldirs.dirs128;
bDir = [bDir -bDir];

M1=getSHMatrix(f1.L,bDir,f1.type,f1.storage,1);
M2=getSHMatrix(f2.L,bDir,f2.type,f2.storage,1);

U=getSHMatrix(L,bDir,outtype,'STA_FIELD_STORAGE_R',0);

M1_inter(1,:,:) = real(M1); M1_inter(2,:,:) = imag(M1);
M2_inter(1,:,:) = real(M2); M2_inter(2,:,:) = imag(M2);
U_inter(1,:,:) = real(U); U_inter(2,:,:) = imag(U);




data = sta_saMultNL(f1.data,f2.data,M1_inter,M2_inter,U_inter,0);
f = stafield(stafieldStruct(data,outtype,f1.storage));




