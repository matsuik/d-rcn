function ofield = sta_S2conv(ofield,rf),

st = ofield.storage;
ty = ofield.type;
L = ofield.L;
shape = ofield.shape;

if not(isreal(rf)),
    warning('only real rf supported!');
    return;
end;


if strcmp(ty,'STA_OFIELD_EVEN')
    for k = 0:2:ofield.L,
        ofield.data(:,(sta_getComponentOffset(st,ty,k)+1:sta_getComponentOffset(st,ty,k+2)),:,:,:) = ...
            rf(k/2+1)*ofield.data(:,(sta_getComponentOffset(st,ty,k)+1:sta_getComponentOffset(st,ty,k+2)),:,:,:);
    end;
elseif strcmp(ty,'STA_OFIELD_ODD')
    for k = 1:2:ofield.L,
        ofield.data(:,(sta_getComponentOffset(st,ty,k)+1:sta_getComponentOffset(st,ty,k+2)),:,:,:) = ...
            rf((k+1)/2)*ofield.data(:,(sta_getComponentOffset(st,ty,k)+1:sta_getComponentOffset(st,ty,k+2)),:,:,:);
    end; 
elseif strcmp(ty,'STA_OFIELD_FULL')
    for k = 0:1:ofield.L,
        ofield.data(:,(sta_getComponentOffset(st,ty,k)+1:sta_getComponentOffset(st,ty,k+1)),:,:,:) = ...
            rf(k+1)*ofield.data(:,(sta_getComponentOffset(st,ty,k)+1:sta_getComponentOffset(st,ty,k+1)),:,:,:);
    end; 
else
    warning('unsupported');
    return;
end;
    