

function r = LtoIndexRange(L,step,realsym)
    if realsym == 0,
        r = (LtoIndex(L,step,realsym)+1):(LtoIndex(L,step,realsym) + (2*L+1));
    elseif realsym == 1,
        r = (LtoIndex(L,step,realsym)+1):(LtoIndex(L,step,realsym) + (L+1));
    end;
