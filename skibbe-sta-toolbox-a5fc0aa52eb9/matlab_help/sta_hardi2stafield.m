function [ofield,b0avg] = sta_hardi2stafield(data,dir,varargin)



if length(size(dir)) == 3,
   numdir = size(dir,3);
   idx = find(squeeze(sum(sum(abs(dir))))>eps);
   newdir = dir(:,:,idx);
else
   numdir = size(dir,2);
   idx = find(squeeze(sum(abs(dir)))>eps); 
   newdir = dir(:,idx);
end;

if not(isempty(setdiff(1:numdir,idx))),

    b0avg = mean(data(:,:,:,setdiff(1:numdir,idx)),4)+1;

    data = data(:,:,:,idx);
    for k = 1:size(data,4),
        data(:,:,:,k) = data(:,:,:,k) ./ b0avg;
    end;
end;
    
ofield = sta_ad2stafield(data,newdir,varargin{:});