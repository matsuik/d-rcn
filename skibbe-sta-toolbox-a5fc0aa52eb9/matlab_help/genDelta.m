function X = genDelta(dir,L,sigma,sz,type,storage)





Gaussian = STAfspecial({'shape',sz,'kname','gauss','kparams',sigma,'centered',1});
D = getSHMatrix(L,dir'/norm(dir),type,storage,0);
X = cat(1,repmat(real(D)',[1 1 sz]),repmat(imag(D)',[1 1 sz])).*repmat(Gaussian.data(1,:,:,:,:),[2 size(D,1) 1 1 1]);
X = stafield(X,type,storage);


