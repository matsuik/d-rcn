%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   [dir M bDir] = getLocalMax(ofield,L,nummax,interp,type,storage)
%
%  extracts local maxima from an orientation field in SH-rep
%
%  ofield - 5D-array of stafield type
%  L - L-max of ofield
%  nummax - maximal number of local maximas to be extracted
%  interp - interpolation to get sub-sphexel resolution
%  type - type of ofield
%  storage - storage of ofield
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





function [dir M bDir] = getLocalMax(ofield,L,nummax,interp,type,storage)


alldirs =  load('symS2distributions.mat');

bDir = alldirs.dirs128;
bDir = [bDir -bDir];

M=getSHMatrix(L,bDir,type,storage,1);
Mil(1,:,:) = real(M);
Mil(2,:,:) = imag(M);

ncliques = computeNeighbors(bDir');

usesym = 0;
if strcmp(type,'STA_OFIELD_FULL'),
    step = 1;
elseif strcmp(type,'STA_OFIELD_EVEN'),
    step = 2;
    usesym = 1;
elseif strcmp(type,'STA_OFIELD_ODD'),
    step = 2;
else
    warning('unsupported type!');
    return;
end;


realNum = @(x) iff(isa(ofield,'single'),single(x),double(x));


if interp,

    locFitPS = zeros(6,size(ncliques.neighborsarray,1)+1,size(bDir,2));
    bDirt = bDir';
    for k = 1:length(ncliques.neighbors),
         Ns = [ncliques.neighborsarray(:,k); k];
         Ms = [bDirt(Ns,1).^2 bDirt(Ns,2).^2 bDirt(Ns,3).^2 2*bDirt(Ns,1).*bDirt(Ns,2) 2*bDirt(Ns,1).*bDirt(Ns,3) 2*bDirt(Ns,2).*bDirt(Ns,3)];
         locFitPS(:,:,k) = pinv(Ms);
    end;
    dir = getLocalMaxC(ofield,realNum(bDir),ncliques.neighborsarray,realNum(Mil),realNum(locFitPS),realNum([nummax usesym]));
else,
    dir = getLocalMaxC(ofield,realNum(bDir),ncliques.neighborsarray,realNum(Mil),[],realNum([nummax usesym]));
end;

return;





function result = iff(condition,trueResult,falseResult)
  error(nargchk(3,3,nargin));
  if condition
    result = trueResult;
  else
    result = falseResult;
  end






function Ncliques = computeNeighbors(bDir);

K = convhulln(double(bDir));
for k = 1:size(bDir,1),
    nn =  K(sum(K == k,2)>0,:);
    neighbors{k} = setdiff(unique(nn(:)),k);    
end;


for k = 1:size(bDir,1),
    if length(neighbors{k}) == 5,
        neighborsarray(:,k) = [neighbors{k}' neighbors{k}(1)]';
    else,
        neighborsarray(:,k) = neighbors{k}(1:6);
    end;
end;

Ncliques.neighbors = neighbors;
Ncliques.neighborsarray = neighborsarray;
Ncliques.bDir = bDir;




