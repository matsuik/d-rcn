#include "mex.h"

#define _SUPPORT_MATLAB_
#include "stafield.h"


#include <stdio.h>
#include <string.h>
#include <list>

//
// Computing coefficients by solving SOE; see e.g.
// William O. Straub
// EFFICIENT COMPUTATION OF CLEBSCH-GORDAN COEFFICIENTS
// for details
//  
template <typename T>
T clebschGordan(int j1, int M1, int j2, int M2,int J, int M)
{
 if ( (std::abs(j1-j2)>J)||
      (j1+j2<J)||
      (std::abs(M1)>j1)||
      (std::abs(M2)>j2)||
      (std::abs(M)>J))
 {
   return 0;
 }
 int m1_min=(M-j1-j2+std::abs(j1-j2+M))/2;
 int m1_max=(M+j1+j2-std::abs(j1-j2-M))/2;
 
 int numel=m1_max-m1_min+1;
 
 T * coefficients=new T[numel];
 
 int count=numel-2; 
 T A1_old=0;
 T sum=1;
 int j1j1=j1*(j1+1);
 int j2j2=j2*(j2+1);
 coefficients[numel-1]=1; 
 for (int m1=m1_max;m1>m1_min;m1--)
 {
    int m2=M-m1;
    T A0=j1j1+j2j2+2*m1*m2-J*(J+1);
    T A1=std::sqrt(double(j1j1-m1*(m1-1)))*std::sqrt(double(j2j2-m2*(m2+1)));
     
    if (m1<m1_max)
    {
      coefficients[count]=(-A0*coefficients[count+1]-A1_old*coefficients[count+2])/A1;
    }else
    {
      coefficients[count]=-A0/A1;
    } 
    sum+=coefficients[count]*coefficients[count];
    A1_old=A1; 
    count--;
 }
 T v=coefficients[M1-m1_min]/std::sqrt(sum);
 delete [] coefficients; 
 return v;
}


template <typename T, typename S>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  if (nrhs!=6)
        mexErrMsgTxt("rhs=<j1,m1,j2,m2,J,M>\\n");

    T parameters[6];
    mxClassID cid=mxUNKNOWN_CLASS;


    for (int i=0;i<nrhs;i++)
    {
        const mxArray *ST1;
        ST1 = prhs[i];
        const int numdimST1 = mxGetNumberOfDimensions(ST1);
        if (numdimST1!=2)
            mexErrMsgTxt("error: Array dimension missmatch\n");
        T *pST1 = (T*) (mxGetData(ST1));
        parameters[i]=(T)(*pST1);
        cid=mxGetClassID(ST1);
    }
    
    mwSize ndims=1;
    plhs[0] = mxCreateNumericArray(1,&ndims,cid,mxREAL);
    T *result_r = (T *) mxGetData(plhs[0]);
    

    T test=clebschGordan<T>(parameters[0],
			      parameters[1],
			      parameters[2],
			      parameters[3],
			      parameters[4],
			      parameters[5]
 			    );
    
    result_r[0]=hanalysis::clebschGordan(parameters[0],
			      parameters[1],
			      parameters[2],
			      parameters[3],
			      parameters[4],
			      parameters[5]
 			    );
    
    sta_assert(std::abs(test-result_r[0])<0.00000000000001);
}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");
   _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
}

