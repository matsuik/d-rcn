	REM your FFTW folder here:
set FFTW_PATH=%CD%\fftw\win64\
	REM your STA-Toolbox folder here:
set STA_TOOLBOX_PATH=%CD%

set PATH=%FFTW_PATH%;%PATH%;

matlab -r "addpath('%STA_TOOLBOX_PATH%');sta_setPaths";