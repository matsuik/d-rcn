%   STA_PROD implements spherical tensor products
%
%   Usage: ofield=STA_PROD(ifield1,ifield2,L,parameters);
%           
%           ifield1 and ifield2 are stafieldStruct 
%           L   output rank with |ifield1.L-ifield2.L|<=L<=ifield1.L+ifield2.L
%           parameters (optional) : list of name-value pairs including
%           parameters={
%                'normalize' ,true/false (default false): normalized products 
%                             exist for ((ifield1.L+ifield2.L+L)%2==0)
%               'alpha' ,     scalar (default 1, multiplicator) 
%               'L1' ,        if ifield1 is not 'STA_OFIELD_SINGLE' you can
%                             pick the field with rank L1
%               'L2' ,        if ifield2 is not 'STA_OFIELD_SINGLE' you can
%                             pick the field with rank L2
%           }
%
%   Example: 
%       gauss=sta_deriv(stafieldStruct('gauss',[64,64,64],3,1),1);    
%       ofield=STA_PROD(gauss,gauss,2,{'normalize',true});
%
% See also sta_deriv sta_mult sta_fft sta_ifft sta_fspecial sta_lap
%