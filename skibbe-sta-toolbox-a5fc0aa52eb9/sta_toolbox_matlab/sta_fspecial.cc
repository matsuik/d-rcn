/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox for Matlab
 * 
 *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox for Matlab
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

#include "mex.h"


#define _SUPPORT_MATLAB_
#include "stafield.h"
#include "sta_mex_helpfunc.h"



template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

    //hanalysis::verbose=1;

    int L=0;
    std::string kname="";
    std::vector<T> kparams;
    hanalysis::STA_FIELD_STORAGE field_storage=hanalysis::STA_FIELD_STORAGE_R;
    std::vector<std::size_t> shape(3);
    bool centered=false;
    std::vector<T> element_size(3);
    element_size[0]=element_size[1]=element_size[2]=T(1);

    if (nrhs>0)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (hanalysis::mex_hasParam(params,"kname")!=-1)
            kname=hanalysis::mex_getParamStr(params,"kname");
        else
            mexErrMsgTxt("error: kname parameter requred");

        if (hanalysis::mex_hasParam(params,"kparams")!=-1)
            kparams=hanalysis::mex_getParam<T>(params,"kparams");
        else
            mexErrMsgTxt("error: kparams parameter requred");

	if (hanalysis::mex_hasParam(params,"storage")!=-1)
            field_storage=hanalysis::enumfromstring_storage(hanalysis::mex_getParamStr(params,"storage"));
	
        if (hanalysis::mex_hasParam(params,"L")!=-1)
            L=hanalysis::mex_getParam<T>(params,"L",1)[0];

        if (hanalysis::mex_hasParam(params,"centered")!=-1)
            centered=hanalysis::mex_getParam<bool>(params,"centered",1)[0];

        if (hanalysis::mex_hasParam(params,"shape")!=-1)
            shape=hanalysis::mex_getParam<std::size_t>(params,"shape",3);
        else
            mexErrMsgTxt("error: shape parameter requred");
	
        if (hanalysis::mex_hasParam(params,"element_size")!=-1)
            element_size=hanalysis::mex_getParam<T>(params,"element_size",3);
    }

     std::swap(shape[0],shape[2]);
     std::swap(element_size[0],element_size[2]);
    
    try {

        hanalysis::stafield<T> ofield=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                      shape.data(),
                                      L,
                                      field_storage,
				      hanalysis::STA_OFIELD_SINGLE,
				      (const T*)element_size.data());
	
	
        hanalysis::stafield<T>::makeKernel(kname,kparams,ofield,centered);

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }
}







void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs>0)
    {
        const mxArray * params=prhs[nrhs-1] ;
        std::string precision="double";
        if (hanalysis::mex_hasParam(params,"precision")!=-1)
            precision=hanalysis::mex_getParamStr(params,"precision");
        if (precision=="single")
        {
            _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
            return;
        }
        
        _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
    }else
       mexErrMsgTxt("error: no parameters given\n");
}















