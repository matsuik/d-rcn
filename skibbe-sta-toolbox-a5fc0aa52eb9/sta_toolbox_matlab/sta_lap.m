%   STA_LAP implements laplace operator
%
%   Usage: ofield=STA_LAP(ifield);
%           
%           ifield is a stafieldStruct 
%           parameters (optional) : list of name-value pairs including
%           parameters={
%               'alpha' ,    scalar (default 1, multiplicator) 
%               'type' ,     default 1 (18 neighbor laplace)
%                                    0 (6 neighbor laplace)   
%           }
%
%   Example: 
%       ofield=STA_LAP(stafieldStruct('gauss',[64,64,64],3,1));
%
% See also sta_prod sta_mult sta_fft sta_ifft sta_fspecial sta_deriv
%