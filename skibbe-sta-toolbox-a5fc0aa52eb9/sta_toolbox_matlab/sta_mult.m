%   STA_MULT implements scalar multiplication
%
%   Usage: ofield=STA_MULT(ifield,alpha);
%           
%           ifield is a stafieldStruct 
%           alpha is a scalar 
%
%   Example: 
%       ofield=STA_MULT(stafieldStruct('gauss',[64,64,64],3,1),4+3i);
%
% See also sta_deriv sta_prod sta_fft sta_ifft sta_fspecial sta_lap
