/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox for Matlab
 * 
 *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox for Matlab
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

#include "mex.h"

#define _SUPPORT_MATLAB_
#include "stafield.h"
#include "sta_mex_helpfunc.h"


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    hanalysis::mex_initMatlabFFTW();
    
#ifndef _STA_LINK_FFTW    
         int FFTW_ESTIMATE=0;
	 int FFTW_MEASURE=0;
	 int FFTW_PATIENT=0;
	 int FFTW_EXHAUSTIVE=0;
#endif
  
    bool conjugate=false;
    std::complex<T> alpha=1;
    bool forward=true;
    int flag=FFTW_ESTIMATE;
    int Flag=0;

    forward=hanalysis::mex_get1Param<bool>(prhs[1]);


    if (nrhs>2)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (hanalysis::mex_hasParam(params,"alpha")!=-1)
            alpha=hanalysis::mex_getParamC<T>(params,"alpha",1)[0];

        if (hanalysis::mex_hasParam(params,"flag")!=-1)
            Flag=hanalysis::mex_getParam<int>(params,"flag",1)[0];

        if (hanalysis::mex_hasParam(params,"conjugate")!=-1)
            conjugate=hanalysis::mex_getParam<int>(params,"conjugate",1)[0];
    }

    switch (Flag)
    {
    case 0:
        flag=FFTW_ESTIMATE;
        break;
    case 1:
        flag=FFTW_MEASURE;
        break;
    case 2:
        flag=FFTW_PATIENT;
        break;
    case 3:
        flag=FFTW_EXHAUSTIVE;
        break;
    }


    try {

        hanalysis::stafield<T> ifield=hanalysis::stafield<T>((mxArray *)prhs[0]);

        hanalysis::STA_FIELD_STORAGE new_field_storage=ifield.getStorage();
        if (new_field_storage==hanalysis::STA_FIELD_STORAGE_R)
            new_field_storage=hanalysis::STA_FIELD_STORAGE_RF;
        else
            if (new_field_storage==hanalysis::STA_FIELD_STORAGE_RF)
                new_field_storage=hanalysis::STA_FIELD_STORAGE_R;	
	
        hanalysis::stafield<T> ofield=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                      ifield.getShape(),
                                      ifield.getRank(),
                                      new_field_storage,
                                      ifield.getType(),
				      ifield.getElementSize());

        hanalysis::stafield<T>::FFT(    ifield,
                                        ofield,
                                        forward,
                                        conjugate,
                                        alpha,
                                        flag);

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }
}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<2)
        mexErrMsgTxt("error: expected (stafield,f/b)\n");

    if (hanalysis::mex_isStaFieldStruct<float>(prhs[0]))
        _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
    else
        if (hanalysis::mex_isStaFieldStruct<double>(prhs[0]))
            _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
        else
            mexErrMsgTxt("error: unsupported datatype");

}




