%
%   See Also stafieldStruct
classdef stafield
    
   properties
      storage = 'STA_FIELD_STORAGE_R';
      L = 0;
      type = 'STA_OFIELD_SINGLE';
      shape= [0,0,0];
      data = zeros([2,1,0,0,0]);
      element_size = [1,1,1];
      precision = 'double';
   end;
   
   methods
       
       

       function n=numVoxel(f1)
           n=prod(f1.shape);
       end;

       function stafield_struct=struct(f1)
           stafield_struct.storage=f1.storage;
           stafield_struct.L=f1.L;
           stafield_struct.type=f1.type;
           stafield_struct.data=f1.data;
           stafield_struct.shape=f1.shape;
           stafield_struct.element_size=f1.element_size;
       end;
       

       function field=get(f1,l)
           field=stafield([2,2,2],0);
           field.element_size=f1.element_size;
           field.storage=f1.storage;
           field.L=l;
           field.shape=f1.shape;
           field.type='STA_OFIELD_SINGLE';           
           st = f1.storage;
           ty = f1.type;           
           success=false;                        
           
           if  strcmp(f1.type,'STA_OFIELD_FULL'),
               step = 1;
           elseif strcmp(f1.type,'STA_OFIELD_EVEN')  | strcmp(f1.type,'STA_OFIELD_ODD') 
               step = 2;
           end;
           
           if strcmp(f1.type,'STA_OFIELD_FULL') | strcmp(f1.type,'STA_OFIELD_EVEN')  | strcmp(f1.type,'STA_OFIELD_ODD') 
               if strcmp(f1.storage,'STA_FIELD_STORAGE_R') | strcmp(f1.storage,'STA_FIELD_STORAGE_C'),
                    field.data = f1.data(:,(sta_getComponentOffset(st,ty,l)+1:sta_getComponentOffset(st,ty,l+step)),:,:,:);
                    success=true;                    
               end;
           elseif strcmp(f1.type,'STA_OFIELD_SINGLE')
               if l == f1.L,
                   field = f1;
                   success=true; 
               end;               
           end;
           if ~success
               error('error field=get(f1,l)');
           end;
       end;
       
%#############################################################       
%
%       constructor
%
%#############################################################
       
       
       
       function field = stafield(varargin)
           Nargin=numel(varargin);
           
           
           if Nargin==1 && ischar(varargin{1}),
               field.precision=varargin{1};
               return;
           end;          
           
            %creates a tensor field from stafield_struct
            % 
            % sta_field(stafield_struct)
            %    
           if Nargin==1 && isstruct(varargin{1});
               ofield=varargin{1};
           else   
               ofield=stafieldStruct(varargin);
           end;                
           
           field.storage=ofield.storage;
           field.L=ofield.L;
           field.type=ofield.type;
           field.data=ofield.data;
           field.element_size=ofield.element_size;
           
           if (isa(field.data, 'double'))
               field.precision='double';
           elseif (isa(field.data, 'single'))
               field.precision='single';
           else
               error('ensure that image is eihter double or sinle precision');
           end;  
            field.shape=size(field.data);
            field.shape=field.shape(3:5);
       end 
       
       
%#############################################################       
%
%       STA operators
%
%#############################################################
       
       
      function ofield = mult(f1,alpha,conjugate)
          if nargin<2, alpha=1; end;
          if nargin<3, conjugate=0; end;
           params={'conjugate',conjugate};
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
                ofield=stafield(sta_mult(f1.struct(),alpha,params));
                return;
          end;
          error('error compuing the double derivatives');
      end  
       
       
       
      function ofield = deriv(f1,L,conjugate,alpha,accuracy)
          assert(abs(L)<=1);          
          if nargin<3, conjugate=0; end;
          if nargin<4, alpha=1; end;
          if nargin<5, accuracy=0; end;
          params={'alpha',alpha,'conjugate',conjugate,'accurate',accuracy};
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
            ofield=stafield(sta_deriv(f1.struct(),L,params));
            return;
          end;
          error('error compuing the derivatives');
      end
      
      function ofield = deriv2(f1,L,conjugate,alpha)
          assert(abs(L)<=2);          
          assert(abs(L)~=1);          
          if nargin<3, conjugate=0; end;
          if nargin<4, alpha=1; end;
          params={'alpha',alpha,'conjugate',conjugate,'dd',1};
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
            ofield=stafield(sta_deriv(f1.struct(),L,params));
            return;
          end;
          error('error compuing the double derivatives');
      end      
      
      function ofield = lap(f1,alpha,type)
          if nargin<2, alpha=1; end;
          if nargin<3, type=1; end;
          params={'alpha',alpha,'type',type};
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
                ofield=stafield(sta_lap(f1.struct(),params));
                return;
          end;
          error('error compuing the laplacian');
      end         
      
      function ofield = prod(f1,f2,L,normalized,alpha)
         if nargin<5, alpha=1; end;
         if nargin<4, normalized=0; end;
         params={'alpha',alpha,'normalize',normalized};
         if ~strcmpi( f1.storage,f2.storage)
             error('same storage type for both fields required!');
         end;
         if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
                ofield=stafield(sta_prod(f1.struct(),f2.struct(),L,params));
                return;
          end;
         error('error compuing the tensor product');
      end
      
      function ofield = fft(f1,conjugate,alpha)
          if nargin<2, conjugate=0; end;          
          if nargin<3, alpha=1; end;
          params={'alpha',alpha,'conjugate',conjugate};
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
                ofield=stafield(sta_fft(f1.struct(),params));
                return;
          end;
      end
      
      function ofield = ifft(f1,conjugate,alpha)
          if nargin<2, conjugate=0; end;          
          if nargin<3, alpha=1; end;          
          params={'alpha',alpha,'conjugate',conjugate};
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
                ofield=stafield(sta_ifft(f1.struct(),params));
                return;
          end;
      end
      
      
      function l = length(f1)
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
            l=sta_norm(f1.data,0,f1.storage);
            return;
          end;
          error('error compuing the norm');
      end;

      function ofield = norm(f1)
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
            ofield=stafield(sta_norm(f1.data,2,f1.storage),f1.type,f1.storage);
            return;
          elseif strcmpi( f1.type,'STA_OFIELD_FULL'),
            l0 = f1.get(0);
            ofield =stafield(sta_norm(l0.data,2,l0.storage),l0.type,l0.storage);
            for l = 1:f1.L,
                ofield = ofield + norm(f1.get(l));
            end;            
            return;
          end;
          error('error compuing the Norm');
      end;      

   end % methods
   
   

       
%#############################################################       
%
%     static STA operators
%
%#############################################################

methods (Static)   
       
       
 function ofield=Mult(f1,alpha,conjugate)
          if nargin<3, alpha=1; end;
          if nargin<4, conjugate=0; end;
          params={'conjugate',conjugate};
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
                ofield=stafield(sta_mult(f1.struct(),alpha,params));
                return;
          end;
          error('error compuing the double derivatives');
       end  
       
      function ofield=Deriv(f1,L,conjugate,alpha,accuracy)
          assert(abs(L)<=1);          
          if nargin<4, conjugate=0; end;
          if nargin<5, alpha=1; end;
          if nargin<6, accuracy=0; end;
          params={'alpha',alpha,'conjugate',conjugate,'accurate',accuracy};
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
            ofield=stafield(sta_deriv(f1.struct(),L,params));
            return;
          end;
          error('error compuing the derivatives');
      end
       
      function ofield=Deriv2(f1,L,conjugate,alpha,accuracy)
          assert(abs(L)<=2);          
          assert(abs(L)~=1);   
          if nargin<4, conjugate=0; end;
          if nargin<5, alpha=1; end;
          if nargin<6, accuracy=0; end;
          params={'alpha',alpha,'conjugate',conjugate,'accurate',accuracy,'dd',1};
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
            ofield=stafield(sta_deriv(f1.struct(),L,params));
            return;
          end;
          error('error compuing the derivatives');
      end 
      
      
      function ofield=Lap(f1,alpha,type)
          if nargin<3, alpha=1; end;
          if nargin<4, type=1; end;
          params={'alpha',alpha,'type',type};
          if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
                ofield=stafield(sta_lap(f1.struct(),params));
                return;
          end;
          error('error compuing the laplacian');
      end         
      
      function ofield=Prod(f1,f2,L,normalized,alpha)
         if nargin<5, normalized=0; end;          
         if nargin<6, alpha=1; end;
         params={'alpha',alpha,'normalize',normalized};
         if ~strcmpi( f1.storage,f2.storage)
             error('same storage type for both fields required!');
         end;
         if strcmpi( f1.type,'STA_OFIELD_SINGLE'),
                ofield=stafield(sta_prod(f1.struct(),f2.struct(),L,params));
                return;
          end;
         error('error compuing the tensor product');
      end    
      
      
%       function ofield=delta(shape,storage,precision)
%           s='STA_FIELD_STORAGE_R';
%           p='double';
%           if (nargin>1)
%               s=storage;
%           end;
%           if (nargin>2)
%               p=precision;
%           end;
%           
%           img=zeros(shape,p);
%           img(ceil(end/2),ceil(end/2),ceil(end/2))=1;
%           ofield=stafield(img,s);
%       end;
      
      
   end;     
      
             
   
%#############################################################       
%
%       overloaded matlab operators
%
%#############################################################
   
   
   methods
       
      function ofield = plus(f1,f2)
          if ~strcmpi( f1.storage,f2.storage)
            error('same storage type for both fields required!');
          end;
          assert(all(f1.shape==f2.shape));
          ofield=f1;
          ofield.data=ofield.data+f2.data;
      end; 
      
      function ofield = minus(f1,f2)
          if ~strcmpi( f1.storage,f2.storage)
            error('same storage type for both fields required!');
          end;
          assert(all(f1.shape==f2.shape));
          ofield=f1;
          ofield.data=ofield.data-f2.data;
      end;       
      
      function ofield = mtimes(f1,alpha)
          ofield=f1.mult(alpha);
      end;           
      
      function ofield = mrdivide(f1,alpha)
          ofield=f1.mult(1/alpha);
      end;           
       
   end; % methods operators
   
   
end% classdef



