%   STA_FFT implements the tensor Fourier transform
%
%   Usage: ofield=STA_FFT(ifield,parameters);
%           
%           ifield is a stafieldStruct 
%
% parameters (optional) : list of name-value pairs including
%           parameters={
%               'conjugate' , true/false (default false)
%               'alpha' ,     scalar (default 1, multiplicator) 
%           }
%
%   Example: 
%       ofield=STA_FFT(stafieldStruct('gauss',[64,64,64],3,1));
%
% See also sta_deriv sta_prod sta_mult sta_ifft sta_fspecial sta_lap

function result=sta_fft(ST,params)
error(nargchk(1, 2, nargin))    
error(nargoutchk(0, 1, nargout))

fft([1 2 3 4]);

if (nargin>1)
    result=sta_fft3fb(ST,1,params);
else
    result=sta_fft3fb(ST,1);
end;
