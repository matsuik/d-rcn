%##########################################################################
%
%   default  type           = 'STA_OFIELD_SINGLE'; 
%   default  storage        = 'STA_FIELD_STORAGE_R';
%   default  precision      = 'double';
%   default  element_size   = [1,1,1];
%
%
%
%   (1) creates a tensor field from image
%
%   field=stafieldStruct(
%           image,       X-by-Y-by-Z- array
%           storage     'STA_FIELD_STORAGE_R' or 'STA_FIELD_STORAGE_C'
%           element_size [element_size_X,element_size_Y,element_size_Z];
%           )
%
%
%
%   (2) creates a tensor field from tensor data 
% 
%   field=stafieldStruct(
%           data,       2-by-N-by-X-by-Y-by-Z- array
%           type,       'STA_OFIELD_SINGLE','STA_OFIELD_FULL',
%                       'STA_OFIELD_ODD' or 'STA_OFIELD_EVEN'
%           storage     'STA_FIELD_STORAGE_R' or 'STA_FIELD_STORAGE_C'
%           element_size [element_size_X,element_size_Y,element_size_Z];
%           )
%
%
%
%   (3) creates a new (empty) tensor field 
% 
%   field=stafieldStruct(
%           shape,      [dimZ,dimY,dimX]
%           L,          Tensor Rank (integer >= 0)
%           type,       'STA_OFIELD_SINGLE','STA_OFIELD_FULL',
%                       'STA_OFIELD_ODD' or 'STA_OFIELD_EVEN'
%           storage     'STA_FIELD_STORAGE_R' or 'STA_FIELD_STORAGE_C' 
%           precision,  'single' or 'double'
%           element_size [element_size_X,element_size_Y,element_size_Z];
%           )
%
%
%
%   (4) creates a tensor field convolution kernel 
% 
%   field=stafieldStruct(
%           kernelname, 'gauss','gaussLaguerre' or 'gaussBessel'
%           shape,      [dimZ,dimY,dimX]
%           param,      [sigma],[sigma,n] or [sigma,k,s]
%                       (depends on the choice of kernelname)
%           centered,   0=not centered, 1=centered
%           L,          Tensor Rank (integer >= 0)
%           storage     'STA_FIELD_STORAGE_R' or 'STA_FIELD_STORAGE_C' 
%           precision,  'single' or 'double'    
%           element_size [element_size_X,element_size_Y,element_size_Z];
%           )
%
%
%
%   (5) extracts a single tensor field from an existing field
% 
%   field=stafieldStruct(
%           f1,         stafieldStruct
%           l           which field is supposed to be extracted
%           )
%
%
%   (6) overwrites a single tensor in a full field
% 
%   field=stafieldStruct(
%           f_full,    stafieldStruct 
%           f_single,  stafieldStruct 
%           )
%

%
%##########################################################################



function field=stafieldStruct(varargin)

          field.storage = 'STA_FIELD_STORAGE_R';
          field.L = 0;
          field.type = 'STA_OFIELD_SINGLE';
          field.data = zeros([2,1,0,0,0]);
          precision = 'double';
          field.shape=[0,0,0];  
          field.element_size=[1,1,1];
            

           if iscell(varargin) && numel(varargin)==1 && iscell(varargin{1})
               varargin=varargin{1};
           end;

           Nargin=numel(varargin);
           success=false;
           
           
            %creates a tensor field based on a component of an existing
            %field
            % 
            % stafieldStruct(
            %           f1,    stafieldStruct
            %           l          which field is supposed to be extracted
            %           )
            %           
           if ~isempty(varargin) && numel(varargin)==2 && isstruct(varargin{1})  && ~isstruct(varargin{2})
               f1=varargin{1};
               l=varargin{2};
               field.element_size=f1.element_size;
               field.storage=f1.storage;
               field.L=l;
               field.shape=f1.shape;
               field.type='STA_OFIELD_SINGLE';           
               st = f1.storage;
               ty = f1.type;           
               success=false;                        

               if  strcmp(f1.type,'STA_OFIELD_FULL'),
                   step = 1;
               elseif strcmp(f1.type,'STA_OFIELD_EVEN')  | strcmp(f1.type,'STA_OFIELD_ODD') 
                   step = 2;
               end;

               if strcmp(f1.type,'STA_OFIELD_FULL') | strcmp(f1.type,'STA_OFIELD_EVEN')  | strcmp(f1.type,'STA_OFIELD_ODD') 
                   if strcmp(f1.storage,'STA_FIELD_STORAGE_R') | strcmp(f1.storage,'STA_FIELD_STORAGE_C'),
                        field.data = f1.data(:,(sta_getComponentOffset(st,ty,l)+1:sta_getComponentOffset(st,ty,l+step)),:,:,:);
                        success=true;                    
                   end;
               elseif strcmp(f1.type,'STA_OFIELD_SINGLE')
                   if l == f1.L,
                       field = f1;
                       success=true; 
                   end;               
               end;
           end;           
           
            %overwrites a single tensor in a full field
            % 
            %   field=stafieldStruct(
            %           f_full,    stafieldStruct 
            %           f_single,  stafieldStruct 
            %           )
           if ~isempty(varargin) && numel(varargin)==2 && isstruct(varargin{1}) && isstruct(varargin{2})
               field=varargin{1};
               f_single=varargin{2};
               l=f_single.L;
 
               st = field.storage;
               ty = field.type;           
               success=false;                        

               if  strcmp(field.type,'STA_OFIELD_FULL'),
                   step = 1;
               elseif strcmp(field.type,'STA_OFIELD_EVEN')  | strcmp(field.type,'STA_OFIELD_ODD') 
                   step = 2;
               end;

               if strcmp(field.type,'STA_OFIELD_FULL') | strcmp(field.type,'STA_OFIELD_EVEN')  | strcmp(field.type,'STA_OFIELD_ODD') 
                   if strcmp(field.storage,'STA_FIELD_STORAGE_R') | strcmp(field.storage,'STA_FIELD_STORAGE_C'),
                        field.data(:,(sta_getComponentOffset(st,ty,l)+1:sta_getComponentOffset(st,ty,l+step)),:,:,:)=f_single.data;
                        success=true;                    
                   end;
               elseif strcmp(field.type,'STA_OFIELD_SINGLE')
                   if l == field.L,
                       field.data=f_single.data;
                       success=true; 
                   end;               
               end;
           end;           
          
           
            %creates a tensor field from image
            % 
            % stafieldStruct(
            %           image,       [X,Y,Z]
            %           storage      {'STA_FIELD_STORAGE_R','STA_FIELD_STORAGE_C'} 
            %           element_size [eX,eY,eZ]   
            %           )
            %           
           if ~isempty(varargin) && numel(varargin{1})>3 && numel(size(varargin{1}))==3
               shape=size(varargin{1});
               field.L=0;
               if (isa(varargin{1}, 'double'))
                   precision='double';
               elseif (isa(varargin{1}, 'single'))
                   precision='single';
               else
                   error('ensure that image is eihter double or sinle precision');
               end;                 
               field.data=zeros([2,1,shape],precision);                   
               if Nargin>1 
                   field.storage = varargin{2};
               end;
               if Nargin>2 
                   field.element_size = varargin{3};
               end;
               if  strcmpi( field.storage,'STA_FIELD_STORAGE_R'),
                   if isreal(varargin{1}),
                    field.storage='STA_FIELD_STORAGE_R';
                    field.data(1,1,:)=varargin{1}(:);
                   else
                    field.storage='STA_FIELD_STORAGE_C';
                   end;
               end;
               if  strcmpi( field.storage,'STA_FIELD_STORAGE_C'),
                   field.storage='STA_FIELD_STORAGE_C';                   
                   if isreal(varargin{1}),
                    field.data(1,1,:)=varargin{1}(:);
                   else
                    field.data(1,1,:)=real(varargin{1}(:));
                    field.data(2,1,:)=imag(varargin{1}(:));
                   end;
               end;         
               success=true;
           end;
           
           
            %creates a tensor field from tensor data [2,N,Z,Y,X]
            % 
            % stafieldStruct(
            %           data,       [2,N,dimX,dimY,dimZ]
            %           type,       {'single','full','odd','even'}
            %           storage     {'STA_FIELD_STORAGE_R','STA_FIELD_STORAGE_C'} 
            %           element_size [eX,eY,eZ]               
            %           )
            %
           if ~isempty(varargin) && numel(varargin{1})>=5 && numel(size(varargin{1}))==5
               shape_=size(varargin{1});
               ncomponents=shape_(2);
               field.data=varargin{1};
               
               if Nargin>1 
                   field.type = varargin{2};
               end;
               if Nargin>2 
                   field.storage = varargin{3};
               end;
               if Nargin>3 
                   field.element_size = varargin{4};
               end;
                if  strcmpi( field.storage,'STA_FIELD_STORAGE_C') && strcmpi( field.type,'STA_OFIELD_SINGLE')
                    field.L=(ncomponents-1)/2;
                end;
                if  strcmpi( field.storage,'STA_FIELD_STORAGE_C') && strcmpi( field.type,'STA_OFIELD_FULL')
                    field.L=sqrt(ncomponents)-1;                    
                end;                
                if  strcmpi( field.storage,'STA_FIELD_STORAGE_C') && strcmpi( field.type,'STA_OFIELD_ODD')
                    field.L=-3/2+sqrt((3/2)^2+2*ncomponents-2);
                end;                
                if  strcmpi( field.storage,'STA_FIELD_STORAGE_C') && strcmpi( field.type,'STA_OFIELD_EVEN')
                    field.L=-3/2+sqrt((3/2)^2+2*ncomponents-2);
                end;                                
                if  strcmpi( field.storage(1:19),'STA_FIELD_STORAGE_R') && strcmpi( field.type,'STA_OFIELD_SINGLE')
                    field.L=ncomponents-1;
                end;               
                if  strcmpi( field.storage(1:19),'STA_FIELD_STORAGE_R') && strcmpi( field.type,'STA_OFIELD_FULL')
                    field.L=-3/2+sqrt((3/2)^2+2*ncomponents-2);
                end;                               
                if  strcmpi( field.storage(1:19),'STA_FIELD_STORAGE_R') && strcmpi( field.type,'STA_OFIELD_ODD')
                    field.L=sqrt(1+4*ncomponents)-2;
                end;                
                if  strcmpi( field.storage(1:19),'STA_FIELD_STORAGE_R') && strcmpi( field.type,'STA_OFIELD_EVEN')
                    field.L=sqrt(4*ncomponents)-2;
                end;                
               success=true;
           end;
           

            %creates a new (empty) tensor field 
            % 
            % stafieldStruct(
            %           shape,      [dimX,dimY,dimZ]
            %           L,          {0,,,N}
            %           type,       {'single','full','odd','even'}
            %           storage     {'STA_FIELD_STORAGE_R','STA_FIELD_STORAGE_C'} 
            %           precision,  {'single','double'}
            %           element_size [eX,eY,eZ]               
            %           )
            %
           if ~isempty(varargin) && numel(varargin{1})==3 
               
               field.shape=varargin{1};
               
                if Nargin>1,
                    field.L=varargin{2};
                end;
                if Nargin>2,
                    field.type=varargin{3};
                end;
                if Nargin>3,
                    field.storage=varargin{4};
                end;
                if Nargin>4,
                    precision=varargin{5};
                end;
               if Nargin>5 
                   field.element_size = varargin{6};
               end;                
                
                if  strcmpi( field.storage,'STA_FIELD_STORAGE_C') && strcmpi( field.type,'STA_OFIELD_SINGLE')
                    ncomponents=2*field.L+1;
                end;
                if  strcmpi( field.storage,'STA_FIELD_STORAGE_C') && strcmpi( field.type,'STA_OFIELD_FULL')
                    ncomponents=(field.L+1)^2;
                end;                
                if  strcmpi( field.storage,'STA_FIELD_STORAGE_C') && strcmpi( field.type,'STA_OFIELD_ODD')
                    ncomponents=(field.L+1)*(field.L+2)/2;
                end;                
                if  strcmpi( field.storage,'STA_FIELD_STORAGE_C') && strcmpi( field.type,'STA_OFIELD_EVEN')
                    ncomponents=(field.L+1)*(field.L+2)/2;
                end;                                
                if  strcmpi( field.storage(1:19),'STA_FIELD_STORAGE_R') && strcmpi( field.type,'STA_OFIELD_SINGLE')
                    ncomponents=field.L+1;
                end;               
                if  strcmpi( field.storage(1:19),'STA_FIELD_STORAGE_R') && strcmpi( field.type,'STA_OFIELD_FULL')
                    ncomponents=(field.L+1)*(field.L+2)/2;
                end;                               
                if  strcmpi( field.storage(1:19),'STA_FIELD_STORAGE_R') && strcmpi( field.type,'STA_OFIELD_ODD')
                    ncomponents=(field.L+1)*(field.L+3)/4;
                end;                
                if  strcmpi( field.storage(1:19),'STA_FIELD_STORAGE_R') && strcmpi( field.type,'STA_OFIELD_EVEN')
                    ncomponents=(field.L+2)^2/4;
                end;                                                
                field.data=zeros([2,ncomponents,field.shape],precision);
                success=true;
           end;
           
           
            % creates a tensor field from kernel
            % 
            % stafieldStruct(
            %           kernelname, {'gauss','gaussLaguerre','gaussBessel'}
            %           shape,      [dimZ,dimY,dimX]
            %           param,      {[sigma],[sigma,n],[sigma,k,s]}
            %           centered,   {0,1}
            %           L,          {0,,,N}
            %           storage     {'STA_FIELD_STORAGE_R','STA_FIELD_STORAGE_C'} 
            %           precision,  {'single','double'}        
            %           element_size [eX,eY,eZ]               
            %           )
            %
           if Nargin>2 &&  ischar(varargin{1}) && numel(varargin{2})==3 
                kernelname=varargin{1};
                shape=varargin{2};
                field.storage='STA_FIELD_STORAGE_R';
                field.type='STA_OFIELD_SINGLE';
                kparams=varargin{3};
                precision='double';
                   
                centered=0;
                if Nargin>3,
                   centered=varargin{4};
                end;                
                if Nargin>4
                    field.L=varargin{5};
                end;
                if Nargin>5,
                    field.storage=varargin{6};
                end;
                if Nargin>6,
                    precision=varargin{7};
                    assert((strcmpi(precision,'double')) || (strcmpi(precision,'single')));
                end;  
               if Nargin>7 
                   field.element_size = varargin{8};
               end;                
               params={'kname',kernelname,'kparams',kparams,'L',field.L,'centered',centered,'shape',shape,'storage',field.storage,'precision',precision,'element_size',field.element_size};
               field=sta_fspecial(params);
               success=true;
           end;
           
           
           
                if ~success,
                    error('invalid parameters');
                end;
            field.shape=size(field.data);                
            field.shape=field.shape(3:5);
                
end
