%   STA_DERIV implements spherical tensor derivatives
%
%   Usage: ofield=STA_DERIV(ifield,L,parameters);
%           
%           ifield is a stafieldStruct 
%           L is either -2,-1,0,1,2 (supported up and down derivatives)
%           parameters (optional) : list of name-value pairs including
%           parameters={
%               'conjugate' , true/false (default false)
%               'alpha' ,     scalar (default 1, multiplicator for deriv) 
%               'accurate' ,  0/1 (default 0, accuracy for derivatives) 
%           }
%
%   Example: 
%       ofield=STA_DERIV(stafieldStruct('gauss',[64,64,64],3,1),2);
%
% See also sta_prod sta_mult sta_fft sta_ifft sta_fspecial sta_lap
%