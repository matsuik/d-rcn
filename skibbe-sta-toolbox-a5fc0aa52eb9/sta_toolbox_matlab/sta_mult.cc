/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox for Matlab
 * 
 *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox for Matlab
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

#include "mex.h"

#define _SUPPORT_MATLAB_
#include "stafield.h"
#include "sta_mex_helpfunc.h"




template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

    bool conjugate=false;
    std::complex<T> alpha=1;

    alpha=hanalysis::mex_get1ParamC<T>(prhs[1]);

    if (nrhs>2)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (hanalysis::mex_hasParam(params,"conjugate")!=-1)
            conjugate=hanalysis::mex_getParam<bool>(params,"conjugate",1)[0];
    }

    try {

        hanalysis::stafield<T> ifield=hanalysis::stafield<T>((mxArray *)prhs[0]);
        if (ifield.getType()!=hanalysis::STA_OFIELD_SINGLE)
            mexErrMsgTxt("error: currently only STA_OFIELD_SINGLE is supported");


        hanalysis::stafield<T> ofield=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                      ifield.getShape(),
                                      ifield.getRank(),
                                      ifield.getStorage(),
                                      ifield.getType(),
				      ifield.getElementSize());

        hanalysis::stafield<T>::Mult(   ifield,
                                        ofield,
                                        alpha,
                                        conjugate,
                                        false);

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }
}




void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
     if (nrhs<2)
        mexErrMsgTxt("error: expected (stafield,L)\n");

    if (hanalysis::mex_isStaFieldStruct<float>(prhs[0]))
        _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
    else
        if (hanalysis::mex_isStaFieldStruct<double>(prhs[0]))
            _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
        else
            mexErrMsgTxt("error: unsupported datatype");
  
	
}