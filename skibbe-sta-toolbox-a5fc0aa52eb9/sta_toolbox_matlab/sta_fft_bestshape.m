function result = sta_fft_bestshape(imageext)
persistent optimal_fft_dim;
persistent MAXDIM;




if ~isempty(optimal_fft_dim)
    %result=0;
    %fprintf('existiert: %d\n',length(optimal_fft_dim));
else
    
count=1;
for a=0:11,
    for b=0:6,
        for c=0:4,
            for d=0:3,
                 for e=0:1,
                     for f=0:1,
                        if e+f<2,
                         d=2^a*3^b*5^c*7^d*11^e*13^f;
                         if (d<=2048)
                             optimal_fft_dim(count)=d;
                             count=count+1;
                         end;
                         %fprintf('%d\n',dim);
                        end;
                     end;
                 end;
            end;
        end;
    end;
end;
optimal_fft_dim=sort(optimal_fft_dim);
MAXDIM=max(optimal_fft_dim(:));
% fprintf('does not exist, creating index: %d\n',length(optimal_fft_dim));
end;

for a=1:length(imageext),
	if (imageext(a)>MAXDIM)
	    fprintf('warning: not supported (too large)\n');
	    result(a)=imageext(a);
	    return;
	end;
	
	index=find(optimal_fft_dim>=imageext(a));
	result(a)=optimal_fft_dim(index(1));
end;

