/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox for Matlab
 * 
 *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox for Matlab
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/


#include "mex.h"

#define _SUPPORT_MATLAB_
#include "stafield.h"
#include "sta_mex_helpfunc.h"



template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

    int L=0;
    int L1=0;
    int L2=0;
    bool normalize=false;
    std::complex<T> alpha=1;

    L=hanalysis::mex_get1Param<int>(prhs[2]);
    
    
    //printf("L %d\n",L);

    if (nrhs>3)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (hanalysis::mex_hasParam(params,"normalize")!=-1)
            normalize=hanalysis::mex_getParam<bool>(params,"normalize",1)[0];

        if (hanalysis::mex_hasParam(params,"alpha")!=-1)
            alpha=hanalysis::mex_getParamC<T>(params,"alpha",1)[0];
	
	if (hanalysis::mex_hasParam(params,"L1")!=-1)
            L1=hanalysis::mex_getParam<int>(params,"L1",1)[0];
	
	if (hanalysis::mex_hasParam(params,"L2")!=-1)
            L2=hanalysis::mex_getParam<int>(params,"L2",1)[0];
    }

    try {

//         hanalysis::stafield<T> ifieldA=hanalysis::stafield<T>((mxArray *)prhs[0]);
//         if (ifieldA.getType()!=hanalysis::STA_OFIELD_SINGLE)
//             mexErrMsgTxt("error: currently only STA_OFIELD_SINGLE is supported");
// 
//         hanalysis::stafield<T> ifieldB=hanalysis::stafield<T>((mxArray *)prhs[1]);
//         if (ifieldB.getType()!=hanalysis::STA_OFIELD_SINGLE)
//             mexErrMsgTxt("error: currently only STA_OFIELD_SINGLE is supported");
      
        hanalysis::stafield<T> ifieldA;
        if (hanalysis::stafield<T>((mxArray *)prhs[0]).getType()!=
	  hanalysis::STA_OFIELD_SINGLE)
	{
	 ifieldA=hanalysis::stafield<T>((mxArray *)prhs[0]).get(L1);
	}else
	{
	 ifieldA=hanalysis::stafield<T>((mxArray *)prhs[0]);   
	}

        hanalysis::stafield<T> ifieldB;
        if (hanalysis::stafield<T>((mxArray *)prhs[1]).getType()!=
	  hanalysis::STA_OFIELD_SINGLE)
	{
	 ifieldB=hanalysis::stafield<T>((mxArray *)prhs[1]).get(L2);  
	}else
	{
	 ifieldB=hanalysis::stafield<T>((mxArray *)prhs[1]); 
	}
        

        hanalysis::stafield<T> ofield=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                      ifieldA.getShape(),
                                      L,
                                      ifieldA.getStorage(),
                                      ifieldA.getType(),
				      ifieldA.getElementSize());

	
	
        hanalysis::stafield<T>::Prod(ifieldA,
                                     ifieldB,
                                     ofield,
                                     L,
                                     normalize,
                                     alpha,
                                     false);

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }

}




void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<3)
        mexErrMsgTxt("error: expected (stafieldA,stafieldB.L)\n");

    if ((hanalysis::mex_isStaFieldStruct<float>(prhs[0]))&&(hanalysis::mex_isStaFieldStruct<float>(prhs[1])))
        _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
    else
        if ((hanalysis::mex_isStaFieldStruct<double>(prhs[0]))&&(hanalysis::mex_isStaFieldStruct<double>(prhs[1])))
            _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
        else
            mexErrMsgTxt("error: unsupported datatype");}




