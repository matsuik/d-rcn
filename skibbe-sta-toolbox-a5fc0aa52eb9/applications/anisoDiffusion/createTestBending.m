%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  ds = createTestBending(R,bD,sigma)
%
%  creates example DWI-measurement of a circle with radius R
%  with model exp(-bD*cos^2) and Rician noise of
%  strength sigma and 64 gradient directions
%
%  EXAMPLE: creates a example and applies AFC regularized SD
%     ds = createTestBending(5,1,0.1);
%     res = sta_spdeconv(ds.data,ds.dirs,ds.mask,'verbose',true,'operation','AFC','lambda',0.001)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function  ds = createTestCrossing(R,bD,sigma,L)

alldirs =  load('symS2distributions.mat');

dst = genBending(alldirs.dirs64,bD,R,sigma);

ds.mask = dst.spatialProbabilities;
ds.data = double(permute(dst.signal,[2 3 4 1]));
ds.dirs = double(dst.sphereInterpolation.bDir);



function datastruct = genBending(dirs,bfac,r,nz)    
    
            N = 24;

            sinterp.bDir = dirs;
            datastruct.sphereInterpolation = sinterp;
     
                       
            K = Kernel(dirs,bfac); K = K / size(K,1);
            
            fac = 10;
            d = 3;
            
            sig = ones(length(dirs),N,N);
            sig_gt = ones(length(dirs),N,N);
            [X Y] = ndgrid(1:N);    
            X = X-N/2+0.5; Y = Y-N/2+0.5;
            v1 = sqrt(X.^2 + Y.^2) < r+d/2 & sqrt(X.^2 + Y.^2) > r-d/2;
            Radius =  sqrt(X.^2 + Y.^2);
            v1 = find(v1(:));
                
            mask = zeros(N,N);
            mask(v1) = 1; 
            sig(:,v1) = 0;  sig_gt(:,v1) = 0;
            
            vx = v1;
            dirx = X; diry = -Y; 
            n = sqrt(dirx.^2+diry.^2)+0.0001; 
            dirx = dirx./n; diry = diry./n;
             ang_gt = cell(N,N);
            for k = 1:length(vx),
                phi = asin(1/(2*Radius(vx(k))));
                for psi = -phi:(phi/5):phi,
                    ddir = [cos(psi) sin(psi) ; -sin(psi) cos(psi)]*[diry(vx(k)) dirx(vx(k))]'; ddir = ddir / norm(ddir);
                    S1 = exp(fac*(ddir(1)*dirs(1,:) + ddir(2)*dirs(2,:)).^2 - fac)*fac;
                    sig_gt(:,vx(k)) = sig_gt(:,vx(k)) + squeeze(S1(:));
                    sig(:,vx(k)) = sig(:,vx(k))+ K'*squeeze(S1(:))/10;
                end;
                d1 = -[diry(vx(k)) dirx(vx(k)) 0] - [dirx(vx(k)) -diry(vx(k)) 0]/Radius(vx(k))/2; d1 = d1/norm(d1);
                d2 = +[diry(vx(k)) dirx(vx(k)) 0] - [dirx(vx(k)) -diry(vx(k)) 0]/Radius(vx(k))/2; d2 = d2/norm(d2);
                ang_gt{vx(k)} = [ang_gt{vx(k)} ; d1 ; d2];
            end;
        
            sig_gt = permute(sig_gt,[2 3 1]);
            sig = permute(sig,[2 3 1]);
            for k = 1:3,            
                signal_gt(:,:,k,:) = reshape(sig_gt,[N N 1 length(dirs)]);
                signal(:,:,k,:) = reshape(sig,[N N 1 length(dirs)]);
                datastruct.spatialProbabilities(:,:,k) = mask;
            end;            
            
            datastruct.spatialProbabilities = single(datastruct.spatialProbabilities);
            for k = 1:length(dirs),
                tens(:,:,k) = dirs(:,k)*(dirs(:,k))';
            end;
            
            datastruct.b0avg = single(1+0*signal(:,:,:,1));
            datastruct.ang_gt = ang_gt;
            datastruct.signal_gt = permute(signal_gt(:,:,:,:),[4 1 2 3]) ;
            datastruct.signal = permute(signal(:,:,:,:),[4 1 2 3]) ;
            datastruct.signal = abs(datastruct.signal + nz*complex(randn(size(datastruct.signal)),randn(size(datastruct.signal))));
           
    
       

function K = Kernel(bDir,alpha)
        C = abs(bDir'*bDir);
        K = exp(-alpha * C.^2);






