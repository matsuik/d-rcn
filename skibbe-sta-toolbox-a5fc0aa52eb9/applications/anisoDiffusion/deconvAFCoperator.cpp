#include "mex.h"
#include <gsl/gsl_blas.h>

#include <stensor.h>
#include "FCoperators.h"



template<typename T>
void blas_matrix_mult(
              const enum CBLAS_TRANSPOSE transA,
              const enum CBLAS_TRANSPOSE transB,
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
  
}

using namespace std;

template<>
void blas_matrix_mult<double>(
              const enum CBLAS_TRANSPOSE transA,
              const enum CBLAS_TRANSPOSE transB,
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
    double one[2] = {1,0};
    double zero[2] = {0,0};
    if (transA == CblasTrans)
        cblas_zgemm (CblasColMajor, transA, transB, ndir,   lp_n, stride, (void*) one, mout, stride, pin, stride, (void*) zero, tmp, ndir);           
    else
        cblas_zgemm (CblasColMajor, transA, transB, stride, lp_n, ndir  , (void*) one, mout, stride, pin, ndir ,  (void*) zero, tmp, stride);           
}

template<>
void blas_matrix_mult<float>(
              const enum CBLAS_TRANSPOSE transA,
              const enum CBLAS_TRANSPOSE transB,        
		      int ndir,
		      int lp_n,
		      int stride,
		      const void *  mout,
		      const void *  pin,
		      void *  tmp)
{
    float one[2] = {1,0};
    float zero[2] = {0,0};
    if (transA == CblasTrans)
        cblas_cgemm (CblasColMajor, transA, transB, ndir,   lp_n, stride, (void*) one, mout, stride, pin, stride, (void*) zero, tmp, ndir);           
    else
        cblas_cgemm (CblasColMajor, transA, transB, stride, lp_n, ndir  , (void*) one, mout, stride, pin, ndir ,  (void*) zero, tmp, stride);           
}










template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
      
   
    const mxArray *ST1;
    ST1 = prhs[0];       
    const mwSize *dimsST1 = mxGetDimensions(ST1);
    const mwSize numdimST1 = mxGetNumberOfDimensions(ST1);
    if (numdimST1!=5)  mexErrMsgTxt("error: Array dimension for the first argument must be 5 (2x2L+1xXxYxZ)\n"); 
    std::complex<T> *pin = (std::complex<T> *) (mxGetData(ST1));
   
    const mxArray *Params  = prhs[1];        
    T* params =  ((T*)(mxGetData(Params))); 
    int L = (int) params[0]; 
               
                
    const mxArray *Params_even = prhs[2];        
    T* params_even =  ((T*)(mxGetData(Params_even)));
    T* params_even_imag =  ((T*)(mxGetImagData(Params_even)));
    std::complex<T> lambda_even[8];
    if (params_even_imag == 0)
        for (int k = 0;k < 8;k++)
            lambda_even[k] = std::complex<T>(params_even[k],0);    
    else
        for (int k = 0;k < 8;k++)
            lambda_even[k] = std::complex<T>(params_even[k],params_even_imag[k]);    
    
    const mxArray *Params_odd = prhs[3];        
    T* params_odd =  ((T*)(mxGetData(Params_odd)));
    T* params_odd_imag =  ((T*)(mxGetImagData(Params_odd)));
    std::complex<T> lambda_odd[8];
    if (params_odd_imag == 0)
        for (int k = 0;k < 8;k++)
            lambda_odd[k] = std::complex<T>(params_odd[k],0);    
    else
        for (int k = 0;k < 8;k++)
            lambda_odd[k] = std::complex<T>(params_odd[k],params_odd_imag[k]);    
    
  
    const mxArray *FRF  = prhs[4];        
    T* frf =  ((T*)(mxGetData(FRF))); 
    
    const mxArray *Mask  = prhs[5];        
    T* mask =  ((T*)(mxGetData(Mask))); 

    const mxArray *Mmat  = prhs[6];        
    T* mmat =  ((T*)(mxGetData(Mmat))); 
    
    const mxArray *Umat  = prhs[7];        
    T* umat =  ((T*)(mxGetData(Umat))); 
    
    const mxArray *Nset  = prhs[8];        
    T* nset =  ((T*)(mxGetData(Nset))); 
    int ndir = mxGetM(Nset);
    
    
    const mxArray *Type = prhs[9];            
    const mxArray *Storage = prhs[10];     
    char buf[256];
    mxGetString(Type,buf,256);
    STA_FIELD_TYPE type = enumfromstring_type(buf);
    mxGetString(Storage,buf,256);
    STA_FIELD_STORAGE storage = enumfromstring_storage(buf);
    if (type != STA_OFIELD_FULL)
    {
        mexPrintf("not supported!!");
        return;
    }
    
    
  
    plhs[0] = mxCreateNumericArray(numdimST1,dimsST1,mxGetClassID(ST1),mxREAL);
    std::complex<T> *result = (std::complex<T> *) mxGetData(plhs[0]);

    std::size_t shape[3];    
    for(int i =2;i <numdimST1;i++)
    	shape[4-i]=dimsST1[i];
    
 
    int sz = dimsST1[2]*dimsST1[3]*dimsST1[4]; 
    int stride = order2numComponents(storage,type,L);
    

    
    
    long int lp_n = 1;
 
 
    T tmp[2*ndir];
    
    
    for (int k = 0; k < sz; k++)
    {            
        bool active = false;
        for(int m = 0; m < ndir;m++)        
            if (nset[k*ndir+m]> 0)
            {
                active =true;
                break;
            }
        if (active)
        {
	
     	    blas_matrix_mult<T>( CblasTrans, CblasNoTrans, ndir, lp_n,  stride, mmat, (void*) &( pin[k*stride] ), tmp);
            
            for(int m = 0; m < ndir;m++)
            {
                  tmp[2*m] *= nset[k*ndir+m];
                  tmp[2*m+1] *= 0;
            }
            
            blas_matrix_mult<T>(CblasNoTrans, CblasNoTrans, ndir, lp_n, stride, umat, tmp, &(result[k*stride]));
	            
        }
    }
        
        
    
    
    for (int l = 0; l <= L; l++)
    {
         if (l%2 == 0)
         {
             int idx = getComponentOffset(storage,type,l);
             int numel = (storage == STA_FIELD_STORAGE_R)?(l+1):(2*l+1);

             for (int k = 0; k < sz; k++)
                 for (int m = 0; m < numel;m++)
                 {
                     result[k*stride+m+idx] += pin[k*stride+m+idx] *frf[l/2];
                     result[k*stride+m+idx] += pin[k*stride+m+idx] *mask[k];
                 }
         }
     }
             
   
    
  // compute AFC    
    bool conj = false;
    T  *v_size = NULL;    
    AFCoperator(pin,result,shape,v_size,L,conj,lambda_even,lambda_odd,storage);  
    
 

}


void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    
 int _single=0;
  int _double=0;

  int numnum = 0;
  for (int i=0;i<nrhs;i++)
  {
    if (mxIsNumeric(prhs[i])) numnum++;
    if (mxIsSingle(prhs[i])) _single++;
    if (mxIsDouble(prhs[i])) _double++;
  }
  if (_single==numnum) _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
  else
  if (_double==numnum) _mexFunction<double>( nlhs, plhs,  nrhs, prhs );  
  else 
  mexErrMsgTxt("error: all parameters must be either double or single precision!\n");       

}



