%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    result = sta_steerFilt(image,varargin)
%
%    projects a scalar or vector image onto a kernel of type 
%        k(r,n) = \sum_{l=0}^L \frac{lambda^l}{(2l-1)!!} Y^l(n) \nabla^l kernel(|r|)
%    where \nabla^l is a spherical derivative of order l and kernel a 
%    radial symmetric basis kernel (e.g. the gaussian)
%
%    Parameters: 
%         image - input image (3D-array, stafield or stafield.struct)
%                 if image=='showkernel', the kernel is rendered and 
%                 visualized
%         varargin - parameter,value pairs
%               L - SH cutoff
%               kernel - a kernel as a stafield.struct, or a string determining
%                        type of kernel (e.g. 'gauss', see sta_fspecial)
%               kparam - if kernel is a string, then kparam contains appropriate
%                        parameters (see sta_fspecial)
%               alpha - elongation parameter of steerable kernel
%               type   - defines type of orientation field, symmetric 'STA_OFIELD_EVEN',
%                        ot anti-symmetric 'STA_OFIELD_ODD' or no symmtry
%                        'STA_OFIELD_FULL'
%               P      - vector that defines generalized filter kernel of type
%                        k(r,n) = \sum_{l=0}^L P(l) Y^l(n) \nabla^l kernel(|r|)
%                        For full field length(P) = L+1, for odd fields 
%                        length(P) = floor((L+1)/2), for even length(P) = ceil((L+1)/2)
%         result - returns the filter reponse as a stafield.struct            
% 
%    Examples:
%         %% line detectors
%         sta_steerFilt('showkernel','L',8,'kparam',2,'alpha',4);
%         sta_steerFilt('showkernel','L',8,'alpha',3,'kernel','gaussBessel','kparam',[8 5 1]);
%         %% edge detector
%         sta_steerFilt('showkernel','L',8,'kparam',2,'alpha',i*5,'type','STA_OFIELD_ODD');
%         %% ridge detector
%         sta_steerFilt('showkernel','L',8,'kparam',2,'alpha',i*5,'type','STA_OFIELD_EVEN');
%         %% custom filter
%         sta_steerFilt('showkernel','L',8,'kparam',2,'P',1.1.^[0 1 2 3 4]','type','STA_OFIELD_EVEN');



function image = sta_steerFilt(image,varargin)


showkernel = false;

if strcmp(image,'showkernel'),
    image = zeros(24,24,24);
    image(12,12,12) = 1;
    image = stafield(image).struct;
    showkernel = true;
end;

if not(strcmp(class(image),'stafield')),  %% this script only works with structs
    image = stafield(image).struct;
end;

if image.L > 1,
    warning('not supported!!');
    return;
end;

kernel = 'gauss';
kparam = 1;

L = 4;
alpha = 1;
P =[];
type = 'STA_OFIELD_EVEN';
precision = iff(isa(image.data,'single'),'single','double');
realNum = @(x) iff(isa(image.data,'single'),single(x),double(x));

for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;

if strcmp(type,'STA_OFIELD_EVEN') | strcmp(type,'STA_OFIELD_ODD'),
    step = realNum(2);
else
    step = realNum(1);
end;

if strcmp(type,'STA_OFIELD_ODD') & image.L==0,
    image = sta_deriv(image,1);    
end;

if isempty(P),
    start = image.L;
    if step == 2,
        l = (start:2:L)';
        P = alpha^2./(l.*(l-1));
    else
        l = (start:L)';
        P = alpha./l;
    end;
    P(1) = 1;
end;
P = realNum(P);

if not(isempty(kernel)),
    if isstr(kernel),
        kernel = stafield(kernel,image.shape,kparam,0,0,image.storage,precision).struct;
    end;
    image = sta_ifft(sta_prod(sta_fft(image),sta_fft(kernel),image.L));
end;

image = stafield(SteerableFilter(image,P,step));

if showkernel,
    [M lidx]=getSHMatrix(L,[0 0 1]',image.type,image.storage,false);
    kernelimg = reshape(M'*squeeze(image.data(1,:,:)),image.shape);
    %stackview(kernelimg,@(x) imagesc(x,[0 max(kernelimg(:))]));
    isoval = max(kernelimg(:))*0.25;
    clf; 
    plot3(12,12,12,'*');
    p = patch(isosurface(kernelimg.*(kernelimg>0),isoval)); 
    set(p,'facecolor','red','edgecolor','none');
    p = patch(isosurface(-kernelimg.*(kernelimg<0),isoval)); 
    set(p,'facecolor','blue','edgecolor','none');
    view(-35,30);
    camlight; grid on;
    axis equal;
end;





return;