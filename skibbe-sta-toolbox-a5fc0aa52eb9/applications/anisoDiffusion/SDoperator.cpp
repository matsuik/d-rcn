#include "mex.h"

#define _LINK_FFTW


#include <stensor.h>
#include "FCoperators.h"
#include "sta_mex_helpfunc.h"

using namespace std;


inline int fielddim(int l, STA_FIELD_STORAGE prop)
{
    if (prop == STA_FIELD_STORAGE_C)
        return 2*l+1;
    else
        return l+1;
}


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    
  
    if (nrhs<5)  mexErrMsgTxt("error: not enough input arguments\n"); 
  
    bool conjugate = false;
    
    const mxArray *ST1;
    ST1 = prhs[0];       
    const mwSize *dimsST1 = mxGetDimensions(ST1);
    const mwSize numdimST1 = mxGetNumberOfDimensions(ST1);
    if (numdimST1!=5)  mexErrMsgTxt("error: Array dimension for the first argument must be 5 (2x2L+1xXxYxZ)\n"); 
    std::complex<T> *pin = (std::complex<T> *) (mxGetData(ST1));
   
    
    const mxArray *Gauss = prhs[1];     
    std::complex<T> *gauss = (std::complex<T> *) (mxGetData(Gauss));
     
        
    const mxArray *Params = prhs[2];        
    T* params =  ((T*)(mxGetData(Params)));
    T* params_imag =  ((T*)(mxGetImagData(Params)));
    int L = (int) params[0];         
  
    std::complex<T> alpha_gum;
    
    if (params_imag != 0)
        alpha_gum = std::complex<T>(params[1],params_imag[1]);
    else
        alpha_gum = std::complex<T>(params[1],0);
        
    std::complex<T> lambda1 = params[2];
    std::complex<T> lambda2 = params[3];
    std::complex<T> lambda3 = params[4];
    std::complex<T> lambda4 = params[5];
  
    
    const mxArray *Type = prhs[3];            
    const mxArray *Storage = prhs[4];     
    char buf[256];
    mxGetString(Type,buf,256);
    STA_FIELD_TYPE type = enumfromstring_type(buf);
    mxGetString(Storage,buf,256);
    STA_FIELD_STORAGE storage = enumfromstring_storage(buf);
    int step;
    if (type == STA_OFIELD_FULL)
        step = 1;
    else if (type == STA_OFIELD_EVEN)
        step = 2;
    else
    {
        mexPrintf("not supported!!");
        return;
    }
    
    

    plhs[0] = mxCreateNumericArray(numdimST1,dimsST1,mxGetClassID(ST1),mxREAL);
    std::complex<T> *result = (std::complex<T> *) mxGetData(plhs[0]);

    mwSize dimsTmp[5];
    for(int i =0;i <numdimST1;i++)
        dimsTmp[i] = dimsST1[i];
    dimsTmp[1] = fielddim(L,storage);

    
    mxArray *Tmp1 = mxCreateNumericArray(numdimST1,dimsTmp,mxGetClassID(ST1),mxREAL);
    mxArray *Tmp2 = mxCreateNumericArray(numdimST1,dimsTmp,mxGetClassID(ST1),mxREAL);
    std::complex<T> *tmp1 = (std::complex<T> *) mxGetData(Tmp1);
    std::complex<T> *tmp2 = (std::complex<T> *) mxGetData(Tmp2);
    
    std::size_t shape[3];    
    for(int i =2;i <numdimST1;i++)
    {
    	shape[4-i]=dimsST1[i];
    }
    

    T  *v_size = NULL;
    std::vector<T> element_size;
    
    if (nrhs>5)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (hanalysis::mex_hasParam(params,"element_size")!=-1)
	{
	  element_size=hanalysis::mex_getParam<T>(params,"element_size",3);
          v_size=element_size.data();  
	  std::swap(v_size[0],v_size[2]);
	}

    }

    std::size_t sz = dimsST1[2]*dimsST1[3]*dimsST1[4];
 
    int stride =  order2numComponents(storage,type,L);
    
    
    // Atranspose
   
   
    
 
    
    std::complex<T> alpha;

    for (int l = L; l >= 0;l-=step)
    {        
        alpha = pow(-conj(alpha_gum),l) / (std::complex<T>)gsl_sf_fact(l);          
        int idx = getComponentOffset(storage,type,l);
   
        if (l == L)
        {   
	    #pragma omp parallel for num_threads(get_numCPUs())
            for (std::size_t k = 0; k < sz; k++)
	    {
                for(int m = 0; m <  fielddim(l,storage);m++)
                    tmp1[k*fielddim(l,storage)+m] = alpha*pin[k*stride+idx+m];                    
	    }
        }
        else                
        {       
            
            if (step == 1)   
                 hanalysis::sta_derivatives( tmp1,tmp2,shape,l+1,-1,conjugate,alpha,storage,v_size);  
            else
                 hanalysis::sta_derivatives2( tmp1,tmp2,shape,l+2,-2,conjugate,alpha,storage,v_size);  
	    
	    #pragma omp parallel for num_threads(get_numCPUs())
            for (std::size_t k = 0; k < sz; k++)
	    {
                for(int m = 0; m <  fielddim(l,storage);m++)
                    tmp1[k* fielddim(l,storage)+m] = tmp2[k] + alpha*pin[k*stride+idx+m];                    
	    }
        }       
    }

    
  
    /// perform convolution
    
    hanalysis::sta_fft<T,T>(tmp1,tmp2,shape,1,true);    



    #pragma omp parallel for num_threads(get_numCPUs())
    for (std::size_t k = 0; k < sz; k++)
    {
         tmp2[k] *= gauss[k]/(std::complex<T>)sz;
    }


    hanalysis::sta_fft<T,T>(tmp2,tmp1,shape,1,false);
    //hanalysis::sta_fft<T,T>(tmp2,tmp1,shape,1,false);
    
    

    #pragma omp parallel for num_threads(get_numCPUs())
    for (std::size_t k = 0; k < sz; k++)
    {
        result[k*stride] = tmp1[k];
    }
 
    
    
  //  apply A
    
    for (int l = step; l <= L; l+=step)
    {
       
          int idx =  getComponentOffset(storage,type,l-step);
          int idxP =  getComponentOffset(storage,type,l);
          if (step == 1)   
          {
	      
               alpha = alpha_gum/(std::complex<T>)l;
               hanalysis::sta_derivatives(&(result[idx]),&(result[idxP]),shape,l-1,1,conjugate,alpha,storage,v_size,stride,stride);  
          }
          else
          {
	    
               alpha = alpha_gum*alpha_gum/(std::complex<T>)(l*(l-1));
               hanalysis::sta_derivatives2(&(result[idx]),&(result[idxP]),shape,l-2,2,conjugate,alpha,storage,v_size,stride,stride);  
          }
    }
  
  // compute FC
  
  FCoperator(pin,result,shape,v_size,L,conjugate,lambda1,lambda2,lambda3,lambda4,storage,type);  
  
}


void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  int _single=0;
  int _double=0;

  int numnum = 0;
  for (int i=0;i<nrhs;i++)
  {
    if (mxIsNumeric(prhs[i])) numnum++;
    if (mxIsSingle(prhs[i])) _single++;
    if (mxIsDouble(prhs[i])) _double++;
  }
  if (_single==numnum) _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
  else
  if (_double==numnum) _mexFunction<double>( nlhs, plhs,  nrhs, prhs );  
  else 
  mexErrMsgTxt("error: all parameters must be either double or single precision!\n");   
}











