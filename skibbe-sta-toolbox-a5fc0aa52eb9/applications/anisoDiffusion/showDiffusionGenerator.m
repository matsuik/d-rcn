%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    showDiffusionGenerator(varargin) 
%
%    implements diffusion generation for illustration purposes
%    The equation 
%        \[ df/dt = Lf \] 
%    in numerically integrated,  where the differential operator L 
%    is contructed by a linear combination of 
%         T2 - horizontal diffusion (contour enhancement) corresponding 
%               to $(n \cdot \nabla)^2$
%         W2 - orthogonal diffusion (plane enhancement) corresponding 
%               to $\Laplace - (n \cdot \nabla)^2$
%         FT2F - corresponding to $F (n \cdot \nabla)^2 F^t$ where
%               F is the unitary Funk-Radon transform
%         FW2F - corresponding to $F (\Laplace - (n \cdot \nabla)^2) F^t$
%                            
%   Parameters: parameter,value tuples
%          sz - size of simulation domain (default [24 24 24])
%          dir - direction of initial pulse (default [0 0 1])
%          sigma - with of initial pulse  (default 1)
%          L - SH-cutoff (default 4)
%          type - orientation field type 
%             'STA_OFIELD_EVEN'/'STA_OFIELD_ODD'/'STA_OFIELD_FULL' (default)
%          maxit - number of iterations (default 20)
%          lambda - integration step-width (default 0.2)
%          operation - string representing generator (default 'T2')
%        
%
%   Examples:
%        %% along the line 
%        showDiffusionGenerator('L',4,'operation','T2')
%        %% orthogonal to the line 
%        showDiffusionGenerator('L',4,'operation','W2')
%        %% a bit of isotropy
%        showDiffusionGenerator('L',4,'operation','T2+0.1*W2')
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function showDiffusionGenerator(varargin)

type = 'STA_OFIELD_EVEN';
storage = 'STA_FIELD_STORAGE_R';

sz = [1 1 1]*24;
dir = [0 0 1];
L = 4;
maxit = 20;
sigma = 1;
lambda = 0.1;
operation = 'T2';


for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;


if strcmp(type, 'STA_OFIELD_EVEN') && mod(L,2) == 1,
    L = L + 1;
end;
if strcmp(type, 'STA_OFIELD_ODD') && mod(L,2) == 0,
    L = L + 1;
end;

Gaussian =  sta_fspecial('gauss',{'kname','gauss','kparams',sigma*sqrt(2),'shape',sz});
Gaussian.data = fftshift(fftshift(fftshift(Gaussian.data,3),4),5);
D = getSHMatrix(L,dir'/norm(dir),type,storage,0);
X = cat(1,repmat(real(D)',[1 1 sz]),repmat(imag(D)',[1 1 sz])).*repmat(Gaussian.data(1,:,:,:,:),[2 size(D,1) 1 1 1]);
X = stafield(X,type,storage);


figure(3);
sta_quiver(X); drawnow;

T2  =  [0.5 1  1  1];
FT2F = [0.5 1 -1 -1];
W2 =   [1  -1 -1 -1];
FW2F = [1  -1  1  1];

op = eval(operation);
opparams = lambda*[op ];
params = ([L opparams]);

tic;
X = graddesc(@(z)FCoperator(z,params,X.type,X.storage),X,maxit);
toc;

function [x] = graddesc(Aop,b,its)
 x = b;
 for i=1:its,
     x.data = x.data - Aop(x.data);
     if mod(i,5) == 0,
        figure(3);
        sta_quiver(x);
        drawnow;
     end;
    fprintf('.');
 end;
end;
end
