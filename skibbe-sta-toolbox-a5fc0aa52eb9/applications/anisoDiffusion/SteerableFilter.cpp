#include "mex.h"

#define _LINK_FFTW

#define _SUPPORT_MATLAB_

#include <stafield.h>
#include <sta_error.h>
#include <sta_mex_helpfunc.h>


/* parameters
      prhs[0] inputfield1
*/
template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)  mexErrMsgTxt("error: nrhs < 1\n");

    try {
        hanalysis::stafield<T>field=hanalysis::stafield<T>((mxArray *)prhs[0]);
        
 
        const mxArray *Params = prhs[1];        
        T* params =  ((T*)(mxGetData(Params)));
 	    int L = mxGetM(Params)-1;
        
        
    
        const mxArray *Step = prhs[2];        
        T* step_ =  ((T*)(mxGetData(Step)));
  	    int step = step_[0];
       
        
        hanalysis::stafield<T> gauss_deriv;
                
        if (step == 1)
            gauss_deriv=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                               field.getShape(),
                                               L,
                                               field.getStorage(),
                                               hanalysis::STA_OFIELD_FULL,
					       field.getElementSize());
        else
        {
            if (field.getRank() == 0)
                gauss_deriv=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                                   field.getShape(),
                                                   2*L,
                                                   field.getStorage(),
                                                   hanalysis::STA_OFIELD_EVEN,
						   field.getElementSize());
            else if (field.getRank() == 1)
                gauss_deriv=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                                   field.getShape(),
                                                   2*L+1,
                                                   field.getStorage(),
                                                   hanalysis::STA_OFIELD_ODD,
						   field.getElementSize());
            else 
            {
                mexPrintf("not supported!!\n"); return;
            }
        }
        

  

       if (step == 1)
       {
           gauss_deriv.get(0)=field;
	   gauss_deriv.get(0)*=(params[0]);  
           for (int l=0;l<L;l++)
                gauss_deriv.get(l+1)=gauss_deriv.get(l).deriv(1,false,params[l+1]);
       }
       else
       {
            if (field.getRank() == 0)
            {
               gauss_deriv.get(0)=field;
	       gauss_deriv.get(0)*=(params[0]);  
               for (int l=0;l<(2*L);l+=2)
                    gauss_deriv.get(l+2)=gauss_deriv.get(l).deriv2(2,false,params[l/2+1]);
            }
            else
            {
               gauss_deriv.get(1)=field;
	       gauss_deriv.get(1)*=(params[0]);  
               for (int l=1;l<(2*L+1);l+=2)
                    gauss_deriv.get(l+2)=gauss_deriv.get(l).deriv2(2,false,params[(l-1)/2+1]);
            }
                
       }
             


    } catch (hanalysis::STAError & error)
    {
        printf(error.what());
    }

}



void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
     if (hanalysis::mex_isStaFieldStruct<float>(prhs[0]))    
            _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
     else if (hanalysis::mex_isStaFieldStruct<double>(prhs[0]))    
            _mexFunction<double>( nlhs, plhs,  nrhs, prhs );  
     else 
         mexErrMsgTxt("error: first argument not a proper stafield!\n");       
}


