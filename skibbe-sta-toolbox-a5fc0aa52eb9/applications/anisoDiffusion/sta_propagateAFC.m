%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    res = sta_propagateAFC(varargin) 
%
%    similar to sta_propagateFC, but more general with more freedom 
%    in the design of the equation 
%        \[ df/dt = Lf \] 
%    which is numerically integrated,  where the differential operator L 
%    is contructed by a linear combination of 
%         T - horizontal translation (contour completetion) corresponding 
%               to $(n \cdot \nabla)$ where LB is the spherical
%               Laplacian 
%         T2 - horizontal diffusion (contour enhancement) corresponding 
%               to $(n \cdot \nabla)^2$
%         W2 - orthogonal diffusion (plane enhancement) corresponding 
%               to $\Laplace - (n \cdot \nabla)^2$
%         AFC - asymmetric contour enhancement following the paper
%                Reisert M, Kellner E, Kiselev VG.
%                IEEE Trans Med Imaging. 2012 Jun;31(6):1240-9. doi: 10.1109/TMI.2012.2187916. Epub 2012 Feb 15.
%                About the geometry of asymmetric fiber orientation distributions.
%                (for symmetric distributions)
%         AFCanti - asymmetric contour enhancement for anti-symmtric distributions
%         Laplace - isotropic spatial Laplacian
%         LB - Spherical Laplacian
%          
%
%   Parameters: parameter,value tuples
%          maxit - number of iterations (default 10)
%          lambda - integration step-width (default 0.1)
%          operation - string representing generator (default 'AFC')
%          verbose - show evolution
%          lb - contribution of spherical Laplacian in operator AFC (default 0.01)
%          beta - curvature parameter in AFC (default 1)          
%
%   Examples:
%
%     %% line enhancement example
%     image=zeros(32,32,32);
%     image(16,16,[1:5 8:32])=1;
%     image(16,:,16) = 1;
%     kernel =  sta_fft(sta_fspecial('gauss',{'kname','gauss','kparams',1,'shape',size(image),'precision','single'}));
%     data = sta_fft(stafield(single(image)).struct);
%     image = sta_ifft(sta_prod(data,kernel,0));
%     image.data(1,1,:,:,:) = image.data(1,1,:,:,:) + 0.2*randn([1 1 image.shape])*max(image.data(:));
%     figure(1); stackview(squeeze(image.data(1,1,:,:,:)),@imagesc);
%     X = sta_steerFilt(image,'type','STA_OFIELD_FULL');
%     Y = sta_propagateAFC(X,'maxit',30,'verbose',true,'operation','-T');    
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




function X = smoothFC(ofield,varargin)

lambda = 0.1;
maxit = 10;
verbose = true;
operation = 'T';
beta = 1;
lb = 0.01;
showfun = @sta_glyph;

for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;


realNum = @(x) iff(isa(ofield.data,'single'),single(x),double(x));
type = ofield.type;
storage = ofield.storage;
L = realNum(ofield.L);

if not(strcmp(type,'STA_OFIELD_FULL')),
    warning('not supported!')
    return;
end;

% op = -lam1*\Delta + lam2 - lam3*(l+1)*l - lam4*Z_ll - lam5*Z_l(l+1) - lam6*Z_l(l-1) - lam7*Z_l(l+2) - lam8*Z_l(l-2)

T2_  =  [0.5  0  0  1  0  0  1  1];
FT2F_ = [0.5  0  0  1  0  0 -1 -1];
W2_ =   [1    0  0 -1  0  0 -1 -1];
FW2F_ = [1    0  0 -1  0  0  1  1];
P2_  =  [0.5  0  0  1  0  0  0  0];
LB_ =   [0    0  1  0  0  0  0  0];
T_  =   [0    0  0  0  1  1  0  0];
damp_ = [0    1  0  0  0  0  0  0];
Lap_ =  [1    0  0  0  0  0  0  0];

AFC = [T2_ - beta*T_ ; damp_*beta^2 + beta*T_] + lb*[LB_;LB_];
AFCanti = [damp_*beta^2 + beta*T_; T2_ - beta*T_] + lb*[LB_;LB_];
T2 = [T2_;T2_];
T = [T_;T_];
W2 = [W2_;W2_];
Laplace = [Lap_ ; Lap_];
LB =  [LB_ ; LB_];
op = realNum(2/3*lambda*eval(operation));




tic;
X = graddesc(@(z)(AFCoperator(z,L,op(1,:),op(2,:),type,storage)),ofield,maxit);
toc;



function [x] = graddesc(Aop,b,its)
 x = b;
 for i=1:its,
     x.data = x.data - Aop(x.data);
     if verbose,
         if mod(i,5) == 0,
            sfigure(3);
            showfun(x);            
            drawnow;
         end;
     end;
     fprintf('.');
 end;
end;
end
