function sta_CircHough(image,varargin)
rng(0);
if strcmp(image,'test'),
    [X Y Z] = ndgrid((-16:15)); R = sqrt(X.^2 + Y.^2 + Z.^2);    
    image = (2-2./(1+exp(-2*abs(R-7)))).*(rand(32,32,32)>0.5);
    image=image+0.3*randn(size(image)).*max(image(:));
    figure(1);
    stackview(image);
end;


L = 4;
lambda = 0.1;
kernel = 'gauss';
kparam = 1;
maxit = 250;

for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;



image = stafield(image);

kernel = stafield(kernel,image.shape,kparam,0,0,image.storage);

image = image.fft().prod(kernel.fft(),image.L,1).ifft();


gimage = image.deriv(1);

shog = sta_shog(gimage.struct,{'BW',L,'gamma',1}); shog.shape = gimage.shape;
%shog = sta_S2conv(shog,(1-(-1).^(1:(L+1)))/2);

%showfun = @(x) stackview(squeeze(x.data(1,1,:,:,:))*0.00003);
%showfun = @(x) imagesc(squeeze(max(squeeze(x.data(1,1,:,:,:)),[],1)));
%showfun = @(x) sta_glyph(x,'scval',0.01,'background','mean'); 
showfun = @(x) stackview(squeeze(x.data(1,1,:,:,:))*0.00003);
%shog = smoothAFC(shog,'operation','-CC+0.0*FC+0.01*LB','lambda',lambda,'maxit',maxit,'showfun',showfun)
shog = smoothAFC(shog,'operation','CC+0.1*FC','lambda',lambda,'maxit',150,'showfun',showfun)

% 
% function showfun(x)
% % sta_glyph(x,'scval',0.01,'background','mean');
% y = x.data(1,1,:,:,:).^2;
% y = squeeze(y);
% imagesc(squeeze(max(y,[],1)));
% %pause; 