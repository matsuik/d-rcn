%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    result = sta_voteFilt(image,varargin)
%
%    transposed operation to sta_steerFilt: renders back an orientation field
%     onto a scalar image with the kernel
%        k(r,n) = \sum_{l=0}^L \frac{lambda^l}{(2l-1)!!} Y^l(n) \nabla^l kernel(|r|)
%    where \nabla^l is a spherical derivative of order l and kernel a 
%    radial symmetric basis kernel (e.g. the gaussian)
%
%    Parameters: 
%         image - orientation field (stafield or stafield.struct)
%         varargin - parameter,value pairs
%               kernel - a kernel as a stafield.struct, or a string determining
%                        type of kernel (e.g. 'gauss', see sta_fspecial)
%               kparam - if kernel is a string, then kparam contains appropriate
%                        parameters (see sta_fspecial)
%               alpha - elongation parameter of steerable kernel
%               P      - vector that defines generalized filter kernel of type
%                        k(r,n) = \sum_{l=0}^L P(l) Y^l(n) \nabla^l kernel(|r|)
%                        For full field length(P) = L+1, for odd fields 
%                        length(P) = floor((L+1)/2), for even length(P) = ceil((L+1)/2)
%         result - returns the filter reponse as a stafield
% 
%    Examples:
%       X = sta_steerFilt('showkernel','L',8,'alpha',3); % computes 'forward' operation
%       Y = sta_voteFilt(X,'alpha',3); % computed 'backward' operation
%       sta_glyph(Y)  % shows the isotropic result



function res = voteFilt(image,varargin)


if image.L == 0,
    warning('not supported!!');
    return;
end;

kernel = 'gauss';
kparam = 1;

L = image.L;
alpha = 1;
P =[];

if strcmp(image.type,'STA_OFIELD_EVEN') | strcmp(image.type,'STA_OFIELD_ODD'),
    step = 2;
else
    step = 1;
end;


if not(strcmp(class(image),'stafield')),  %% this script only works with stafield-class
    image = stafield(image);
end;


for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;

if isempty(P),
    start = 0;
    if step == 2,
        l = (start:2:L)';
        P = alpha^2./(l.*(l-1));
    else
        l = (start:L)';
        P = alpha./l;
    end;
    P(1) = 1;
end;

P = cumprod(P);

for l = L:-step:step,
    if step == 1,
        image = image.deriv(-1)*P(l+1);
    else,
        if l == L,
            res = image.get(l).deriv2(-2)*P(ceil((l+1)/2));
        else,
            res = image.get(l)*P(ceil((l+1)/2))+res;
            res = res.deriv2(-2);
        end;
    end;
end;
res = res + image.get(0)*P(1);

if not(isempty(kernel)),
    if isstr(kernel),
        kernel = stafield(kernel,image.shape,kparam,0,0,image.storage).struct;
    end;
    res = sta_ifft(sta_prod(sta_fft(res.struct),sta_fft(kernel),0));
end;

return;


