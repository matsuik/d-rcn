
using namespace hanalysis;




template <typename T>
void FCoperator (std::complex<T> *pin , std::complex<T> *result,std::size_t *shape,T *v_size, int L,bool conj,
        std::complex<T> lambda1 , std::complex<T> lambda2 , std::complex<T> lambda3 , std::complex<T> lambda4, STA_FIELD_STORAGE storage, STA_FIELD_TYPE type)
{
    int stride = order2numComponents(storage,type,L);
  
    int step = 1; 
    if (type == STA_OFIELD_EVEN)
        step = 2;
    else if (type == STA_OFIELD_FULL)
        step = 1;
    else
        mexPrintf("unsupported\n");
            

    hanalysis::sta_laplace(pin,result,shape,stride,1,(std::complex<T>)-lambda1,storage,v_size);
   
    for (int l = 0; l <= L; l+=step)
    {
         int idx = getComponentOffset(storage,type,l);
   
         if (l>0)
         {
             std::complex<T> alpha = (std::complex<T>)hanalysis::clebschGordan(2,0,l,0,l,0);
             alpha =  -lambda2*alpha*alpha;
             hanalysis::sta_derivatives2( &(pin[idx]),&(result[idx]),shape,l,0,conj,alpha,storage,v_size,stride,stride);
         }
         
         if (l+2 <= L)
         {             
             int idxP =  getComponentOffset(storage,type,l+2);

             std::complex<T> alpha = (std::complex<T>)hanalysis::clebschGordan(2,0,l+2,0,l,0);
             alpha =  -lambda3*alpha*alpha  * T((2*(l+2)+1))/T((2*l+1));
             hanalysis::sta_derivatives2( &(pin[idxP]),&(result[idx]),shape,l+2,-2,conj,alpha,storage,v_size,stride,stride);
         }
         
         if (l-2 >= 0)
         {             
             int idxP = getComponentOffset(storage,type,l-2);
  
             std::complex<T> alpha =(std::complex<T>)hanalysis::clebschGordan(2,0,l-2,0,l,0);           
             alpha =  -lambda4*alpha*alpha *  T((2*(l-2)+1))/T((2*l+1));             
             hanalysis::sta_derivatives2( &(pin[idxP]),&(result[idx]),shape,l-2,2,conj,alpha,storage,v_size,stride,stride);
         }
     
         
    }
}




template <typename T>
void AFCoperator (std::complex<T> *pin , std::complex<T> *result,std::size_t *shape,T *v_size, int L,bool conj,
        std::complex<T> *lambda_even , std::complex<T> *lambda_odd, STA_FIELD_STORAGE storage)
{
    STA_FIELD_TYPE type = STA_OFIELD_FULL;
    int stride = order2numComponents(storage,type,L);
    std::complex<T> *lambda;
    std::size_t sz =shape[0]*shape[1]*shape[2];

    for (int l = 0; l <= L; l++)
    {
         if (l%2 == 0)
            lambda = lambda_even;
         else
            lambda = lambda_odd;

         int idx = getComponentOffset(storage,type,l);

         int numel = (storage == STA_FIELD_STORAGE_R)?(l+1):(2*l+1);

         if (abs(lambda[0]) > 0)
              hanalysis::sta_laplace( &(pin[idx]),&(result[idx]),shape,numel,1,(std::complex<T>)-lambda[0],storage,v_size,stride,stride);
         
         std::complex<T> factor = (lambda[1] + lambda[2]*(T)((l+1)*l));
         if (abs(factor) > 0)
         {
	     #pragma omp parallel for num_threads(get_numCPUs())
             for (std::size_t k = 0; k < sz; k++)
	     {
                 for (int m = 0; m < numel;m++)
                     result[k*stride+m+idx] += pin[k*stride+m+idx]*factor;
	     }
         }
             
         
         if (l>0 && abs(lambda[3]) > 0)
         {
             std::complex<T> alpha = (std::complex<T>)hanalysis::clebschGordan(2,0,l,0,l,0);
             alpha =  -lambda[3]*alpha*alpha;
             hanalysis::sta_derivatives2( &(pin[idx]),&(result[idx]),shape,l,0,conj,alpha,storage,v_size,stride,stride);
         }

         if (l+1 <= L && abs(lambda[4]) > 0)
         {             
             int idxP =  getComponentOffset(storage,type,l+1);
             std::complex<T> alpha = (std::complex<T>)hanalysis::clebschGordan(1,0,l+1,0,l,0);
             alpha =  -lambda[4]*alpha*alpha  * T((2*(l+1)+1))/T((2*l+1));
             hanalysis::sta_derivatives( &(pin[idxP]),&(result[idx]),shape,l+1,-1,conj,alpha,storage,v_size,stride,stride);
         }

         if (l-1 >= 0 && abs(lambda[5]) > 0)
         {             
             int idxP =  getComponentOffset(storage,type,l-1);
             std::complex<T> alpha = (std::complex<T>)hanalysis::clebschGordan(1,0,l-1,0,l,0);
             alpha =  -lambda[5]*alpha*alpha  * T((2*(l-1)+1))/T((2*l+1));
             hanalysis::sta_derivatives( &(pin[idxP]),&(result[idx]),shape,l-1,1,conj,alpha,storage,v_size,stride,stride);
         }


         if (l+2 <= L && abs(lambda[6]) > 0)
         {             
             int idxP =  getComponentOffset(storage,type,l+2);
             std::complex<T> alpha = (std::complex<T>)hanalysis::clebschGordan(2,0,l+2,0,l,0);
             alpha =  -lambda[6]*alpha*alpha  * T((2*(l+2)+1))/T((2*l+1));
             hanalysis::sta_derivatives2( &(pin[idxP]),&(result[idx]),shape,l+2,-2,conj,alpha,storage,v_size,stride,stride);
         }

         if (l-2 >= 0 && abs(lambda[7]) > 0)
         {             
             int idxP = getComponentOffset(storage,type,l-2);
             std::complex<T> alpha =(std::complex<T>)hanalysis::clebschGordan(2,0,l-2,0,l,0);           
             alpha =  -lambda[7]*alpha*alpha *  T((2*(l-2)+1))/T((2*l+1));             
             hanalysis::sta_derivatives2( &(pin[idxP]),&(result[idx]),shape,l-2,2,conj,alpha,storage,v_size,stride,stride);
         }
         
     
         
    }

}

  