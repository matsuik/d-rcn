%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    res = sta_propagateFC(varargin) 
%
%    implements diffusion time evolution.
%    The equation 
%        \[ df/dt = Lf \] 
%    in numerically integrated,  where the differential operator L 
%    is contructed by a linear combination of 
%         T2 - horizontal diffusion (contour enhancement) corresponding 
%               to $(n \cdot \nabla)^2$
%         W2 - orthogonal diffusion (plane enhancement) corresponding 
%               to $\Laplace - (n \cdot \nabla)^2$
%         FT2F - corresponding to $F (n \cdot \nabla)^2 F^t$ where
%               F is the unitary Funk-Radon transform
%         FW2F - corresponding to $F (\Laplace - (n \cdot \nabla)^2) F^t$
%                            
%   Parameters: parameter,value tuples
%          maxit - number of iterations (default 20)
%          lambda - integration step-width (default 0.2)
%          operation - string representing generator (default 'T2')
%          verbose - show evolution
%
%   Examples:
%
%     %% line enhancement example
%     image=zeros(32,32,32);
%     image(16,16,[1:5 8:32])=1;
%     image(16,:,16) = 1;
%     kernel =  sta_fft(sta_fspecial('gauss',{'kname','gauss','kparams',1,'shape',size(image),'precision','single'}));
%     data = sta_fft(stafield(single(image)).struct);
%     image = sta_ifft(sta_prod(data,kernel,0));
%     image.data(1,1,:,:,:) = image.data(1,1,:,:,:) + 0.2*randn([1 1 image.shape])*max(image.data(:));
%     figure(1); stackview(squeeze(image.data(1,1,:,:,:)),@imagesc);
%     X = sta_steerFilt(image);
%     Y = sta_propagateFC(X,'maxit',50,'verbose',true);
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







function X = sta_propagateFC(ofield,varargin)


lambda = 0.1;
maxit = 10;
verbose = false;
operation = 'T2';

for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;


type = ofield.type;
storage = ofield.storage;
L = ofield.L;
realNum = @(x) iff(isa(ofield.data,'single'),single(x),double(x));


T2  =  [0.5 1  1  1];
FT2F = [0.5 1 -1 -1];
W2 =   [1  -1 -1 -1];
FW2F = [1  -1  1  1];

op = eval(operation);


opparams = realNum(lambda*[op ]);
params = realNum([L opparams]);


tic;
X = graddesc(@(z)(FCoperator(z,params,type,storage)),ofield,maxit);
toc;



function [x] = graddesc(Aop,b,its)
 x = b;
 for i=1:its,
     x.data = x.data - Aop(x.data);
     if verbose,
         if mod(i,3) == 0,
            sfigure(3);
            sta_glyph(x);
            drawnow;
         end;
     end;
     fprintf('.');
 end;
end;
end
