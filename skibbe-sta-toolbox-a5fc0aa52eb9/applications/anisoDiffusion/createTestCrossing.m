%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  ds = createTestBending(ang1,ang2,bD,sigma)
%
%  creates example DWI-measurement of a crossing with angles (ang1,ang2)
%  with model exp(-bD*cos^2) and Rician noise of
%  strength sigma and 64 gradient directions
%
%  EXAMPLE: creates an example and applies FC regularized SD
%     ds = createTestCrossing(-pi/8,pi/4,1,0.01)
%     res = sta_spdeconv(ds.data,ds.dirs,ds.mask,'verbose',true,'lambda',0.1)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function  [ds] = createTestCrossing(ang1,ang2,bD,sigma)


alldirs =  load('symS2distributions.mat');

dst = genCrossing(alldirs.dirs64,ang1,ang2,bD,sigma);

ds.mask = dst.spatialProbabilities;
ds.data = double(permute(dst.signal,[2 3 4 1]));
ds.dirs = double(dst.sphereInterpolation.bDir);



    
function datastruct = genCrossing(dirs,angle,ang2,bfac,nz)    
    
            N = 24;

            sinterp.bDir = dirs;
            datastruct.sphereInterpolation = sinterp;
     
            
            K = Kernel(dirs,bfac); K = K/size(K,1);
            
            fac = 10;
                 
            N1 = [cos(ang2) sin(ang2) 0];
            S1s = exp(fac*((N1*dirs).^2 -1))*fac;                      
 %           S1 = genSignal(dirs,N1,bfac);
            S1 = S1s*K;
            
            
            N2 = [cos(angle) sin(angle) 0];
            S2s = exp(fac*(((N2*dirs).^2) -1))*fac;
 %          S2 = genSignal(dirs,N2,bfac); 
            S2 = S2s*K;
            
                    
            
            d = 3^2;
            sig = ones(length(dirs),N,N);
            sig_gt = ones(length(dirs),N,N);
            ang_gt = cell(N,N);
            [X Y] = ndgrid(-(N/2-1):(N/2));
            R2 = X.^2 + Y.^2; 
            v1 =  (R2-(N1(1)*X+N1(2)*Y).^2 < d); v1 = find(v1(:));
            v2 =  (R2-(N2(1)*X+N2(2)*Y).^2 < d); v2 = find(v2(:));
            sig(:,v1) = 0; sig(:,v2) = 0; sig_gt(:,v1) = 0; sig_gt(:,v2) = 0;
            for k = 1:length(v1),
                sig(:,v1(k)) = sig(:,v1(k)) + S1(:);
                sig_gt(:,v1(k)) = sig_gt(:,v1(k)) + S1s(:);
                ang_gt{v1(k)} = [ang_gt{v1(k)} ; N1; -N1 ];
            end;
            for k = 1:length(v2),
                sig(:,v2(k)) = sig(:,v2(k)) + S2(:);
                sig_gt(:,v2(k)) = sig_gt(:,v2(k)) + S2s(:);
                ang_gt{v2(k)} = [ang_gt{v2(k)} ; N2; -N2];
            end;
            mask = zeros(N,N); mask(v1) = 1; mask(v2) = 1;
            
            
            sig = permute(sig,[2 3 1]);
            sig_gt = permute(sig_gt,[2 3 1]);
            for k = 1:3,
                signal(:,:,k,:) = reshape(sig,[N N 1 length(dirs)]);
                signal_gt(:,:,k,:) = reshape(sig_gt,[N N 1 length(dirs)]);
                datastruct.spatialProbabilities(:,:,k) = mask;
            end;      
%            datastruct.spatialProbabilities(:,:,[1 3]) = 0;            
%            signal(:,:,1,:) = 0;  signal(:,:,3,:) = 0;
        
            datastruct.ang_gt = ang_gt;
            datastruct.spatialProbabilities = single(datastruct.spatialProbabilities);
            for k = 1:length(dirs),
                tens(:,:,k) = dirs(:,k)*(dirs(:,k))';
            end;
            
            tens(:,:,end+1) = 0;
            signal(:,:,:,end+1) = 1;
            datastruct = preprocessODF(datastruct,tens,signal,'crossing',[3 3 3],nz);
            datastruct.signal = permute(signal(:,:,:,1:end-1),[4 1 2 3]) ;
            datastruct.signal = abs(datastruct.signal + nz*complex(randn(size(datastruct.signal)),randn(size(datastruct.signal))));
            datastruct.signal_gt = permute(signal_gt(:,:,:,:),[4 1 2 3]) ;
           
           
            
    
function datastruct = preprocessODF(datastruct,bTensor,signal,patient,vox,nz),

   
       b0indicator = squeeze(sum(sum(abs(bTensor),1),2));
       b0idx = find(b0indicator == 0);
       datastruct.b0avg = single(sum(signal(:,:,:,b0idx),4) /length(b0idx));

       didx = setdiff(1:size(bTensor,3),b0idx);
       datastruct.signal = single(zeros(length(didx),size(signal,1),size(signal,2),size(signal,3)));
       for k = 1:length(didx),
            datastruct.signal(k,:,:,:) = single((signal(:,:,:,didx(k)) ./(datastruct.b0avg+0.001))); 
       end;
       datastruct.original_signal = single(signal);
       datastruct.original_bTensor = bTensor;
           
           
       

function K = Kernel(bDir,alpha)
C = abs(bDir'*bDir);
K = exp(-alpha * C.^2);



       

function S = genSignal(bDir,N,alpha)
S = exp(-alpha * (bDir(1,:)*N(1)+bDir(2,:)*N(2) + bDir(2,:)*N(3)).^2 );






