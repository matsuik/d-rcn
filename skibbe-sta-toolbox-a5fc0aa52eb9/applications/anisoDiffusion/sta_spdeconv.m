%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       
%   res = sta_spdeconv(data,dirs,mask,varargin)
%
%   implements spatially regularized spherical deconvolution 
%
%   Basically it optimizes J = |Af-S|^2 + lambda*|Lf|^2
%   where f is a FOD represented with Spherical Harmonics, A represents 
%   the convolution with the fiber response function (FRF) and 
%   L is the operator used as a prior. Similar to sta_propagateAFC 
%   the operator L'*L can be built of several basic building blocks, which
%   are described in 
%
%        Reisert M, Kellner E, Kiselev VG.
%        About the geometry of asymmetric fiber orientation distributions.
%        IEEE Trans Med Imaging. 2012 Jun;31(6):1240-9. doi:
%        10.1109/TMI.2012.2187916. Epub 2012 Feb 15.
%
%        Reisert M, Skibbe H
%        Left-Invariant Diffusion on the Motion Group in terms of the Irreducible Representations of SO(3)
%        Arxiv preprint 2012 arXiv:1202.5414
%
%   The building blocks are
%         T - horizontal translation (contour completetion) corresponding 
%               to $(n \cdot \nabla)$ where LB is the spherical
%               Laplacian 
%         T2 - horizontal diffusion (contour enhancement) corresponding 
%               to $(n \cdot \nabla)^2$
%         W2 - orthogonal diffusion (plane enhancement) corresponding 
%               to $\Laplace - (n \cdot \nabla)^2$
%         AFC - asymmetric contour enhancement following the paper
%                Reisert M, Kellner E, Kiselev VG.
%                IEEE Trans Med Imaging. 2012 Jun;31(6):1240-9. doi: 10.1109/TMI.2012.2187916. Epub 2012 Feb 15.
%                About the geometry of asymmetric fiber orientation distributions.
%                (for symmetric distributions)
%         AFCanti - asymmetric contour enhancement for anti-symmtric distributions
%         Laplace - isotropic spatial Laplacian
%         LB - Spherical Laplacian
%          
%
%   PARAMETERS:
%          data - a 4D array containing DWI-measurement [w h d N]
%               where N is the number of gradient directions
%          dirs - the gradient directions [3 N]
%          mask - a white matter mask 
%          varargin - parameter,value tuples
%            L - SH cutoff (default 8)
%            frf - vector of size ceil((L+1)/2) describing the fiber reponse function
%                  by projection onto the Schmidth semi-normalized legendre
%                  polynomials.
%                  (default: projection corresponing to exp(-kernwid*(N*N_f)^2))
%            kernwid - parameter of default FRF (default 1)
%            alpha - curvature parameter of AFC operator (default 1)
%            lb - strength of spherical Laplace operator in AFC (default 0)
%            lambda - regularization strength (default 0.1)
%            maxit - maximum number of iterations of CG (default 50)
%            numround - number of CG rounds for CSD (default 4)
%            csd_threshold - threshold to determine negative values (default 0)
%            verbose - verbose output (default false)
%            operation - default 'T2'
%            
%    EXAMPLES: 
%       % create 45 degree crossing and compute SD with T2 prior 
%          ds = createTestCrossing(pi/8,pi/4+pi/8,1,0.01)
%          res = sta_spdeconv(ds.data,ds.dirs,ds.mask,'verbose',true,'lambda',0.01)
%
%       % create bending and compute SD with AFC prior 
%          ds = createTestBending(5,1,0.01)
%          res = sta_spdeconv(ds.data,ds.dirs,ds.mask,'verbose',true,'lambda',0.01,'operation','AFC')
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function res = deconvAFC(data,dirs,mask,varargin)


L = 8;
kernwid = 1;
lambda = 0.1;
storage = 'STA_FIELD_STORAGE_R';
type = 'STA_OFIELD_FULL';
alpha = 1;
beta = 2/alpha;
lb = 0.0;
maxit = 50;
lambda_gm = 2;
csd_threshold = 0.0;
lambda_csd =  1/2; 
numround = 4;
verbose = false;
frf = [];
operation = 'T2';


for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;


realNum = @(x) iff(isa(data,'single'),single(x),double(x));


%%%%%%%%%%%%% build FRF
if isempty(frf),
    t = -1:0.01:1;
    frf = zeros(ceil((L+1)/2),1);
    for l = 0:2:L,
        leg = legendre(l,t,'sch');
        frf(l/2+1) = exp(-t.^2*kernwid)*leg(1,:)'; 
    end;
    frf = frf/frf(1);    
end;

frffull = zeros(L+1,1);
for l = 0:2:L,
    frffull(l+1) = frf(l/2+1);
end;
frfsquare = realNum(frf.^2);
frf = realNum(frf);


%%%%%%%%%%%% build prior

T2_  =  [0.5  0  0  1  0  0  1  1];
FT2F_ = [0.5  0  0  1  0  0 -1 -1];
W2_ =   [1    0  0 -1  0  0 -1 -1];
FW2F_ = [1    0  0 -1  0  0  1  1];
P2_  =  [0.5  0  0  1  0  0  0  0];
LB_ =   [0    0  1  0  0  0  0  0];
T_  =   [0    0  0  0  1  1  0  0];
damp_ = [0    1  0  0  0  0  0  0];
Lap_ =  [1    0  0  0  0  0  0  0];

AFC = [T2_ - beta*T_ ; damp_*beta^2 + beta*T_] + lb*[LB_;LB_];
AFCanti = [damp_*beta^2 + beta*T_; T2_ - beta*T_] + lb*[LB_;LB_];
T2 = [T2_;T2_];
T = [T_;T_];
W2 = [W2_;W2_];
Laplace = [Lap_ ; Lap_];
LB =  [LB_ ; LB_];
op = realNum(2/3*lambda*eval(operation));


%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%% forward measurement / prepare mask

ofield = sta_hardi2stafield(data,dirs,'L',L,'type',type,'storage',storage,'pseudoinverse',false);
X = sta_S2conv(ofield,frffull);
X.data(:,:,not(mask(:))) = 0;
gmsupress = realNum(lambda_gm*double(1-mask));



%%%%%%%%%%%%%%%%%%%%%%%%%% prepare matrix for nonneg-suppression

alldirs =  load('symS2distributions.mat');

bDir = alldirs.dirs128;
bDir = realNum([bDir -bDir]);

M=realNum(getSHMatrix(ofield.L,bDir,ofield.type,ofield.storage,1));
U=realNum(getSHMatrix(ofield.L,bDir,ofield.type,ofield.storage,0));

 % switch to complex-interleaved rep used in C++
M_inter(1,:,:) = real(M); M_inter(2,:,:) = imag(M);
U_inter(1,:,:) = real(U); U_inter(2,:,:) = imag(U);

 % find inital value
Mlowres=realNum(getSHMatrix(4,bDir,type,storage,1));
ofield_lowres = sta_hardi2stafield(data,dirs,'L',4,'type',type,'storage',storage,'pseudoinverse',true);
init = sta_S2conv(ofield_lowres,frffull./(eps+frffull).^2);


 

nset = realNum(0)*computeNset(init,Mlowres); 
res = X; 
res.data = res.data*0;
L = realNum(L);
lambda_csd = realNum(lambda_csd);
for k = 1:numround,
    if verbose,
        fprintf('round: %i  #aset: %f\n',k,sum(nset(:)));
    end;
    res = conjgrad(@(z)(deconvAFCoperator(z,L,op(1,:),op(2,:),frfsquare,gmsupress,M_inter,U_inter,lambda_csd*nset,type,storage)),X,res,maxit);
    nset = computeNset(res,M); 
end;





function nset = computeNset(ofield,M)
    
    nset = (real(reshape(M.'*squeeze(ofield.data(1,:,:)+i*ofield.data(2,:,:)),[size(M,2) ofield.shape])) < csd_threshold);
    nset(:,not(mask(:))) = 0;
        
end;








function X = conjgrad(Aop,b,X,its)
x = X.data;

r=b.data-Aop(x);
p=r;
rsold=r(:)'*r(:);
 
for i=1:its
   
        Ap=Aop(p);
   
        ralpha=(r(:)'*r(:))/(p(:)'*Ap(:));
        x=x+ralpha*p;
        r=r-ralpha*Ap;
        rsnew=r(:)'*r(:);
        
        if sqrt(rsnew)<1e-40
              break;
        end
        p=r+rsnew/rsold*p;
        rsold=rsnew;
        if mod(i,20) == 0 & verbose,
            sfigure(3);
            X.data = x;           
            sta_glyph(X,'init_view',3,'init_slice',2,'scval',nan,'mask',mask); 
%             sfigure(4);
%             sta_quiver(X,'init_view',3,'init_slice',2,'nummax',4); 
            drawnow;
        end;
        if verbose,
            fprintf('.');
        end;
end

X.data = x;
if verbose, 
    fprintf('\n');
end;
end;


end