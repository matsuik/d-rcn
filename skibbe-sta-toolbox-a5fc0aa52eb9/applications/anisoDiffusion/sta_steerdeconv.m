%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Steerable Deconvolution 
%
% implements a 3D generalization of the method proposed in 
%    Reisert, M. and Skibbe, H.
%    Steerable deconvolution feature detection as an inverse problem
%    DAGM 2011, 326-335, Springer LNCS
%
%   Usage: result = sta_steerdeconv(image,varargin)
% 
%      image - a 3D array or a stafield struct of L=0 order
%      varargin - parameter,value tuples
%           L - expansion SH-cutoff (default L=6)
%           sigma - size of deconvolution kernel (default sigma=1.5)
%           alpha - elongation parameter of kernel (default alpha = 1)
%                real value - stick-like kernel
%                imaginary value - plate-like kernel
%           aniso - anisotropic diffusion parameter (default aniso = 1)
%                   -0.5 <= aniso <= 1
%                positive value - diffusion 'along' the direction
%                negative value - diffusion 'orthogonal' to the direction
%           lambda - regualirization strength (default lambda = 0.2)
%           maxit - number of CG-iterations (default maxit = 100)
%           tolerance - precision stop-criteria of CG
%
%   Example: 
% 
%     %% line enhancement example
%     image=zeros(32,32,32);
%     image(16,16,[1:5 8:32])=1;
%     image(16,:,16) = 1;
%     kernel =  sta_fft(sta_fspecial('gauss',{'kname','gauss','kparams',1,'shape',size(image),'precision','single'}));
%     data = sta_fft(stafield(single(image)).struct);
%     image = sta_ifft(sta_prod(data,kernel,0));
%     image.data(1,1,:,:,:) = image.data(1,1,:,:,:) + 0.2*randn([1 1 image.shape])*max(image.data(:));
%     res = sta_steerdeconv(image);
%       
%     %% sphere example
%     [X Y Z] = ndgrid(-16:15); R = sqrt(X.^2 + Y.^2 + Z.^2);    
%     image = (2-2./(1+exp(-2*abs(R-10)))).*(rand(32,32,32)>0.8);
%     nimage=image+0.1*rand(size(image)).*max(image(:));
%     res = sta_steerdeconv(nimage,'alpha',i,'aniso',-0.5,'lambda',0.1);
% 
%


function X =sta_steerdeconv(image,varargin)


if not(isstruct(image)),
    image = stafieldStruct(image);
end;

L = 6;
maxit = 100;
sigma = 1.5; 
alpha = 1;
aniso = 1;
lambda = 0.2;
tolerance = 0.01;
verbose = true;
element_size=[1,1,1];

for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;

%element_size = image.element_size;
alpha = alpha /sigma;
type = 'STA_OFIELD_EVEN';
storage = image.storage;
realNum = @(x) iff(isa(image.data,'single'),single(x),double(x));
precision = iff(isa(image.data,'single'),'single','double');

if strcmp(type,'STA_OFIELD_EVEN')
    L = L + mod(L,2);
end;

[X , kernel] = applyForwardOp(L,image,type,storage,precision,realNum,sigma,alpha,element_size);


%figure(3);
%sta_glyph(X); drawnow;

%kernel = sta_fft(stafield('gauss',image.shape,sigma,0,0,storage,precision,element_size).struct);


opparams = realNum(lambda*[0.5 aniso*[1 1 1]]);
params = realNum([L alpha opparams]);

tic;
X.data = conjgrad(@(z)(SDoperator(z,kernel.data,params,type,storage,{'element_size',element_size})),X,X,maxit,tolerance,verbose);
toc;


    

function [X, kernelft] = applyForwardOp(L,image,type,storage,precision,realNum,sigma,alpha,element_size)            
kernelft =sta_fft(stafieldStruct('gauss',image.shape,sigma,0,0,storage,precision,element_size));
image = sta_ifft(sta_prod(sta_fft(image,{'alpha',1/prod(image.shape)}),kernelft,0));
if strcmp(type,'STA_OFIELD_EVEN')
    l = (0:2:L)';
    P = alpha^2./(l.*(l-1));
    step = 2;
else
    l = (0:L)';
    P = alpha./l;
    step = 1;
end;
P(1)=1;
X = SteerableFilter(image,realNum(P),realNum(step));

    
    
        


function [x] = conjgrad(Aop,b,X,its,tolerance,verbose)
x = X.data;
r=b.data-Aop(x);
p=r;
rsold=r(:)'*r(:); 
r0 = r(:)'*r(:); 
for i=1:its
        if verbose,
            fprintf('-');
        end;
        Ap=Aop(p);
        if verbose,
            fprintf('-');
        end;
        alpha=(r(:)'*r(:))/(p(:)'*Ap(:));
        x=x+alpha*p;
        r=r-alpha*Ap;
        rsnew=r(:)'*r(:);        
        
        p=r+rsnew/rsold*p;
        rsold=rsnew;
        if verbose,
            %if mod(i,1) == 0,
             %   sfigure(3);
             %   X.data = x;
             %    sta_glyph(X); 
             %   sta_quiver(X); 
             %   drawnow;
            %end;
            fprintf('precision %f\n',sqrt(rsnew/r0));
        end;
        if sqrt(rsnew/r0)<tolerance,
              break;
        end
end
if verbose,
    fprintf('\n');
end;




