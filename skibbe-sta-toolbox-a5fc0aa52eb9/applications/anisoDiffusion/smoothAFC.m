function [X report] = smoothFC(ofield,varargin)

lambda = 0.1;
maxit = 10;
verbose = 5;
operation = 'AFC';
beta = 1;
lb = 0.01;
showfun = @sta_glyph;
reportfun = [];
report = [];

for k = 1:2:length(varargin),
    if not(exist(varargin{k})),
        display(['invalid parameter: ' varargin{k}]);
    else,
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
end;



storage = ofield.storage;
L = ofield.L;

ofield = sta_tofull(ofield);
type = ofield.type;

% op = -lam1*\Delta + lam2 - lam3*(l+1)*l - lam4*Z_ll - lam5*Z_l(l+1) - lam6*Z_l(l-1) - lam7*Z_l(l+2) - lam8*Z_l(l-2)

T2  =  [0.5  0  0  1  0  0  1  1];
FT2F = [0.5  0  0  1  0  0 -1 -1];
W2 =   [1    0  0 -1  0  0 -1 -1];
FW2F = [1    0  0 -1  0  0  1  1];
P2  =  [0.5  0  0  1  0  0  0  0];
LB =   [0    0  1  0  0  0  0  0];
T  =   [0    0  0  0  1  1  0  0];
damp = [0    1  0  0  0  0  0  0];
Lap =  [1    0  0  0  0  0  0  0];

AFC = [T2 - beta*T ; damp*beta^2 + beta*T] + lb*[LB;LB];
AFCanti = [damp*beta^2 + beta*T; T2 - beta*T] + lb*[LB;LB];
CC = [T+lb*LB;T+lb*LB];
FC = [T2;T2];
WC = [W2;W2];
Laplace = [Lap ; Lap];

op = 2/3*lambda*eval(operation);




tic;
X = graddesc(@(z)(AFCoperator(z,L,op(1,:),op(2,:),type,storage)),ofield,maxit);
toc;



function [x] = graddesc(Aop,b,its)
 x = b;
 for i=1:its,
     x.data = x.data - Aop(x.data);
     if  mod(i,verbose) == 0,
            if not(isempty(showfun)),
                figure(3);
                showfun(x);
                drawnow;
            end;
            fprintf('\n%i)',i);          
            if not(isempty(reportfun)),
                thisreport = reportfun(x);
                report = [report thisreport];
            end;    
     end;
     fprintf('.');
 end;
end;
end
