#include "mex.h"


#include <stensor.h>
#include "FCoperators.h"


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
      
   
    const mxArray *ST1;
    ST1 = prhs[0];       
    const mwSize *dimsST1 = mxGetDimensions(ST1);
    const mwSize numdimST1 = mxGetNumberOfDimensions(ST1);
    if (numdimST1!=5)  mexErrMsgTxt("error: Array dimension for the first argument must be 5 (2x2L+1xXxYxZ)\n"); 
    std::complex<T> *pin = (std::complex<T> *) (mxGetData(ST1));
   
            
    const mxArray *Params = prhs[1];        
    T* params =  ((T*)(mxGetData(Params)));
//     T* params_imag =  ((T*)(mxGetImagData(Params)));
    int L = (int) params[0];              
    std::complex<T> lambda1 = params[1];
    std::complex<T> lambda2 = params[2];
    std::complex<T> lambda3 = params[3];
    std::complex<T> lambda4 = params[4];
  
    
    
    const mxArray *Type = prhs[2];            
    const mxArray *Storage = prhs[3];     
    char buf[256];
    mxGetString(Type,buf,256);
    STA_FIELD_TYPE type = enumfromstring_type(buf);
    mxGetString(Storage,buf,256);
    STA_FIELD_STORAGE storage = enumfromstring_storage(buf);
//     int step;
//     if (type == STA_OFIELD_FULL)
//         step = 1;
//     else if (type == STA_OFIELD_EVEN)
//         step = 2;
//     else
//     {
//         mexPrintf("not supported!!");
//         return;
//     }
    if ((type != STA_OFIELD_FULL)&&(type != STA_OFIELD_EVEN))
    {
        mexPrintf("not supported!!");
        return;
    }
    
  
    plhs[0] = mxCreateNumericArray(numdimST1,dimsST1,mxGetClassID(ST1),mxREAL);
    std::complex<T> *result = (std::complex<T> *) mxGetData(plhs[0]);

    std::size_t shape[3];    
    for(int i =2;i <numdimST1;i++)
    	shape[4-i]=dimsST1[i];
    
 
//     int sz = dimsST1[2]*dimsST1[3]*dimsST1[4]; 

    
    
  // compute FC    
    bool conj = false;
    T  *v_size = NULL;    
   FCoperator(pin,result,shape,v_size,L,conj,lambda1,lambda2,lambda3,lambda4,storage,type);  
    
 

}


void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    
 int _single=0;
  int _double=0;

  int numnum = 0;
  for (int i=0;i<nrhs;i++)
  {
    if (mxIsNumeric(prhs[i])) numnum++;
    if (mxIsSingle(prhs[i])) _single++;
    if (mxIsDouble(prhs[i])) _double++;
  }
  if (_single==numnum) _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
  else
  if (_double==numnum) _mexFunction<double>( nlhs, plhs,  nrhs, prhs );  
  else 
  mexErrMsgTxt("error: all parameters must be either double or single precision!\n");       

}









