#include "mex.h"

#define _SUPPORT_MATLAB_
#include "stafield.h"


#include <stdio.h>
#include <string.h>
#include <list>


template <typename T, typename S>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<2)
        mexErrMsgTxt("error: not enough parameters\n");
    hanalysis::mex_initMatlabFFTW();

    try {
        std::size_t numFields=mxGetNumberOfElements(prhs[0]);
        std::vector<hanalysis::stafield<T>* > features(numFields);
        for (std::size_t a=0; a<numFields; a++)
        {
            features[a]=new hanalysis::stafield<T>((mxArray *)mxGetCell(prhs[0],a));
            if (features[a]->getStorage()!=hanalysis::STA_FIELD_STORAGE_R)
                mexErrMsgTxt("error: storage type must be STA_FIELD_STORAGE_R\n");
        }



        int numprodIndices=7;
        if ((!(mxGetClassID(prhs[1])==mxDOUBLE_CLASS))||
                (mxGetNumberOfDimensions(prhs[1])!=2)||
                (mxGetDimensions(prhs[1])[0]!=numprodIndices))
            mexErrMsgTxt("error: product matrix has wrong dimension\n");

        std::size_t num_of_products=mxGetDimensions(prhs[1])[1];



        double * products=(double *)mxGetData(prhs[1]);

        printf("num of products: %d\n",num_of_products);

        int maxBW=-1;
        for (std::size_t a=0; a<num_of_products; a++)
        {
            maxBW=std::max(maxBW,(int)products[a*numprodIndices+numprodIndices-2]);
        }
        printf("max BW: %d\n",maxBW);



#ifdef _STA_CUDA
        T * bufferA=NULL;
        if (maxBW>-1)
        {
            std::size_t size_buffer_A=hanalysis::order2numComponents(features[0]->getStorage(),features[0]->getType(),maxBW)*features[0]->getNumVoxel();
            hanalysis_cuda::gpu_malloc(bufferA,size_buffer_A*sizeof(std::complex<T>));
            cudaMemset(bufferA,0,size_buffer_A*sizeof(std::complex<T>));
        }
#else
        std::complex<T> * bufferA=NULL;
        if (maxBW>-1)
        {
            std::size_t size_buffer_A=hanalysis::order2numComponents(features[0]->getStorage(),features[0]->getType(),maxBW)*features[0]->getNumVoxel();
            bufferA= new std::complex<T>[size_buffer_A];
            memset(bufferA,0,size_buffer_A*sizeof(std::complex<T>));
        }
#endif





        mwSize ndims[4];
        ndims[0]=num_of_products;
        ndims[1]=features[0]->getShape()[2];
        ndims[2]=features[0]->getShape()[1];
        ndims[3]=features[0]->getShape()[0];

        plhs[0] = mxCreateNumericArray(4,ndims, hanalysis::mex_getClassId<S>(),mxREAL);
        S * Fimage = (S *) mxGetData(plhs[0]);


        for (std::size_t a=0; a<num_of_products; a++)
        {
            int order=0;
            if (products[0]!=-1) order++;
            if (products[2]!=-1) order++;
            if (products[4]!=-1) order++;

            if (order<2)
                mexErrMsgTxt("error: supporting only features of order 2 or 3\n");


            switch (order)
            {
            case 2:
            {
                int field_index1=products[0];
                int l1=products[1];
                int field_index2=products[2];
                int l2=products[3];
                double alpha=products[6];
                if ((field_index1<0)||(field_index1>=(int)numFields))
                    mexErrMsgTxt("error: tensor field does not exist\n");
                if ((field_index2<0)||(field_index2>=(int)numFields))
                    mexErrMsgTxt("error: tensor field does not exist\n");

                printf("(%5.d) [%d %d] (%d %d) -> %d\n",a+1,field_index1,field_index2,(*features[field_index1])[l1].getRank(),(*features[field_index2])[l2].getRank(),0);
                hanalysis::sta_feature_product_R<T,S>((*features[field_index1])[l1].getDataConst(),
                                                      (*features[field_index2])[l2].getDataConst(),
                                                      Fimage+a,
                                                      (*features[field_index2])[l2].getShape(),
                                                      (*features[field_index2])[l2].getRank(),
                                                      alpha,
                                                      false,
                                                      (*features[field_index1])[l1].getStride(),
                                                      (*features[field_index2])[l2].getStride(),
                                                      num_of_products,
                                                      true);
            }
            break;
            case 3:
            {
                int field_index1=products[0];
                int l1=products[1];
                int field_index2=products[2];
                int l2=products[3];
                int field_index3=products[4];
                int l3=products[5];
                double alpha=products[6];

                if ((field_index1<0)||(field_index1>=(int)numFields))
                    mexErrMsgTxt("error: tensor field does not exist\n");
                if ((field_index2<0)||(field_index2>=(int)numFields))
                    mexErrMsgTxt("error: tensor field does not exist\n");
                if ((field_index3<0)||(field_index3>=(int)numFields))
                    mexErrMsgTxt("error: tensor field does not exist\n");

                printf("(%5.d) [%d %d %d] (((%d %d) -> %d) %d) -> %d\n",a+1,field_index1,field_index2,field_index3,(*features[field_index1])[l1].getRank(),(*features[field_index2])[l2].getRank(),l3,(*features[field_index3])[l3].getRank(),0);
                hanalysis::stafield<T> prodbuffer=hanalysis::stafield<T>(features[0]->getShape(),
                                                  l3,
                                                  features[0]->getStorage(),
                                                  hanalysis::STA_OFIELD_SINGLE,
                                                  bufferA);

                hanalysis::stafield<T>::Prod((*features[field_index1])[l1],(*features[field_index2])[l2],prodbuffer,l3,false,T(1),true);

                hanalysis::sta_feature_product_R<T,S>((*features[field_index3])[l3].getDataConst(),
                                                      prodbuffer.getDataConst(),
                                                      Fimage+a,
                                                      prodbuffer.getShape(),
                                                      (*features[field_index3])[l3].getRank(),
                                                      alpha,
                                                      false,
                                                      (*features[field_index3])[l3].getStride(),
                                                      prodbuffer.getStride(),
                                                      num_of_products,
                                                      true);

            }
            break;

            default:
                mexErrMsgTxt("error: not implemented yet or doesn't exist\n");
            }


            products+=numprodIndices;
            hanalysis::mex_dumpStringNOW();
        }

#ifdef _STA_CUDA
        if (bufferA!=NULL)
            hanalysis_cuda::gpu_mfree(bufferA);
#else
        if (bufferA!=NULL)
            delete [] bufferA;
#endif

        for (std::size_t a=0; a<numFields; a++)
        {
            delete features[a];
        }



    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }

}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

    std::size_t is_double=0;
    std::size_t is_single=0;
    if (!mxIsCell(prhs[0]))
        mexErrMsgTxt("error: first parameter must be cell array of stafields\n");

    std::size_t numFields=mxGetNumberOfElements(prhs[0]);
    for (std::size_t a=0; a<numFields; a++)
    {
        if (hanalysis::mex_isStaFieldStruct<float>(mxGetCell(prhs[0],a))) is_single++;
        if (hanalysis::mex_isStaFieldStruct<double>(mxGetCell(prhs[0],a))) is_double++;
    }

    std::string fprecision="single";
    if (is_single==numFields) fprecision="single";
    if (is_double==numFields) fprecision="double";

    if (nrhs>2)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (hanalysis::mex_hasParam(params,"fprecision")!=-1)
            fprecision=hanalysis::mex_getParamStr(params,"fprecision");
    }
    printf("%s\n",fprecision.c_str());
    if (is_single==numFields)
        if (fprecision=="single")
            _mexFunction<float,float>( nlhs, plhs,  nrhs, prhs );
        else
            _mexFunction<float,double>( nlhs, plhs,  nrhs, prhs );
    else if (is_double==numFields)
        if (fprecision=="single")
            _mexFunction<double,float>( nlhs, plhs,  nrhs, prhs );
        else
            _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
    else
        mexErrMsgTxt("error: unsupported datatype");
}

