% STA_SHOG computes covariant spherical hog (SHOG) tensor features
%
%  ofield=STA_SHOG(ifield,params);
%   
%  ifield is a stafieldStruct of order 0 or 1 
%  params are optional string-values pairs:
%
%  example:
%  params={'kname','gauss','kparams',5,... 
%          'BW',10,...                      
%          'presmooth',1.5,...              
%          'conjugate',true,...             
%          'accuracy',0,...             
%          'gamma',0.5,...             
%           }
%
%  uses a Gaussian window with standard deviation 5 for an expansion up to
%  order 10. The ordinary central differences are used (1 for higher accuracy).
%  If the input field is a scalar: it is initially smoothed with a
%     Gaussian with standard deviation 1.5 and the conjugate derivative field
%     is computed. Furthermore, the square root is taken from the gradient
%     magnitude
%
%  kname can be        'gauss' or      'sh' (the Gaussian smoothed sphere)
%  with kparams         [sigma] or   [sigma,radius]  
%
%
%  default:
%  params={'BW',5,...                      
%          'conjugate',false,...             
%          'accuracy',0,...             
%          'gamma',1,...             
%           }
%
%
%  See also sta_wavelet sta_invrts
%
