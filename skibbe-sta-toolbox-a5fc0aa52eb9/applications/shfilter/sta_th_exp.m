function [fields,valid_index]=sta_th_exp(field,L,varargin)

BW=5;
params = {'kname','gauss','kparams',3,'BW',BW};
expfunc = @sta_wavelet;

for k = 1:2:numel(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


for a=1:size(field.data,2),
    data=reshape(field.data(:,a,:,:,:),[2,1,field.shape]);
    components=stafieldStruct(data,'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');
    decomposed_field{a}=expfunc(components,params);
end;


if (~iscell(L))
    J=field.L;
    if numel(L)==1
        newL={};
        for l=0:L,
            for j=-J:J,
                if ((abs(j)<=J)&&(J<=abs(j+2*l))) && (l+j>=0)  && (mod(j+J,2)==0)
                    newL=[newL,[l,j]];
                end;
            end;
        end;
        L=newL;
    else
        L={L};
    end;
end;
                    
for a=1:numel(L),
    fprintf('%d ----\n',a);
    if (length(L{a})==1)
        fields{a}=sta_th(decomposed_field,{'BW',decomposed_field{end}.L,'j',L{a}(1)});
        valid_index{a}=ones(1,decomposed_field{end}.L+1+field.L);
        valid_index{a}(1:field.L)=0;
    else
        fields{a}=sta_th(decomposed_field,{'L',L{a}(1),'j',L{a}(2)});        
        fields{a}.subindx=L{a};
    end;
end;

