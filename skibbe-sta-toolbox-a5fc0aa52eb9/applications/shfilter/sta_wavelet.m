% STA_WAVELET computes up-(and down)derivative based features like
% Gauss-Bessel (Gabor) features or Gauss-Laguerre (local-jet) features
%
%  ofield=STA_WAVELET(ifield,params);
%   
%  ifield is a stafieldStruct of order L
%  params are optional string-values pairs:
%
%  example:
%  params={'kname','gauss','kparams',5,... 
%          'BW',10,...                      
%          'lap',1,...              
%          'conjugate',true,...             
%          'accuracy',0,...             
%          'accurateLap',1,...             
%           }
%
%  uses a Gaussian window with standard deviation 5 for an expansion up to
%  order 10. Before applying the tensor derivative operator (conjugate one) 
%  the Laplace operator is applied one time (lap 1). The ordinary central 
%  differences (1 for higher accuracy) and the more accurate Laplace 
%  operator is used (0 for ordinary). 
%
%  kname can either be 'gauss','gaussLaguerre' or 'gaussBessel'
%  with kparams         [sigma],    [sigma,n]   or  [sigma,k,s]
%
%  default:
%  params={'BW',5,...                      
%          'lap',0,...              
%          'conjugate',false,...             
%          'accuracy',0,...             
%          'accurateLap',0,...             
%           }
%
%
%
%  See also sta_shog sta_invrts
%