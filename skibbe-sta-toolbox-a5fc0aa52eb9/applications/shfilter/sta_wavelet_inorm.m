% STA_WAVELET_INORM computes locally intensity normalized features. For
% this the intensity variation is computed in a voxel neighborhood within a
% Gaussian window.
%
% Ref: M. Reisert and H. Burkhardt, “Harmonic filters for generic feature
% detection in 3D,” in Proc. of the DAGM.  Jena, Germany: LNCS,  Springer,
% 2009, pp. 131–140. 
%
%
% In addition to the parameters of sta_wavelet  it suppors the parameter
% sigman which defines the size of the Gaussian neighborhood. Default is
% sigman=5.  
%
%  See also sta_wavelet sta_shog sta_invrts
%
function ofield=sta_wavelet_inorm(ifield,options)
featurefun=@sta_wavelet;

assert(strcmp(ifield.type,'STA_OFIELD_SINGLE'));
assert(strcmp(ifield.storage,'STA_FIELD_STORAGE_R'));

epsilon=0.01;
sigman=5;

% we use options here in order to be consistent with our mex functions
if exist('options')
    for k = 1:2:numel(options),
            eval(sprintf('%s=options{k+1};',options{k}));
    end;
else
    options={};
end;

img=squeeze(ifield.data(1,1,:,:,:));
fprintf('standard deviation in radius %f\n',sigman);

[mean,gaussin_ft]=sta_smooth_img(img,sigman./ifield.element_size);
mean2=sta_smooth_img(img.^2,sigman./ifield.element_size,'gaussin_ft',gaussin_ft);
std_dev=real(1./(sqrt(abs(mean2-mean.^2))+(epsilon+eps)));

ofield=featurefun(ifield,stafieldStruct(std_dev,'STA_FIELD_STORAGE_R',ifield.element_size),options);


