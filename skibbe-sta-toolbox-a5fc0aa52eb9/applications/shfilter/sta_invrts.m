% STA_INVRTS computes locally rotation invariant features based on one or 
% several tensor fields.   
%
%
% examples:
% fimg=STA_INVRTS(ifield,params{:})  % one tensor field if stafieldStruct
% fimg=STA_INVRTS(ifields,params{:}) % several fields ifields is cell array
%                                      of stafieldStruct
%
% size(fimg)=[numfeatures x dimX x dimY x dimZ]
%
%
% parameter examples:
% params={'power2',true,...         % power spectrum on/off
%         'cross2',false,...        % cross spectrum on/off
%         'power3',false,...        % bi spectrum on/off
%         'cross3',false,...        % bi cross-spectrum on/off
%         'o3even',false,...         % even products on/off
%         'o3odd',false,...          % odd products on/off
%         'BWmin',0,...             % min input rank considered for products
%         'BWmax',0,...             % max rank for intermediated o3 product 
%         'fprecision','double',... % fimg is single or double precision  
%               }
%
%  note that for wavelet based features BWmin=1 makes features invairant to 
%  an intensity offset. Furthermore, odd3=true allows for resolving 
%  reflections. 
%
% default:
% params={'power2',true,...         
%         'cross2',false,...        
%         'power3',false,...        
%         'cross3',false,...        
%         'o3even',true,...         
%         'o3odd',false,...          
%         'BWmin',0,...             
%         'BWmax',5,...    
%         'fprecision','single',...  
%               }
%
% See also sta_wavelet sta_shog sta_wavelet_inorm
%


function fimg=sta_invrts(tfields,varargin)


 

fprecision='single';


if (~iscell(tfields))
    tfields={tfields};
end;

if ~exist('products')
    products=[];
    cross2=0;
    power2=1;
    cross3=0;
    power3=0;
    
    o3odd=0;    
    o3even=1;        
    BWmax=5;
    BWmin=0;
    
    for k = 1:2:numel(varargin),
        %if (exist(varargin{k})),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
        %end;
    end;
    
    
    o2=power2|cross2;
    o3=power3|cross3;    
    
    for a=1:numel(tfields),
        a_startl=0;
        a_endl=tfields{a}.L;
        a_add=1;
        if strcmp(tfields{a}.type,'STA_OFIELD_SINGLE'),
            a_startl=a_endl;
        end;
        if strcmp(tfields{a}.type,'STA_OFIELD_ODD'),
            a_startl=1;
            a_add=2;
        end;
        if strcmp(tfields{a}.type,'STA_OFIELD_EVEN'),
            a_add=2;
        end;
        
        for b=a:numel(tfields),
            b_startl=0;
            b_endl=tfields{b}.L;
            b_add=1;
            if strcmp(tfields{b}.type,'STA_OFIELD_SINGLE'),
                b_startl=b_endl;
            end;
            if strcmp(tfields{b}.type,'STA_OFIELD_ODD'),
                b_startl=1;
                b_add=2;
            end;
            if strcmp(tfields{b}.type,'STA_OFIELD_EVEN'),
                b_add=2;
            end; 
            
            for la=a_startl:a_add:a_endl,
                if (b==a)
                    b_startl=la;
                end;                
                for lb=b_startl:b_add:b_endl,
                    
 
            
                    if 1
                        if o2 && ((cross2 && a~=b) || (a==b && power2)) ,
                            if (la==lb) && (la>=BWmin) && (lb>=BWmin),
                                    products=[products;a-1,la,b-1,lb,-1,-1,1];
                            end;
                        end;
                        if o3 &&  ((cross3 && a~=b) || (a==b && power3)) ,
                            for c=b:numel(tfields),
                                c_startl=0;
                                c_endl=tfields{c}.L;
                                c_add=1;
                                if strcmp(tfields{c}.type,'STA_OFIELD_SINGLE'),
                                    c_startl=c_endl;
                                end;
                                if strcmp(tfields{c}.type,'STA_OFIELD_ODD'),
                                    c_startl=1;
                                    c_add=2;
                                end;
                                if strcmp(tfields{c}.type,'STA_OFIELD_EVEN'),
                                    c_add=2;
                                end; 
                                
                                if (b==c)
                                    c_startl=lb;
                                else
                                    if (a==c)
                                        c_startl=la;
                                    end;
                                end;                
                                for lc=c_startl:c_add:c_endl,
                                    if (((cross3) || (c==b && power3)))
                                          if lc>=abs(la-lb) && lc<=abs(la+lb) && lc <= BWmax && la >= BWmin && lb >= BWmin && lc >= BWmin, 
                                                if (mod(la+lb+lc,2)==0)
                                                        if o3even, 
                                                                products=[products;a-1,la,b-1,lb,c-1,lc,1];
                                                        end;
                                                else
                                                    if o3odd,
                                                        test=zeros(numel(tfields),1+max([a_endl,b_endl,c_endl]));
                                                        test(a,la+1)=test(a,la+1)+1;
                                                        test(b,lb+1)=test(b,lb+1)+1;
                                                        test(c,lc+1)=test(c,lc+1)+1;
                                                        if max(test(:))==1
                                                                products=[products;a-1,la,b-1,lb,c-1,lc,1];
                                                        end;
                                                    end;
                                                end;
                                           end;                            
                                    end;
                                end;
                            end;
                        end;
                    end;
               end;
            end;            
            
            
        end;
    end;
end;


for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

if exist('getproducts')
    fimg=products;
    return;
end;


assert(numel(products)>0)

fimg=sta_invariants(tfields,products',{'fprecision',fprecision});
        