#include "mex.h"


#define _SUPPORT_MATLAB_
#include "stafield.h"

#include <stdio.h>
#include <string.h>
#include <list>


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    hanalysis::mex_initMatlabFFTW();
    if (nrhs<1)
        mexErrMsgTxt("error: not enough parameters\n");

    int BW=5;
    T gamma=T(1);
    std::string kname="";
    std::vector<T> kparams;
    bool complex_con=false;
    int accuracy=0;


    T presmooth=0;


    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (hanalysis::mex_hasParam(params,"gamma")!=-1)
            gamma=hanalysis::mex_getParam<T>(params,"gamma",1)[0];

        if (hanalysis::mex_hasParam(params,"accuracy")!=-1)
            accuracy=hanalysis::mex_getParam<int>(params,"accuracy",1)[0];

        if (hanalysis::mex_hasParam(params,"presmooth")!=-1)
            presmooth=hanalysis::mex_getParam<T>(params,"presmooth",1)[0];

        if (hanalysis::mex_hasParam(params,"BW")!=-1)
            BW=hanalysis::mex_getParam<int>(params,"BW",1)[0];

        if (hanalysis::mex_hasParam(params,"kname")!=-1)
            kname=hanalysis::mex_getParamStr(params,"kname");

        if (hanalysis::mex_hasParam(params,"kparams")!=-1)
            kparams=hanalysis::mex_getParam<T>(params,"kparams");

        if (hanalysis::mex_hasParam(params,"conjugate")!=-1)
            complex_con=hanalysis::mex_getParam<bool>(params,"conjugate",1)[0];
    }

    bool do_smoothing=(kname!="");

    printf("BW    : %d\n",BW);

    sta_assert(BW>0);

    printf("gamma : %f\n",gamma);
    if (presmooth!=0)
        printf("presmooth : %f\n",presmooth);

    if (accuracy==1)
        printf("using more accurate derivatives\n");

    if (do_smoothing)
    {
        printf("kname : %s\n",kname.c_str());
        printf("kparam: [");
        for (std::size_t t=0; t<kparams.size(); t++)
            printf("%f ",kparams[t]);
        printf("]\n");
    }

    try {
	printf("[.");hanalysis::mex_dumpStringNOW();
        hanalysis::stafield<T> gradient;
        hanalysis::stafield<T> ifield=hanalysis::stafield<T>((mxArray *)prhs[0]);

        hanalysis::stafield<T> convoluion_kernel;

        if (!((ifield.getRank()==1)&&(ifield.getType()==hanalysis::STA_OFIELD_SINGLE)))
        {
            if (presmooth!=0)
            {
                std::vector<T> p_kparams;
                p_kparams.push_back(presmooth);
                convoluion_kernel=hanalysis::stafield<T>("gauss",
                                  ifield.getShape(),
                                  p_kparams,
                                  false,
                                  0,
                                  ifield.getStorage()).fft(true,false,T(1)/T(ifield.getNumVoxel()));

                gradient=ifield[0].fft(true).prod(convoluion_kernel,0,true).fft(false).deriv(1,complex_con,1,accuracy);

                if ((nrhs>1) && (hanalysis::mex_isStaFieldStruct<T>(prhs[1])))
                {
                    hanalysis::stafield<T> nfield=hanalysis::stafield<T>((mxArray *)prhs[1]);
                    sta_assert(nfield.getRank()==0);
                    gradient=gradient.prod(nfield,0,true);
                    printf("normalizing gradient with given weights\n");
                }
            }
            else
            {

                if ((nrhs>1) && (hanalysis::mex_isStaFieldStruct<T>(prhs[1])))
                {
                    hanalysis::stafield<T> nfield=hanalysis::stafield<T>((mxArray *)prhs[1]);
                    sta_assert(nfield.getRank()==0);
                    gradient=ifield[0].deriv(1,complex_con,1,accuracy).prod(nfield,1,true);
                    printf("normalizing gradient with given weights\n");
                }  else
		{
		   
// 		    printf("ok? nan %d inf %d\n",
// 			   hanalysis::stafield<T>::Is_nan(ifield.deriv(1)),
// 			   hanalysis::stafield<T>::Is_inf(ifield.deriv(1)));
// 		    printf("ok? nan %d inf %d\n",
// 			   hanalysis::stafield<T>::Is_nan(ifield[0]),
// 			   hanalysis::stafield<T>::Is_inf(ifield[0]));
                    gradient=ifield[0].deriv(1,complex_con,1,accuracy);
		}

            }
        }
        else
            gradient=ifield;
	
	printf(".");hanalysis::mex_dumpStringNOW();

        hanalysis::stafield<T> magnitude=gradient.norm();
        hanalysis::stafield<T> ngradient=gradient.prod(magnitude.invert(),1,true);
	
// 	printf("ok? nan %d inf %d\n",hanalysis::stafield<T>::Is_nan(ifield),hanalysis::stafield<T>::Is_inf(ifield));
// 	printf("ok? nan %d inf %d\n",hanalysis::stafield<T>::Is_nan(gradient),hanalysis::stafield<T>::Is_inf(gradient));
// 	printf("ok? nan %d inf %d\n",hanalysis::stafield<T>::Is_nan(magnitude),hanalysis::stafield<T>::Is_inf(magnitude));
// 	printf("ok? nan %d inf %d\n",hanalysis::stafield<T>::Is_nan(ngradient),hanalysis::stafield<T>::Is_inf(ngradient));


        {
            std::size_t buffer_size=gradient.getNumVoxel()
                                    *hanalysis::order2numComponents(
                                        gradient.getStorage(),
                                        hanalysis::STA_OFIELD_SINGLE,
                                        BW);
            std::complex<T> * bufferA=new std::complex<T>[buffer_size];
            std::complex<T> * bufferB=new std::complex<T>[buffer_size];
            std::complex<T> * bufferC=new std::complex<T>[buffer_size];



            hanalysis::stafield<T> current(	ngradient.getShape(),
                                            ngradient.getRank(),
                                            ngradient.getStorage(),
                                            ngradient.getType(),
                                            bufferA);


            current=ngradient;


            if (gamma!=1)
                magnitude=magnitude.pow(gamma);


            hanalysis::stafield<T> shog=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                        gradient.getShape(),
                                        BW,
                                        gradient.getStorage(),
                                        hanalysis::STA_OFIELD_FULL);


            if (do_smoothing)
            {
                convoluion_kernel=hanalysis::stafield<T>(kname,
                                  gradient.getShape(),
                                  kparams,
                                  false,
                                  0,
                                  gradient.getStorage());
                T sum=convoluion_kernel.sum().real();
                convoluion_kernel=convoluion_kernel.fft(true,false,T(1)/T(sum*gradient.getNumVoxel()));
            }

	    
            for (int l=1; l<=BW; l++)
            {
	        printf(".");hanalysis::mex_dumpStringNOW();
                hanalysis::stafield<T> currentA(
                    ngradient.getShape(),
                    l,
                    ngradient.getStorage(),
                    ngradient.getType(),
                    bufferA);
                if (do_smoothing)
                {
                    hanalysis::stafield<T> currentB(
                        ngradient.getShape(),
                        l,
                        ngradient.getStorage(),
                        ngradient.getType(),
                        bufferB);
                    hanalysis::stafield<T> currentC(
                        ngradient.getShape(),
                        l,
                        ngradient.getStorage(),
                        ngradient.getType(),
                        bufferC);


                    hanalysis::stafield<T>::Prod(currentA,
                                                 magnitude,
                                                 currentB,
                                                 l,
                                                 true,1,true);
                    currentC.switchFourierFlag();
                    hanalysis::stafield<T>::FFT(currentB,
                                                currentC,
                                                true);
                    currentB.switchFourierFlag();

                    hanalysis::stafield<T>::Prod(currentC,
                                                 convoluion_kernel,
                                                 currentB,
                                                 l,
                                                 true,1,true);
                    hanalysis::stafield<T>::FFT(currentB,
                                                currentC,
                                                false);
                    shog[l]=currentC;
                } else
                    shog[l]=currentA.prod(magnitude,
                                          l,
                                          true);

                if (l<BW)
                {
                    hanalysis::stafield<T> currentB(
                        ngradient.getShape(),
                        l+1,
                        ngradient.getStorage(),
                        ngradient.getType(),
                        bufferB);
                    hanalysis::stafield<T>::Prod(currentA,
                                                 ngradient,
                                                 currentB,
                                                 l+1,
                                                 true,1,true);
                    std::swap(bufferA,bufferB);
                }
            }
            delete [] bufferA;
            delete [] bufferB;
            delete [] bufferC;
            if (do_smoothing)
                shog[0]=magnitude.fft(true).prod(convoluion_kernel,0,true).fft(false);
            else
                shog[0]=magnitude;
	    printf(".]\n");hanalysis::mex_dumpStringNOW();
        }


    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }
}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

    if (hanalysis::mex_isStaFieldStruct<float>(prhs[0]))
    {
        _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
    }
    else
    {if (hanalysis::mex_isStaFieldStruct<double>(prhs[0]))
        _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
    else
        mexErrMsgTxt("error: unsupported datatype");
    }
}
