#include "mex.h"


#define _SUPPORT_MATLAB_
#include "stafield.h"


#include <stdio.h>
#include <string.h>
#include <list>

template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

    hanalysis::mex_initMatlabFFTW();

    if (nrhs<1)
        mexErrMsgTxt("error: not enough parameters\n");

    int L=-1;
    int BW=5;
    int j=0;

    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (hanalysis::mex_hasParam(params,"L")!=-1)
            L=hanalysis::mex_getParam<int>(params,"L",1)[0];

        if (hanalysis::mex_hasParam(params,"BW")!=-1)
            BW=hanalysis::mex_getParam<int>(params,"BW",1)[0];

        if (hanalysis::mex_hasParam(params,"j",true)!=-1)
            j=hanalysis::mex_getParam<int>(params,"j",1)[0];
    }


    if (L!=-1)
        printf("L     : %d\n",L);
    else
        printf("BW     : %d\n",BW);

    printf("j     : %d\n",j);

    try {
        std::size_t numFields=mxGetNumberOfElements(prhs[0]);

        int J=numFields-1;


        printf("J     : %d\n",J);
        printf("rank  : %d\n",L+j);

        std::vector<hanalysis::stafield<T> * > features(numFields);

        for (std::size_t a=0; a<numFields; a++)
        {
            features[a]=new hanalysis::stafield<T>((mxArray *)mxGetCell(prhs[0],a));
            if ((features[a]->getStorage()!=hanalysis::STA_FIELD_STORAGE_C))
                throw  (hanalysis::STAError("error, expansion fields must be STA_FIELD_STORAGE_C!!!"));

        }
        for (std::size_t a=1; a<numFields; a++)
        {
            if ((*features[a])!=(*features[0]))
                throw  (hanalysis::STAError("error, all input fields musbe have the same properties"));
        }



        const std::complex<T> ** field_data=new const std::complex<T>*[numFields];

        if (L!=-1)
        {
            int l=L;
            int coeff_rank=l+j;
            hanalysis::stafield<T> exp_coeff=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                             features[0]->getShape(),
                                             coeff_rank,
                                             hanalysis::STA_FIELD_STORAGE_R,
                                             hanalysis::STA_OFIELD_SINGLE);
            std::vector<hanalysis::stafield<T> > ifields(numFields);
            for (std::size_t a=0; a<numFields; a++)
            {
                ifields[a]=features[a]->get(l);
                field_data[a]=ifields[a].getDataConst();
            }

            if (l+j>=0)
            {
                hanalysis::stafield<T> current_exp_coeff=exp_coeff.get(l+j);

                int result=hanalysis::sta_th_R (
                               field_data,
                               current_exp_coeff.getData() ,
                               current_exp_coeff.getShape(),
                               J,
                               l,
                               j,
                               T(1.0),
                               features[0]->getStride(),
                               current_exp_coeff.getStride(),
                               true);

                if (result==-1)
                    throw  (hanalysis::STAError("error in sta_th_R"));
            }
        } else
        {
            int coeff_rank=BW+j;
            hanalysis::stafield<T> exp_coeff=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                             features[0]->getShape(),
                                             coeff_rank,
                                             hanalysis::STA_FIELD_STORAGE_R,
                                             hanalysis::STA_OFIELD_FULL);
            for (int l=0; l<=BW; l++)
            {
                std::vector<hanalysis::stafield<T> > ifields(numFields);
                for (std::size_t a=0; a<numFields; a++)
                {
                    ifields[a]=features[a]->get(l);
                    field_data[a]=ifields[a].getDataConst();
                }

                if (l+j>=0)
                {
                    hanalysis::stafield<T> current_exp_coeff=exp_coeff.get(l+j);

                    int result=hanalysis::sta_th_R (
                                   field_data,
                                   current_exp_coeff.getData() ,
                                   current_exp_coeff.getShape(),
                                   J,
                                   l,
                                   j,
                                   T(1.0),
                                   features[0]->getStride(),
                                   current_exp_coeff.getStride(),
                                   true);

                    if (result==-1)
                        throw  (hanalysis::STAError("error in sta_th_R"));
                }
            }
        }

        delete [] field_data;

        for (std::size_t a=0; a<numFields; a++)
        {
            delete features[a];
        }

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }
}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

    std::size_t is_double=0;
    std::size_t is_single=0;
    if (!mxIsCell(prhs[0]))
        mexErrMsgTxt("error: first parameter must be cell array of stafields\n");

    std::size_t numFields=mxGetNumberOfElements(prhs[0]);
    for (std::size_t a=0; a<numFields; a++)
    {
        if (hanalysis::mex_isStaFieldStruct<float>(mxGetCell(prhs[0],a))) is_single++;
        if (hanalysis::mex_isStaFieldStruct<double>(mxGetCell(prhs[0],a))) is_double++;
    }

    if (is_single==numFields)
        _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
    else if (is_double==numFields)
        _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
    else
        mexErrMsgTxt("error: unsupported datatype");
}

