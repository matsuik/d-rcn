#include "mex.h"

#define _SUPPORT_MATLAB_
#include "stafield.h"

#include <stdio.h>
#include <string.h>
#include <list>


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

    if (nrhs<1)
        mexErrMsgTxt("error: not enough parameters\n");

    hanalysis::mex_initMatlabFFTW();

    const mxArray * params=prhs[nrhs-1] ;

    int BW=5;
    int Lap=0;
    int L=0;
    std::string kname="";
    std::vector<T> kparams;
    bool complex_con=false;
    int accuracy=0;
    int laptype=1;

    if (nrhs>1)
    {
        if (hanalysis::mex_hasParam(params,"accuracy")!=-1)
            accuracy=hanalysis::mex_getParam<int>(params,"accuracy",1)[0];

        if (hanalysis::mex_hasParam(params,"accurateLap")!=-1)
            if (hanalysis::mex_getParam<bool>(params,"accurateLap",1)[0])
                laptype=0;


        if (hanalysis::mex_hasParam(params,"BW")!=-1)
            BW=hanalysis::mex_getParam<int>(params,"BW",1)[0];

        if (hanalysis::mex_hasParam(params,"kname")!=-1)
            kname=hanalysis::mex_getParamStr(params,"kname");

        if (hanalysis::mex_hasParam(params,"kparams")!=-1)
            kparams=hanalysis::mex_getParam<T>(params,"kparams");

        if (hanalysis::mex_hasParam(params,"lap")!=-1)
            Lap=hanalysis::mex_getParam<int>(params,"lap",1)[0];

//         if (hanalysis::mex_hasParam(params,"L")!=-1)
//             L=hanalysis::mex_getParam<int>(params,"L",1)[0];

        if (hanalysis::mex_hasParam(params,"conjugate")!=-1)
            complex_con=hanalysis::mex_getParam<bool>(params,"conjugate",1)[0];
    }

    bool do_smoothing=(kname!="");

    printf("BW    : %d\n",BW);
//     printf("L     : %d\n",L);
    if (accuracy==1)
        printf("using more accurate derivatives\n");
    if (laptype==0)
        printf("using more accurate Laplace operator\n");

//     if (BW<L)
//         mexErrMsgTxt("error:BW<L\n");

    if (Lap>0)
        printf("applying %d times the Laplace opertator\n",Lap);

    if (do_smoothing)
    {
        printf("kname : %s\n",kname.c_str());
        printf("kparam: [");
        for (std::size_t t=0; t<kparams.size(); t++)
            printf("%f ",kparams[t]);
        printf("]\n");
    }

    try {

	
	
	L=hanalysis::stafield<T>((mxArray *)prhs[0]).getRank();
	
        hanalysis::stafield<T> ifield=hanalysis::stafield<T>((mxArray *)prhs[0])[L];
	
	
	
	
	if (BW<L)
	  throw hanalysis::STAError("error:BW<L");	
	
	printf("input rank: %d\n",L);
	
        ifield.createMemCopy();

        hanalysis::stafield<T> features=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                        ifield.getShape(),
                                        BW,
                                        ifield.getStorage(),
                                        hanalysis::STA_OFIELD_FULL);



#ifdef _STA_CUDA
        try {
            hanalysis_cuda::gpu_selectBestDevice();
        }
        catch (hanalysis::STAError & error)
        {
            printf("ERROR: %s\n",error.what());
        }


        hanalysis::stafieldGPU<T> featuresGPU;
        featuresGPU=features.cpu2gpu();

        hanalysis::stafieldGPU<T> ifieldGPU;
        ifieldGPU=ifield;


        if (do_smoothing)
        {
            hanalysis::stafieldGPU<T>  convoluion_kernel=hanalysis::stafield<T>(kname,
                    ifield.getShape(),
                    kparams,
                    false,
                    0,
                    ifield.getStorage()).cpu2gpu().fft(true,false,T(1)/T(ifield.getNumVoxel()));

            featuresGPU[L]=ifieldGPU.fft(true).prod(convoluion_kernel,L,true).fft(false);
        }
        else
            featuresGPU[L]=ifieldGPU;


        for (int a=0; a<Lap; a++)
            featuresGPU[L]=featuresGPU[L].lap();

        for (int l=L+1; l<=BW; l++)
        {
            hanalysis::stafieldGPU<T> in=featuresGPU[l-1];
            hanalysis::stafieldGPU<T> out=featuresGPU[l];
            hanalysis::stafieldGPU<T>::Deriv(in,out,1,complex_con,1,true);
        }

        for (int l=L; l>0; l--)
        {
            hanalysis::stafieldGPU<T> in=featuresGPU[l];
            hanalysis::stafieldGPU<T> out=featuresGPU[l-1];
            hanalysis::stafieldGPU<T>::Deriv(in,out,-1,complex_con,1,true);
        }

        features=featuresGPU;
#else



        if (do_smoothing)
        {
            hanalysis::stafield<T>  convoluion_kernel=hanalysis::stafield<T>(kname,
                    ifield.getShape(),
                    kparams,
                    false,
                    0,
                    ifield.getStorage()).fft(true,false,T(1)/T(ifield.getNumVoxel()));


            features[L]=ifield.fft(true).prod(convoluion_kernel,L,true).fft(false);
        }
        else
            features[L]=ifield;


        for (int a=0; a<Lap; a++)
            features[L]=features[L].lap(1,laptype);


        bool normalize=false;
        if ((nrhs>1) && (hanalysis::mex_isStaFieldStruct<T>(prhs[1])))
            normalize=true;

        for (int l=L+1; l<=BW; l++)
        {
            hanalysis::stafield<T> in=features[l-1];
            hanalysis::stafield<T> out=features[l];
            hanalysis::stafield<T>::Deriv(in,out,1,complex_con,1,true,accuracy);
	    
            if (normalize && (l==1))
            {
                hanalysis::stafield<T> nfield=hanalysis::stafield<T>((mxArray *)prhs[1]);
                sta_assert(nfield.getRank()==0);
                out=out.prod(nfield,l,true);
            }
        }
        if (normalize)
        {
            hanalysis::stafield<T> nfield=hanalysis::stafield<T>((mxArray *)prhs[1]);
            features[L]=features[L].prod(nfield,0,true);
        }
        for (int l=L; l>0; l--)
        {
            hanalysis::stafield<T> in=features[l];
            hanalysis::stafield<T> out=features[l-1];
            hanalysis::stafield<T>::Deriv(in,out,-1,complex_con,1,true,accuracy);
        }
#endif

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }
}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

    if (hanalysis::mex_isStaFieldStruct<float>(prhs[0]))
        _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
    else if (hanalysis::mex_isStaFieldStruct<double>(prhs[0]))
        _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
    else
        mexErrMsgTxt("error: unsupported datatype");
}

