#include "mex.h"


#define _SUPPORT_MATLAB_
#include "stafield.h"

#include <stdio.h>
#include <string.h>
#include <list>






template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<2)
        mexErrMsgTxt("error: not enough parameters\n");
    hanalysis::mex_initMatlabFFTW();

    const mxArray * params=prhs[nrhs-1];


    // counting number of voting kernels
    int num_kernels=0;
    while (hanalysis::mex_hasParam(params,"kname"+hanalysis::mex_value2string(num_kernels))!=-1)
        num_kernels++;
    printf("found %d kernel parameters\n",num_kernels);
    std::vector<std::vector<T> > kparams(num_kernels);
    std::vector<std::string> knames(num_kernels);
    for (int k=0;k<num_kernels;k++)
    {
        knames[k]=hanalysis::mex_getParamStr(params,"kname"+hanalysis::mex_value2string(k));
        kparams[k]=hanalysis::mex_getParam<T>(params,"kparams"+hanalysis::mex_value2string(k));
    }


    try {
	// counting and refering the input fields
        std::size_t numFields=mxGetNumberOfElements(prhs[0]);
        std::vector<hanalysis::stafield<T> * > features(numFields);
        for (std::size_t a=0;a<numFields;a++)
        {
            features[a]=new hanalysis::stafield<T>((mxArray *)mxGetCell(prhs[0],a));
        }

	// precomputing the voting kernels
        std::vector<hanalysis::stafield<T> > kernels_ft(num_kernels);
        for (int k=0;k<num_kernels;k++)
        {
            printf("found %s kernel with ",knames[k].c_str());
            printf("param: [");
            for (std::size_t t=0;t<kparams[k].size();t++)
                printf("%f ",kparams[k][t]);
            printf("]\n");

            kernels_ft[k]=hanalysis::stafield<T>(knames[k],
                                                 features[0]->getShape(),
                                                 kparams[k],
                                                 false,
                                                 0,
                                                 features[0]->getStorage()).fft(true,true,T(1)/T(features[0]->getNumVoxel()));

        }

        mxArray * Products=NULL;
        if (hanalysis::mex_hasParam(params,"Products")!=-1)
            Products=hanalysis::mex_getParamPtr(params,"Products");
        else
            mexErrMsgTxt("error: product matrix expected\n");

        if (!(mxGetClassID(Products)==mxDOUBLE_CLASS)||
                (mxGetNumberOfDimensions(Products)!=2)||
                (mxGetDimensions(Products)[0]!=10))
            mexErrMsgTxt("error: product matrix has wrong dimension\n");


        std::size_t num_of_products=mxGetDimensions(Products)[1];




        hanalysis::stafield<T> Fimg;

        double * products=(double *)mxGetData(Products);

        printf("num of products: %d\n",num_of_products);

        int maxBW=0;
        for (std::size_t a=0;a<num_of_products;a++)
        {
            maxBW=std::max(std::max(maxBW,(int)products[a*10+7]),(int)products[a*10+6]);
        }
        printf("my BW: %d\n",maxBW);


	
	// allocating buffers necessary for temporal results
        std::complex<T> * bufferA=NULL;
        std::complex<T> * bufferB=NULL;


        std::size_t size_buffer_A=hanalysis::order2numComponents(
	  features[0]->getStorage(),
	  features[0]->getType(),maxBW)*features[0]->getNumVoxel();
        bufferA= new std::complex<T>[size_buffer_A];
        memset(bufferA,0,size_buffer_A*sizeof(std::complex<T>));

        std::size_t size_buffer_B=hanalysis::order2numComponents(
	  features[0]->getStorage(),
	  features[0]->getType(),maxBW)*features[0]->getNumVoxel();
        bufferB= new std::complex<T>[size_buffer_B];


        hanalysis::stafield<T> result=hanalysis::stafield<T>::createFieldAndmxStruct(plhs[0],
                                      features[0]->getShape(),
                                      0,
                                      features[0]->getStorage(),
                                      hanalysis::STA_OFIELD_SINGLE);
        result=0;

        int currentKernel=0;
        hanalysis::stafield<T> kernel;
        hanalysis::stafield<T> current;
        std::size_t current_product=0;
        printf("running filter\n");


        int L=-1;
        while (current_product<num_of_products)
        {


            currentKernel=products[8];
            if (currentKernel>=num_kernels)
                mexErrMsgTxt("error: kernel index missmatch\n");

            kernel=kernels_ft[currentKernel];
            L=products[7];

            printf("running filter with out-kernel %d\n",currentKernel);
            while ((currentKernel==products[8])&&(current_product<num_of_products))
            {
                hanalysis::stafield<T> current=hanalysis::stafield<T>(features[0]->getShape(),
                                               L,
                                               features[0]->getStorage(),
                                               hanalysis::STA_OFIELD_SINGLE,
                                               bufferA);
                while (((L==products[7])&&(currentKernel==products[8]))&&(current_product<num_of_products))
                {
                    int order=0;
                    if (products[0]!=-1) order++;
                    if (products[2]!=-1) order++;
                    if (products[4]!=-1) order++;


                    double alpha=products[9];
                    switch (order)
                    {
                    case 1:
                    {
                        int field_index1=products[0];
                        int l1=products[1];

                        printf("(%d) [%d] %d (%f)\n",currentKernel,field_index1,L,alpha);
			current+=(*features[field_index1])[l1]*alpha;
                    }break;
                    case 2:
                    {
                        int field_index1=products[0];
                        int l1=products[1];
                        int field_index2=products[2];
                        int l2=products[3];
			bool normalize=((*features[field_index1])[l1].getRank()+(*features[field_index2])[l2].getRank()+L)%2==0;
			
			printf("(%d) [%d %d] (%d %d) -> %d [%d] (%f)\n",currentKernel,field_index1,field_index2,(*features[field_index1])[l1].getRank(),(*features[field_index2])[l2].getRank(),L,normalize,alpha);
			hanalysis::stafield<T>::Prod((*features[field_index1])[l1],(*features[field_index2])[l2],current,L,normalize,(T)alpha);
                        
                    }break;
                    case 3:
                    {
                        int field_index1=products[0];
                        int l1=products[1];
                        int field_index2=products[2];
                        int l2=products[3];
                        int field_index3=products[4];
                        int l3=products[5];
                        int L1=products[6];

                        hanalysis::stafield<T> prodbuffer=hanalysis::stafield<T>(features[0]->getShape(),
                                                          L1,
                                                          features[0]->getStorage(),
                                                          hanalysis::STA_OFIELD_SINGLE,
                                                          bufferB);
                        
                            printf("(%d) [%d %d %d] (((%d %d) -> %d) %d) -> %d (%f)\n",currentKernel,field_index1,field_index2,field_index3,(*features[field_index1])[l1].getRank(),(*features[field_index2])[l2].getRank(),L1,(*features[field_index3])[l3].getRank(),L,alpha);
                            hanalysis::stafield<T>::Prod((*features[field_index1])[l1],(*features[field_index2])[l2],prodbuffer,L1,false,T(1),true);
                            hanalysis::stafield<T>::Prod(prodbuffer,(*features[field_index3])[l3],current,L,false,(T)alpha);
                    }break;
                    default:
                        mexErrMsgTxt("error: not implemented yet or doesn't exist\n");
                    }
                    products+=10;
                    current_product+=1;
		    hanalysis::mex_dumpStringNOW();
                }
                if (L >= 1)
                {
                    hanalysis::stafield<T> newbuffer=hanalysis::stafield<T>(features[0]->getShape(),
                                                     L-1,
                                                     features[0]->getStorage(),
                                                     hanalysis::STA_OFIELD_SINGLE,
                                                     bufferB);
                    current=hanalysis::stafield<T>(features[0]->getShape(),
                                                   L,
                                                   features[0]->getStorage(),
                                                   hanalysis::STA_OFIELD_SINGLE,
                                                   bufferA);
                    printf("deriv %d -> %d \n",L,L-1);
                    hanalysis::stafield<T>::Deriv(current,newbuffer,-1,false,(T)1,true);
                    std::swap(bufferA,bufferB);
                    L--;
                }
                hanalysis::mex_dumpStringNOW();
            }
            while (L >= 1)
            {
		printf("?\n");
                hanalysis::stafield<T> newbuffer=hanalysis::stafield<T>(features[0]->getShape(),
                                                 L-1,
                                                 features[0]->getStorage(),
                                                 hanalysis::STA_OFIELD_SINGLE,
                                                 bufferB);
                current=hanalysis::stafield<T>(features[0]->getShape(),
                                               L,
                                               features[0]->getStorage(),
                                               hanalysis::STA_OFIELD_SINGLE,
                                               bufferA);
                printf("deriv %d -> %d \n",L,L-1);
                hanalysis::stafield<T>::Deriv(current,newbuffer,-1,false,(T)1,true);
                std::swap(bufferA,bufferB);
                L--;
            }

            current=hanalysis::stafield<T>(features[0]->getShape(),
                                           0,
                                           features[0]->getStorage(),
                                           hanalysis::STA_OFIELD_SINGLE,
                                           bufferA);
            printf("convolving with kernel %d\n",currentKernel);
            result+=current.fft(true).prod(kernel,0,true).fft(false);
            if (current_product<num_of_products)
                memset(bufferA,0,size_buffer_A*sizeof(std::complex<T>));
        }

        delete [] bufferA;
        delete [] bufferB;
      

        for (std::size_t a=0;a<numFields;a++)
        {
            delete features[a];
        }

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }

}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

    std::size_t is_double=0;
    std::size_t is_single=0;
    if (!mxIsCell(prhs[0]))
        mexErrMsgTxt("error: first parameter must be cell array of stafields\n");

    std::size_t numFields=mxGetNumberOfElements(prhs[0]);
    for (std::size_t a=0;a<numFields;a++)
    {
        if (hanalysis::mex_isStaFieldStruct<float>(mxGetCell(prhs[0],a))) is_single++;
        if (hanalysis::mex_isStaFieldStruct<double>(mxGetCell(prhs[0],a))) is_double++;
    }

    if (is_single==numFields)
        _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
    else
        if (is_double==numFields)
            _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
        else
            mexErrMsgTxt("error: unsupported datatype");
}

