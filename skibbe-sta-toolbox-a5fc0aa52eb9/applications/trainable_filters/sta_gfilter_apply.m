% STA_GFILTER_APPLY is a filter for object detection in 3D images. The
% filter parameters are learned in a training step. 
%
% 
% [H,detections]=STA_GFILTER_APPLY(Image,...
%                                  model,...
%                                  varargin)
%
%  Given an image Image and a filter model it returns the filter response
%  as an image H. The position of local maxima are given in detections
%  together with the filter response.
%
%
%
%   varargin: optional parameters ares
%       'bo',[bx,by,bz]  border cropped from the image 
%       'detect_t' threshold for the detections appearing in the list of
%       coordinates (-1 for no threshold)
%       'precision', precision of the computations, either 'single' or 'double'
%
%  default:
% [model]=STA_GFILTER_APPLY(...
%       images,...   % no default
%       model...     %  { 'bo',[0,0,0],...
%                       'precision','double',...
%                       'detect_t',-1,...
%                       }
%       )
%
%
%
%
% See also sta_gfilter_coordinates_train sta_shog sta_wavelet sta_wavelet_inorm
%



function [H,detections,FI]=sta_gfilter_apply(Image,model,varargin)
    tic
    %storage='STA_FIELD_STORAGE_C';
    storage='STA_FIELD_STORAGE_R';

    precision='double';

    detect_t=-1;
    bo(1:3)=0;
    
    for k = 1:2:numel(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;

    kparamI=model.kparamI;
    kparamO=model.kparamO;
    options_exp=model.options_exp;
    BW=model.BW;
    featurefunc=model.featurefunc;
    
    image=Image;
    imgsz = size(image);
    tmpsize=imgsz;
    
    if (min(bo(:))~=0),
            bo=bo+mod(bo+imgsz,2);
            tmpsize=imgsz+bo;
            tmpsize=sta_fft_bestshape(tmpsize);
    end;
        
    
    imagetmp=zeros(tmpsize);
    imagetmp(1:imgsz(1),1:imgsz(2),1:imgsz(3))=(image);
    bcount=1;
    
    
    if ~exist('FImage')
        image=stafieldStruct(imagetmp,storage);
        for f1=1:size(kparamI,2)-1,
                if ~isempty(options_exp) && iscell(options_exp{1})
                    option_exp=options_exp{f1};
                else
                    option_exp=options_exp;
                end;        
                features{f1}=featurefunc(image,...
                    {'kname',kparamI{1},...
                    'kparams',kparamI{f1+1},...
                    'BW',BW,...
                    option_exp{:}});
        end;

        okernel_param={};
        for F1=1:size(kparamO,2)-1
            okernel_param=[okernel_param,['kname',num2str(F1-1)],kparamO{1}];
            okernel_param=[okernel_param,['kparams',num2str(F1-1)],kparamO{F1+1}];
        end;
    end;
    
        
    if (nargout>2)&&(~exist('FImage')) 
         if strcmp(precision,'double')
            FI=sta_filter_train(features,model.C',okernel_param);
         else
            FI=cast(sta_filter_train(features,model.C',okernel_param),precision); 
         end;
         H=reshape((model.alpha')*FI(:,:),tmpsize);
         H=squeeze(H(1:imgsz(1),1:imgsz(2),1:imgsz(3)));
    else
        if exist('FImage') 
            fprintf('features exist\n');
            FI=FImage;
            if isa(FI, 'double')
                H=reshape((model.alpha')*FI(:,:),tmpsize);
                H=squeeze(H(1:imgsz(1),1:imgsz(2),1:imgsz(3)));
            elseif isa(FI, 'single')
                H=reshape(single(model.alpha')*FI(:,:),tmpsize);
                H=double(squeeze(H(1:imgsz(1),1:imgsz(2),1:imgsz(3))));
            end;
        else
            result=sta_filter_apply(features,[okernel_param,'Products',model.C']);
            H=squeeze(result.data(1,1,1:imgsz(1),1:imgsz(2),1:imgsz(3)));
        end;
    end;


detections=sta_loacalmax(H);
if detect_t==-1
    detections=detections(1:4,detections(4,:)>0.25*max(detections(4,:)))';
else
    detections=detections(1:4,detections(4,:)>detect_t)';
end;

    fprintf('applying the filter took %d seconds\n',toc);    
    
return






