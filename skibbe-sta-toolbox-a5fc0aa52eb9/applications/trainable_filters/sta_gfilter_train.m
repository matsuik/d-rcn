%
% training the filter using label images instead of label coordinates
% (will be merged in future)
%
% See also sta_gfilter_coordinates_train
%


function [model,results,nFIdata,CorrData]=sta_gfilter_train(images,labels,BW,kparamI,kparamO,varargin)
tic
%storage='STA_FIELD_STORAGE_C';
storage='STA_FIELD_STORAGE_R';

consider_border=false;
bo(1:3)=0;
options_combo={'o2_options_power',[1,0,BW,0,BW]};
options_exp={};
featurefunc=@sta_shog;


FIcol = []; %% features
licol = []; %% targets
nVcol = 0;  %% number of voxels per image

regu=0.00001;



for k = 1:2:numel(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


model.REG=regu;
model.options=options_combo;
model.options_exp=options_exp;
model.featurefunc=featurefunc;

size_padded={};
size_not_padded={};


for nI = 1:numel(images),
    image = double(images{nI}); 
    labelimg = double(labels{nI});
    imgsz = size(image);
    tmpsize=imgsz;
    
    size_padded{nI}=imgsz;
    size_not_padded{nI}=imgsz;
    
    if (min(bo(:))>0),
        bo=bo+mod(bo+imgsz,2);
        tmpsize=imgsz+bo;
        tmpsize=sta_fft_bestshape(tmpsize);
    end;
    
    
    if consider_border
     size_padded{nI}=tmpsize;
    end;
    
    
    imagetmp=zeros(tmpsize);
    imagetmp(1:imgsz(1),1:imgsz(2),1:imgsz(3))=(image);
    image=stafieldStruct(imagetmp,storage);
    imagetmp(1:imgsz(1),1:imgsz(2),1:imgsz(3))=labelimg;
    labelimg=imagetmp;
    

    
    if exist('FIdata')
        FI=FIdata.fv{nI};
        ComboSorted=FIdata.ComboSorted;
        
        if ~consider_border
            li = labelimg(1:imgsz(1),1:imgsz(2),1:imgsz(3));
        else
            li = labelimg(:);
        end;
    else    
        features={};
        for f1=1:size(kparamI,2)-1,
                if ~isempty(options_exp) && iscell(options_exp{1})
                    option_exp=options_exp{f1};
                else
                    option_exp=options_exp;
                end;
                features{f1}=featurefunc(image,...
                                        {'kname',kparamI{1},...
                                        'kparams',kparamI{f1+1},...
                                        'BW',BW,...
                                        option_exp{:}});
        end;

        okernel_param={};
        for F1=1:size(kparamO,2)-1
            okernel_param=[okernel_param,['kname',num2str(F1-1)],kparamO{1}];
            okernel_param=[okernel_param,['kparams',num2str(F1-1)],kparamO{F1+1}];
        end;
        if ~consider_border
            okernel_param=[okernel_param,'shape',imgsz];
            li = labelimg(1:imgsz(1),1:imgsz(2),1:imgsz(3));
        else
            li = labelimg(:);
        end;

        if (nI==1)
                ComboSorted=sta_product_matrix(features,options_combo{:},'numVF',size(kparamO,2)-1);
        end;

        FI=sta_filter_train(features,ComboSorted',okernel_param);

        if nargout>2
            nFIdata.fv{nI}=FI;
            nFIdata.ComboSorted=ComboSorted;
            if (nI==1)
               fprintf('feature dim: %d\n',size(FI,1)); 
            end;
        end;
    end;        
        
       
    FI = double(FI(:,:));
    numVoxels = size(FI,2);
    
    % collecting training examples 
    FIcol = [FIcol FI];
    
    % collecting training ground truth 
    licol = [licol ; li(:)];
    
    nVcol = nVcol + numVoxels;
    
    clear li;
    clear FI;
    clear ft_kernel_vote;
    clear D;
end;


    display('normalizing features');
    renormfac = sqrt(sum(FIcol.^2,2)/nVcol)+eps;
    invrenormfac = 1./renormfac;
    FIcol = FIcol.* (invrenormfac*ones(1,nVcol));

    display('computing covariances');
    % compute covariances
    if ~exist('Mask','var')
        Corr = FIcol*FIcol';
    else
        Corr =(FIcol(:,Mask(:)))*(FIcol(:,Mask(:)))';
    end;
    if nargout>3,
        CorrData.Corr=Corr;
        CorrData.invrenormfac=invrenormfac;
        CorrData.renormfac=renormfac;
    end;
    
    if ~exist('Mask','var')
        b = FIcol*double(licol(:));
    else
        b = FIcol(:,Mask(:))*double(licol(Mask(:)));
    end;
    display('solving regression problem');
    alpha = (Corr+regu*eye(size(invrenormfac,1)))\b;
    alpha = alpha / (alpha'*b) *sum(licol);
    alpha = invrenormfac.*alpha;
    display('done');    
 
    
    
    
    
    if nargout>1,
        FIcol = FIcol.* (renormfac*ones(1,nVcol));
        current=1;
        for i=1:size(images,2),
            tmp=reshape(alpha'*FIcol(:,current:current+prod(size_padded{i})-1),size_padded{i});
            results{i}=tmp(1:size_not_padded{i}(1),1:size_not_padded{i}(2),1:size_not_padded{i}(3));
            current=current+prod(size_padded{i});
        end;
    end;


    drawnow;
    


    model.alpha=alpha;
    ComboSorted(:,end)=alpha;
    model.C=ComboSorted;
    model.BW=BW;
    model.kparamI=kparamI;
    model.kparamO=kparamO;

    fprintf('the whole training procedure took %d seconds\n',toc);









