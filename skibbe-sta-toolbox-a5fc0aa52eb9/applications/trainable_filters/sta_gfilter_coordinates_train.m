% STA_GFILTER_COORDINATES_TRAIN  is training a filter for detecting objects in one or
% several images based on a given list of coordinates representing the
% centers of the positive-class
%
%
% [model,results]=STA_GFILTER_COORDINATES_TRAIN(...
%       images,...   % single or cell array of 3D images 
%       labels,...   % corresponding list of n object pos per image, each nx3 
%       BW,...       % maximum tensor rank for expansion and products
%       kparamI,...  % list of pairs of 'kname' and kparams for the features
%       kparamO,...  % list of pairs of 'kname' and kparams for the voting
%       varargin...  % see below
%       )
%   
%   model: contains all parameters for the filter
%   results: cell array of filter results when applying to the training
%   data
%
%   varargin: optional parameters ares
%       'featurefunc',  @sta_shog | @sta_wavelet | @sta_wavelet_inorm
%       'options_combo' cell array of product options including
%     {'o1_options',[use: true/false,minInRank,maxInRank],...        
%      'o2_options_power',[use: true/false,minInRank,maxInRank,minOutRank,maxOutRank],...        
%      'o2_options_cross',[use: true/false,minInRank,maxInRank,minOutRank,maxOutRank],...        
%      'o3_options_power',[use: true/false,even: true/false, odd:true/false,minInRank,maxInRank,minOutRank,maxOutRank],...
%     }
%       'bo',[bx,by,bz]  border cropped from the image 
%       'options_exp' additional options for the 'featurefunc'
%
%  default:
% [model]=STA_GFILTER_COORDINATES_TRAIN(...
%       images,...   % no default
%       labels,...   % no default
%       BW,...       % no default
%       kparamI,...  % no default
%       kparamO,...  % no default
%       varargin...  % {'options_combo',{'o2_options_power',[1,0,BW,0,BW]},...
%                       'bo',[0,0,0],...
%                       'options_exp',{},...
%                       'featurefunc',@sta_shog,...
%                       }
%       )
%
%
%
%
% See also sta_gfilter_apply sta_shog sta_wavelet sta_wavelet_inorm
%

function [model,results,nFIdata,CorrData]=sta_gfilter_coordinates_train(images,labels,BW,kparamI,kparamO,varargin)
tic
%storage='STA_FIELD_STORAGE_C';
storage='STA_FIELD_STORAGE_R';

consider_border=false;
bo(1:3)=0;
options_combo={'o2_options_power',[1,0,BW,0,BW]};
options_exp={};
featurefunc=@sta_shog;

FIcol = []; %% features
label_indices = []; %% targets
nVcol = 0;  %% number of voxels per image

regu=0.00001;



for k = 1:2:numel(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


model.REG=regu;
model.options=options_combo;
model.options_exp=options_exp;
model.featurefunc=featurefunc;

size_padded={};
size_not_padded={};


for nI = 1:numel(images),
    image = double(images{nI}); 
    label_coordinates = labels{nI};
    imgsz = size(image);
    tmpsize=imgsz;
    
    size_padded{nI}=imgsz;
    size_not_padded{nI}=imgsz;
    
    if (min(bo(:))>0),
        bo=bo+mod(bo+imgsz,2);
        tmpsize=imgsz+bo;
        tmpsize=sta_fft_bestshape(tmpsize);
    end;
    
    
    if consider_border
     size_padded{nI}=tmpsize;
    end;
    
    
    imagetmp=zeros(tmpsize);
    imagetmp(1:imgsz(1),1:imgsz(2),1:imgsz(3))=(image);
    image=stafieldStruct(imagetmp,storage);
    
    

    
    if exist('FIdata')
        FI=FIdata.fv{nI};
        ComboSorted=FIdata.ComboSorted;
        
%         if ~consider_border
%             li = labelimg(1:imgsz(1),1:imgsz(2),1:imgsz(3));
%         else
%             li = labelimg(:);
%         end;
    else    
        features={};
        for f1=1:size(kparamI,2)-1,
                if ~isempty(options_exp) && iscell(options_exp{1})
                    option_exp=options_exp{f1};
                else
                    option_exp=options_exp;
                end;
                features{f1}=featurefunc(image,...
                                        {'kname',kparamI{1},...
                                        'kparams',kparamI{f1+1},...
                                        'BW',BW,...
                                        option_exp{:}});
        end;

        okernel_param={};
        for F1=1:size(kparamO,2)-1
            okernel_param=[okernel_param,['kname',num2str(F1-1)],kparamO{1}];
            okernel_param=[okernel_param,['kparams',num2str(F1-1)],kparamO{F1+1}];
        end;
        if ~consider_border
            okernel_param=[okernel_param,'shape',imgsz];
            %li = labelimg(1:imgsz(1),1:imgsz(2),1:imgsz(3));
        else
          %  li = labelimg(:);
        end;

        if (nI==1)
                ComboSorted=sta_product_matrix(features,options_combo{:},'numVF',size(kparamO,2)-1);
        end;

        %FI=sta_shog_train_fast(features,ComboSorted',okernel_param);
        FI=sta_filter_train(features,ComboSorted',okernel_param);
        %FI=sta_shog_train(features,ComboSorted',okernel_param);

        if nargout>2
            nFIdata.fv{nI}=FI;
            nFIdata.ComboSorted=ComboSorted;
            if (nI==1)
               fprintf('feature dim: %d\n',size(FI,1)); 
            end;
        end;
    end;        
      
    assert(prod(imgsz)==size(FI,2));
    label_indx=sub2ind(imgsz,label_coordinates(:,1),label_coordinates(:,2),label_coordinates(:,3));
       
    FI = double(FI(:,:));
    numVoxels = size(FI,2);
    
    % collecting training examples 
    FIcol = [FIcol FI];
    
    % collecting training ground truth 
    label_indices=[label_indices,label_indx+nVcol];
    
    nVcol = nVcol + numVoxels;
    
    clear li;
    clear FI;
    clear ft_kernel_vote;
    clear D;
end;


    display('normalizing features');
    renormfac = sqrt(sum(FIcol.^2,2)/nVcol)+eps;
    invrenormfac = 1./renormfac;
    FIcol = FIcol.* (invrenormfac*ones(1,nVcol));

    display('computing covariances');
    % compute covariances
    Corr = FIcol*FIcol';
    if nargout>3,
        CorrData.Corr=Corr;
        CorrData.invrenormfac=invrenormfac;
        CorrData.renormfac=renormfac;
    end;
    
        b=sum(FIcol(:,label_indices),2);
        display('solving regression problem');
        alpha = (Corr+regu*eye(size(invrenormfac,1)))\b;
        alpha = alpha / (alpha'*b) *numel(label_indices);
        alpha = invrenormfac.*alpha;
        display('done');    
    
    
    
    
    if nargout>1,
        FIcol = FIcol.* (renormfac*ones(1,nVcol));
        current=1;
        for i=1:size(images,2),
            tmp=reshape(alpha'*FIcol(:,current:current+prod(size_padded{i})-1),size_padded{i});
            results{i}=tmp(1:size_not_padded{i}(1),1:size_not_padded{i}(2),1:size_not_padded{i}(3));
            current=current+prod(size_padded{i});
        end;
    end;


    drawnow;
    


    model.alpha=alpha;
    ComboSorted(:,end)=alpha;
    model.C=ComboSorted;
    model.BW=BW;
    model.kparamI=kparamI;
    model.kparamO=kparamO;

    fprintf('the whole training procedure took %d seconds\n',toc);









