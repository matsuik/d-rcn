#include "mex.h"


#define _SUPPORT_MATLAB_
#include "stafield.h"


#include <stdio.h>
#include <string.h>
#include <list>


template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<3)
        mexErrMsgTxt("error: not enough parameters\n");
    hanalysis::mex_initMatlabFFTW();


    const mxArray * params=prhs[nrhs-1];

    // counting number of voting kernels
    int num_kernels=0;
    while (hanalysis::mex_hasParam(params,"kname"+hanalysis::mex_value2string(num_kernels))!=-1)
        num_kernels++;
    printf("found %d kernel parameters\n",num_kernels);
    std::vector<std::vector<T> > kparams(num_kernels);
    std::vector<std::string> knames(num_kernels);
    for (int k=0; k<num_kernels; k++)
    {
        knames[k]=hanalysis::mex_getParamStr(params,"kname"+hanalysis::mex_value2string(k));
        kparams[k]=hanalysis::mex_getParam<T>(params,"kparams"+hanalysis::mex_value2string(k));
    }

    std::vector<int> shape(3);
    shape[0]=shape[1]=shape[2]=-1;

    if (hanalysis::mex_hasParam(params,"shape")!=-1)
    {
        shape=hanalysis::mex_getParam<int>(params,"shape",3);
        std::swap(shape[0],shape[2]);
    }


    hanalysis::CtimeStopper tic("computing features");


    try {
	// counting and refering the input fields
        std::size_t numFields=mxGetNumberOfElements(prhs[0]);
        std::vector<hanalysis::stafield<T> * > features(numFields);
        for (std::size_t a=0; a<numFields; a++)
        {
            features[a]=new hanalysis::stafield<T>((mxArray *)mxGetCell(prhs[0],a));
        }


        if (shape[0]==-1)
        {
            shape[0]=features[0]->getShape()[0];
            shape[1]=features[0]->getShape()[1];
            shape[2]=features[0]->getShape()[2];
        }




        std::vector<hanalysis::stafield<T> > kernels_ft(num_kernels);
        for (int k=0; k<num_kernels; k++)
        {
            printf("found %s kernel with ",knames[k].c_str());
            printf("param: [");
            for (std::size_t t=0; t<kparams[k].size(); t++)
                printf("%f ",kparams[k][t]);
            printf("]\n");

            kernels_ft[k]=hanalysis::stafield<T>(knames[k],
                                                 features[0]->getShape(),
                                                 kparams[k],
                                                 false,
                                                 0,
                                                 features[0]->getStorage()).fft(true,true,T(1)/T(features[0]->getNumVoxel()));

        }

        if (!(mxGetClassID(prhs[1])==mxDOUBLE_CLASS)||
                (mxGetNumberOfDimensions(prhs[1])!=2)||
                (mxGetDimensions(prhs[1])[0]!=10))
            mexErrMsgTxt("error: product matrix has wrong dimension\n");

        std::size_t num_of_products=mxGetDimensions(prhs[1])[1];


        std::size_t fv_length=num_of_products;
        mwSize ndims[2];
        ndims[1]=shape[0]*shape[1]*shape[2];
        ndims[0]=fv_length;
        plhs[0] = mxCreateNumericArray(2,ndims, mxDOUBLE_CLASS,mxREAL);
        double * Fimage = (double *) mxGetData(plhs[0]);

        double * products=(double *)mxGetData(prhs[1]);


        tic.addEvent("init");

        int maxBW=0;
        for (std::size_t a=0; a<num_of_products; a++)
        {
            maxBW=std::max(std::max(maxBW,(int)products[a*10+7]),(int)products[a*10+6]);
        }
        printf("my BW: %d\n",maxBW);


	// allocating buffers necessary for temporal results
        std::complex<T> * bufferA=NULL;
        std::complex<T> * bufferB=NULL;


        std::size_t size_buffer_A=hanalysis::order2numComponents(
	  features[0]->getStorage(),
	  features[0]->getType(),maxBW)*features[0]->getNumVoxel();
        bufferA= new std::complex<T>[size_buffer_A];


        std::size_t size_buffer_B=hanalysis::order2numComponents(
	  features[0]->getStorage(),
	  features[0]->getType(),maxBW)*features[0]->getNumVoxel();
        bufferB= new std::complex<T>[size_buffer_B];




        printf("num of products: %d\n",num_of_products);


        printf("shape out: %d %d %d\n",shape[0],shape[1],shape[2]);
        printf("shape in: %d %d %d\n",features[0]->getShape()[0],features[0]->getShape()[1],features[0]->getShape()[2]);

        for (std::size_t a=0; a<num_of_products; a++)
        {
            int order=0;
            if (products[0]!=-1) order++;
            if (products[2]!=-1) order++;
            if (products[4]!=-1) order++;

            int kernel_index=products[8];
            if (kernel_index>num_kernels)
                mexErrMsgTxt("error: kernel index missmatch\n");

            int L=products[7];

            hanalysis::stafield<T> Fimg=hanalysis::stafield<T>(features[0]->getShape(),
                                        L,
                                        features[0]->getStorage(),
                                        hanalysis::STA_OFIELD_SINGLE,
                                        bufferA);

            {

                switch (order)
                {
                case 1:
                {
                    int field_index1=products[0];
                    int l1=products[1];
                    printf("[%d] %d\n",field_index1,L);
                    Fimg=(*features[field_index1])[l1];
                    Fimg.createMemCopy();
                }
                break;
                case 2:
                {
                    int field_index1=products[0];
                    int l1=products[1];
                    int field_index2=products[2];
                    int l2=products[3];
                    bool normalize=((*features[field_index1])[l1].getRank()+(*features[field_index2])[l2].getRank()+L)%2==0;
                    printf("[%d %d] (%d %d) -> %d [%d]\n",field_index1,field_index2,(*features[field_index1])[l1].getRank(),(*features[field_index2])[l2].getRank(),L,normalize);
                    hanalysis::stafield<T>::Prod((*features[field_index1])[l1],(*features[field_index2])[l2],Fimg,L,normalize,(T)1,true);

                }
                break;
                case 3:
                {

                    int field_index1=products[0];
                    int l1=products[1];
                    int field_index2=products[2];
                    int l2=products[3];
                    int field_index3=products[4];
                    int l3=products[5];
                    int L1=products[6];

                    hanalysis::stafield<T> prodbuffer=hanalysis::stafield<T>(features[0]->getShape(),
                                                      L1,
                                                      features[0]->getStorage(),
                                                      hanalysis::STA_OFIELD_SINGLE,
                                                      bufferB);

                    printf("[%d %d %d] (((%d %d) -> %d) %d) -> %d\n",field_index1,field_index2,field_index3,(*features[field_index1])[l1].getRank(),(*features[field_index2])[l2].getRank(),L1,(*features[field_index3])[l3].getRank(),L);
                    hanalysis::stafield<T>::Prod((*features[field_index1])[l1],(*features[field_index2])[l2],prodbuffer,L1,false,T(1),true);
                    hanalysis::stafield<T>::Prod(prodbuffer,(*features[field_index3])[l3],Fimg,L,false,(T)1,true);
                }
                break;

                default:
                    mexErrMsgTxt("error: not implemented yet or doesn't exist\n");
                }

                while (L >= 1)
                {
                    hanalysis::stafield<T> newbuffer=hanalysis::stafield<T>(features[0]->getShape(),
                                                     L-1,
                                                     features[0]->getStorage(),
                                                     hanalysis::STA_OFIELD_SINGLE,
                                                     bufferB);
                    hanalysis::stafield<T> current=hanalysis::stafield<T>(features[0]->getShape(),
                                                   L,
                                                   features[0]->getStorage(),
                                                   hanalysis::STA_OFIELD_SINGLE,
                                                   bufferA);
                    hanalysis::stafield<T>::Deriv(current,newbuffer,-1,false,(T)1,true);
                    std::swap(bufferA,bufferB);
                    L--;
                }



                hanalysis::stafield<T> current=hanalysis::stafield<T>(features[0]->getShape(),
                                               0,
                                               features[0]->getStorage(),
                                               hanalysis::STA_OFIELD_SINGLE,
                                               bufferA);

                hanalysis::stafield<T> fftbuffer=hanalysis::stafield<T>(features[0]->getShape(),
                                                 0,
                                                 features[0]->getStorage(),
                                                 hanalysis::STA_OFIELD_SINGLE,
                                                 bufferB);

                hanalysis::stafield<T>::FFT(current,fftbuffer,true);

                current.switchFourierFlag();
                hanalysis::stafield<T>::Prod(fftbuffer,kernels_ft[kernel_index],current,0,true,(T)1,true);
                hanalysis::stafield<T>::FFT(current,fftbuffer,false);





                const T * source=(T *)fftbuffer.getDataConst();
                std::size_t jumpz_source=2*(features[0]->getShape()[1]*features[0]->getShape()[2]);
                std::size_t jumpy_source=2*(features[0]->getShape()[2]);
                std::size_t jumpz_dest=shape[1]*shape[2];



                #pragma omp parallel for num_threads(hanalysis::get_numCPUs())
                for (int z=0; z<shape[0]; z++)
                {
                    const T * current_source_z=source+z*jumpz_source;
                    double * dest=Fimage+z*jumpz_dest*fv_length+a;
                    for (int y=0; y<shape[1]; y++)
                    {
                        const T * current_source_y=current_source_z+y*jumpy_source;
                        for (int x=0; x<shape[2]; x++)
                        {
                            *dest=*current_source_y;
                            dest+=fv_length;
                            current_source_y+=2;
                        }
                    }
                }
            }
            products+=10;
            tic.addEvent("compuing products"+hanalysis::mex_value2string(a));
            hanalysis::mex_dumpStringNOW();
        }

        for (std::size_t a=0; a<numFields; a++)
        {
            delete features[a];
        }
        delete [] bufferA;
        delete [] bufferB;


        tic.addEvent("cleaning up");

        tic.printEvents();

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }

}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

    std::size_t is_double=0;
    std::size_t is_single=0;
    if (!mxIsCell(prhs[0]))
        mexErrMsgTxt("error: first parameter must be cell array of stafields\n");

    std::size_t numFields=mxGetNumberOfElements(prhs[0]);
    for (std::size_t a=0; a<numFields; a++)
    {
        if (hanalysis::mex_isStaFieldStruct<float>(mxGetCell(prhs[0],a))) is_single++;
        if (hanalysis::mex_isStaFieldStruct<double>(mxGetCell(prhs[0],a))) is_double++;
    }

    if (is_single==numFields)
        _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
    else if (is_double==numFields)
        _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
    else
        mexErrMsgTxt("error: unsupported datatype");
}

