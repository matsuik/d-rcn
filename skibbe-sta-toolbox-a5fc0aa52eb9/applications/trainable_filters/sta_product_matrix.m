function products_sorted=sta_product_matrix(features,varargin)
    numVF=1;

    if (~iscell(features))
        features={features};
    end;

    products=[];
    
    o1_options=[0,0,5]; % [use coeff,minInL,maxInL]
    o2_options_power=[1,0,5,0,5]; % [use angular power,minInL,maxInL,minOutL,maxOutL]
    o2_options_cross=[0,1,2,0,5]; % [use cross power,minInL,maxInL,minOutL,maxOutL]
    o2_options_cross_odd=false;
    
    o3_options_power=[0,1,0,0,5,0,5]; % [use angular power,even,odd,minInL,maxInL,minOutL,maxOutL]
    
    all_o3_feats=0;
    
    
    options={};
    
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    for k = 1:2:length(options),
            eval(sprintf('%s=options{k+1};',options{k}));
    end;    
    
    
    if numel(o2_options_power)==3,
        o2_options_power=[o2_options_power,0,0];
    end;
    if numel(o2_options_cross)==3,
        o2_options_cross=[o2_options_cross,0,0];
    end;
    if numel(o3_options_power)==5,
        o3_options_power=[o3_options_power,0,0];
    end;
    
    
for a=1:numel(features),
    a_startl=0;
    a_endl=features{a}.L;
    a_add=1;
    if strcmp(features{a}.type,'STA_OFIELD_SINGLE'),
        a_startl=a_endl;
    end;
    if strcmp(features{a}.type,'STA_OFIELD_ODD'),
        a_startl=1;
        a_add=2;
    end;
    if strcmp(features{a}.type,'STA_OFIELD_EVEN'),
        a_add=2;
    end;
    
    for la0=a_startl:a_add:a_endl,
        if o1_options(1),
            if (la0>=o1_options(2)) && (la0<=o1_options(3)),
                  for F1=1:numVF,
                        products=[products;a-1,la0, -1,-1,  -1,-1,0,la0,F1-1,0];
                  end;
            end;
        end;
    end;
    

    for b=a:numel(features),
        b_startl=0;
        b_endl=features{b}.L;
        b_add=1;
        if strcmp(features{b}.type,'STA_OFIELD_SINGLE'),
            b_startl=b_endl;
        end;
        if strcmp(features{b}.type,'STA_OFIELD_ODD'),
            b_startl=1;
            b_add=2;
        end;
        if strcmp(features{b}.type,'STA_OFIELD_EVEN'),
            b_add=2;
        end; 

        for la=a_startl:a_add:a_endl,

            if (b==a)
                b_startl=la;
            end;
            for lb=b_startl:b_add:b_endl,

                    % la(a) x lb(a) -> l (radial func a)
                    if o2_options_power(1),
                                if a==b
                                    for l=abs(la-lb):la+lb,
                                          if ((mod(la+lb+l,2)==0)&&...
                                              (la>=o2_options_power(2))&&...
                                              (lb>=o2_options_power(2))&&...
                                              (la<=o2_options_power(3))&&...
                                              (lb<=o2_options_power(3))&&...
                                              (l>=o2_options_power(4))&&...
                                              (l<=o2_options_power(5)))
                                              for F1=1:numVF,
                                                    products=[products;...
                                                        a-1,la,... 
                                                        b-1,lb,...
                                                        -1,-1,0,l,...
                                                        F1-1,0];
                                              end;
                                          end;
                                    end;
                                end;
                    end;
                    
                    % la(a) x lb(b) -> l (radial func a and b, a~=b)
                    if o2_options_cross(1),
                                if a~=b
                                    for l=abs(la-lb):la+lb,
                                          if (((mod(la+lb+l,2)==0) || (o2_options_cross_odd))&&...
                                                  (la>=o2_options_cross(2))&&...
                                                  (lb>=o2_options_cross(2))&&...
                                                  (la<=o2_options_cross(3))&&...
                                                  (lb<=o2_options_cross(3))&&...
                                                  (l>=o2_options_cross(4))&&...
                                                  (l<=o2_options_cross(5)))
                                              for F1=1:numVF,
                                                    products=[products;...
                                                        a-1,la,...
                                                        b-1,lb,...
                                                        -1,-1,0,l,...
                                                        F1-1,0];
                                              end;
                                          end;
                                    end;
                                end;
                    end;                    
                    
                    
                    % la(a) x lb(a) x lc(a) -> l (radial func a)
                    if o3_options_power(1) 
                         for c=a:numel(features),
                                 
                                c_startl=0;
                                c_endl=features{c}.L;
                                c_add=1;
                                if strcmp(features{c}.type,'STA_OFIELD_SINGLE'),
                                    c_startl=c_endl;
                                end;
                                if strcmp(features{c}.type,'STA_OFIELD_ODD'),
                                    c_startl=1;
                                    c_add=2;
                                end;
                                if strcmp(features{c}.type,'STA_OFIELD_EVEN'),
                                    c_add=2;
                                end; 
                                if (b==c)
                                    c_startl=lb;
                                else
                                    if (a==c)
                                        c_startl=la;
                                    end;
                                end;          
                                for lc=c_startl:c_add:c_endl,
                                    if o3_options_power(1),
                                        if ((a==b)&&(b==c)&&...
                                                  (la>=o3_options_power(4))&&...
                                                  (lb>=o3_options_power(4))&&...
                                                  (lc>=o3_options_power(4))&&...
                                                  (la<=o3_options_power(5))&&...
                                                  (lb<=o3_options_power(5))&&...
                                                  (lc<=o3_options_power(5)))
                                                  if o3_options_power(2) % even products
                                                    products=[products;...
                                                        compute_o3_products(la,lb,lc,a,b,c,numVF,o3_options_power(6:7),true,all_o3_feats)];
                                                  end;
                                                  if o3_options_power(3) % odd products
                                                      if ~((la==lb)||(la==lc)||(lc==lb))
                                                            products=[products;....
                                                                compute_o3_products(la,lb,lc,a,b,c,numVF,o3_options_power(6:7),false,all_o3_feats)];
                                                      end;
                                                  end;
                                        end;
                                    end;
                                end; % end lc
                         end; % end radial func c 
                    end; 
                    
            end; % end lb
        end; % end la
    end; % end radial func b 
end; % end radial func a
                    
 

BW=max(products);
if (numel(BW)>1)
BW=BW(8);
end

products_sorted=[];
for F1=1:numVF
    for L2 = BW:-1:0,        
        index = find((products(:,8)==L2) .* (products(:,9)==F1-1))';
        products_sorted=cat(1,products_sorted,products(index,:));
    end;
end;



















function products=compute_o3_products(  la,lb,lc,...% tensor field order
                                        a,b,c,...   % radial function id
                                        numVF,...   % number of different output funcs
                                        bw,...      % maximum output tensor rank
                                        even,...    % even or odd products
                                        all_o3_feats)


    assert((a==b)&&(b==c));

    products=[];

    Jrange=[0,la+lb+lc];
    l1=la;
    l2=lb;
    l3=lc;
    if (l3>l2+l1)
        Jrange(1)=l3-l2-l1;
    elseif (l3<abs(l2-l1))
        Jrange(1)=l3-abs(l2-l1);
    end;

    for J=Jrange(1):Jrange(2),
        if J<bw(1) || J>bw(2)
            continue;
        end;

        l1=la;
        l2=lb;
        l3=lc;
        f1=a-1;
        f2=b-1;
        f3=c-1;
        L{1}.p = max(abs(l1-l2),abs(J-l3)):1:min(l1+l2,l3+J);
        L{1}.f=[f1,l1,f2,l2,f3,l3];
        if (l1==l2)
           L{1}.p=L{1}.p(mod(L{1}.p,2)==0);
        end;

        if all_o3_feats==1
            r=1;
            if numel(L{r}.p)>0
                    for F=1:numVF,
                        new_product_candidates=[repmat(L{r}.f,numel(L{r}.p),1),L{r}.p',repmat([J,F-1,0],numel(L{r}.p),1)];
                        if (((mod(sum(new_product_candidates([2,4,6,8])),2)==0) && even) ...
                            || ((mod(sum(new_product_candidates([2,4,6,8])),2)==1) && ~even))
                        products=[products;new_product_candidates]; 
                        end;
                    end;
            end;
        end;

        l1=la;
        l2=lc;
        l3=lb;   
        f1=a-1;
        f2=c-1;
        f3=b-1;                
        L{2}.p=max(abs(l1-l2),abs(J-l3)):1:min(l1+l2,l3+J);
        L{2}.f=[f1,l1,f2,l2,f3,l3];                
        if (l1==l2)
           L{2}.p=L{2}.p(mod(L{2}.p,2)==0);
        end;

        if all_o3_feats==1
            r=2;
            if numel(L{r}.p)>0 && (lb~=lc)
                    for F=1:numVF,
                        new_product_candidates=[repmat(L{r}.f,numel(L{r}.p),1),L{r}.p',repmat([J,F-1,0],numel(L{r}.p),1)];
                        if (((mod(sum(new_product_candidates([2,4,6,8])),2)==0) && even) ...
                            || ((mod(sum(new_product_candidates([2,4,6,8])),2)==1) && ~even))
                        products=[products;new_product_candidates]; 
                        end;
                    end;
            end;
        end;

        l1=lc;
        l2=lb;
        l3=la;                
        f1=c-1;
        f2=b-1;
        f3=a-1;                
        L{3}.p = max(abs(l1-l2),abs(J-l3)):1:min(l1+l2,l3+J);
        L{3}.f=[f1,l1,f2,l2,f3,l3];                
        if (l1==l2)
           L{3}.p=L{3}.p(mod(L{3}.p,2)==0);
        end;

        if all_o3_feats==1 % ALL products
            r=3;
            if numel(L{r}.p)>0 && (la~=lc)
                    for F=1:numVF,
                        new_product_candidates=[repmat(L{r}.f,numel(L{r}.p),1),L{r}.p',repmat([J,F-1,0],numel(L{r}.p),1)];
                        if (((mod(sum(new_product_candidates([2,4,6,8])),2)==0) && even) ...
                            || ((mod(sum(new_product_candidates([2,4,6,8])),2)==1) && ~even))
                        products=[products;new_product_candidates]; 
                        end;
                    end;
            end;
        elseif all_o3_feats==2 % ALL but equvalent products
                numcmb=[numel(L{1}.p),numel(L{2}.p),numel(L{3}.p)];
                numcmb(numcmb==0)=realmax;

                [v,indx]=min(numcmb);

                if ~isempty(indx) && v~=realmax,

                if  1 && (sum(numcmb==v)>1)
                    %choose fastest combo
                    rankcmb=[max(L{1}.p),max(L{2}.p),max(L{3}.p)];
                    rankcmb(numcmb>v)=realmax;
                    [v,indx]=min(rankcmb);
                end;                                                            

                r=indx;

                    for F=1:numVF,
                            new_product_candidates=[repmat(L{r}.f,numel(L{r}.p),1),L{r}.p',repmat([J,F-1,0],numel(L{r}.p),1)];
                            %new_product_candidates
                            if (((mod(sum(new_product_candidates([2,4,6,8])),2)==0) && even) ...
                                || ((mod(sum(new_product_candidates([2,4,6,8])),2)==1) && ~even))
                            products=[products;new_product_candidates]; 
                            end;
                    end;


                end;

        else  % my selection
            numcmb=[numel(L{1}.p),numel(L{2}.p),numel(L{3}.p)];
            numcmb(numcmb==0)=realmax;
            [v,indx]=min(numcmb);

            if ~isempty(indx) && v~=realmax,

            if  1 && (sum(numcmb==v)>1)
                %choose fastest combo
                rankcmb=[max(L{1}.p),max(L{2}.p),max(L{3}.p)];
                rankcmb(numcmb>v)=realmax;
                [v,indx]=min(rankcmb);
            end;                                                            
                   r=indx;
                    for F=1:numVF,

                        if (l1~=l2)&&(l2~=l3)&&(l1~=l3)
                            new_product_candidates=[repmat(L{r}.f,numel(L{r}.p),1),L{r}.p',repmat([J,F-1,0],numel(L{r}.p),1)];
                            if (((mod(sum(new_product_candidates([2,4,6,8])),2)==0) && even) ...
                                || ((mod(sum(new_product_candidates([2,4,6,8])),2)==1) && ~even))
                            products=[products;new_product_candidates]; 
                            end;
                        end;

                    end;
            end;
        end; % compute all o3 featurs or only selection

    end; % J range
    

