/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox for Matlab
 * 
 *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox for Matlab
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/


#include "mex.h"
#include "stensor.h"
#include "sta_mex_helpfunc.h"


/* parameters
      prhs[0] inputfield1
      prhs[1] operation
      prhs[2] field_property
*/
template <typename T>
void _mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{


  if ( nrhs<1 )
    mexErrMsgTxt ( "ST1(rank l1)" );

  hanalysis::STA_FIELD_PROPERTIES field_property=hanalysis::STA_FIELD_STORAGE_R;

  int operation=0;
  if ( nrhs>1 )
    operation= ( * ( double* ) mxGetData ( prhs[1] ) );

  if ( nrhs>2 )
    field_property=hanalysis::enumfromstring ( mex2string ( prhs[2] ) );
  if ( field_property==-1 )
    mexErrMsgTxt ( "invalid field property\n" );


  const mxArray *ST1;
  ST1 = prhs[0];
  const mwSize *dimsST1 = mxGetDimensions ( ST1 );
  const mwSize numdimST1 = mxGetNumberOfDimensions ( ST1 );
  if ( numdimST1!=5 )
    mexErrMsgTxt ( "error: Array dimension for the first argument must be 5 (2x2L+1xXxYxZ)\n" );
  int numComponents=dimsST1[1];
  std::complex<T> *pST1 = ( std::complex<T> * ) ( mxGetData ( ST1 ) );



  std::size_t shape[3];
  for ( int i =2;i <numdimST1;i++ )
    {
      shape[i-2]=dimsST1[i];
    }
  std::swap ( shape[0],shape[2] );

  if ( hanalysis::verbose>0 )     printf ( "%i %i %i %i\n",shape[0],shape[1],shape[2],dimsST1[1] );

  std::size_t numvoxel=shape[0]*shape[1]*shape[2];
  if ( operation==1 )
    {
      mwSize ndims[2];
      ndims[0]=1;
      ndims[1]=dimsST1[1];
      plhs[0] = mxCreateNumericArray ( 2,ndims,mxGetClassID ( ST1 ),mxREAL );
      T *result = ( T * ) mxGetData ( plhs[0] );


#pragma omp parallel for num_threads(hanalysis::get_numCPUs())
      for ( int m=0;m<numComponents;m++ )
        {
          double length=0;
          std::complex<T> * current=pST1+m;
          for ( std::size_t z=0;z<numvoxel;z++ )
            {
              length+=std::norm ( *current );
              current+=numComponents;
            }
          * ( result+m ) =std::sqrt ( length );
        }
      return;
    }

  if ( operation==2 )
    {



      int ndims[5];
      ndims[0] = 2;
      ndims[1] = 1;
      std::size_t shape[3];
      for ( int i =2;i <numdimST1;i++ )
        {
          ndims[i] = dimsST1[i];
          shape[i-2]=dimsST1[i];
        }
      std::swap ( shape[0],shape[2] );
      plhs[0] = mxCreateNumericArray ( numdimST1,ndims,mxGetClassID ( ST1 ),mxREAL );
      std::complex<T> *result = ( std::complex<T> * ) mxGetData ( plhs[0] );

      std::size_t jumpz=shape[1]*shape[2];

      if ( field_property==hanalysis::STA_FIELD_STORAGE_C )
        {
#pragma omp parallel for num_threads(hanalysis::get_numCPUs())
          for ( std::size_t z=0;z<shape[0];z++ )
            {
              std::size_t Z=z;
              Z*=jumpz;
              const std::complex<T> * current_J1=&pST1[Z*numComponents];
              std::complex<T> * current_J=&result[Z];
              for ( std::size_t i=0;i<jumpz;i++ )
                {
                  std::complex<T>  length=T ( 0 );
                  for ( int m=0;m<numComponents;m++ )
                    {
                      length+=std::norm ( *current_J1++ );
                    }
                  * ( current_J++ ) =std::sqrt ( length );
                }
            }
        }
      else
        {
#pragma omp parallel for num_threads(hanalysis::get_numCPUs())
          for ( std::size_t z=0;z<shape[0];z++ )
            {
              std::size_t Z=z;
              Z*=jumpz;
              const std::complex<T> * current_J1=&pST1[Z*numComponents];
              std::complex<T> * current_J=&result[Z];
              for ( std::size_t i=0;i<jumpz;i++ )
                {
                  std::complex<T>  length=T ( 0 );
                  for ( int m=0;m<numComponents-1;m++ )
                    {
                      length+=2*std::norm ( *current_J1++ );
                    }
                  length+=std::norm ( *current_J1++ );
                  * ( current_J++ ) =std::sqrt ( length );
                }
            }
        }
      return;
    }

  if ( operation==0 )
    {
      int ndims[1];
      ndims[0]=1;
      plhs[0] = mxCreateNumericArray ( 1,ndims,mxGetClassID ( ST1 ),mxREAL );
      T *result = ( T * ) mxGetData ( plhs[0] );

      double length=0;
      std::complex<T> * current=pST1;
      numvoxel*=numComponents;
      for ( std::size_t z=0;z<numvoxel;z++ )
        {
          length+=std::norm ( *current );
          current++;
        }
      * ( result ) =std::sqrt ( length );
      return;
    }

  mexErrMsgTxt ( "error: unsuported option\n" );

}


void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  int _single=0;
  int _double=0;

  const int max_expected_param=3;
  int float_relevant_param[max_expected_param]=
  {
    1, 	// input field 1
    0,
    0
  };
  int num_float_relevant_param=0;
  int num_single_non_relevant_param=0;

  if ( nrhs>max_expected_param )
    mexErrMsgTxt ( "error: toooooo many parameters!\n" );

  for ( int i=0;i<nrhs;i++ )
    {
      if ( float_relevant_param[i]==1 )
        {
          if ( mxIsSingle ( prhs[i] ) ) _single++;
          if ( mxIsDouble ( prhs[i] ) ) _double++;
          num_float_relevant_param++;
        }
      else
        if ( ( float_relevant_param[i]==0 ) && ( mxIsSingle ( prhs[i] ) ) )
          num_single_non_relevant_param++;
    }

  if ( num_single_non_relevant_param>0 )
    mexErrMsgTxt ( "error: please only use double precision numbers as parameters!! \n (the field might also have single precision numbers)\n" );

  if ( _single==num_float_relevant_param ) _mexFunction<float> ( nlhs, plhs,  nrhs, prhs );
  else
    if ( _double==num_float_relevant_param ) _mexFunction<double> ( nlhs, plhs,  nrhs, prhs );
    else
      mexErrMsgTxt ( "error: all field parameters must be either double or single!\n" );
}


