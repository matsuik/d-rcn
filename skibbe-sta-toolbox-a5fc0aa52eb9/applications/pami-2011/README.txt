  matlab & c++ code for densely computing local 3D rotation invariant
  image descriptors based on our paper:     

    H. Skibbe, M. Reisert, T. Schmidt, T. Brox, O. Ronneberger, H. Burkhardt
    Fast Rotation Invariant 3D Feature Computation utilizing 
    Efficient Local Neighborhood Operators
    In IEEE Trans. Pattern Anal. Mach. Intell., 2012. 


  licence:
  -------
    GPL 3.0
    see http://www.gnu.org/licenses/


  we offer the following functions (matlab & octave):
    *sta_localfeat_SLD_cfast	(spherical Laguerre Descriptor SLD)
     sta_localfeat_SLD_c	(spherical Laguerre Descriptor SLD)
    *sta_localfeat_SGD_cfast	(spherical Gabor Descriptor SGD)

    * preferred implementation

  matlab only:
    sta_localfeat_SLD_m		(spherical Laguerre Descriptor SLD)



  If you use our functions or partially make use of our code then please 
  cite our paper "Fast Rotation Invariant 3D Feature Computation 
  utilizing Efficient Local Neighborhood Operators":
  

  Bibliography:
  ------------
    H. Skibbe, M. Reisert, T. Schmidt, T. Brox, O. Ronneberger, H. Burkhardt
    Fast Rotation Invariant 3D Feature Computation utilizing 
    Efficient Local Neighborhood Operators
    In IEEE Trans. Pattern Anal. Mach. Intell., 2012. 