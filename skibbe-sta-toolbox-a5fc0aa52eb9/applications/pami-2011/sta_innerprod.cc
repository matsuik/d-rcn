/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox for Matlab
 * 
 *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox for Matlab
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/


#include "mex.h"

#define _SUPPORT_MATLAB_
#include "stafield.h"
#include "sta_mex_helpfunc.h"



template <typename T>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

 
  
    
    T normfact=1;

    if (nrhs>2)
    {
        const mxArray * params=prhs[nrhs-1] ;

        if (hasParam(params,"normfact")!=-1)
            normfact=getParam<T>(params,"normfact",1)[0];
    }
  
  
    try {

        hanalysis::stafield<T> ifieldA=hanalysis::stafield<T>((mxArray *)prhs[0]);
        if (ifieldA.getType()!=hanalysis::STA_OFIELD_SINGLE)
            mexErrMsgTxt("error: currently only STA_OFIELD_SINGLE is supported");

        hanalysis::stafield<T> ifieldB=hanalysis::stafield<T>((mxArray *)prhs[1]);
        if (ifieldB.getType()!=hanalysis::STA_OFIELD_SINGLE)
            mexErrMsgTxt("error: currently only STA_OFIELD_SINGLE is supported");

        int ndims[3];
        ndims[0]=ifieldA.getShape()[2];
        ndims[1]=ifieldA.getShape()[1];
        ndims[2]=ifieldA.getShape()[0];

        plhs[0] = mxCreateNumericArray(3,ndims, getclassid<double>(),mxREAL);
        double * Fimage = (double *) mxGetData(plhs[0]);
	
	hanalysis::sta_feature_product_R<T,double>(
			    ifieldA.getDataConst(),
			    ifieldB.getDataConst(),
			    Fimage,
			    ifieldA.getShape(),
			    ifieldB.getRank(),
			    normfact,
			    false,
			    ifieldA.getStride(),
			    ifieldB.getStride(),
			    1,
			    true);

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }

}




void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<2)
        mexErrMsgTxt("error: expected (stafieldA,stafieldB.L)\n");

    if ((isStaFieldStruct<float>(prhs[0]))&&(isStaFieldStruct<float>(prhs[1])))
        _mexFunction<float>( nlhs, plhs,  nrhs, prhs );
    else
        if ((isStaFieldStruct<double>(prhs[0]))&&(isStaFieldStruct<double>(prhs[1])))
            _mexFunction<double>( nlhs, plhs,  nrhs, prhs );
        else
            mexErrMsgTxt("error: unsupported datatype");}




