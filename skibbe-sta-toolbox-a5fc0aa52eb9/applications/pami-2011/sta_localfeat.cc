/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox for Matlab
 * 
 *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox for Matlab
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

#include "mex.h"


#define _SUPPORT_MATLAB_
#include "stafield.h"

#include <stdio.h>
#include <string>
#include <list>


template<typename T>
int getLap() {
    return 1;
};
template<>
int getLap<float>() {
    return 0;
};


template <typename T, typename S>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)
        mexErrMsgTxt("error: not enough parameters\n");

    hanalysis::mex_initMatlabFFTW();

    const mxArray * params=prhs[nrhs-1] ;

    int BW=5;
    int Lap=0;
    int L=0;

    std::vector<std::vector<T> > kparams;
    std::string kname;

    int num_kernels=0;

    if (nrhs>1)
    {
        if (hanalysis::mex_hasParam(params,"BW")!=-1)
            BW=hanalysis::mex_getParam<int>(params,"BW",1)[0];



        if (hanalysis::mex_hasParam(params,"kname")!=-1)
            kname=hanalysis::mex_getParamStr(params,"kname");
        else
            mexErrMsgTxt("error: missing kernel name\n");

        while (hanalysis::mex_hasParam(params,"kparams"+hanalysis::mex_value2string(num_kernels))!=-1)
            num_kernels++;
        for (int k=0;k<num_kernels;k++)
        {
            kparams.push_back(hanalysis::mex_getParam<T>(params,"kparams"+hanalysis::mex_value2string(k)));
        }



        if (hanalysis::mex_hasParam(params,"lap")!=-1)
            Lap=hanalysis::mex_getParam<int>(params,"lap",1)[0];

        if (hanalysis::mex_hasParam(params,"L")!=-1)
            L=hanalysis::mex_getParam<int>(params,"L",1)[0];
    }

    printf("BW    : %d\n",BW);
    printf("L     : %d\n",L);
    if (BW<L)
        mexErrMsgTxt("error:BW<L\n");


    for (int k=0;k<num_kernels;k++)
    {
        printf("found %s kernel with ",kname.c_str());
        printf("param: [");
        for (std::size_t t=0;t<kparams[k].size();t++)
            printf("%f ",kparams[k][t]);
        printf("]\n");
    }

    if (Lap>0)
        printf("applying %d times the Laplace operator\n",Lap);

    int featureDim=(Lap+1)*(BW+1)*num_kernels;
    printf("feature dimension: %d\n",featureDim);


    try {
        hanalysis::stafield<T> ifield=hanalysis::stafield<T>((mxArray *)prhs[0]);
        if (ifield.getRank()!=0)
            mexErrMsgTxt("expecting tensor field of rank 0\n");
        hanalysis::stafield<T> ifield_ft=ifield.fft(true);

        int current_feat=0;
        mwSize ndims[4];
        ndims[0]=featureDim;
        ndims[1]=ifield.getShape()[2];
        ndims[2]=ifield.getShape()[1];
        ndims[3]=ifield.getShape()[0];

        plhs[0] = mxCreateNumericArray(4,ndims, hanalysis::mex_getClassId<S>(),mxREAL);
        S * Fimage = (S *) mxGetData(plhs[0]);


        hanalysis::stafield<T>  convoluion_kernel_ft;
        hanalysis::stafield<T>  convoluion_kernel;

        std::size_t size_bufferA=hanalysis::order2numComponents(ifield.getStorage(),ifield.getType(),(BW+1))*ifield.getNumVoxel();
        std::size_t size_bufferB=hanalysis::order2numComponents(ifield.getStorage(),ifield.getType(),(BW))*ifield.getNumVoxel();
        std::complex<T> * bufferA=NULL;
        std::complex<T> * bufferB=NULL;
        bufferA=new std::complex<T>[size_bufferA];
        if (size_bufferB>0)
            bufferB=new std::complex<T>[size_bufferB];
        bool largeBufferA=true;


        convoluion_kernel=hanalysis::stafield<T>(
                              ifield.getShape(),
                              0,
                              ifield.getStorage(),
                              hanalysis::STA_OFIELD_SINGLE);
        convoluion_kernel_ft=hanalysis::stafield<T>(
                                 ifield.getShape(),
                                 0,
                                 ifield.getStorage(),
                                 hanalysis::STA_OFIELD_SINGLE);
        convoluion_kernel_ft.switchFourierFlag();

        for (int k=0;k<num_kernels;k++)
        {
            T t=1;
            T normfact=1;
            if (kname=="gauss")
            {
                t=kparams[k][0]*kparams[k][0]; // t=sigma^2
                normfact=std::pow(2*t*M_PI,3.0/2.0);
            }
            if (kname=="gaussBessel")
            {
                t=kparams[k][0]*kparams[k][0]; // t=sigma^2
                normfact=std::pow(2*t*M_PI,3.0/2.0);
            }
            
            printf("(I*k) = ");
	    hanalysis::mex_dumpStringNOW();
            
            
            hanalysis::stafield<T>::makeKernel(
                kname,
                kparams[k],
                convoluion_kernel);

            hanalysis::stafield<T>::FFT(
                convoluion_kernel,
                convoluion_kernel_ft,
                true,
                false,
                T(1)/T(ifield.getNumVoxel()));

            hanalysis::stafield<T> viewA=hanalysis::stafield<T>(
                                             ifield.getShape(),
                                             0,
                                             ifield.getStorage(),
                                             hanalysis::STA_OFIELD_SINGLE,
                                             bufferA);
            viewA.switchFourierFlag();

            hanalysis::stafield<T>::Prod(
                convoluion_kernel_ft,
                ifield_ft,
                viewA,
                0,
                true,1,true);

            hanalysis::stafield<T>::FFT(
                viewA,
                convoluion_kernel,
                false);


            hanalysis::stafield<T> * lap_imageA=&convoluion_kernel;
            convoluion_kernel_ft.switchFourierFlag();
            hanalysis::stafield<T> * lap_imageB=&convoluion_kernel_ft;


            for (int lap=0;lap<=Lap;lap++)
            {
                if (lap>0)
                {
                    if (kname=="gauss")
                        normfact*=(double)std::max(2*lap-1,1)/(double)(2*lap);
		    printf("        ");
                    hanalysis::stafield<T>::Lap(*lap_imageA,*lap_imageB,t/(lap*2),true,getLap<T>());
                    std::swap(lap_imageA,lap_imageB);
                }
                
                if (BW>0)
                    printf("a(%d,%d)->",lap,lap);
                else
                    printf("a(%d,%d)\n",lap,lap);
		hanalysis::mex_dumpStringNOW();                

                hanalysis::sta_feature_product_R<T,S>(
                    lap_imageA->getDataConst(),
                    lap_imageA->getDataConst(),
                    Fimage+(current_feat++),
                    lap_imageA->getShape(),
                    lap_imageA->getRank(),
                    1/std::sqrt(normfact),
                    false,
                    lap_imageA->getStride(),
                    lap_imageA->getStride(),
                    featureDim,
                    true);

                T normfact_deriv=normfact;


                if (((BW%2==0)&&(largeBufferA))||(((BW%2==1)&&(!largeBufferA))))
                {
                    std::swap(bufferA,bufferB);
                    largeBufferA=!largeBufferA;
                }

                for (int l=0;l<BW;l++)
                {
		    if (l<BW-1)
                        printf("a(%d,%d)->",lap,lap+l+1);
                    else
                        printf("a(%d,%d)\n",lap,lap+l+1);
		    hanalysis::mex_dumpStringNOW();		  
		  
		  
                    if (kname=="gauss")
                        normfact_deriv*=std::max(2*l-1,1)*t;
                    if (kname=="gaussBessel")
                        normfact_deriv*=t;

                    hanalysis::stafield<T> viewA;
                    if (l==0)
                        viewA=lap_imageA->get(0);
                    else
                        viewA=hanalysis::stafield<T>(
                                  ifield.getShape(),
                                  l,
                                  ifield.getStorage(),
                                  hanalysis::STA_OFIELD_SINGLE,
                                  bufferA).get(l);



                    hanalysis::stafield<T> viewB=hanalysis::stafield<T>(
                                                     ifield.getShape(),
                                                     l+1,
                                                     ifield.getStorage(),
                                                     hanalysis::STA_OFIELD_SINGLE,
                                                     bufferB);



                    hanalysis::stafield<T>::Deriv(viewA,viewB,1,true,t,true);
                    std::swap(bufferA,bufferB);
                    largeBufferA=!largeBufferA;

                    hanalysis::sta_feature_product_R<T,S>(
                        viewB.getDataConst(),
                        viewB.getDataConst(),
                        Fimage+(current_feat++),
                        viewB.getShape(),
                        viewB.getRank(),
                        1/std::sqrt(normfact_deriv),
                        false,
                        viewB.getStride(),
                        viewB.getStride(),
                        featureDim,
                        true);
                }
                if (lap<Lap)
                {
                    printf("        ");
                    printf("|\n");
                    printf("        ");
                    printf("V\n");
                } else
                    printf("\n");
		hanalysis::mex_dumpStringNOW();                
                
            }
        }

        if (bufferA!=NULL) delete [] bufferA;
        if (bufferB!=NULL) delete [] bufferB;

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }
}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

    std::string fprecision="double";

    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;
        if (hanalysis::mex_hasParam(params,"fprecision")!=-1)
            fprecision=hanalysis::mex_getParamStr(params,"fprecision");
    }
    printf("feature precision : %s\n",fprecision.c_str());


    if (hanalysis::mex_isStaFieldStruct<float>(prhs[0]))
        if (fprecision=="single")
            _mexFunction<float,float>( nlhs, plhs,  nrhs, prhs );
        else
            _mexFunction<float,double>( nlhs, plhs,  nrhs, prhs );
    else
        if (hanalysis::mex_isStaFieldStruct<double>(prhs[0]))
            if (fprecision=="single")
                _mexFunction<double,float>( nlhs, plhs,  nrhs, prhs );
            else
                _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
        else
            mexErrMsgTxt("error: unsupported datatype");
}

