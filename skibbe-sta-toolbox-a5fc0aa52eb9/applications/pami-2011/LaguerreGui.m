function varargout = LaguerreGui(varargin)
% LAGUERREGUI MATLAB code for LaguerreGui.fig
%      LAGUERREGUI, by itself, creates a new LAGUERREGUI or raises the existing
%      singleton*.
%
%      H = LAGUERREGUI returns the handle to a new LAGUERREGUI or the handle to
%      the existing singleton*.
%
%      LAGUERREGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LAGUERREGUI.M with the given input arguments.
%
%      LAGUERREGUI('Property','Value',...) creates a new LAGUERREGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before LaguerreGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to LaguerreGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help LaguerreGui

% Last Modified by GUIDE v2.5 06-Nov-2011 20:38:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LaguerreGui_OpeningFcn, ...
                   'gui_OutputFcn',  @LaguerreGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before LaguerreGui is made visible.
function LaguerreGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to LaguerreGui (see VARARGIN)

% Choose default command line output for LaguerreGui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LaguerreGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);

h=handles.axes1;
axis(h,'off');
h=handles.axes2;
axis(h,'off');
h=handles.axes3;
axis(h,'off');


% --- Outputs from this function are returned to the command line.
function varargout = LaguerreGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


hwait = waitbar(0,'computing');

L = str2num(get(handles.edit1,'String'));
N = str2num(get(handles.edit2,'String'));
sigma = str2num(get(handles.edit3,'String'));
Isize = str2num(get(handles.edit4,'String'));

Laplace=1-get(handles.checkbox2,'Value');

Expansions={};


shape=Isize*[16,16,16];
kernel='gaussLaguerre';
gauss_size=[sigma,N];
clear sim;
t=gauss_size(1)^2;

set(handles.slider3,'Max',shape(1));
set(handles.slider3,'Value',ceil(shape(1)/2));


image1_s=stafield(kernel,shape,[gauss_size(1),0],1);


image1=image1_s;

fact=(2*t*pi)^(3/2);

for l=1:gauss_size(2),
   fact=fact*max((2*l-1),1)/(l*2); 
   image1=image1.lap(t/(l*2),Laplace);
end;
accuracy=0;

clear Magnitudes;
clear Magnitudesv;



for l=0:L,
    waitbar(l*0.5/L,hwait);
    image2=stafield(kernel,shape,gauss_size,1,l);
    waitbar(l*0.5/L,hwait);
    sim(l+1)=abs(sum((image1.data(:)./norm(image1.data(:))).*(image2.data(:)./norm(image2.data(:)))));
    
    Magnitudes(l+1,3)=mean(sta_norm(1/sqrt(fact)*image2.data,1));
    Magnitudes(l+1,1)=mean(sta_norm(1/sqrt(fact)*image1.data,1));
    Magnitudesv(l+1,1)=var(sta_norm(1/sqrt(fact)*image1.data,1));
    Expansions{l+1}.expl=1/sqrt(fact)*image2.data;
    Expansions{l+1}.second=1/sqrt(fact)*image1.data;
    if (l<L),
        fact=fact*max((2*l-1),1)*t;
        image1=image1.deriv(1,1,t,accuracy);
    end;
    waitbar(l*0.5/L,hwait);
end;



h=handles.axes4;
cla(h);
hold(h,'on');
plot(h,[0:L]./L,sim,'b','LineWidth',2);
axis(h,[0 1 min(sim(:)) 1]);
set(h,'XTick',[0:1/L:1]);
set(h,'XTickLabel',[0:1:L]);

waitbar(0.5,hwait);

sim_=sim;

image1=image1_s;

fact=(2*t*pi)^(3/2);
for l=1:gauss_size(2),
   fact=fact*max((2*l-1),1)/(l*2); 
   image1=image1.lap(t/(l*2),Laplace);
end;
accuracy=1;

for l=0:L,
    waitbar(0.5+0.9*l*0.5/L,hwait);
    image2=Expansions{l+1}.expl;
    sim(l+1)=abs(sum((image1.data(:)./norm(image1.data(:))).*(image2(:)./norm(image2(:)))));
    
    Magnitudes(l+1,2)=mean(sta_norm(1/sqrt(fact)*image1.data,1));
    Magnitudesv(l+1,2)=var(sta_norm(1/sqrt(fact)*image1.data,1));
    Expansions{l+1}.fourth=1/sqrt(fact)*image1.data;
    if (l<L),
        fact=fact*max((2*l-1),1)*t;
        image1=image1.deriv(1,1,t,accuracy);
    end;
    waitbar(0.5+0.9*l*0.5/L,hwait);
end;


waitbar(0.5+0.9*0.5,hwait,'generating remaining plots');

h=handles.axes4;
hold(h,'on');
plot(h,[0:L]./L,sim,'r--','LineWidth',2);
axis(h,[0 1 min(min(sim(:)),min(sim_(:))) 1]);
set(h,'XTick',[0:1/L:1]);
set(h,'XTickLabel',[0:1:L]);
legend(h,{'2nd','4th'},'Location','SouthWest');
title(h,'similarity');

set(handles.edit3,'UserData',Expansions);

listboxL={};
for l=0:L,
        listboxL{l+1}=num2str(l);
end;
set(handles.listbox1,'Value',length(listboxL));
set(handles.listbox1,'String',listboxL);
listboxM={};
for m=-L:0,
        listboxM{m+L+1}=num2str(m);
end;
set(handles.listbox2,'Value',length(listboxM));
set(handles.listbox2,'String',listboxM);


contents = cellstr(get(handles.listbox2,'String'));
M=str2num(contents{get(handles.listbox2,'Value')});
contents2 = cellstr(get(handles.listbox1,'String'));
L=str2num(contents2{get(handles.listbox1,'Value')});

drawme(L,M,handles);


h=handles.axes5;
cla(h);
plot(h,(Magnitudes),'LineWidth',2);
axis(h,[1 L min((Magnitudes(:))) max((Magnitudes(:)))]);
title(h,'avg magnitude');
legend(h,{'2nd','4th','original'},'Location','NorthWest');

h=handles.axes6;
cla(h);
plot(h,(Magnitudesv),'LineWidth',2);
axis(h,[1 L 0 max((Magnitudesv(:)))]);
title(h,'var magnitude');
legend(h,{'2nd','4th'},'Location','NorthWest');

waitbar(1,hwait,'generating remaining plots');
close(hwait);


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1

contents = cellstr(get(hObject,'String'));
L=str2num(contents{get(hObject,'Value')});


listboxM={};
for m=-L:0,
        listboxM{m+L+1}=num2str(m);
end;
set(handles.listbox2,'Value',length(listboxM));
set(handles.listbox2,'String',listboxM);
%set(handles.listbox2,'String');

drawme(L,0,handles);



% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2

%return;

contents = cellstr(get(hObject,'String'));
M=str2num(contents{get(hObject,'Value')});
contents2 = cellstr(get(handles.listbox1,'String'));
L=str2num(contents2{get(handles.listbox1,'Value')});
%L = str2num(get(handles.edit1,'String'));

drawme(L,M,handles);


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function drawme(L,M,handles)

%Gamma = str2num(get(handles.edit5,'String'));
Gamma = get(handles.slider2,'Value');
Expansions=get(handles.edit3,'UserData');

if isempty(Expansions) return; end;

N = str2num(get(handles.edit2,'String'));
Dim=get(handles.listbox3,'Value');

%str2num(get(handles.checkbox1,'Value'))
showimag=get(handles.checkbox1,'Value')+1;

contents = cellstr(get(handles.listbox4,'String'));
colormap(contents{get(handles.listbox4,'Value')});


position=ceil(get(handles.slider3,'Value'));


%colormap gray;

switch Dim,
    case 1
        img=squeeze(Expansions{L+1}.expl(showimag,L+M+1,position,:,:));
    case 2
        img=squeeze(Expansions{L+1}.expl(showimag,L+M+1,:,position,:));
    case 3
        img=squeeze(Expansions{L+1}.expl(showimag,L+M+1,:,:,position));
end;
h=handles.axes1;
cla(h);
if strcmp(contents{get(handles.listbox4,'Value')},'gray'),
    Max_=squeeze(max(abs(Expansions{L+1}.expl(showimag,L+M+1,:))));
    img=img./Max_;
    img=img.^Gamma;
    img=real(img)-imag(img);
    img=0.5*(img-min(img(:)));
    imshow(img,'Parent',h);
else
    img=img./max(abs(img(:)));
    img=img.^Gamma;
    img=real(img)-imag(img);
    img=0.5*(img-min(img(:)));
    imagesc(img,'Parent',h);  
end;
title(h,'original');
axis(h,'off');

switch Dim,
    case 1
        img=squeeze((-1)^(L+N)*Expansions{L+1}.second(showimag,L+M+1,position,:,:));
    case 2
        img=squeeze((-1)^(L+N)*Expansions{L+1}.second(showimag,L+M+1,:,position,:));
    case 3
        img=squeeze((-1)^(L+N)*Expansions{L+1}.second(showimag,L+M+1,:,:,position));
end;
h=handles.axes2;
cla(h);
if strcmp(contents{get(handles.listbox4,'Value')},'gray'),
    Max_=squeeze(max(abs(Expansions{L+1}.second(showimag,L+M+1,:))));
    img=img./Max_;
    img=img.^Gamma;
    img=real(img)-imag(img);
    img=0.5*(img-min(img(:)));
    imshow(img,'Parent',h);
else
    img=img./max(abs(img(:)));
    img=img.^Gamma;
    img=real(img)-imag(img);
    img=0.5*(img-min(img(:)));
    imagesc(img,'Parent',h);  
end;
title(h,'2nd order accuracy');
axis(h,'off');

switch Dim,
    case 1
        img=squeeze((-1)^(L+N)*Expansions{L+1}.fourth(showimag,L+M+1,position,:,:));
    case 2
        img=squeeze((-1)^(L+N)*Expansions{L+1}.fourth(showimag,L+M+1,:,position,:));
    case 3
        img=squeeze((-1)^(L+N)*Expansions{L+1}.fourth(showimag,L+M+1,:,:,position));
end;
h=handles.axes3;
cla(h);
if strcmp(contents{get(handles.listbox4,'Value')},'gray'),
    Max_=squeeze(max(abs(Expansions{L+1}.fourth(showimag,L+M+1,:))));
    img=img./Max_;
    img=img.^Gamma;
    img=real(img)-imag(img);
    img=0.5*(img-min(img(:)));
    imshow(img,'Parent',h);
else
    img=img./max(abs(img(:)));
    img=img.^Gamma;
    img=real(img)-imag(img);
    img=0.5*(img-min(img(:)));
    imagesc(img,'Parent',h);   
end;
title(h,'4th order accuracy');
axis(h,'off');



% figure (1234);
% clf;
% hold on
% tmp=squeeze((-1)^(L+N)*Expansions{L+1}.fourth(showimag,L+1,:,end/2,end/2));
% plot(tmp);
% axis([1 size(tmp,1) min(tmp(:)) max(tmp(:))]);


h=handles.axes7;
cla(h);
hold on
tmp=zeros([L+1,size(Expansions{L+1}.fourth,3)]);
for l=0:L,
    tmp(l+1,:)=squeeze(Expansions{l+1}.expl(1,l+1,end/2,end/2,:));
end;
tmp=squeeze(tmp(:,end/2+1:end));
plot(h,tmp','LineWidth',2);
lower=min(tmp(:));
upper=max(tmp(:));
%axis(h,[1 size(tmp,2) min(tmp(:)) max(tmp(:))]);

h=handles.axes8;
cla(h);
hold on
tmp=zeros([L+1,size(Expansions{L+1}.fourth,3)]);
for l=0:L,
    tmp(l+1,:)=squeeze((-1)^(l+N)*Expansions{l+1}.second(1,l+1,end/2,end/2,:));
end;
tmp=squeeze(tmp(:,end/2+1:end));
plot(h,tmp','LineWidth',2);
lower=min(lower,min(tmp(:)));
upper=max(upper,max(tmp(:)));
%axis(h,[1 size(tmp,2) min(tmp(:)) max(tmp(:))]);


h=handles.axes9;
cla(h);
hold on
tmp=zeros([L+1,size(Expansions{L+1}.fourth,3)]);
for l=0:L,
    tmp(l+1,:)=squeeze((-1)^(l+N)*Expansions{l+1}.fourth(1,l+1,end/2,end/2,:));
end;
tmp=squeeze(tmp(:,end/2+1:end));
plot(h,tmp','LineWidth',2);
lower=min(lower,min(tmp(:)));
upper=max(upper,max(tmp(:)));
if lower==upper,
    upper=lower+1;
end;
axis(h,[1 size(tmp,2) lower upper]);
title(h,'radial profile');
h=handles.axes7;
axis(h,[1 size(tmp,2) lower upper]);
title(h,'radial profile');
h=handles.axes8;
axis(h,[1 size(tmp,2) lower upper]);
title(h,'radial profile');

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


contents = cellstr(get(handles.listbox2,'String'));
M=str2num(contents{get(handles.listbox2,'Value')});
contents2 = cellstr(get(handles.listbox1,'String'));
L=str2num(contents2{get(handles.listbox1,'Value')});
%L = str2num(get(handles.edit1,'String'));

drawme(L,M,handles);


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

contents = cellstr(get(handles.listbox2,'String'));
M=str2num(contents{get(handles.listbox2,'Value')});
contents2 = cellstr(get(handles.listbox1,'String'));
L=str2num(contents2{get(handles.listbox1,'Value')});
%L = str2num(get(handles.edit1,'String'));

drawme(L,M,handles);


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in listbox3.
function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox3
contents = cellstr(get(handles.listbox2,'String'));
M=str2num(contents{get(handles.listbox2,'Value')});
contents2 = cellstr(get(handles.listbox1,'String'));
L=str2num(contents2{get(handles.listbox1,'Value')});

drawme(L,M,handles);

% --- Executes during object creation, after setting all properties.
function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox4.
function listbox4_Callback(hObject, eventdata, handles)
% hObject    handle to listbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox4
contents = cellstr(get(handles.listbox2,'String'));
M=str2num(contents{get(handles.listbox2,'Value')});
contents2 = cellstr(get(handles.listbox1,'String'));
L=str2num(contents2{get(handles.listbox1,'Value')});
%L = str2num(get(handles.edit1,'String'));

drawme(L,M,handles);

% --- Executes during object creation, after setting all properties.
function listbox4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
contents = cellstr(get(handles.listbox2,'String'));
M=str2num(contents{get(handles.listbox2,'Value')});
contents2 = cellstr(get(handles.listbox1,'String'));
L=str2num(contents2{get(handles.listbox1,'Value')});
%L = str2num(get(handles.edit1,'String'));

drawme(L,M,handles);



% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
