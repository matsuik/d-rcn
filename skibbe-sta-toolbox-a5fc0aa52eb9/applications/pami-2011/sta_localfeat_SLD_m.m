%##############################################################
%   Computes rotation invariant Gaussian Laguerre Descriptors
%   (Spherical Laguerre Descriptors SLD) according to eq. (16) in
%
%   Henrik Skibbe, Marco Reisert, Thorsten Schmidt, Thomas Brox,
%   Olaf Ronneberger and Hans Burkhardt, "Fast Rotation Invariant
%   3D Feature Computation utilizing Efficient Local Neighborhood
%   Operators"  in IEEE Transactions on Pattern Analysis 
%   and Machine Intelligence, accepted 
%
%
%   features=sta_localfeat_SLD_m(img,scales,BW,Lap);
%   img     = volumetric image, e.g. randn([128,128,128]);
%                   single(img) -> all computations using float
%      recommend:   double(img) -> all computations using double 
%   scales  = initial Gauss size(s), e.g. [3,6]
%   BW      = max bandwidth
%   Lap     = how many different radial funcions
%
%   we recommend to use the square root of features as 
%   feature image, feature=sqrt(feature)
%
%   This is a reference implementation in Matlab.
%   Note that this function will not work with Octave.
%   If you prefer Octave consider 
%   sta_localfeat_SLD_c and its corresponding mex file. 
%   The function sta_localfeat_SLD_cfast 
%   is a memory efficient implementation.
%
%   See also sta_localfeat_SLD_c ...
%   sta_localfeat_SLD_cfast ...
%   sta_localfeat_SGD_cfast
%   
%
%##############################################################

%/*#############################################################################
% *
% *	Copyright 2011 by Henrik Skibbe and Marco Reisert
% *     
% *	This file is part of the STA-ImageAnalysisToolbox for Matlab
% * 
% *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
% *	it under the terms of the GNU General Public License as published by
% *	the Free Software Foundation, either version 3 of the License, or
% *	(at your option) any later version.
% * 
% *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
% *	but WITHOUT ANY WARRANTY; without even the implied warranty of
% *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% *	GNU General Public License for more details.
% * 
% *	You should have received a copy of the GNU General Public License
% *	along with STA-ImageAnalysisToolbox for Matlab
% *	If not, see <http://www.gnu.org/licenses/>.
% *
% *
% *#############################################################################*/
function Fimage=sta_localfeat_SLD_m(img,scales,BW,Lap)


if nargin<1
    img=randn([128,128,128]);
end;
if nargin<2
    scales=5;
end;
if nargin<3
    BW=5;
end;
if nargin<4
    Lap=2;
end;

if isa(img, 'double');
    lapOperator=1;
    %faster
elseif isa(img, 'single');
    lapOperator=0;
    %less numerical problems when using single precision data
else
    error('image must be either double or single precision');
end;

num_kernels=length(scales);
kaparams=scales;


ifield=stafield(img);
ifield_ft=ifield.fft();


featureDim=(Lap+1)*(BW+1)*num_kernels;

Fimage=zeros([featureDim,ifield.shape],ifield.precision);

current_feat=1;

%computing features for different scales,
%corresponds to the initial convolution in  Figure 7.(a)
for k=1:num_kernels,
    sigma=kaparams(k);
    t=sigma*sigma;
    normfact=(2*t*pi)^(3.0/2.0);
    
    fprintf('(I*gauss) = ');
    
    convoluion_kernel_ft=stafield('gauss',ifield.shape,sigma,0,0,...
            'STA_FIELD_STORAGE_R',ifield.precision).fft(true,1/prod(ifield.shape));
    
    ifield_convolved=convoluion_kernel_ft.prod(ifield_ft,0,true).ifft();
    
    % corresponds to the rows in Figure 7.(a) [Laplace Operator]
    for lap=0:Lap,
        if (lap>0)
                    fprintf('            ');
                    normfact=normfact*max(2*lap-1,1)/(2*lap);
                    ifield_convolved=ifield_convolved.lap(t/(lap*2),lapOperator);
        end;
        if BW>0
            fprintf('a(%d,%d)->',lap,lap);
        else
            fprintf('a(%d,%d)\n',lap,lap);
        end;
        
        % computes the invariants
        %fimage=ifield_convolved.prod(ifield_convolved,0,true,1/sqrt(normfact));
        %Fimage(current_feat,:)=squeeze(fimage.data(1,1,:));
        fimage=sta_innerprod(ifield_convolved.struct,ifield_convolved.struct,{'normfact',1/sqrt(normfact)});
        Fimage(current_feat,:)=fimage(:);
        current_feat=current_feat+1;
        
        
        deriv_image=ifield_convolved;
        
        normfact_deriv=normfact;
        
        % corresponds to the columns in Figure 7.(a) [Derivative Operator]
        % (you can find implementation details of the sphercial derivatives
        %   (algorithm 1) in src/stensor.h, line 835,638 and 1628)
        for l=0:BW-1,
            normfact_deriv=normfact_deriv*max(2*l-1,1)*t;
            
            if (l<BW-1)
               fprintf('a(%d,%d)->',lap,lap+l+1);
            else
               fprintf('a(%d,%d)\n',lap,lap+l+1);
            end;
            
            deriv_image=deriv_image.deriv(1,true,t);
            
             % computes the invariants
            %fimage=deriv_image.prod(deriv_image,0,true,1/sqrt(normfact_deriv));
            %Fimage(current_feat,:)=squeeze(fimage.data(1,1,:));
            fimage=sta_innerprod(deriv_image.struct,deriv_image.struct,{'normfact',1/sqrt(normfact_deriv)});
            Fimage(current_feat,:)=fimage(:);
            current_feat=current_feat+1;
            
        end;
        
        if (lap<Lap)
                
                    fprintf('            ');
                    fprintf('|\n');
                    fprintf('            ');
                    fprintf('V\n');
        else 
                    fprintf('\n');        
        end;
    end;
end;



        