/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox for Matlab
 * 
 *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox for Matlab
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/


#include "mex.h"


#define _SUPPORT_MATLAB_
#include "stafield.h"

#include <stdio.h>
#include <string>
#include <list>


template<typename T>
int getLap() {
    return 1;
};
template<>
int getLap<float>() {
    return 0;
};



template <typename T, typename S>
void _mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{

    if (nrhs<1)
        mexErrMsgTxt("error: not enough parameters\n");

    hanalysis::mex_initMatlabFFTW();

    const mxArray * params=prhs[nrhs-1] ;

    int BW=5;
    int Lap=0;

    std::vector<std::vector<T> > kparams;

    int num_kernels=0;

    if (nrhs>1)
    {
        if (hanalysis::mex_hasParam(params,"BW")!=-1)
            BW=hanalysis::mex_getParam<int>(params,"BW",1)[0];

        while (hanalysis::mex_hasParam(params,"kparams"+hanalysis::mex_value2string(num_kernels))!=-1)
            num_kernels++;
        for (int k=0;k<num_kernels;k++)
        {
            kparams.push_back(hanalysis::mex_getParam<T>(params,"kparams"+hanalysis::mex_value2string(k)));
        }

        if (hanalysis::mex_hasParam(params,"lap")!=-1)
            Lap=hanalysis::mex_getParam<int>(params,"lap",1)[0];
    }

    printf("BW    : %d\n",BW);

    for (int k=0;k<num_kernels;k++)
    {
        printf("found gauss kernel with ");
        printf("param: [");
        for (std::size_t t=0;t<kparams[k].size();t++)
            printf("%f ",kparams[k][t]);
        printf("]\n");
    }

    if (Lap>0)
        printf("applying %d times the Laplace operator\n",Lap);

    int featureDim=(Lap+1)*(BW+1)*num_kernels;
    printf("feature dimension: %d\n",featureDim);


    try {
        hanalysis::stafield<T> ifield=hanalysis::stafield<T>((mxArray *)prhs[0]);
        if (ifield.getRank()!=0)
            mexErrMsgTxt("expecting tensor field of rank 0\n");

        hanalysis::stafield<T> ifield_ft=ifield.fft(true);



        mwSize ndims[4];
        ndims[0]=featureDim;
        ndims[1]=ifield.getShape()[2];
        ndims[2]=ifield.getShape()[1];
        ndims[3]=ifield.getShape()[0];

        plhs[0] = mxCreateNumericArray(4,ndims, hanalysis::mex_getClassId<S>(),mxREAL);
        S * Fimage = (S *) mxGetData(plhs[0]);



        int current_feat=0;
        // computing features for different scales,
        // corresponds to the initial convolution in  Figure 7.(a)
        for (int k=0;k<num_kernels;k++)
        {
            T t=kparams[k][0]*kparams[k][0]; // t=sigma^2
            T normfact=std::pow(2*t*M_PI,3.0/2.0);

            printf("(I*gauss) = ");
	    hanalysis::mex_dumpStringNOW();
            hanalysis::stafield<T>  convoluion_kernel_ft=hanalysis::stafield<T>(
                        "gauss",
                        ifield.getShape(),
                        kparams[k],
                        false,
                        0,
                        ifield.getStorage()).fft(true,true,T(1)/T(ifield_ft.getNumVoxel()));

            hanalysis::stafield<T> ifield_convolved=convoluion_kernel_ft.prod(ifield_ft,0,true).fft(false);

            // corresponds to the rows in Figure 7.(a) [Laplace Operator]
            for (int lap=0;lap<=Lap;lap++)
            {
                if (lap>0)
                {
                    normfact*=(double)std::max(2*lap-1,1)/(double)(2*lap);
                    printf("            ");
                    ifield_convolved=ifield_convolved.lap(t/(lap*2),getLap<T>());
                }
                if (BW>0)
                    printf("a(%d,%d)->",lap,lap);
                else
                    printf("a(%d,%d)\n",lap,lap);
		hanalysis::mex_dumpStringNOW();
                // computes the invariants
                hanalysis::sta_feature_product_R<T,S>(
                    ifield_convolved.getDataConst(),
                    ifield_convolved.getDataConst(),
                    Fimage+(current_feat++),
                    ifield_convolved.getShape(),
                    ifield_convolved.getRank(),
                    1/std::sqrt(normfact),
                    false,
                    ifield_convolved.getStride(),
                    ifield_convolved.getStride(),
                    featureDim,
                    true);

                hanalysis::stafield<T> deriv_image=ifield_convolved;


                T normfact_deriv=normfact;
                // corresponds to the columns in Figure 7.(a) [Derivative Operator]
                // (you can find implementation details of the sphercial derivatives
                //    (algorithm 1) in src/stensor.h, line 835,638 and 1628)
                for (int l=0;l<BW;l++)
                {
                    normfact_deriv*=std::max(2*l-1,1)*t;

                    if (l<BW-1)
                        printf("a(%d,%d)->",lap,lap+l+1);
                    else
                        printf("a(%d,%d)\n",lap,lap+l+1);
		    hanalysis::mex_dumpStringNOW();
                    deriv_image=deriv_image.deriv(1,true,t);


                    // computes the invariants
                    hanalysis::sta_feature_product_R<T,S>(
                        deriv_image.getDataConst(),
                        deriv_image.getDataConst(),
                        Fimage+(current_feat++),
                        deriv_image.getShape(),
                        deriv_image.getRank(),
                        1/std::sqrt(normfact_deriv),
                        false,
                        deriv_image.getStride(),
                        deriv_image.getStride(),
                        featureDim,
                        true);
                }
                if (lap<Lap)
                {
                    printf("            ");
                    printf("|\n");
                    printf("            ");
                    printf("V\n");
                } else
                    printf("\n");
		hanalysis::mex_dumpStringNOW();
            }
        }

    } catch (hanalysis::STAError & error)
    {
        mexErrMsgTxt(error.what());
    }
}





void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
    if (nrhs<1)
        mexErrMsgTxt("error: nrhs<1\n");

    std::string fprecision="double";

    if (nrhs>1)
    {
        const mxArray * params=prhs[nrhs-1] ;
        if (hanalysis::mex_hasParam(params,"fprecision")!=-1)
            fprecision=hanalysis::mex_getParamStr(params,"fprecision");
    }
    printf("feature precision : %s\n",fprecision.c_str());


    if (hanalysis::mex_isStaFieldStruct<float>(prhs[0]))
        if (fprecision=="single")
            _mexFunction<float,float>( nlhs, plhs,  nrhs, prhs );
        else
            _mexFunction<float,double>( nlhs, plhs,  nrhs, prhs );
    else
        if (hanalysis::mex_isStaFieldStruct<double>(prhs[0]))
            if (fprecision=="single")
                _mexFunction<double,float>( nlhs, plhs,  nrhs, prhs );
            else
                _mexFunction<double,double>( nlhs, plhs,  nrhs, prhs );
        else
            mexErrMsgTxt("error: unsupported datatype");
}

