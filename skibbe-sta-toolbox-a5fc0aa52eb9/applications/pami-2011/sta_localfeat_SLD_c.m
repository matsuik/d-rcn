%##############################################################
%   Computes rotation invariant Gaussian Laguerre Descriptors
%   (Spherical Laguerre Descriptors SLD) according to eq. (16) in
%
%   Henrik Skibbe, Marco Reisert, Thorsten Schmidt, Thomas Brox,
%   Olaf Ronneberger and Hans Burkhardt, "Fast Rotation Invariant
%   3D Feature Computation utilizing Efficient Local Neighborhood
%   Operators"  in IEEE Transactions on Pattern Analysis 
%   and Machine Intelligence, accepted 
%
%
%   features=sta_localfeat_SLD_c(img,scales,BW,Lap);
%   img     = volumetric image, e.g. randn([128,128,128]);
%                   single(img) -> all computations using float
%      recommend:   double(img) -> all computations using double 
%   scales  = initial Gauss size(s), e.g. [3,6]
%   BW      = max bandwidth
%   Lap     = how many different radial funcions
%
%   we recommend to use the square root of features as 
%   feature image, feature=sqrt(feature)
%
%   This is a reference implementation in C++ for 
%   Octave users (works with Matlab, too)
%   If you have Matlab, it is worth to consider 
%   sta_localfeat_SLD_m. 
%
%   The function sta_localfeat_SLD_cfast 
%   is a memory efficient implementation.
%
%
%   See also sta_localfeat_SLD_m ...
%   sta_localfeat_SLD_cfast ...
%   sta_localfeat_SGD_cfast
%
%##############################################################

%/*#############################################################################
% *
% *	Copyright 2011 by Henrik Skibbe and Marco Reisert
% *     
% *	This file is part of the STA-ImageAnalysisToolbox for Matlab
% * 
% *	STA-ImageAnalysisToolbox for Matlab is free software: you can redistribute it and/or modify
% *	it under the terms of the GNU General Public License as published by
% *	the Free Software Foundation, either version 3 of the License, or
% *	(at your option) any later version.
% * 
% *	STA-ImageAnalysisToolbox for Matlab is distributed in the hope that it will be useful,
% *	but WITHOUT ANY WARRANTY; without even the implied warranty of
% *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% *	GNU General Public License for more details.
% * 
% *	You should have received a copy of the GNU General Public License
% *	along with STA-ImageAnalysisToolbox for Matlab
% *	If not, see <http://www.gnu.org/licenses/>.
% *
% *
% *#############################################################################*/
function Fimage=sta_localfeat_SLD_c(img,scales,BW,Lap)


if nargin<1
    img=randn([128,128,128]);
end;
if nargin<2
    scales=5;
end;
if nargin<3
    BW=5;
end;
if nargin<4
    Lap=2;
end;

if isa(img, 'double');
    precision='double';
elseif isa(img, 'single');
    precision='single';
else
    error('image must be either double or single precision');
end;


param={};
for s=1:length(scales),
    param=cat(2,{['kparams',num2str(s-1)],scales(s)},param);
end;
param=cat(2,param,{'BW',BW,'lap',Lap,'fprecision',precision});

Fimage=sta_localfeat_GaussLaguerre(stafieldStruct(img),param);

