function demo_Invariant_features


% loading an image
load ./data/airplane.mat

% showing isosurface
figure('Position',[1 1 300 300]);
clf;
isurf=isosurface(Img, 0.1,'noshare');
%isurf=isosurface(Img, 0.1);
p1 = patch(isurf,'FaceColor',[0.2,0.6,0.8],'EdgeColor','none','FaceAlpha',1);
shape=(size(Img));
axis([1 shape(2) 1 shape(1) 1 shape(3)]);
daspect([1,1,1]);
view(-142,-32);
camlight;
lighting phong;
isonormals(Img,p1);
axis off;
camup([0,-1,0]);


% showing MIP
figure('Position',[1 1 300 300]);
imagesc(squeeze(max(Img,[],2)));
colormap gray

% creating roated version 
figure('Position',[1 1 300 300]);
ImgR=permute(Img,[3,2,1]);
imagesc(squeeze(max(ImgR,[],2)));
colormap gray


%% Covariant feature images

% Computing Gaussian derivatives up to order 5 for both images
%waveletsI=sta_wavelet(stafieldStruct(Img),{'kname','gauss','kparams',3,'BW',5});
%waveletsIR=sta_wavelet(stafieldStruct(ImgR),{'kname','gauss','kparams',3,'BW',5});

% Computing SHOG features up to order 5 for both images
waveletsI=sta_shog(stafieldStruct(Img),{'kname','gauss','kparams',3,'BW',5});
waveletsIR=sta_shog(stafieldStruct(ImgR),{'kname','gauss','kparams',3,'BW',5});


% waveletsI and waveletsIR are both a collection of tensor fields
% representing all derivatives of order 0 up to 5 (type: 'STA_OFIELD_FULL')
waveletsI

%% Plotting real and imaginary parts of the tensor fields
position=[1,1, 900 800];
figure('Position',position);
for a=0:5
    for b=0:5
        if b<=a
            subplot(6,6,6*a+b+1);
            stafield_figure(stafieldStruct(waveletsI,a),'c',1,'m',b,'dim',2);
        end;
    end;
end;    

figure('Position',position);
for a=0:5
    for b=0:5
        if b<=a
            subplot(6,6,6*a+b+1);
            stafield_figure(stafieldStruct(waveletsI,a),'c',2,'m',b,'dim',2);
        end;
    end;
end;    
    
figure('Position',position);
for a=0:5
    for b=0:5
        if b<=a
            subplot(6,6,6*a+b+1);
            stafield_figure(stafieldStruct(waveletsIR,a),'c',1,'m',b,'dim',2);
        end;
    end;
end;    

figure('Position',position);
for a=0:5
    for b=0:5
        if b<=a
            subplot(6,6,6*a+b+1);
            stafield_figure(stafieldStruct(waveletsIR,a),'c',2,'m',b,'dim',2);
        end;
    end;
end;    
    
    


%% Locally rotation invariant feature images (powerspectrum)

fimgI=zeros([6,waveletsI.shape],'single');
for l=0:5
    ofield=sta_prod(waveletsI,waveletsI,0,{'L1',l,'L2',l,'normalize',true});
    fimgI(l+1,:,:,:)=(sqrt(ofield.data(1,1,:,:,:)));
end;

fimgIR=zeros([6,waveletsIR.shape],'single');
for l=0:5
    ofield=sta_prod(waveletsIR,waveletsIR,0,{'L1',l,'L2',l,'normalize',true});
    fimgIR(l+1,:,:,:)=(sqrt(ofield.data(1,1,:,:,:)));
end;


close all
position=[1,1, 1000 300];
figure('Position',position);

for a=0:5
            subplot(2,6,6*0+a+1);
            imagesc(squeeze((fimgI(a+1,:,end/2,:))));
            daspect([1,1,1]);   axis off;  colormap gray
            subplot(2,6,6*1+a+1);
            imagesc(squeeze((fimgIR(a+1,:,end/2,:))));
            daspect([1,1,1]);   axis off;  colormap gray
end;    


%% Locally rotation invariant feature images (powerspectrum) using sta_invrts


fimgI=sqrt(sta_invrts(waveletsI));
fimgIR=sqrt(sta_invrts(waveletsIR));


close all
position=[1,1, 1000 300];
figure('Position',position);

for a=0:5
            subplot(2,6,6*0+a+1);
            imagesc(squeeze((fimgI(a+1,:,end/2,:))));
            daspect([1,1,1]);   axis off;  colormap gray
            subplot(2,6,6*1+a+1);
            imagesc(squeeze((fimgIR(a+1,:,end/2,:))));
            daspect([1,1,1]);   axis off;  colormap gray
end;    


%% Locally rotation invariant feature images (bispectrum/even) using sta_invrts

fimgI=sta_invrts(waveletsI,'power2',false,'power3',true,'o3even',true,'o3odd',false);
fimgIR=sta_invrts(waveletsIR,'power2',false,'power3',true,'o3even',true,'o3odd',false);



fimgI=sign(fimgI).*(abs(fimgI).^(1/3));
fimgIR=sign(fimgIR).*(abs(fimgIR).^(1/3));
numfeats=size(fimgI,1);

close all
position=[1,1, 1800 200];
figure('Position',position);

for a=1:numfeats
            subplot(2,numfeats,numfeats*0+a);
            imagesc(squeeze((fimgI(a,:,end/2,:))));
            daspect([1,1,1]);   axis off;  colormap gray
            subplot(2,numfeats,numfeats*1+a);
            imagesc(squeeze((fimgIR(a,:,end/2,:))));
            daspect([1,1,1]);   axis off;  colormap gray
end;




%% Locally rotation invariant feature images (bispectrum/odd) using sta_invrts

% the odd bispectrum components can resolve reflection symmetries

fimgI=sta_invrts(waveletsI,'power2',false,'power3',true,'o3even',false,'o3odd',true);
fimgIR=sta_invrts(waveletsIR,'power2',false,'power3',true,'o3even',false,'o3odd',true);



fimgI=sign(fimgI).*(abs(fimgI).^(1/3));
fimgIR=sign(fimgIR).*(abs(fimgIR).^(1/3));
numfeats=size(fimgI,1);

close all
figure

for a=1:numfeats
            subplot(2,numfeats,numfeats*0+a);
            imagesc(squeeze((fimgI(a,:,end/2,:))));
            daspect([1,1,1]);   axis off;  colormap gray
            subplot(2,numfeats,numfeats*1+a);
            imagesc(squeeze((fimgIR(a,:,end/2,:))));
            daspect([1,1,1]);   axis off;  colormap gray
end;



%% References
% 
%  H. Skibbe, M. Reisert, T. Schmidt, T. Brox, O. Ronneberger, H. Burkhardt
% Fast Rotation Invariant 3D Feature Computation utilizing Efficient Local Neighborhood Operators
% In IEEE Trans. Pattern Anal. Mach. Intell., 2012.
% 
%  K. Liu, H. Skibbe, T. Schmidt, T. Blein, K. Palme, T. Brox, O. Ronneberger
% Rotation-Invariant HOG Descriptors using Fourier Analysis in Polar and Spherical Coordinates
% In International Journal of Computer Vision, volume Online First, 2013. 
% 
%  H. Skibbe, M. Reisert, H. Burkhardt
% SHOG - Spherical HOG Descriptors for Rotation Invariant 3D Object Detection
% In Proc. of the DAGM, LNCS, Springer, 2011.
% 
%  H. Skibbe, M. Reisert, H. Burkhardt
% Gaussian Neighborhood Descriptors for Brain Segmentation
% In Proc. of the MVA, 2011.
% 
%  H. Skibbe, M. Reisert
% Dense Rotation Invariant Brain Pyramids for Automated Human Brain Parcellation
% In Proc. of the Informatik 2011, Workshop on Emerging Technologies for Medical Diagnosis and Therapy, 2011.
% 
%  H. Skibbe
% Spherical Tensor Algebra for Biomedical Image Analysis
% PhD thesis, Albert-Ludwigs-Universität Freiburg, 2013.

































