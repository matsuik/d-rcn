function demo_SE3_covariant_detection

%% Loading the images
 
% there are three images. One containg two objects for training and
% two images for evaluation. The objects vary in size, appereance, position, 
% orientation and brightness.

Noise=0.05;
load('./data/imgdata_filter.mat','data');

titles={'training image','test image 1','test image 2'};
for a=1:numel(data)
    figure('Position',[1 1 500 300]);
    clf;
    isurf=isosurface(data(a).img, Noise,'noshare');
    p1 = patch(isurf,'FaceColor',[0.2,0.8,0.6],'EdgeColor','none','FaceAlpha',1);
    shape=(size(data(a).img));
    daspect([1,1,1]);
    view(-147,-28);
    camlight;
    lighting phong;
    isonormals(data(a).img,p1);
    axis off;
    title (titles{a});
end;

%% Showing the center slice of each image after adding noise

for a=1:numel(data)
    figure('Position',[1 1 400 400]);
    imgn=data(a).img+Noise*randn(size(data(a).img));
    imagesc(imgn(:,:,end/2));
    daspect([1,1,1]);   axis off;  colormap gray
    title (titles{a});
end;



%% We aim at detecting the small spikes attached to the surface

for a=1:numel(data)
        figure('Position',[1 1 500 300]);
        clf;
        isurf=isosurface(data(a).img, Noise,'noshare');
        p1 = patch(isurf,'FaceColor',[0.2,0.8,0.6],'EdgeColor','none','FaceAlpha',1);
        shape=(size(data(a).img));
        daspect([1,1,1]);
        view(-147,-28);
        camlight;
        lighting phong;
        isonormals(data(a).img,p1);
        axis off;
        title (titles{a});
        
         gt_pos=data(a).lables(:,1:3);
         for p=1:size(gt_pos,1)
            line([gt_pos(p,2)-3,gt_pos(p,2)+3],[gt_pos(p,1)-3,gt_pos(p,1)+3],[gt_pos(p,3),gt_pos(p,3)],'linewidth',2,'color',[1,0,0]);
            line([gt_pos(p,2)-3,gt_pos(p,2)+3],[gt_pos(p,1)+3,gt_pos(p,1)-3],[gt_pos(p,3),gt_pos(p,3)],'linewidth',2,'color',[1,0,0]);
            line([gt_pos(p,2),gt_pos(p,2)],[gt_pos(p,1),gt_pos(p,1)],[gt_pos(p,3)+3,gt_pos(p,3)-3],'linewidth',2,'color',[1,0,0]);
         end;
end;     
     
     

%% Training a filter

% use the precomputed filter
load_model=true;


if load_model
    load data/models.mat;
else
    
    train_img=1; % first image for training
    imgn=data(train_img).img+Noise*randn(size(data(train_img).img));

    gt_pos=data(train_img).lables(:,1:3); % position of ground truth labels
    fprintf('\nground truth lables:\n');
    display(gt_pos);
    fprintf('--------------------\n\n');


    fprintf('traing a SHOG filter\n');
    model=sta_gfilter_coordinates_train(...
        {imgn},...
        {gt_pos},...
        5,...
        {'sh',[0,2],[2,2]},...
        {'gauss',3},...
        'options_exp',{'gamma',0.2,...
                       'presmooth',1.5},...
        'options_combo',{'o2_options_power',[1,1,5,0,5]},...
        'featurefunc',@sta_shog);
    % save('data/models.mat','model');
end;

%% Applying the filter to all images

for a=1:numel(data)
     imgn=data(a).img+Noise*randn(size(data(a).img));

     fprintf('\n\n--------------------\n');
     fprintf('applying the filter\n');
     H=sta_gfilter_apply(imgn,model);

     
     

     %filter response;
     figure('Position',[1 1 400 400]);
     imagesc(squeeze(max(H,[],3)));
     colormap gray
     title ([titles{a},' : filter response']);
     
     %detections (green) + ground truth(red)
     figure('Position',[1 1 400 400]);
     imagesc(sta_3Dto2D(data(a).img));
     daspect([1,1,1]);   axis off;  colormap gray


     gt_pos=data(a).lables(:,1:2);
     for p=1:size(gt_pos,1)
        line([gt_pos(p,2)-1,gt_pos(p,2)+1],[gt_pos(p,1)-1,gt_pos(p,1)+1],'linewidth',2,'color',[1,0,0]);
     end;

     detections=sta_loacalmax(H,0.5);

     gt_pos=detections(1:2,:)';
     for p=1:size(gt_pos,1)
        line([gt_pos(p,2)-1,gt_pos(p,2)+1],[gt_pos(p,1)+1,gt_pos(p,1)-1],'linewidth',2,'color',[0,1,0]);
     end;
     title ([titles{a},' : detections (green) + ground truth(red)']);
end; 
 
 
 
%% References
% 
%  H. Skibbe, M. Reisert, H. Burkhardt
% SHOG - Spherical HOG Descriptors for Rotation Invariant 3D Object Detection
% In Proc. of the DAGM, LNCS, Springer, 2011.
% 
%  H. Skibbe, M. Reisert
% Detection of Unique Point Landmarks in HARDI Images of the Human Brain
% Proc. of the Workshop on Computational Diffusion MRI (CDMRI'12), part of the MICCAI 2012, 2012.
%
%  H. Skibbe, M. Reisert, O. Ronneberger, H. Burkhardt
% Spherical Bessel Filter for 3D Object Detection
% In Proc. of the ISBI, 2011. 
%
%  H. Skibbe, M. Reisert
% Rotation Covariant Image Processing for Biomedical Applications
% In Computational and Mathematical Methods in Medicine, Special Issue on 
% Mathematical Methods in Biomedical Imaging, Hindawi Publishing Corporation, volume 2013, 2013. 
%

 