function demo_The_stafield_structure_and_stafield_class


%% The stafieldStruct structure (Matlab,Octave)
    
    %The stafieldStruct structure is the default container for 
    %spherical tensor fields
    
    %suppose you have an image
    img=rand([64,64,64]);
    
    %you can transform the image into a tensor field by
    field=stafieldStruct(img);
    field
    %that is, an image is a tensor field of rank L=0.
    
    
    % All our functions working on tensor fields are expecting 
    % a stafieldStruct  as input
    
    %type 
    %   help stafieldStruct
    %for further details
    

%% The stafield class (Matlab only)

    % The stafield class is not available in octave
    % It extents the stafieldStruct structure  with
    % member functions
    
    imgA=rand([64,64,64]);
    fieldA=stafield(imgA);
    fieldA
    
    % A stafield can be reduced to a stafieldStruct via
    fprintf('stafield fieldA as struct:');
    fieldA.struct
    
        
    
    imgB=rand([64,64,64]);
    fieldB=stafield(imgB);
    
    % you cann add/substruct fields in a point-by-point manner
    fieldC=fieldA+fieldB;
    fieldC=fieldA-fieldB;
    
    % it further supports the multiplication with a scalar
    a=4+3i;
    fieldC=fieldA*a;
    fieldC=fieldA/a;
    
    % tensor derivatives
    fieldC=fieldA.deriv(1); %up-derivative
    fieldC=fieldA.deriv(1).deriv(-1);  %up- and down-derivative
    % for details type
    % help sta_deriv
    
    
    % tensor products
    fieldC=fieldA.deriv(1); %up-derivative
    fieldD=fieldA.deriv(1).deriv(1);  %up- and down-derivative
    fieldE=fieldC.prod(fieldD,2); % computes a tensor field of order 2 out 
                                  %of fieldC and fieldD 
    fieldE
    % for details type
    % help sta_prod
    
    
    % tensor FFT
    fprintf('field in spatial domain\n');
    display(fieldA.storage)
    fieldC=fieldA.fft;  %forward fft
    fprintf('field in frequency domain\n');
    display(fieldC.storage)
    fieldD=fieldC.ifft(0,1/prod(fieldC.shape)); %backward fft
    fprintf('field in spatial domain again\n');
    display(fieldD.storage)
    
    
    % for details type
    % help sta_fft
    % help sta_ifft
    
    %convolution: Gaussian derivative with Gaussian
    fieldF=fieldA.deriv(1).fft.prod(fieldA.fft,1).ifft;  
    
    %laplace:
    fieldF=fieldA.lap;      
    % help lap
                                      
     
    % type 
    %   help stafield
    % for further details


%% References
% 
%  H. Skibbe, M. Reisert
% Rotation Covariant Image Processing for Biomedical Applications
% In Computational and Mathematical Methods in Medicine, Special Issue on 
% Mathematical Methods in Biomedical Imaging, Hindawi Publishing Corporation, volume 2013, 2013. 
%
%  H. Skibbe
% Spherical Tensor Algebra for Biomedical Image Analysis
% PhD thesis, Albert-Ludwigs-Universität Freiburg, 2013.
