function img2D=sta_3Dto2D(img,varargin)

shape=size(img);
threshold=0.01;

lightpos=[shape(1:2)/2,-100];
viewdir=lightpos./norm(lightpos);

alpha1=0.85;
alpha2=1;
alpha3=0.8;

    for k = 1:2:numel(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;



[gX,gY,gZ]=gradient(smoothme(img,1.5));

gnorm=sqrt(gX.^2+gY.^2+gZ.^2)+eps;
gX=gX./gnorm;
gY=gY./gnorm;
gZ=gZ./gnorm;

[Y,X,Z]=meshgrid(1:shape(1),1:shape(2),1:shape(3));

lx=X-lightpos(1);
ly=Y-lightpos(2);
lz=Z-lightpos(3);
lnorm=sqrt(lx.^2+ly.^2+lz.^2);
lx=lx./lnorm;
ly=ly./lnorm;
lz=lz./lnorm;


diffuse=max((lx.*gX)+(ly.*gY)+(lz.*gZ),0);

refx=(2*diffuse.*gX-lx);
refy=(2*diffuse.*gY-ly);
refz=(2*diffuse.*gZ-lz);

reflect=viewdir(1)*refx+viewdir(2)*refy+viewdir(3)*refz;

imgM=img>threshold;

img2D=zeros(shape(1:2));


for d=shape(3)-1:-1:1
    current=squeeze((1+alpha3*reflect(:,:,d)+alpha2*diffuse(:,:,d)).*img(:,:,d));
    indx=squeeze(imgM(:,:,d));
    indx=indx(:);
    img2D(indx)=img2D(indx)*alpha1+(1-alpha1)*current(indx);
end;

img2D=img2D-min(img2D(:));
img2D=img2D./max(img2D(:));

function img = smoothme(img,sigma)
imgsz=size(img);
  [X, Y, Z] = ndgrid(0:(imgsz(1)-1),0:(imgsz(2)-1),0:(imgsz(3)-1));
X = X - ceil(imgsz(1)/2);
Y = Y - ceil(imgsz(2)/2);
Z = Z - ceil(imgsz(3)/2);
R2 = (X.^2/(2*sigma^2) + Y.^2/(2*sigma^2) + Z.^2/(2*sigma^2));
    
    gaussin = (fftshift(exp(-R2)));
    gaussin=gaussin./sum(gaussin(:));

    img=real(ifftn(fftn(img).*fftn(gaussin)));    

