% Note that all these functions implement an optional
% scalar multiplication (which can be used to reduce memory copy operations). 
function demo_The_STA_operators 


%% Tensor operators: spherical derivatives

    % centered Gauss function with sigma=3
    ifield=stafieldStruct('gauss',[32,32,32],3,1);
    
    figure;
    subplot(1,2,1);stafield_figure(ifield,'c',1,'m',0);
    subplot(1,2,2);stafield_figure(ifield,'c',2,'m',0);
    
    % spherical up-derivatives of the Gaussian
    ofield=sta_deriv(ifield,1);
    
    subplot(3,2,1);stafield_figure(ofield,'c',1,'m',1);
    subplot(3,2,2);stafield_figure(ofield,'c',2,'m',1);
    
    % spherical up-derivatives of the Gaussian (2 times)
    ofield=sta_deriv(sta_deriv(ifield,1),1);
    
    subplot(3,2,3);stafield_figure(ofield,'c',1,'m',2);
    subplot(3,2,4);stafield_figure(ofield,'c',2,'m',2);
    
    % spherical double-up-derivatives of the Gaussian
    ofield=sta_deriv(ifield,2);
    
    subplot(3,2,5);stafield_figure(ofield,'c',1,'m',2);
    subplot(3,2,6);stafield_figure(ofield,'c',2,'m',2);
    
%% Tensor operators: Laplace opertator

    ifield=stafieldStruct('gauss',[32,32,32],3,1);
    
    % Computing the Laplacian of Gaussian
    ofield=sta_lap(ifield);
    figure;
    subplot(2,2,1);stafield_figure(ofield,'c',1,'m',0);
    subplot(2,2,2);stafield_figure(ofield,'c',2,'m',0);
    
    
    % 2x computing the Laplacian of the spherical derivatives of the Gaussian
    ofield=sta_lap(sta_lap(sta_deriv(ifield,2)));
    subplot(2,2,3);stafield_figure(ofield,'c',1,'m',1);
    subplot(2,2,4);stafield_figure(ofield,'c',2,'m',1);
    
%% Tensor operators: products

    ifield=stafieldStruct('gauss',[32,32,32],3,1);
    L1=1;
    ofield1=sta_deriv(ifield,L1);
    
    L2=2;
    ifield=stafieldStruct('gauss',[32,32,32],5,1);
    ofield2=sta_deriv(ifield,L2);
    
    figure;
    for Lnew=abs(L1-L2):L1+L2
        ofield3=sta_prod(ofield1,ofield2,Lnew);
        ofield3
        subplot(2,3,Lnew);stafield_figure(ofield3,'c',1,'m',Lnew);
        subplot(2,3,Lnew+3);stafield_figure(ofield3,'c',2,'m',Lnew);
    end;
    
%% Tensor operators: FFT
    

%% Tensor operators: scalar multiplication / conjunction


%% References
% 
%  H. Skibbe, M. Reisert
% Rotation Covariant Image Processing for Biomedical Applications
% In Computational and Mathematical Methods in Medicine, Special Issue on 
% Mathematical Methods in Biomedical Imaging, Hindawi Publishing Corporation, volume 2013, 2013. 
%
%  H. Skibbe
% Spherical Tensor Algebra for Biomedical Image Analysis
% PhD thesis, Albert-Ludwigs-Universität Freiburg, 2013.

