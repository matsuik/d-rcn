function sta_updatehelp()

[pathstr, name, ext]=fileparts([mfilename('fullpath'),'.m']);

demofiles=dir([pathstr,'/demos/demo*.m']);
opts.outputDir = [pathstr,'/html/'];
addpath([pathstr,'/demos/']);
 
 
helppage{1}='<!DOCTYPE HTML>\n';
helppage{2}='<html xmlns="http://www.w3.org/1999/xhtml" itemscope itemtype="http://www.mathworks.com/help/schema/MathWorksDocPage">\n<head>\n</head>\n<body>\n';
helppage{3}='<h1>The Spherical Tensor Toolkit</h1>';
helppage{4}='<h2>list of demos</h2>\n';
helppage{5}='<ul>\n';
linecount2=6;


 
helptoc{1}='<?xml version=''1.0'' encoding="utf-8"?>\n';
helptoc{2}='<toc version="2.0">\n';
helptoc{3}='<tocitem target="sta-toolbox.html">Spherical Tensor Algebra-Toolbox\n';
linecount=4;
% 
%         <tocitem target="sta_wavelet.html" image="HelpIcon.GETTING_STARTED">
%             Getting Started with the STA-Toolbox
%         </tocitem>

    

 cd demos
 for a=1:numel(demofiles)
    fnamehtml=demofiles(a).name(1:end-2);
    linkname=fnamehtml(6:end);
    linkname(linkname=='_')=' ';
    
    close all;
    publish(demofiles(a).name,opts);
    close all;
    helptoc{linecount}=['<tocitem target="',[fnamehtml,'.html'],'" image="HelpIcon.GETTING_STARTED">\n'];
    linecount=linecount+1;
    helptoc{linecount}=['',linkname,'\n'];
    linecount=linecount+1;
    helptoc{linecount}=['</tocitem>\n'];
    linecount=linecount+1;
    
    
    helppage{linecount2}=['<li><a href="',fnamehtml,'.html">',linkname,'</a></li>\n'];
    linecount2=linecount2+1;
 end;
cd ..

helptoc{linecount}='</tocitem>\n</toc>\n';


fid = fopen([ opts.outputDir,'/helptoc.xml'],'w');
for a=1:numel(helptoc)
    fprintf(fid,helptoc{a});
end
fclose(fid);




helppage{linecount2}='</ul></body></html>\n';


fid = fopen([ opts.outputDir,'/sta-toolbox.html'],'w');
for a=1:numel(helppage)
    fprintf(fid,helppage{a});
end
fclose(fid);

%builddocsearchdb([pathstr,'/html/']);
close all;
doc
msgbox('you might need to restart matlab and run sta_setPaths. Then after running doc you will find additional docs in the Supplemental Software tab (location:bottom-left)');