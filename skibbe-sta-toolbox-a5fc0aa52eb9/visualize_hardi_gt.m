ax = [37 101 27 83]
indiv = '07';
inex = 'ex';
sta_setPaths 
data=load(['../data/hardi_in_ex/20160407/15' indiv '_' inex 'vivo_hardi.mat']);
if indiv == '06'
    gamma = -0.3
elseif indiv == '07'
    gamma = -0.2
end

tdata=data.tensor_features;

% d is a number of a feature, the last is row data
% l is the order, [1, 2, 3] indexing [0, 2, 4]
slice = 14
d=43;l=2;shape=size(tdata{l});if (l==1),ofieldF=stafieldStruct(shape(3:5),0,'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');ofieldF.data(1,1,:,:,:)=tdata{l}(:,d,:,:,:); else ofieldF=stafieldStruct(reshape(tdata{l}(:,d,:,:,:),[2,4*(l-1)+1,shape(3:5)]),'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');end;
sta_glyph(ofieldF,'gamma',gamma,'mask',data.wm>0.5,'init_view',3,'init_slice',slice,'backgound',data.b0);axis(ax);
saveas(gcf, ['HARDIfigures/' indiv inex num2str(l) num2str(slice) '.png'])

slice = 20
d=43;l=2;shape=size(tdata{l});if (l==1),ofieldF=stafieldStruct(shape(3:5),0,'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');ofieldF.data(1,1,:,:,:)=tdata{l}(:,d,:,:,:); else ofieldF=stafieldStruct(reshape(tdata{l}(:,d,:,:,:),[2,4*(l-1)+1,shape(3:5)]),'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');end;
sta_glyph(ofieldF,'gamma',gamma,'mask',data.wm>0.5,'init_view',3,'init_slice',slice,'backgound',data.b0);axis(ax);
saveas(gcf, ['HARDIfigures/', indiv, inex, num2str(l), num2str(slice), '.png'])


slice = 14
d=37;l=3;shape=size(tdata{l});if (l==1),ofieldF=stafieldStruct(shape(3:5),0,'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');ofieldF.data(1,1,:,:,:)=tdata{l}(:,d,:,:,:); else ofieldF=stafieldStruct(reshape(tdata{l}(:,d,:,:,:),[2,4*(l-1)+1,shape(3:5)]),'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');end;
sta_glyph(ofieldF,'gamma',gamma,'mask',data.wm>0.5,'init_view',3,'init_slice', slice,'backgound',data.b0);axis(ax);
saveas(gcf, ['HARDIfigures/' indiv inex num2str(l) num2str(slice) '.png'])


slice = 20
d=37;l=3;shape=size(tdata{l});if (l==1),ofieldF=stafieldStruct(shape(3:5),0,'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');ofieldF.data(1,1,:,:,:)=tdata{l}(:,d,:,:,:); else ofieldF=stafieldStruct(reshape(tdata{l}(:,d,:,:,:),[2,4*(l-1)+1,shape(3:5)]),'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');end;
sta_glyph(ofieldF,'gamma',gamma,'mask',data.wm>0.5,'init_view',3,'init_slice', slice,'backgound',data.b0);axis(ax);
saveas(gcf, ['HARDIfigures/' indiv inex num2str(l) num2str(slice) '.png'])
