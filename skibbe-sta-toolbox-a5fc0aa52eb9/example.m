ax = [37 101 27 83]
sta_setPaths 
data=load('../data/hardi_in_ex/20160407/1506_exvivo_hardi.mat');
data

tdata=data.tensor_features;
size(tdata{2})

% d is a number of a feature, the last is row data
% l is the order, [1, 2, 3] indexing [0, 2, 4]
d=43;l=2;shape=size(tdata{l});if (l==1),ofieldF=stafieldStruct(shape(3:5),0,'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');ofieldF.data(1,1,:,:,:)=tdata{l}(:,d,:,:,:); else ofieldF=stafieldStruct(reshape(tdata{l}(:,d,:,:,:),[2,4*(l-1)+1,shape(3:5)]),'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');end;
sta_glyph(ofieldF,'gamma',-0.3,'mask',data.wm>0.5,'init_view',3,'init_slice',20,'backgound',data.b0);axis(ax);

% see the place I saw the last time
ax = axis
% not expanded data
sta_glyph(data.ofield,'gamma',-0.4,'mask',max(data.wm,data.gm)>0.5,'init_view',3,'init_slice',14,'backgound',data.b0);

load('../results/hardi_in_ex/Jun7/a0_all_train_1506.mat')
l=1;shape=size(tdata{l});
slice = 14
ofieldF=stafieldStruct(reshape(pred,[2,4*(l-1)+1,shape(3:5)]),'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');
sta_glyph(ofieldF,'gamma',-0.3,'mask',data.wm>0.5,'init_view',3,'init_slice',slice,'backgound',data.b0);

saveas(gcf, ['../results/hardi_in_ex/Jun3/06train' num2str(l) num2str(slice) '.png'])
