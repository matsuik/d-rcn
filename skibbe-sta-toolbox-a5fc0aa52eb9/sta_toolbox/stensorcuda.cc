#include "stensorcuda.h"



namespace hanalysis_cuda
{
  
   

    __global__ void Lap_cuda(
         float * stIn,
         float * stOut ,
         std::size_t numvoxel,
	 std::size_t shapeZ,
	 std::size_t shapeY,
	 std::size_t shapeX,	 
	 std::size_t jumpZ,  // extx*exty
	 std::size_t jumpY,  // extx
         int strides,
         cuFloatComplex norm)
      {
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	
	if (idx<numvoxel)
	{
	  	  strides*=2;	// complex numbers
	  	  
		  int pZ=idx/jumpZ;
		  int pY=(idx%jumpZ)/jumpY;
		  int pX=(idx%jumpZ)%jumpY;
		  
		
		  std::size_t Z1=(((pZ+1)%shapeZ)*jumpZ);
		  std::size_t Y1=(((pY+1)%shapeY)*jumpY);
		  std::size_t X1=(((pX+1)%shapeX));
		  
		  std::size_t Z2=(((pZ+2)%shapeZ)*jumpZ);
		  std::size_t Y2=(((pY+2)%shapeY)*jumpY);
		  std::size_t X2=(((pX+2)%shapeX));
		  
		  
		  std::size_t Z0=(((pZ)%shapeZ)*jumpZ);
		  std::size_t Y0=(((pY)%shapeY)*jumpY);
		  std::size_t X0=(((pX)%shapeX));
		  
		  
		  float * center=(stIn+(Z0+Y0+X0)*strides);
		  
		  float * derivX1=(stIn+(Z1+Y1+X0)*strides);
		  float * derivX0=(stIn+(Z1+Y1+X2)*strides);

		  float * derivY1=(stIn+(Z1+Y0+X1)*strides);
		  float * derivY0=(stIn+(Z1+Y2+X1)*strides);
 
		  float * derivZ1=(stIn+(Z0+Y1+X1)*strides);
		  float * derivZ0=(stIn+(Z2+Y1+X1)*strides);		  

		  float * result=(stOut+(Z1+Y1+X1)*strides);		  
		  

		  float real=derivX1[0] +
			    derivX0[0] +
			    derivY1[0] -
			    6*center[0] +
			    derivY0[0] +		   
			    derivZ1[0] +
			    derivZ0[0]; 

		  float imag=derivX1[1] +
			    derivX0[1] +
			    derivY1[1] -
			    6*center[1] +
			    derivY0[1] +		   
			    derivZ1[1] +
			    derivZ0[1]; 			    
			    
		  result[0]+=norm.x*real-norm.y*imag;
		  result[1]+=norm.x*imag-norm.y*real;
	}
   }	 
   
   
   
   void Laplace_cuda(			const  cuFloatComplex * d_IN,
					    cuFloatComplex  * d_OUT, 
					    std::complex<float> alpha,
					    std::size_t shape[], 
					    int strides)
      {
	cudaError_t error;
	
	std::size_t N=shape[0]*shape[1]*shape[2];
	//int block_size = deviceProp.maxThreadsPerBlock;
 	int block_size = 64;
	//int block_size = 128;
	
	int n_blocks = N/block_size + (N%block_size == 0 ? 0:1);
	

	error = cudaGetLastError();
	if (error != cudaSuccess) printf("before entering the kernel %s\n", cudaGetErrorString( error ) );
  
	
	cuFloatComplex norm;
	norm.x=alpha.real();
	norm.y=alpha.imag();
	
	 Lap_cuda<<< n_blocks, block_size >>>(
	    (float *)d_IN,
	    (float *)d_OUT,
	    N,
		    shape[0],
		    shape[1],
		    shape[2],
		    (shape[2]*shape[1]),  // extx*exty
		    (shape[2]),  // extx
		    strides,
		    norm);
		

		error = cudaGetLastError();
		if (error != cudaSuccess) printf("after executing the kernel %s\n", cudaGetErrorString( error ) );	 
	      }
	  
	  
	  
	  
	  
	    __global__ void SD_cuda(
		float * stIn,
		float * stOut ,
		float * CGcoeff,
		std::size_t numvoxel,
		std::size_t shapeZ,
		std::size_t shapeY,
		std::size_t shapeX,	 
		std::size_t jumpZ,  // extx*exty
		std::size_t jumpY,  // extx
		int J,
		int Jupdown,
		float sign)
	      {
		int idx = blockIdx.x * blockDim.x + threadIdx.x;
		
		if (idx<numvoxel)
		{
		  
	// 	  	  stOut[idx*2*(J+Jupdown+1)]=1234;
			  
			  int J1=(J+Jupdown);

			  std::size_t vectorLengthJ=2*(J+1);
			  std::size_t vectorLengthJ1=2*(J1+1);
			  
			  
			  int pZ=idx/jumpZ;
			  int pY=(idx%jumpZ)/jumpY;
			  int pX=(idx%jumpZ)%jumpY;
			  
			
			  std::size_t Z1=(((pZ+1)%shapeZ)*jumpZ);
			  std::size_t Y1=(((pY+1)%shapeY)*jumpY);
			  std::size_t X1=(((pX+1)%shapeX));
			  
			  std::size_t Z2=(((pZ+2)%shapeZ)*jumpZ);
			  std::size_t Y2=(((pY+2)%shapeY)*jumpY);
			  std::size_t X2=(((pX+2)%shapeX));
			  
			  
			  std::size_t Z0=(((pZ)%shapeZ)*jumpZ);
			  std::size_t Y0=(((pY)%shapeY)*jumpY);
			  std::size_t X0=(((pX)%shapeX));
	// 		  
			  
	// 		  float * derivX1=&stIn[(Z1+Y1+X0)*vectorLengthJ]+2*J;
	// 		  float * derivX0=&stIn[(Z1+Y1+X2)*vectorLengthJ]+2*J;
	// 
	// 		  float * derivY1=&stIn[(Z1+Y0+X1)*vectorLengthJ]+2*J;
	// 		  float * derivY0=&stIn[(Z1+Y2+X1)*vectorLengthJ]+2*J;
	//  
	// 		  float * derivZ1=&stIn[(Z0+Y1+X1)*vectorLengthJ]+2*J;
	// 		  float * derivZ0=&stIn[(Z2+Y1+X1)*vectorLengthJ]+2*J;
			  
			  float * derivX1=stIn+(Z1+Y1+X0)*vectorLengthJ+2*J;
			  float * derivX0=stIn+(Z1+Y1+X2)*vectorLengthJ+2*J;

			  float * derivY1=stIn+(Z1+Y0+X1)*vectorLengthJ+2*J;
			  float * derivY0=stIn+(Z1+Y2+X1)*vectorLengthJ+2*J;
	
			  float * derivZ1=stIn+(Z0+Y1+X1)*vectorLengthJ+2*J;
			  float * derivZ0=stIn+(Z2+Y1+X1)*vectorLengthJ+2*J;		  

			  std::size_t offset=(Z1+Y1+X1)*vectorLengthJ1+2*J1;
	// 		  

			  std::size_t count=0;
			  for (int M=-(J1);M<=(0);M++)
			  {
				float & current_r=stOut[offset+2*M];
				float & current_i=stOut[offset+2*M+1];
				
				current_r=float(0);
				current_i=float(0);
				
				if (abs(M+1)<=J)  // m1=-1    m2=M+1    M
				{
				  int m2=M+1;
				  if (M==0)
				  {
				    current_r-=CGcoeff[count]*((derivX0[-2*m2]-derivX1[-2*m2])+sign*(derivY0[-2*m2+1]-derivY1[-2*m2+1]));
				    current_i-=CGcoeff[count]*(-(derivX0[-2*m2+1]-derivX1[-2*m2+1])+sign*(derivY0[-2*m2]-derivY1[-2*m2]));
				    count+=2;
				  }else 
				  {
				    current_r+=CGcoeff[count]*((derivX0[2*m2]-derivX1[2*m2])-sign*(derivY0[2*m2+1]-derivY1[2*m2+1]));
				    current_i+=CGcoeff[count]*((derivX0[2*m2+1]-derivX1[2*m2+1])+sign*(derivY0[2*m2]-derivY1[2*m2]));
				    count+=2;
				  }
				}
				if (M>=-J)  // m1=0     m2=M        M
				{
				  current_r+=CGcoeff[count]*(derivZ0[2*M]-derivZ1[2*M]);
				  current_i+=CGcoeff[count]*(derivZ0[2*M+1]-derivZ1[2*M+1]);
				  count+=2;
				}
				if (M-1>=-J)  // m1=1     m2=M-1    M
				{
				  int m2=M-1;
				  current_r+=CGcoeff[count]*(-(derivX0[2*m2]-derivX1[2*m2])-sign*(derivY0[2*m2+1]-derivY1[2*m2+1]));
				  current_i+=CGcoeff[count]*(-(derivX0[2*m2+1]-derivX1[2*m2+1])+sign*(derivY0[2*m2]-derivY1[2*m2]));
				  count+=2;
				  
				}
				

			  }
	//        
		}
	      }










	    template<typename T>
	      std::complex<T> * CG_SD_cuda( int J, 
					    int Jupdown, 
					    std::complex<T> fact,
					    std::size_t & numcoeff)
	      {
		  std::complex<T>  norm=T(0.5);
		    norm*=fact;
		    
		    int J1=(J+Jupdown);
		    std::size_t count=0;
		    
			  for (int M=-(J1);M<=(0);M++)
			  {
				if (abs(M+1)<=J)  // m1=-1    m2=M+1    M
				{
				  count++;
				}
				if (M>=-J)  // m1=0     m2=M        M
				{
				  count++;
				}
				if (M-1>=-J)  // m1=1     m2=M-1    M
				{
				  count++;
				}
			  }
			  
		    double shnorm=hanalysis::clebschGordan(1,0,J,0,J1,0);
		    if (Jupdown==0) shnorm=1;

		    
		    numcoeff=count;
		    std::complex<T> * cg= new std::complex<T>[count];
		    count=0;
		    
			  for (int M=-(J1);M<=(0);M++)
			  {
				if (abs(M+1)<=J)  // m1=-1    m2=M+1    M
				{
				  cg[count++]=norm*(T)((1.0/std::sqrt(2.0))*hanalysis::clebschGordan(1,-1,J,M+1,J1,M)/shnorm);
				}
				if (M>=-J)  // m1=0     m2=M        M
				{
				  cg[count++]=norm*(T)(hanalysis::clebschGordan(1,0,J,M,J1,M)/shnorm);
				}
				if (M-1>=-J)  // m1=1     m2=M-1    M
				{
				  cg[count++]=norm*(T)((1.0/std::sqrt(2.0))*hanalysis::clebschGordan(1,1,J,M-1,J1,M)/shnorm);
				}
			  }
		    return cg;
	      }
	      
	      /* template<typename T>
	      std::complex<T> * CG_SD_cuda( int J, 
					    int Jupdown, 
					    std::complex<T> fact,
					    std::size_t & numcoeff)
	      {
		  std::complex<T>  norm=T(0.5);
		    norm*=fact;
		    
		    int J1=(J+Jupdown);
		    std::size_t count=0;
		    
			  for (int M=-(J1);M<=(0);M++)
			  {
				if (abs(M+1)<=J)  // m1=-1    m2=M+1    M
				{
				  count++;
				}
				if (M>=-J)  // m1=0     m2=M        M
				{
				  count++;
				}
				if (M-1>=-J)  // m1=1     m2=M-1    M
				{
				  count++;
				}
			  }
			  
			std::size_t vectorLengthJ=2*(J+1);
			std::size_t vectorLengthJ1=2*(J1+1);
			T  * CGTable=new T[3*vectorLengthJ1];
			T shnorm=hanalysis::clebschGordan(1,0,J,0,J1,0);
			if (Jupdown==0) shnorm=1;
			for (int M=-(J1);M<=(0);M++)
			{
			    CGTable[M+(J1)]                 =T(1.0/std::sqrt(2.0))*hanalysis::clebschGordan(1,-1,J,M+1,J1,M)/shnorm;;
			    CGTable[M+(J1)+vectorLengthJ1]  =hanalysis::clebschGordan(1,0,J,M,J1,M)/shnorm;
			    CGTable[M+(J1)+2*vectorLengthJ1]=T(1.0/std::sqrt(2.0))*hanalysis::clebschGordan(1,1,J,M-1,J1,M)/shnorm;
			}
			T * CGTable0=&CGTable[0];
			CGTable0+=(J1);
			T * CGTable1=&CGTable[vectorLengthJ1];
			CGTable1+=(J1);
			T * CGTable2=&CGTable[2*vectorLengthJ1];
			CGTable2+=(J1);
			  
		    
		    numcoeff=count;
		    std::complex<T> * cg= new std::complex<T>[count];
		    count=0;
		    
		    for (int M=-(J1);M<=(0);M++)
			  {
				if (abs(M+1)<=J)  // m1=-1    m2=M+1    M
				{
				  cg[count++]=norm*CGTable0[M];
				}
				if (M>=-J)  // m1=0     m2=M        M
				{
				  cg[count++]=norm*CGTable1[M];
				}
				if (M-1>=-J)  // m1=1     m2=M-1    M
				{
				  cg[count++]=norm*CGTable2[M];
				}
			  }
		    
		    delete [] CGTable;
		    
		    return cg;
	      }
	      
	      
	      
	      */
	      
	      
	      void sphericalDerivatives_cuda(
						    const  cuFloatComplex * d_IN,
						    cuFloatComplex  * d_OUT, 
						    std::complex<float> alpha,
						    std::size_t shape[], 
						    int L, 
						    int J, 
						    int sign)
	      {
		cudaError_t error;
		
		std::size_t N=shape[0]*shape[1]*shape[2];
		//int block_size = deviceProp.maxThreadsPerBlock;
		int block_size = 64;
		//int block_size = 128;
		
		int n_blocks = N/block_size + (N%block_size == 0 ? 0:1);
		
		std::size_t numcoeff=0;
		std::complex<float> * CGTable=CG_SD_cuda(L,J,  alpha, numcoeff);
	// 	for (int a=0;a<numcoeff;a++)
	// 	{
	// 	 printf("%f %f\n",CGTable[a].real(),CGTable[a].imag());
	// 	}
		
		//printf("num of blocks: %d , num threads %d \n",n_blocks,block_size);
		
		float *d_CG=NULL;
		error=cudaMalloc((void **) &d_CG, 2*numcoeff*sizeof(float)); 
		if (error!=cudaSuccess)  {printf("%d\n",numcoeff);throw  hanalysis::STAError("error allocating device memory (CG coefficients)\n");}
		error=cudaMemcpy(d_CG, (float*)CGTable, 2*numcoeff*sizeof(float), cudaMemcpyHostToDevice);
		if (error!=cudaSuccess) {if (d_CG!=NULL) cudaFree(d_CG); delete [] CGTable; throw  hanalysis::STAError("error copying host memory to device memory\n"); }
	  
	  

		error = cudaGetLastError();
		if (error != cudaSuccess) printf("before entering the kernel %s\n", cudaGetErrorString( error ) );
	  
		
		SD_cuda<<< n_blocks, block_size >>>(
		    (float *)d_IN,
		    (float *)d_OUT,
		    d_CG,
		    N,
		    shape[0],
		    shape[1],
		    shape[2],
		    (shape[2]*shape[1]),  // extx*exty
		    (shape[2]),  // extx
		    L,
		    J,
		    sign);
		

		error = cudaGetLastError();
		if (error != cudaSuccess) printf("after executing the kernel %s\n", cudaGetErrorString( error ) );	 
		
		delete [] CGTable;
		cudaFree(d_CG);
		
	      }
	      
	      
	      
	      
	      
	     
	      
	      __global__ void Prod_cuda_alpha_real(
		const float * stIn1,
		const float * stIn2,
		float * stOut ,
		const float * CGcoeff,
		std::size_t numvoxel,
		int J1,
		int J2,
		int J)
	      {
		int idx = blockIdx.x * blockDim.x + threadIdx.x;
		
		if (idx<numvoxel)
		{
		  int vectorLengthJ1=2*(J1+1);
		  int vectorLengthJ2=2*(J2+1);
		  int vectorLengthJ=2*(J+1);
		  
		  const float  * current_J1=stIn1+idx*vectorLengthJ1+2*J1;
		  const float  * current_J2=stIn2+idx*vectorLengthJ2+2*J2;
		  float * current_J=stOut+idx*vectorLengthJ+2*J;
		  

		      std::size_t count=0;
		      float tmp0_r;
		      float tmp0_i;
		      float tmp1_r;
		      float tmp1_i;
		      
		      for (int m=-J;m<=0;m++)
		      {
			
			for (int m1=-J1;m1<=J1;m1++)
			{
			    int m2=m-m1;
			    if (abs(m2)<=J2)
			    {
			      if (m1>0)
			      {
				if (m1%2==0)
				{
				  tmp0_r=current_J1[-2*m1];
				  tmp0_i=-current_J1[-2*m1+1];
				}
				else  
				{
				tmp0_r=-current_J1[-2*m1];
				tmp0_i=current_J1[-2*m1+1];
				}
			      }else 
			      {
				tmp0_r=current_J1[2*m1];
				tmp0_i=current_J1[2*m1+1];			
			      }
			      
			      if (m2>0)
			      {
				if (m2%2==0) 
				{
				  tmp1_r=current_J2[-2*m2];
				  tmp1_i=-current_J2[-2*m2+1];			  
				}
				else 
				{
				  tmp1_r=-current_J2[-2*m2];
				  tmp1_i=current_J2[-2*m2+1];			  
				}
			      }else 
			      {
				tmp1_r=current_J2[2*m2]; 
				tmp1_i=current_J2[2*m2+1]; 			
			      }
			      
			      current_J[m*2]+=CGcoeff[count]*(tmp0_r*tmp1_r-tmp0_i*tmp1_i);
			      current_J[m*2+1]+=CGcoeff[count++]*(tmp0_r*tmp1_i+tmp0_i*tmp1_r);
			    }
			}
		      }
		}
	      }
	      
	      
	      template<typename T>
	      T * CG_Prod_cuda( int J1,
				int J2,
				int J,
				bool normalized,
				T fact, std::size_t & numcoeff)
	      {
		    T norm=(T)1;
		    if (normalized)
		    {
			norm=(T)1/(T)hanalysis::clebschGordan(J1,0,J2,0,J,0);
		    }
		    norm*=fact;
		    std::size_t count=0;
		    for (int m=-J;m<=0;m++)
		    {
		      for (int m1=-J1;m1<=J1;m1++)
		      {
			  int m2=m-m1;
			  if (abs(m2)<=J2)
			    {
			      count++;
			  }
		      }
		    }
		    
		    numcoeff=count;
		    T * cg= new T[count];
		    count=0;
		    for (int m=-J;m<=0;m++)
		    {
		      for (int m1=-J1;m1<=J1;m1++)
		      {
			  int m2=m-m1;
			  if (abs(m2)<=J2)
			  {
			    cg[count++]=norm*(T)hanalysis::clebschGordan(J1,m1,J2,m2,J,m);
			  }
		      }
		    }
		    return cg;
	      }

	      
	     
	  
	  
	
	
//       template<typename T>	      
//       void sta_product_R (
// 	  const float * stIn1,
// 	  const float * stIn2,
// 	  float * stOut ,
// 	  const std::size_t shape[],
// 	  int J1,
// 	  int J2,
// 	  int J,
// 	  T alpha,
// 	  bool normalize,
// 	  int stride_in1 = -1,
// 	  int stride_in2 = -1,
// 	  int stride_out = -1,
// 	  bool clear_field=false)
	  
	  
	  
	    void prod_cuda_real_alpha(
				    const  cuFloatComplex * d_IN1,
				    const  cuFloatComplex * d_IN2,
				    cuFloatComplex  * d_OUT,
				    float alpha,
				    std::size_t shape[],
				    int L1,
				    int L2,
				    int L,
				    bool normalize)
	      {
		cudaError_t error;
		
		std::size_t N=shape[0]*shape[1]*shape[2];
		//int block_size = deviceProp.maxThreadsPerBlock;
		int block_size = 64;
		//int block_size = 128;
		//int block_size = 256;
	// 	int block_size = 48;
		
		int n_blocks = N/block_size + (N%block_size == 0 ? 0:1);
		
		std::size_t numcoeff=0;
		float * CGTable=CG_Prod_cuda(L1,L2, L, normalize, alpha, numcoeff);

		
		float *d_CG=NULL;
		error=cudaMalloc((void **) &d_CG, numcoeff*sizeof(float)); 
		if (error!=cudaSuccess)  {printf("%d\n",numcoeff);delete[] CGTable;throw  hanalysis::STAError("error allocating device memory\n");}
		error=cudaMemcpy(d_CG, (float*)CGTable, numcoeff*sizeof(float), cudaMemcpyHostToDevice);
		if (error!=cudaSuccess) {if (d_CG!=NULL) cudaFree(d_CG); delete [] CGTable; throw  hanalysis::STAError("error copying host memory to device memory\n"); }
	  
	  
		error = cudaGetLastError();
		if (error != cudaSuccess) printf("before entering the kernel %s\n", cudaGetErrorString( error ) );
	  
	  
		Prod_cuda_alpha_real<<< n_blocks, block_size >>>(
		    (float *)d_IN1,
		    (float *)d_IN2,
		    (float *)d_OUT,
		    d_CG,
		    N,
		    L1,
		    L2,
		    L);
		
		error = cudaGetLastError();
		if (error != cudaSuccess) printf("after executing the kernel %s\n", cudaGetErrorString( error ) );	 
		
		
		delete [] CGTable;
		cudaFree(d_CG);
	      }
	      
	      
	      
	      
	      
		
	int fft_cuda(cuFloatComplex* IN,cuFloatComplex * OUT, int shape[],int numComponents, bool forward)	 
	{	 


		int rank=3;
		int * n=shape;
		int howmany=numComponents;
		cuComplex * in =(cuComplex *)IN;
		int * inembed=n;
		
		
		int istride=numComponents;
		int idist=1;
		cuComplex * out =(cuComplex *)OUT;    
		int * onembed=inembed;    
		int odist=idist;
		int ostride=istride;

		
		cufftType fft_type=CUFFT_C2C;
		

		cufftResult error;
		cufftHandle plan;
		error=cufftPlanMany(&plan,rank,n,inembed,istride, idist,onembed, ostride, odist, fft_type,howmany);
		if (error!=CUFFT_SUCCESS)
		{
		  switch (error)
		  {
		    case CUFFT_ALLOC_FAILED:
		      throw  hanalysis::STAError("Allocation of GPU resources for the plan failed\n");
		    case CUFFT_INVALID_TYPE:
		      throw  hanalysis::STAError("The type parameter is not supported\n");
		    case CUFFT_INVALID_VALUE:
		      throw  hanalysis::STAError("Invalid parameter(s) passed to the API\n");
		    case CUFFT_INTERNAL_ERROR:
		      throw  hanalysis::STAError("Internal driver error is detected\n");
		    case CUFFT_SETUP_FAILED:
		      throw  hanalysis::STAError("CUFFT library failed to initialize\n");
		    case CUFFT_INVALID_SIZE:
		      throw  hanalysis::STAError("The nx parameter is not a supported size\n");
		    default:
		      throw  hanalysis::STAError("unknown error\n");
		  }
		  
		}

		int sign=CUFFT_FORWARD; if (!forward) sign=CUFFT_INVERSE;
		cufftExecC2C(plan,in,out,sign);

		cufftDestroy(plan);
		return 0;
	}	







  
  
}




