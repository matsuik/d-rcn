#include "stensorcuda.h"


//static const int block_size_LAPLACE=255;
//static const int block_size_CPY=255;
//static const int block_size_DERV=128;
//static const int block_size_PROD=256;
//static const int block_size_PRODft=256;
//static const int block_size_MULT=256;

static const int block_size_LAPLACE=64;
static const int block_size_CPY=64;
static const int block_size_DERV=64;
static const int block_size_PROD=64;
static const int block_size_PRODft=64;
static const int block_size_MULT=64;


// static const int block_size_LAPLACE=255;
// static const int block_size_CPY=255;
// static const int block_size_DERV=256;
// static const int block_size_PROD=256;
// static const int block_size_PRODft=256;
// static const int block_size_MULT=256;


namespace hanalysis_cuda
{


template<typename T>
__global__ void lap_cuda_kernel(
    const T * stIn,
    T * stOut ,
    std::size_t numvoxel,
    std::size_t shapeZ,
    std::size_t shapeY,
    std::size_t shapeX,
    std::size_t jumpZ,  // extx*exty
    std::size_t jumpY,  // extx
    int strides_in,
    int strides_out,
    int numComponents,
    T norm,
    bool clear_field)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx<numvoxel)
    {
        //strides*=2;	// complex numbers

        int pZ=idx/jumpZ;
        int pY=(idx%jumpZ)/jumpY;
        int pX=(idx%jumpZ)%jumpY;


        std::size_t Z1=(((pZ+1)%shapeZ)*jumpZ);
        std::size_t Y1=(((pY+1)%shapeY)*jumpY);
        std::size_t X1=(((pX+1)%shapeX));

        std::size_t Z2=(((pZ+2)%shapeZ)*jumpZ);
        std::size_t Y2=(((pY+2)%shapeY)*jumpY);
        std::size_t X2=(((pX+2)%shapeX));


        std::size_t Z0=(((pZ)%shapeZ)*jumpZ);
        std::size_t Y0=(((pY)%shapeY)*jumpY);
        std::size_t X0=(((pX)%shapeX));


        const T * center=(stIn+(Z1+Y1+X1)*strides_in);

        const T * derivX1=(stIn+(Z1+Y1+X0)*strides_in);
        const T * derivX0=(stIn+(Z1+Y1+X2)*strides_in);

        const T * derivY1=(stIn+(Z1+Y0+X1)*strides_in);
        const T * derivY0=(stIn+(Z1+Y2+X1)*strides_in);

        const T * derivZ1=(stIn+(Z0+Y1+X1)*strides_in);
        const T * derivZ0=(stIn+(Z2+Y1+X1)*strides_in);

        T * result=(stOut+(Z1+Y1+X1)*strides_out);

        for (int a=0;a<numComponents;a++)
        {
            if ( clear_field )
                *result=0;
            (*result++)+=norm*(*derivX1++ +
                               *derivX0++ +
                               *derivY1++ -
                               6*(*center++) +
                               *derivY0++ +
                               *derivZ1++ +
                               *derivZ0++);

        }
    }
}


template<typename T>
void sta_laplace (
    const T * stIn,
    T * stOut ,
    const std::size_t shape[],
    int components,
    T alpha,
    int stride_in,
    int stride_out,
    bool clear_field)
{
    if ( components<= 0)
        throw hanalysis::STAError("expecing at least 1 component");

    std::size_t N=shape[1]*shape[2]*shape[0];
    //int block_size = 64;
    int block_size = block_size_LAPLACE;
    int n_blocks = N/block_size + (N%block_size == 0 ? 0:1);

    cudaError_t error;

    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(std::string(cudaGetErrorString( error )));

    lap_cuda_kernel<T> <<< n_blocks, block_size >>>(
        stIn,
        stOut ,
        N,
        shape[0],
        shape[1],
        shape[2],
        shape[1]*shape[2],  // extx*exty
        shape[2],  // extx
        2*stride_in,
        2*stride_out,
        2*components,
        alpha,
        clear_field);

    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(std::string(cudaGetErrorString( error )));
}







template
void sta_laplace (
    const float * stIn,
    float * stOut ,
    const std::size_t shape[],
    int components,
    float alpha,
    int stride_in,
    int stride_out,
    bool clear_field);


template
void sta_laplace (
    const double * stIn,
    double * stOut ,
    const std::size_t shape[],
    int components,
    double alpha,
    int stride_in,
    int stride_out,
    bool clear_field);




















template<typename T>
__global__ void copySubfield2Subfield_kernel(
    const T * data_src,
    T * data_dest,
    std::size_t numVoxel,
    int stride_in,
    int stride_out,
    int numComponents)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx<numVoxel)
    {
        data_src+=stride_in*idx;
        data_dest+=stride_out*idx;
        for (int c=0;c<numComponents;c++)
        {
            *data_dest++=*data_src++;
        }
    }

}


template<typename T>
void copySubfield2Subfield(	const T * data_src,
                            T * data_dest,
                            std::size_t numVoxel,
                            int stride_in,
                            int stride_out,
                            int numComponents)
{
    std::size_t N=numVoxel;
    //int block_size = 64;
    int block_size = block_size_CPY;
    int n_blocks = N/block_size + (N%block_size == 0 ? 0:1);

    cudaError_t error;

    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(std::string(cudaGetErrorString( error )));

    copySubfield2Subfield_kernel<T> <<< n_blocks, block_size >>>(
        data_src,
        data_dest,
        numVoxel,
        stride_in,
        stride_out,
        numComponents);

    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(std::string(cudaGetErrorString( error )));
}


template
void copySubfield2Subfield(	const float * data_src,
                            float * data_dest,
                            std::size_t numVoxel,
                            int stride_in,
                            int stride_out,
                            int numComponents);

template
void copySubfield2Subfield(	const double * data_src,
                            double * data_dest,
                            std::size_t numVoxel,
                            int stride_in,
                            int stride_out,
                            int numComponents);






template<typename T>
__global__ void sta_derivatives_R_kernel(
    const T * stIn,
    T * stOut ,
    T * CGcoeff,
    std::size_t numvoxel,
    std::size_t shapeZ,
    std::size_t shapeY,
    std::size_t shapeX,
    std::size_t jumpZ,  // extx*exty
    std::size_t jumpY,  // extx
    int J,
    int J1,
    T sign,
    int stride_in,
    int stride_out,
    bool clear_field)
{
      int idx = blockIdx.x * blockDim.x + threadIdx.x;
      if (idx<numvoxel)
      {

	  int pZ=idx/jumpZ;
	  int pY=(idx%jumpZ)/jumpY;
	  int pX=(idx%jumpZ)%jumpY;
	  
	  std::size_t Z1=(((pZ+1)%shapeZ)*jumpZ);
	  std::size_t Y1=(((pY+1)%shapeY)*jumpY);
	  std::size_t X1=(((pX+1)%shapeX));
	  //
	  const T * derivX1=stIn+(Z1+Y1+(((pX)%shapeX)))*stride_in+2*J;
	  const T * derivX0=stIn+(Z1+Y1+(((pX+2)%shapeX)))*stride_in+2*J;

	  const T * derivY1=stIn+(Z1+(((pY)%shapeY)*jumpY)+X1)*stride_in+2*J;
	  const T * derivY0=stIn+(Z1+(((pY+2)%shapeY)*jumpY)+X1)*stride_in+2*J;

	  const T * derivZ1=stIn+((((pZ)%shapeZ)*jumpZ)+Y1+X1)*stride_in+2*J;
	  const T * derivZ0=stIn+((((pZ+2)%shapeZ)*jumpZ)+Y1+X1)*stride_in+2*J;	
	  


	  T * current_r=stOut+(Z1+Y1+X1)*stride_out;

	  for (int M=-(J1);M<=(0);M++)
	  {
	      if (clear_field)
	      {
		  *current_r=T(0);
		  current_r[1]=T(0);
	      }

	      if (abs(M+1)<=J)  // m1=-1    m2=M+1    M
	      {
		  int m2=2*(M+1);
		  if (M==0)
		  {
		      *current_r-=(*CGcoeff)*((derivX0[-m2]-derivX1[-m2])+sign*(derivY0[-m2+1]-derivY1[-m2+1]));
		      current_r[1]-=(*CGcoeff++)*(-(derivX0[-m2+1]-derivX1[-m2+1])+sign*(derivY0[-m2]-derivY1[-m2]));
		  } else
		  {
		      *current_r+=(*CGcoeff)*((derivX0[m2]-derivX1[m2])-sign*(derivY0[m2+1]-derivY1[m2+1]));
		      current_r[1]+=(*CGcoeff++)*((derivX0[m2+1]-derivX1[m2+1])+sign*(derivY0[m2]-derivY1[m2]));
		  }
	      }
	      if (M>=-J)  // m1=0     m2=M        M
	      {
		  *current_r+=(*CGcoeff)*(derivZ0[2*M]-derivZ1[2*M]);
		  current_r[1]+=(*CGcoeff++)*(derivZ0[2*M+1]-derivZ1[2*M+1]);
	      }
	      if (M-1>=-J)  // m1=1     m2=M-1    M
	      {
		  int m2=2*(M-1);
		  *current_r+=(*CGcoeff)*(-(derivX0[m2]-derivX1[m2])-sign*(derivY0[m2+1]-derivY1[m2+1]));
		  current_r[1]+=(*CGcoeff++)*(-(derivX0[m2+1]-derivX1[m2+1])+sign*(derivY0[m2]-derivY1[m2]));
	      }
	      current_r+=2;
	  }
      }
}



template<typename T>
T * CG_SD_cuda( 		    int J,
                      int Jupdown,
                      T fact,
                      std::size_t & numcoeff)
{
    T norm=T(0.5);
    norm*=fact;

    int J1=(J+Jupdown);
    std::size_t count=0;

    for (int M=-(J1);M<=(0);M++)
    {
        if (abs(M+1)<=J)  // m1=-1    m2=M+1    M
        {
            count++;
        }
        if (M>=-J)  // m1=0     m2=M        M
        {
            count++;
        }
        if (M-1>=-J)  // m1=1     m2=M-1    M
        {
            count++;
        }
    }

    double shnorm=hanalysis::clebschGordan(1,0,J,0,J1,0);
    if (Jupdown==0) shnorm=1;


    numcoeff=count;
    T * cg= new T[count];
    count=0;

    for (int M=-(J1);M<=(0);M++)
    {
        if (abs(M+1)<=J)  // m1=-1    m2=M+1    M
        {
            cg[count++]=norm*(T)((1.0/std::sqrt(2.0))*hanalysis::clebschGordan(1,-1,J,M+1,J1,M)/shnorm);
        }
        if (M>=-J)  // m1=0     m2=M        M
        {
            cg[count++]=norm*(T)(hanalysis::clebschGordan(1,0,J,M,J1,M)/shnorm);
        }
        if (M-1>=-J)  // m1=1     m2=M-1    M
        {
            cg[count++]=norm*(T)((1.0/std::sqrt(2.0))*hanalysis::clebschGordan(1,1,J,M-1,J1,M)/shnorm);
        }
    }
    return cg;
}








template<typename T>
void sta_derivatives_R(
    const T * stIn,
    T * stOut ,
    const std::size_t shape[],
    int J,
    int Jupdown,    // either -1 0 or 1
    bool conjugate,
    T alpha,
    const T  v_size[],
    int stride_in,
    int stride_out,
    bool clear_field)
{
    cudaError_t error;

    std::size_t N=shape[0]*shape[1]*shape[2];
    int block_size = block_size_DERV;
    int n_blocks = N/block_size + (N%block_size == 0 ? 0:1);
    
//     std::size_t N=(shape[0]*shape[1]*shape[2]);
//     std::size_t N_part=N/10 + (N%10 == 0 ? 0:1);
//     int block_size = block_size_DERV;
//     int n_blocks = N_part/block_size + (N_part%block_size == 0 ? 0:1);
    
    
    //printf("%d %d\n",shape[0]*shape[1]*shape[2],block_size*n_blocks*10);

    std::size_t numcoeff=0;
    T * CGTable=CG_SD_cuda(J,Jupdown,  alpha, numcoeff);

    T *d_CG=NULL;
    error=cudaMalloc((void **) &d_CG, numcoeff*sizeof(T));
    if (error!=cudaSuccess)  {
        printf("%d\n",(int)numcoeff);
        throw  hanalysis::STAError("error allocating device memory (CG coefficients)\n");
    }
    error=cudaMemcpy(d_CG, (T*)CGTable, numcoeff*sizeof(T), cudaMemcpyHostToDevice);
    if (error!=cudaSuccess) {
        if (d_CG!=NULL) cudaFree(d_CG);
        delete [] CGTable;
        throw  hanalysis::STAError("error copying host memory to device memory\n");
    }



    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(cudaGetErrorString( error ));

    T sign=1;
    if (clear_field) sign*=-1;
    
   int J1=(J+Jupdown);  
    
  std::size_t vectorLengthJ=2*(J+1);
  std::size_t vectorLengthJ1=2*(J1+1);

  if (stride_in == -1)
      stride_in = vectorLengthJ;
  if (stride_out == -1)
      stride_out = vectorLengthJ1;    
    

    sta_derivatives_R_kernel<T> <<< n_blocks, block_size >>>(
        stIn,
        stOut,
        d_CG,
        N,
        shape[0],
        shape[1],
        shape[2],
        (shape[2]*shape[1]),  // extx*exty
        (shape[2]),  // extx
        J,
        J1,
        sign,
        stride_in,
        stride_out,
        clear_field);





    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(cudaGetErrorString( error ));

    delete [] CGTable;
    cudaFree(d_CG);

}


template
void sta_derivatives_R(
    const float * stIn,
    float * stOut ,
    const std::size_t shape[],
    int J,
    int Jupdown,    // either -1 0 or 1
    bool conjugate,
    float alpha,
    const float  v_size[],
    int stride_in1,
    int stride_out,
    bool clear_field);

template
void sta_derivatives_R(
    const double * stIn,
    double * stOut ,
    const std::size_t shape[],
    int J,
    int Jupdown,    // either -1 0 or 1
    bool conjugate,
    double alpha,
    const double  v_size[],
    int stride_in,
    int stride_out,
    bool clear_field);



template<typename T>
__global__ void sta_product_R_kernel(
    const T * stIn1,
    const T * stIn2,
    T * stOut ,
    const T * CGcoeff,
    std::size_t numvoxel,
    int J1,
    int J2,
    int J,
    int stride_in1,
    int stride_in2,
    int stride_out,
    bool clear_field,
    bool resultInIv )
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx<numvoxel)
    {


        const T  * current_J1=stIn1+idx*stride_in1+2*J1;
        const T  * current_J2=stIn2+idx*stride_in2+2*J2;
        T * current_J=stOut+idx*stride_out+2*J;



        T tmp0_r;
        T tmp0_i;
        T tmp1_r;
        T tmp1_i;

        for (int m=-J;m<=0;m++)
        {

            if ( clear_field )
            {
                current_J[m*2]=T ( 0 );
                current_J[m*2+1]=T ( 0 );
            }

            for (int m1=-J1;m1<=J1;m1++)
            {
                int m2=m-m1;
                if (abs(m2)<=J2)
                {
                    if (m1>0)
                    {
                        if (m1%2==0)
                        {
                            tmp0_r=current_J1[-2*m1];
                            tmp0_i=-current_J1[-2*m1+1];
                        }
                        else
                        {
                            tmp0_r=-current_J1[-2*m1];
                            tmp0_i=current_J1[-2*m1+1];
                        }
                    } else
                    {
                        tmp0_r=current_J1[2*m1];
                        tmp0_i=current_J1[2*m1+1];
                    }

                    if (m2>0)
                    {
                        if (m2%2==0)
                        {
                            tmp1_r=current_J2[-2*m2];
                            tmp1_i=-current_J2[-2*m2+1];
                        }
                        else
                        {
                            tmp1_r=-current_J2[-2*m2];
                            tmp1_i=current_J2[-2*m2+1];
                        }
                    } else
                    {
                        tmp1_r=current_J2[2*m2];
                        tmp1_i=current_J2[2*m2+1];
                    }
                    if ( resultInIv )
                    {
                        current_J[m*2]-=(*CGcoeff)*(tmp0_r*tmp1_i+tmp0_i*tmp1_r);
                        current_J[m*2+1]+=(*CGcoeff++)*(tmp0_r*tmp1_r-tmp0_i*tmp1_i);
                    } else
                    {
                        current_J[m*2]+=(*CGcoeff)*(tmp0_r*tmp1_r-tmp0_i*tmp1_i);
                        current_J[m*2+1]+=(*CGcoeff++)*(tmp0_r*tmp1_i+tmp0_i*tmp1_r);
                    }
                }
            }
        }
    }
}


template<typename T>
T * CG_Prod_cuda( int J1,
                  int J2,
                  int J,
                  bool normalized,
                  T fact, std::size_t & numcoeff)
{
    T norm=(T)1;
    if (normalized)
    {
        norm=(T)1/(T)hanalysis::clebschGordan(J1,0,J2,0,J,0);
    }
    norm*=fact;
    std::size_t count=0;
    for (int m=-J;m<=0;m++)
    {
        for (int m1=-J1;m1<=J1;m1++)
        {
            int m2=m-m1;
            if (abs(m2)<=J2)
            {
                count++;
            }
        }
    }

    numcoeff=count;
    T * cg= new T[count];
    count=0;
    for (int m=-J;m<=0;m++)
    {
        for (int m1=-J1;m1<=J1;m1++)
        {
            int m2=m-m1;
            if (abs(m2)<=J2)
            {
                cg[count++]=norm*(T)hanalysis::clebschGordan(J1,m1,J2,m2,J,m);
            }
        }
    }
    return cg;
}








template<typename T>
void sta_product_R (
    const T * stIn1,
    const T * stIn2,
    T * stOut ,
    const std::size_t shape[],
    int J1,
    int J2,
    int J,
    T alpha,
    bool normalize,
    int stride_in1,
    int stride_in2,
    int stride_out,
    bool clear_field)
{
    cudaError_t error;

    std::size_t N=shape[0]*shape[1]*shape[2];
    //int block_size = 64;

    int block_size = block_size_PROD;

    int n_blocks = N/block_size + (N%block_size == 0 ? 0:1);

    std::size_t numcoeff=0;
    T * CGTable=CG_Prod_cuda(J1,J2, J, normalize, alpha, numcoeff);


    T *d_CG=NULL;
    error=cudaMalloc((T **) &d_CG, numcoeff*sizeof(T));
    if (error!=cudaSuccess)  {
        printf("%d\n",(int)numcoeff);
        throw  hanalysis::STAError("error allocating device memory\n");
    }
    error=cudaMemcpy(d_CG, (T*)CGTable, numcoeff*sizeof(T), cudaMemcpyHostToDevice);
    if (error!=cudaSuccess) {
        if (d_CG!=NULL) cudaFree(d_CG);
        delete [] CGTable;
        throw  hanalysis::STAError("error copying host memory to device memory\n");
    }


    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(cudaGetErrorString( error ));

    bool resultInIv= ( ( J1+J2+J ) %2!=0 );

    if ( stride_in1 == -1 )
	stride_in1 = 2*(J1+1);
    if ( stride_in2 == -1 )
	stride_in2 = 2*(J2+1);
    if ( stride_out == -1 )
	stride_out = 2*(J+1);

    sta_product_R_kernel<T> <<< n_blocks, block_size >>>(
        stIn1,
        stIn2,
        stOut,
        d_CG,
        N,
        J1,
        J2,
        J,
        stride_in1,
        stride_in2,
        stride_out,
        clear_field,
        resultInIv);

    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(cudaGetErrorString( error ));


    delete [] CGTable;
    cudaFree(d_CG);
}






template
void sta_product_R (
    const float * stIn1,
    const float * stIn2,
    float * stOut ,
    const std::size_t shape[],
    int J1,
    int J2,
    int J,
    float alpha,
    bool normalize,
    int stride_in1,
    int stride_in2,
    int stride_out,
    bool clear_field);

template
void sta_product_R (
    const double * stIn1,
    const double * stIn2,
    double * stOut ,
    const std::size_t shape[],
    int J1,
    int J2,
    int J,
    double alpha,
    bool normalize,
    int stride_in1,
    int stride_in2,
    int stride_out,
    bool clear_field);



template<typename T>
__global__ void sta_product_Rft_kernel(
    const T * stIn1,
    const T * stIn2,
    T * stOut ,
    const T * CGcoeff,
    std::size_t numvoxel,
    std::size_t shapeZ,
    std::size_t shapeY,
    std::size_t shapeX,
    std::size_t jumpZ,  // extx*exty
    std::size_t jumpY,  // extx
    int J1,
    int J2,
    int J,
    int stride_in1,
    int stride_in2,
    int stride_out,
    bool clear_field,
    bool resultInIv )
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx<numvoxel)
    {
        if ( stride_in1 == -1 )
            stride_in1 = 2*(J1+1);
        if ( stride_in2 == -1 )
            stride_in2 = 2*(J2+1);
        if ( stride_out == -1 )
            stride_out = 2*(J+1);


        int pZ=idx/jumpZ;
        int pY=(idx%jumpZ)/jumpY;
        int pX=(idx%jumpZ)%jumpY;

        std::size_t mpos= ((shapeZ-pZ)%shapeZ)*jumpZ+
                          ((shapeY-pY)%shapeY)*jumpY+
                          ((shapeX-pX)%shapeX);

        const T * current_J1Rmirrowed=stIn1+ (mpos*stride_in1+2*J1 );
        const T * current_J2Rmirrowed=stIn2+ (mpos*stride_in2+2*J2 );


        const T  * current_J1=stIn1+idx*stride_in1+2*J1;
        const T  * current_J2=stIn2+idx*stride_in2+2*J2;
        T * current_J=stOut+idx*stride_out+2*J;


        std::size_t count=0;
        T tmp0_r;
        T tmp0_i;
        T tmp1_r;
        T tmp1_i;

        for (int m=-J;m<=0;m++)
        {

            if ( clear_field )
            {
                current_J[m*2]=T ( 0 );
                current_J[m*2+1]=T ( 0 );
            }

            for (int m1=-J1;m1<=J1;m1++)
            {
                int m2=m-m1;
                if (abs(m2)<=J2)
                {
                    if (m1>0)
                    {
                        if (m1%2==0)
                        {
                            tmp0_r=current_J1Rmirrowed[-2*m1];
                            tmp0_i=-current_J1Rmirrowed[-2*m1+1];
                        }
                        else
                        {
                            tmp0_r=-current_J1Rmirrowed[-2*m1];
                            tmp0_i=current_J1Rmirrowed[-2*m1+1];
                        }
                    } else
                    {
                        tmp0_r=current_J1[2*m1];
                        tmp0_i=current_J1[2*m1+1];
                    }

                    if (m2>0)
                    {
                        if (m2%2==0)
                        {
                            tmp1_r=current_J2Rmirrowed[-2*m2];
                            tmp1_i=-current_J2Rmirrowed[-2*m2+1];
                        }
                        else
                        {
                            tmp1_r=-current_J2Rmirrowed[-2*m2];
                            tmp1_i=current_J2Rmirrowed[-2*m2+1];
                        }
                    } else
                    {
                        tmp1_r=current_J2[2*m2];
                        tmp1_i=current_J2[2*m2+1];
                    }
                    if ( resultInIv )
                    {
                        current_J[m*2]-=CGcoeff[count++]*(tmp0_r*tmp1_i+tmp0_i*tmp1_r);
                        current_J[m*2+1]+=CGcoeff[count]*(tmp0_r*tmp1_r-tmp0_i*tmp1_i);
                    } else
                    {
                        current_J[m*2]+=CGcoeff[count]*(tmp0_r*tmp1_r-tmp0_i*tmp1_i);
                        current_J[m*2+1]+=CGcoeff[count++]*(tmp0_r*tmp1_i+tmp0_i*tmp1_r);
                    }
                }
            }
        }
    }
}



template<typename T>
void sta_product_Rft (
    const T * stIn1,
    const T * stIn2,
    T * stOut ,
    const std::size_t shape[],
    int J1,
    int J2,
    int J,
    T alpha,
    bool normalize,
    int stride_in1,
    int stride_in2,
    int stride_out,
    bool clear_field)
{
    cudaError_t error;

    std::size_t N=shape[0]*shape[1]*shape[2];
    //int block_size = 64;

    int block_size = block_size_PRODft;

    int n_blocks = N/block_size + (N%block_size == 0 ? 0:1);

    std::size_t numcoeff=0;
    T * CGTable=CG_Prod_cuda(J1,J2, J, normalize, alpha, numcoeff);


    T *d_CG=NULL;
    error=cudaMalloc((T **) &d_CG, numcoeff*sizeof(T));
    if (error!=cudaSuccess)  {
        printf("%d\n",(int)numcoeff);
        throw  hanalysis::STAError("error allocating device memory\n");
    }
    error=cudaMemcpy(d_CG, (T*)CGTable, numcoeff*sizeof(T), cudaMemcpyHostToDevice);
    if (error!=cudaSuccess) {
        if (d_CG!=NULL) cudaFree(d_CG);
        delete [] CGTable;
        throw  hanalysis::STAError("error copying host memory to device memory\n");
    }


    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(cudaGetErrorString( error ));

    bool resultInIv= ( ( J1+J2+J ) %2!=0 );



    sta_product_Rft_kernel<T> <<< n_blocks, block_size >>>(
        stIn1,
        stIn2,
        stOut,
        d_CG,
        N,
        shape[0],
        shape[1],
        shape[2],
        shape[1]*shape[2],  // extx*exty
        shape[2],  // extx
        J1,
        J2,
        J,
        stride_in1,
        stride_in2,
        stride_out,
        clear_field,
        resultInIv);

    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(cudaGetErrorString( error ));


    delete [] CGTable;
    cudaFree(d_CG);
}


template
void sta_product_Rft (
    const float * stIn1,
    const float * stIn2,
    float * stOut ,
    const std::size_t shape[],
    int J1,
    int J2,
    int J,
    float alpha,
    bool normalize,
    int stride_in1,
    int stride_in2,
    int stride_out,
    bool clear_field);

template
void sta_product_Rft (
    const double * stIn1,
    const double * stIn2,
    double * stOut ,
    const std::size_t shape[],
    int J1,
    int J2,
    int J,
    double alpha,
    bool normalize,
    int stride_in1,
    int stride_in2,
    int stride_out,
    bool clear_field);




template<typename T>
__global__ void sta_mult_kernel(
    const T * stIn,
    T * stOut,
    std::size_t numVoxel,
    int numComponents,
    T alpha,
    bool conjugate,
    int stride_in,
    int stride_out,
    bool clear_field)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx<numVoxel)
    {
        stIn+=stride_in*idx;
        stOut+=stride_out*idx;
        for (int c=0;c<numComponents;c++)
        {
// 	    T tmp_r=*stIn++;
// 	    T tmp_i=*stIn++;
//
// 	      if ( conjugate )
// 		tmp_i=-tmp_i;
//
// 	      if ( clear_field )
// 	      {
// 		*stOut++=alpha*tmp_r;
// 		*stOut++=alpha*tmp_i;
// 	      }
// 	      else
// 	      {
// 		(*stOut++)+=alpha*tmp_r;
// 		(*stOut++)+=alpha*tmp_i;
// 	      }
            if ( clear_field )
            {
                *stOut++=alpha*(*stIn++);
                if (conjugate)
                    *stOut++=-alpha*(*stIn++);
                else
                    *stOut++=alpha*(*stIn++);
            }
            else
            {
                (*stOut++)+=alpha*(*stIn++);
                if (conjugate)
                    (*stOut++)-=alpha*(*stIn++);
                else
                    (*stOut++)+=alpha*(*stIn++);
            }

        }
    }
}






template<typename T>
void sta_mult (
    const T * stIn,
    T * stOut,
    std::size_t numVoxel,
    int ncomponents,
    T alpha,
    bool conjugate,
    int  stride_in,
    int  stride_out,
    bool clear_field)
{

    if (stride_in==-1)
        stride_in=ncomponents;
    if (stride_out==-1)
        stride_out=ncomponents;


    std::size_t N=numVoxel;
    //int block_size = 64;
    int block_size = block_size_MULT;
    int n_blocks = N/block_size + (N%block_size == 0 ? 0:1);

    cudaError_t error;

    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(std::string(cudaGetErrorString( error )));

    sta_mult_kernel<T> <<< n_blocks, block_size >>>(
        stIn,
        stOut,
        numVoxel,
        ncomponents,
        alpha,
        conjugate,
        2*stride_in,
        2*stride_out,
        clear_field);

    error = cudaGetLastError();
    if (error != cudaSuccess)  throw hanalysis::STAError(std::string(cudaGetErrorString( error )));
}


template
void sta_mult (
    const float * stIn,
    float * stOut,
    std::size_t numVoxel,
    int ncomponents,
    float alpha,
    bool conjugate,
    int  stride_in,
    int  stride_out,
    bool clear_field);

template
void sta_mult (
    const double * stIn,
    double * stOut,
    std::size_t numVoxel,
    int ncomponents,
    double alpha,
    bool conjugate,
    int  stride_in ,
    int  stride_out,
    bool clear_field );


void fft(const float* IN,float * OUT, int shape[],int numComponents, bool forward)
{


    int rank=3;
    int * n=shape;
    int howmany=numComponents;
    cufftComplex * in =(cufftComplex *)IN;
    int * inembed=n;


    int istride=numComponents;
    int idist=1;
    cufftComplex * out =(cufftComplex *)OUT;
    int * onembed=inembed;
    int odist=idist;
    int ostride=istride;


    cufftType fft_type=CUFFT_C2C;

    cufftResult error;
    cufftHandle plan;
    error=cufftPlanMany(&plan,rank,n,inembed,istride, idist,onembed, ostride, odist, fft_type,howmany);
    if (error!=CUFFT_SUCCESS)
    {
        switch (error)
        {
        case CUFFT_ALLOC_FAILED:
            throw hanalysis::STAError("Allocation of GPU resources for the plan failed\n");
        case CUFFT_INVALID_TYPE:
            throw hanalysis::STAError("The type parameter is not supported\n");
        case CUFFT_INVALID_VALUE:
            throw hanalysis::STAError("Invalid parameter(s) passed to the API\n");
        case CUFFT_INTERNAL_ERROR:
            throw hanalysis::STAError("Internal driver error is detected\n");
        case CUFFT_SETUP_FAILED:
            throw hanalysis::STAError("CUFFT library failed to initialize\n");
        case CUFFT_INVALID_SIZE:
            throw hanalysis::STAError("The nx parameter is not a supported size\n");
        default:
            throw hanalysis::STAError("unknown error\n");
        }
    }

    int sign=CUFFT_FORWARD;
    if (!forward) sign=CUFFT_INVERSE;
    cufftExecC2C(plan,in,out,sign);

    cufftDestroy(plan);
}


void fft(const double* IN,double * OUT, int shape[],int numComponents, bool forward)
{


    int rank=3;
    int * n=shape;
    int howmany=numComponents;
    cufftDoubleComplex * in =(cufftDoubleComplex *)IN;
    int * inembed=n;


    int istride=numComponents;
    int idist=1;
    cufftDoubleComplex * out =(cufftDoubleComplex *)OUT;
    int * onembed=inembed;
    int odist=idist;
    int ostride=istride;


    cufftType fft_type=CUFFT_Z2Z;

    cufftResult error;
    cufftHandle plan;
    error=cufftPlanMany(&plan,rank,n,inembed,istride, idist,onembed, ostride, odist, fft_type,howmany);
    if (error!=CUFFT_SUCCESS)
    {
        switch (error)
        {
        case CUFFT_ALLOC_FAILED:
            throw hanalysis::STAError("Allocation of GPU resources for the plan failed\n");
        case CUFFT_INVALID_TYPE:
            throw hanalysis::STAError("The type parameter is not supported\n");
        case CUFFT_INVALID_VALUE:
            throw hanalysis::STAError("Invalid parameter(s) passed to the API\n");
        case CUFFT_INTERNAL_ERROR:
            throw hanalysis::STAError("Internal driver error is detected\n");
        case CUFFT_SETUP_FAILED:
            throw hanalysis::STAError("CUFFT library failed to initialize\n");
        case CUFFT_INVALID_SIZE:
            throw hanalysis::STAError("The nx parameter is not a supported size\n");
        default:
            throw hanalysis::STAError("unknown error\n");
        }
    }

    int sign=CUFFT_FORWARD;
    if (!forward) sign=CUFFT_INVERSE;
    cufftExecZ2Z(plan,in,out,sign);

    cufftDestroy(plan);
}



}

