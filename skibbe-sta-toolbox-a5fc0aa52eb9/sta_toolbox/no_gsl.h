/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 * 
 *     
 *	This file is part of the STA-ImageAnalysisToolbox
 * 
 *	STA-ImageAnalysisToolbox is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox. 
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

// For full functionality the GSL is required. However, in some scenarios the 
// GSL might not be accessible and thus this code here 
// can be used. (We highly recommend to link and use the GSL)



#ifndef _STA_GSL_DUMMY
#define _STA_GSL_DUMMY
#include <cstddef>

#include <cmath>
#include <sstream>
#include <cstddef>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <limits>

inline
double     gsl_sf_laguerre_n(int n, double a, double x)
{
  static bool printwarning=true;
  if (printwarning)
  {
    printf("##############################################################################\n"); 
    printf("gsl_sf_laguerre_n: is currently not supported on your system, returning 0\n");
    printf("##############################################################################\n"); 
    printwarning=false;
  }
    return 0;
};



inline
double gsl_sf_fact(const unsigned int n)
{
  static bool printwarning=true;
  if (printwarning)
  {
    printf("##############################################################################\n"); 
    printf("gsl_sf_fact: is currently not supported on your system, returning 0\n");
    printf("##############################################################################\n"); 
    printwarning=false;
  }
    return 0;
};




inline
double  gsl_sf_legendre_sphPlm(const int l, const int m, const double x)
{
    static bool printwarning=true;
  if (printwarning)
  {
    printf("##############################################################################\n"); 
    printf("gsl_sf_legendre_sphPlm: is currently not supported on your system, returning 0\n");
    printf("##############################################################################\n"); 
    printwarning=false;
  }
    return 0;
};


inline
double gsl_sf_bessel_zero_Jnu(double nu, unsigned int s)
{
    static bool printwarning=true;
  if (printwarning)
  {
    printf("##############################################################################\n"); 
    printf("gsl_sf_bessel_zero_Jnu: is currently not supported on your system, returning 0\n");
    printf("##############################################################################\n"); 
    printwarning=false;
  }  
    return 0;
};


inline
double gsl_sf_bessel_jl(const int l, const double x)
{
    static bool printwarning=true;
    if (printwarning)
    {
        printf("##############################################################################\n");
        printf("gsl_sf_bessel_jl: is currently not supported on your system, returning 0\n");
        printf("##############################################################################\n");
        printwarning=false;
    }
    return 0;
};


inline 
double gsl_sf_bessel_j0(const double x)
{
  if(std::abs(x) < std::numeric_limits<double>::epsilon()) {
    return 1.0;
  }
  else {
    return (std::sin(x)/x);
  }
};

//
// Computing coefficients by solving SOE; see 
// William O. Straub
// EFFICIENT COMPUTATION OF CLEBSCH-GORDAN COEFFICIENTS
// for details
//  
template <typename T>
T nogsl_clebschGordan(int j1, int M1, int j2, int M2,int J, int M)
{
  if ( (std::abs(j1-j2)>J)||
      (j1+j2<J)||
      (std::abs(M1)>j1)||
      (std::abs(M2)>j2)||
      (std::abs(M)>J))
 {
   return 0;
 }
 int m1_min=(M-j1-j2+std::abs(j1-j2+M))/2;
 int m1_max=(M+j1+j2-std::abs(j1-j2-M))/2;
 
 int numel=m1_max-m1_min+1;
 
 T * coefficients=new T[numel];
 
 int count=numel-2; 
 T A1_old=0;
 T sum=1;
 int j1j1=j1*(j1+1);
 int j2j2=j2*(j2+1);
 coefficients[numel-1]=1; 
 for (int m1=m1_max;m1>m1_min;m1--)
 {
    int m2=M-m1;
    T A0=j1j1+j2j2+2*m1*m2-J*(J+1);
    T A1=std::sqrt(double(j1j1-m1*(m1-1)))*std::sqrt(double(j2j2-m2*(m2+1)));
     
    if (m1<m1_max)
    {
      coefficients[count]=(-A0*coefficients[count+1]-A1_old*coefficients[count+2])/A1;
    }else
    {
      coefficients[count]=-A0/A1;
    } 
    sum+=coefficients[count]*coefficients[count];
    A1_old=A1; 
    count--;
 }
 T v=coefficients[M1-m1_min]/std::sqrt(sum);
 delete [] coefficients; 
 return v;
}

#endif