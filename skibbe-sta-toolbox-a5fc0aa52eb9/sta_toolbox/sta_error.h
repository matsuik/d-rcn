/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 * 
 *     
 *	This file is part of the STA-ImageAnalysisToolbox
 * 
 *	STA-ImageAnalysisToolbox is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox. 
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

#ifndef STA_COMMON_ERROR_H
#define STA_COMMON_ERROR_H
#include <ctime>
#include <list>
#include <sstream>
#include <string>
#include <iomanip>
#include "stensor.h"
#include <assert.h>


#ifdef __linux__
  #include <sys/time.h>
#endif


//mxAssert sometimes didn't work, so we use this workaround

#ifdef _SUPPORT_MATLAB_ 
  #define sta_assert( S ) \
  { \
    if (!( S )) \
    {\
     std::stringstream s; \
     s<<"Assertion \""<<#S<<"\" failed (line "<<__LINE__<<" in "<<__FILE__<<")";\
      mexErrMsgTxt(s.str().c_str()); \
    }\
  }
#else
  #define sta_assert( S ) \
  { \
    if (!( S )) \
    {\
      assert(S); \
    }\
  }
#endif


namespace hanalysis
{

/// the STA error class
class STAError
{
public:
    STAError() {}

    STAError(const std::string& message, STA_RESULT error_code=STA_RESULT_FAILED)
            :_message(message),_error_code(error_code)
    {}
    
    STAError(STA_RESULT error_code)
            :_error_code(error_code)
    {_message=errortostring(error_code);}    


    template<class T>
    STAError & operator<<(const T &data)
    {
        std::ostringstream os;
        os << data;
        _message += os.str();
        return *this;
    }
    
    void setCode(STA_RESULT error_code)
    {
      _error_code=error_code;
    }
    
    STA_RESULT getCode() const
    {
      return _error_code;
    }    

    /// \returns  the error string
    std::string str() const
    {
        return _message;
    }


    /// \returns  the error c-string
    const char* what() const
    {
        return _message.c_str();
    }

private:
    std::string _message;
    STA_RESULT _error_code;
};


class CtimeStopper
{
private:
    double _startSecond;
    double _lastSecond;
    int steps;
    std::string startevent;
    std::list<std::string> event_names;
    std::list<double> event_times;
    std::list<double> event_all_times;
public:
    CtimeStopper(std::string event="")
    {
        startevent=event;
        steps=0;
#ifdef __linux__	
        struct timeval _Time;
        gettimeofday(&_Time,NULL);
        _startSecond=_Time.tv_sec + 0.000001*_Time.tv_usec;
#else
	_startSecond=0;
#endif
        _lastSecond=_startSecond;
	if (startevent!="")
	{
	  addEvent(startevent);
	  //printEvents();
	}	
    }
//     ~CtimeStopper()
//     {
//        if (startevent!="")
//        {
// 	 addEvent("done");
// 	 printEvents();
//        }
//     }
    
    
    void addEvent(std::string event)
    {
#ifdef __linux__      
        struct timeval  currentTime;
        gettimeofday(&currentTime,NULL);
        double currentSeconds=currentTime.tv_sec + 0.000001*currentTime.tv_usec;
#else
	double currentSeconds=0;
#endif
	
	event_times.push_back(currentSeconds-_lastSecond);
	event_all_times.push_back(currentSeconds-_startSecond);
	event_names.push_back(event);	
        _lastSecond=currentSeconds;
        steps++;
    }    
    
    void printEvents(int precision=3)
    {
      printf("\n computation time (summary):\n");
      printf("    all         step\n");
      std::list<std::string>::iterator iter2=event_names.begin();
      std::list<double>::iterator iter3=event_all_times.begin();
      for (std::list<double>::iterator iter=event_times.begin();
	  iter!=event_times.end();iter++,iter2++,iter3++)
      {
	std::stringstream s;
	s<<std::fixed<<std::setprecision(precision)<<*iter3<<" sec. | "<<*iter<<" sec. |\t("<<*iter2<<")";
	printf("%s\n",s.str().c_str());
      }
      printf("\n");
    }
};


}

#endif

