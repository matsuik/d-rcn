/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox
 * 
 *	STA-ImageAnalysisToolbox is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox. 
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

#include "stafield.h"

int main ()
{
    std::size_t shape[3]={64,64,64};
    std::vector<float> params_v;
    params_v.push_back(5);
    bool centered=true;
    int L=2;

    //hanalysis::do_classcount=true;

    hanalysis::stafield<float> b(
        "gauss",
        shape,
        params_v,
        centered,
        L);

    int example=1;

    printf("example %d: ",example++);
    {
        hanalysis::stafield<float> a;
        a=b;

        if (a.ownMemory()) printf("data copied\n");
        else printf("reference\n");
    }
    // example 1: data copied

    printf("example %d: ",example++);
    {
        hanalysis::stafield<float> a;
        a=b.get(L);

        if (a.ownMemory()) printf("data copied\n");
        else printf("reference\n");

        a.createMemCopy();

        if (a.ownMemory()) printf("data copied\n");
        else printf("reference\n");
    }
    // example 2: data reference
    // data copied

    printf("example %d: ",example++);
    {
        hanalysis::stafield<float> a;
        a=b*2;

        if (a.ownMemory()) printf("data copied\n");
        else printf("reference\n");
    }
    // example 3: data copied

    printf("example %d: ",example++);
    {
        hanalysis::stafield<float> a;
        a=b.deriv(1);

        if (a.ownMemory()) printf("data copied\n");
        else printf("reference\n");
    }
    // example 4: data copied

    printf("example %d: ",example++);
    {
        hanalysis::stafield<float> a(
            shape,
            L,
            a.getStorage(),
            hanalysis::STA_OFIELD_FULL);
        a.get(L)=b;

        if (a.ownMemory()) printf("data copied\n");
        else printf("reference\n");
    }
    // example 5: data copied



    try {
        hanalysis::stafield<float> a;

        std::size_t shape[3]={64,64,64};
        std::vector<float> params_v;
        params_v.push_back(5);
        bool centered=true;
        int L=2;

        hanalysis::stafield<float> b(
            "gauss",
            shape,
            params_v,
            centered,
            L);
        a=b.get(-1);

    } catch (hanalysis::STAError & error)
    {
        printf("%s\n",error.what());
    }


}


