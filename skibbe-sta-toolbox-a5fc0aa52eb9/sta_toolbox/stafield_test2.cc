/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 *     
 *	This file is part of the STA-ImageAnalysisToolbox
 * 
 *	STA-ImageAnalysisToolbox is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox. 
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

#include "stafield.h"





using namespace std;

template<typename T>
void test_stafield()
{

  
  
    int BW=3;
    T gamma=T(1);
    std::string kname="gauss";
    std::vector<T> kparams;
    kparams.push_back(4);

    

    std::size_t shape[3];
    //shape[0]=shape[1]=shape[2]=32;
    shape[0]=shape[1]=shape[2]=64;
    //shape[0]=shape[1]=shape[2]=128;

    bool do_smoothing=true;

    try {

//       {
// 	hanalysis::stafield<T> gradient=hanalysis::stafield<T>(kname,
//                                         shape,
//                                         kparams,
//                                         false,
//                                         0,
//                                         hanalysis::STA_FIELD_STORAGE_R).lap();//.deriv(1).deriv(1).lap().lap();
// 	
//       }
{
hanalysis::CtimeStopper tic;

for (int a=0;a<100;a++)
   {
       //hanalysis::CtimeStopper tic("GPU");
       hanalysis::stafield<T> dududu(shape,BW);
       hanalysis::stafieldGPU<T> testGPU(shape,BW);
       testGPU=dududu;

       hanalysis::stafieldGPU<T> testGPU2(testGPU);
       
       hanalysis::stafieldGPU<T> testGPU3(shape,BW);
       
       //tic.addEvent("GPU init");
       //testGPU3=testGPU2.prod(testGPU3,BW);
       //testGPU3=testGPU2.prod(testGPU3,BW+1);
    
       testGPU3=testGPU2.prod(testGPU3.deriv(1).deriv(-1).deriv(1).deriv(1),BW);
       
       //tic.addEvent("GPU ops");
       testGPU2[BW]=testGPU3[BW];
     
       dududu=testGPU3;
      // printf("dududu rank: %d\n",dududu.getRank());
       //tic.addEvent("GPU CPU init");
       //dududu=dududu.cpu2gpu().deriv(1).gpu2cpu().deriv(1);
       //tic.addEvent("GPU CPU ops");
       //printf("dududu rank: %d\n",dududu.getRank());
       testGPU=testGPU.fft(true).fft(false);
       testGPU=testGPU.lap();
       //tic.addEvent("GPU ops (fft + lap)");
   }
//    {
//        hanalysis::CtimeStopper tic("CPU\n");
//        hanalysis::stafield<T> test(shape,5);
//        hanalysis::stafield<T> test2(test);
//        hanalysis::stafield<T> test3(shape,5);
//        test3=test2.prod(test3,2).deriv(1).deriv(-1).deriv(1).deriv(1);     
//    }
tic.addEvent("GPU ops");
tic.printEvents(3);
}
      printf(":-)\n");
      return;					
      
      
        hanalysis::stafield<T> gradient=hanalysis::stafield<T>(kname,
                                        shape,
                                        kparams,
                                        false,
                                        0,
                                        hanalysis::STA_FIELD_STORAGE_R).deriv(1);
	
					

					
	
	const hanalysis::stafield<T> & bla=gradient.deriv2(2);				
	hanalysis::stafield<T> bla2;
	bla2=bla.deriv(1);
	

        hanalysis::stafield<T> magnitude=gradient.norm();
        hanalysis::stafield<T> ngradient=gradient.prod(magnitude.invert(),1,true);


        hanalysis::stafield<T> current=ngradient;
        if (gamma!=1)
            magnitude=magnitude.pow(gamma);


        hanalysis::stafield<T> shog(gradient.getShape(),
                                    BW,
                                    gradient.getStorage(),
                                    hanalysis::STA_OFIELD_FULL);


        hanalysis::stafield<T> convoluion_kernel;

        if (do_smoothing)
            convoluion_kernel=hanalysis::stafield<T>(kname,
                              gradient.getShape(),
                              kparams,
                              false,
                              0,
                              gradient.getStorage()).fft(true,false,T(1)/T(gradient.getNumVoxel()));


        for (int l=1;l<=BW;l++)
        {

            if (do_smoothing)
            {
                hanalysis::stafield<T> ngradient_ft=current.prod(magnitude,l,true).fft(true);
                shog[l]=ngradient_ft.prod(convoluion_kernel,l,true).fft(false);
            } else
                shog[l]=current.prod(magnitude,l,true);

            if (l<BW)
                current=current.prod(ngradient,l+1,true);
        }

        if (do_smoothing)
            shog[0]=magnitude.fft(true).prod(convoluion_kernel,0,true).fft(false);
        else
            shog.get(0)=magnitude;

    } catch (hanalysis::STAError & error)
    {
        printf("ERROR: %s\n",error.what());
    }
}



int main ()
{
  
  
  try { 
  int device=hanalysis_cuda::gpu_selectBestDevice();
  printf("selecting device : %d \n",device);
}
    catch (hanalysis::STAError & error)
    {
        printf("ERROR: %s\n",error.what());
    }

//cudaSetDevice(2);

  //hanalysis::do_classcount=true;
  
   test_stafield<float>();
   test_stafield<double>();
  
  
//     printf("using %d CPU-core(s)\n",hanalysis::get_numCPUs());
// 
//     hanalysis::do_classcount=false;
//     
//     {
//       hanalysis::CtimeStopper tic("float");
//       test_stafield<float>();
//     }
//     {
//       hanalysis::CtimeStopper tic("double");
//       test_stafield<double>();
//     }
//     {
//       hanalysis::CtimeStopper tic("float (2nd run)");
//       test_stafield<float>();
//     }    
//     {
//       hanalysis::CtimeStopper tic("double (2nd run");
//       test_stafield<double>();
//     }
}


