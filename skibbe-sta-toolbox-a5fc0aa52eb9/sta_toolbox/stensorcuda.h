/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 * 
*     
 *	This file is part of the STA-ImageAnalysisToolbox
 * 
 *	STA-ImageAnalysisToolbox is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox. 
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

#ifndef STA_STENSOR_CUDA_H
#define STA_STENSOR_CUDA_H


#include <cuda.h>
#include "cufft.h"
#include "cuda_runtime.h"
#include <complex>
#include "stensor.h"
#include "sta_error.h"


namespace hanalysis_cuda
{
template<typename T>
void gpu_malloc(T * & mem,std::size_t mem_size)
{
    cudaError_t error;
    error=cudaMalloc((void **) &mem, mem_size);
    if (error!=cudaSuccess)
        throw  hanalysis::STAError("error allocating device memory\n");
}

template<typename T>
void gpu_mfree(T * & mem)
{
    if (mem!=NULL)
        cudaFree(mem);
}



template<typename T>
void gpu_memcpy_d2d( T * const  mem_src,T *  mem_dest,std::size_t mem_size)
{
    cudaError_t error;
    error=cudaMemcpy(mem_dest, mem_src, mem_size, cudaMemcpyDeviceToDevice);
    if (error!=cudaSuccess)
        throw  hanalysis::STAError("error coopying device memory to device memory\n");
}

template<typename T>
void gpu_memcpy_h2d( T * const  mem_src,T *  mem_dest,std::size_t mem_size)
{
    cudaError_t error;
    error=cudaMemcpy(mem_dest, mem_src, mem_size, cudaMemcpyHostToDevice);
    if (error!=cudaSuccess)
        throw  hanalysis::STAError("error coopying host memory to device memory\n");
}

template<typename T>
void gpu_memcpy_d2h( T * const  mem_src,T *  mem_dest,std::size_t mem_size)
{
    cudaError_t error;
    error=cudaMemcpy(mem_dest, mem_src, mem_size, cudaMemcpyDeviceToHost);
    if (error!=cudaSuccess)
        throw  hanalysis::STAError("error coopying device memory to device host\n");
}



int gpu_selectBestDevice()
{
    //cudaChooseDevice
  
    cudaError_t error;
    cudaError_enum _error;

    _error=cuInit(0);
    if (_error==CUDA_ERROR_INVALID_VALUE)
        throw hanalysis::STAError("CUDA_ERROR_INVALID_VALUE\n");
    if (_error==CUDA_ERROR_INVALID_DEVICE)
        throw hanalysis::STAError("CUDA_ERROR_INVALID_DEVICE\n");

    int numDevices=0;
    error=cudaGetDeviceCount(&numDevices);
    if (error!=cudaSuccess)
        throw hanalysis::STAError("error getting #devices\n");

    int best_minor=0;
    int best_major=0;
    int most_cap_device=-1;

    for (int device=0;device<numDevices;device++)
    {
        cudaDeviceProp deviceProp; 
	error=cudaGetDeviceProperties(&deviceProp,  device); 
	if (error!=cudaSuccess) 
	  hanalysis::STAError("error getting device infos\n"); 
      
	int minor=deviceProp.minor;
	int major=deviceProp.major;	
	
        if (major>=best_major)
        {
	    if (minor>=best_minor)
	    {
	      best_minor=minor;
	      best_major=major;
	      most_cap_device=device;
	    }
        }
    }
    cudaSetDevice(most_cap_device);

    error = cudaGetLastError();
    if (error != cudaSuccess)
        throw hanalysis::STAError(cudaGetErrorString( error ));

    //printf("mem free %d %\n",(int)std::ceil(100*(double)max_freemem/(double)max_totalmem));
    
    printf("device %d, cuda version %d.%d\n",most_cap_device,best_major,best_minor);
	
    return most_cap_device;
}




template<typename T>
void copySubfield2Subfield(	const T * data_src,
                            T * data_dest,
                            std::size_t numVoxel,
                            int stride_in,
                            int stride_out,
                            int numComponents);





template<typename T>
void sta_product_R (
    const T * stIn1,
    const T * stIn2,
    T * stOut ,
    const std::size_t shape[],
    int J1,
    int J2,
    int J,
    T alpha,
    bool normalize,
    int stride_in1,
    int stride_in2,
    int stride_out,
    bool clear_field);


template<typename T>
void sta_product_Rft (
    const T * stIn1,
    const T * stIn2,
    T * stOut ,
    const std::size_t shape[],
    int J1,
    int J2,
    int J,
    T alpha,
    bool normalize,
    int stride_in1,
    int stride_in2,
    int stride_out,
    bool clear_field);


template<typename T>
void sta_product (
    const T * stIn1,
    const T * stIn2,
    T * stOut,
    const std::size_t shape[],
    int J1,
    int J2,
    int J,
    std::complex<T> alpha,
    bool normalize = false,
    hanalysis::STA_FIELD_STORAGE field_property=hanalysis::STA_FIELD_STORAGE_R,
    int  stride_in1 = -1,
    int  stride_in2 = -1,
    int  stride_out = -1,
    bool clear_field = false )
{
    bool alpha_real=(alpha.imag()==0);
    if (!alpha_real)
        throw  hanalysis::STAError("complex valued weights are not supported yet");
    if (field_property==hanalysis::STA_FIELD_STORAGE_C)
        throw  hanalysis::STAError("STA_FIELD_STORAGE_C is not supported yet");
//     if (field_property==hanalysis::STA_FIELD_STORAGE_RF)
//         throw  hanalysis::STAError("STA_FIELD_STORAGE_RF is not supported yet");


    switch (field_property)
    {
    case hanalysis::STA_FIELD_STORAGE_R:
        try
        {
            hanalysis_cuda::sta_product_R(
                stIn1,
                stIn2,
                stOut ,
                shape,
                J1,
                J2,
                J,
                alpha.real(),
                normalize,
                stride_in1,
                stride_in2,
                stride_out,
                clear_field);
        } catch (hanalysis::STAError & error)
        {
            throw error;
        }
        break;
    case hanalysis::STA_FIELD_STORAGE_RF:
        try
        {
            hanalysis_cuda::sta_product_Rft(
                stIn1,
                stIn2,
                stOut ,
                shape,
                J1,
                J2,
                J,
                alpha.real(),
                normalize,
                stride_in1,
                stride_in2,
                stride_out,
                clear_field);
        } catch (hanalysis::STAError & error)
        {
            throw error;
        }
        break;
    default:
        throw  hanalysis::STAError("STA_FIELD_STORAGE_C is not supported yet");
    }
}

template<typename T>
void sta_derivatives_R(
    const T * stIn,
    T * stOut ,
    const std::size_t shape[],
    int J,
    int Jupdown,
    bool conjugate,
    T alpha,
    const T  v_size[],
    int stride_in,
    int stride_out,
    bool clear_field);


template<typename T>
void sta_mult (
    const T * stIn,
    T * stOut,
    std::size_t numVoxel,
    int ncomponents,
    T alpha = T ( 1 ),
    bool conjugate=false,
    int  stride_in = -1,
    int  stride_out = -1,
    bool clear_field = false );


template<typename T>
void sta_laplace (
    const T * stIn,
    T * stOut ,
    const std::size_t shape[],
    int components=0,
    T alpha=1,
    int stride_in = -1,
    int stride_out = -1,
    bool clear_field = false  );


void fft(const double* IN,double * OUT, int shape[],int numComponents, bool forward);
void fft(const float* IN,float * OUT, int shape[],int numComponents, bool forward);

template<typename T>
void sta_fft ( const T * IN,
               T * OUT,
               const std::size_t shape[],
               int components,
               bool forward,
               bool conjugate=false,
               T alpha = T ( 1 ))
{


    int shape_[3];
    shape_[0]=shape[0];
    shape_[1]=shape[1];
    shape_[2]=shape[2];
    fft ( IN,OUT,shape_,components,forward);

    sta_mult (OUT,
              OUT,
              shape[0]*shape[1]*shape[2],
              components,
              alpha,
              conjugate,
              -1,
              -1,
              true);

}



template<typename T>
void sta_derivatives (
    const T * stIn,
    T * stOut ,
    const std::size_t shape[],
    int J,
    int Jupdown,    // either -1 0 or 1
    bool conjugate=false,
    std::complex<T> alpha= ( T ) 1.0,
    hanalysis::STA_FIELD_STORAGE field_property=hanalysis::STA_FIELD_STORAGE_R,
    const T  v_size[]=NULL,
    int stride_in = -1,
    int stride_out = -1,
    bool clear_field = false,
    int accuracy=0)
{
    bool alpha_real=(alpha.imag()==0);
    if (!alpha_real)
        throw  hanalysis::STAError("complex valued weights are not supported yet");
    if (field_property==hanalysis::STA_FIELD_STORAGE_C)
        throw  hanalysis::STAError("STA_FIELD_STORAGE_C is not supported yet");
    if (field_property==hanalysis::STA_FIELD_STORAGE_RF)
        throw  hanalysis::STAError("STA_FIELD_STORAGE_RF is not supported yet");


    try
    {
        hanalysis_cuda::sta_derivatives_R(
            stIn,
            stOut ,
            shape,
            J,
            Jupdown,    // either -1 0 or 1
            conjugate,
            alpha.real(),
            v_size,
            stride_in,
            stride_out,
            clear_field);
    } catch (hanalysis::STAError & error)
    {
        throw error;
    }
}



}






#endif

