/*#############################################################################
 *
 *	Copyright 2011 by Henrik Skibbe and Marco Reisert
 * 
 *     
 *	This file is part of the STA-ImageAnalysisToolbox
 * 
 *	STA-ImageAnalysisToolbox is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 * 
 *	STA-ImageAnalysisToolbox is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 * 
 *	You should have received a copy of the GNU General Public License
 *	along with STA-ImageAnalysisToolbox. 
 *	If not, see <http://www.gnu.org/licenses/>.
 *
 *
*#############################################################################*/

#ifndef STA_FIELD_CLASS_CUDA_H
#define STA_FIELD_CLASS_CUDA_H

#include "stafield.h"
#include "stensorcuda.h"
#include "cuda_runtime.h"
#include <stdio.h>




namespace hanalysis
{




/// represents spherical tensor fields (GPU version)
template<typename T>
class stafieldGPU : public _stafield<T>
{
private:

    T * data;
public:


    /*!
    * \returns data pointer
    * */
    T *  getData() {
        return this->data;
    };

    /*!
    * \returns constant data pointer
    * */
    const T *  getDataConst() const {
        return this->data;
    };



    stafieldGPU()  : _stafield<T>(), data(NULL) {};

    stafieldGPU(const stafieldGPU & field) : _stafield<T>(), data(NULL)
    {
        (*this)=field;
    }




    stafieldGPU(const std::size_t shape[],
                int L,
                hanalysis::STA_FIELD_STORAGE field_storage=hanalysis::STA_FIELD_STORAGE_R,
                hanalysis::STA_FIELD_TYPE field_type=hanalysis::STA_OFIELD_SINGLE)
            : _stafield<T>(), data(NULL)
    {
        if (field_storage==hanalysis::STA_FIELD_STORAGE_C)
            throw  (hanalysis::STAError("error STA_FIELD_STORAGE_C is not supported by stafieldGPU!"));

        if (L<0)
            throw STAError("L must be >= 0");
        this->field_storage=field_storage;
        this->field_type=field_type;
        this->shape[0]=shape[0];
        this->shape[1]=shape[1];
        this->shape[2]=shape[2];
        this->L=L;
        int numcomponents=hanalysis::order2numComponents(field_storage,field_type,L);
        int numVoxel=shape[0]*shape[1]*shape[2];
        if (hanalysis::verbose>0)
            printf("L: %d , (%i,%i,%i) , // %i\n",L,shape[0],shape[1],shape[2],numcomponents);
        if (hanalysis::verbose>0)
            printf("allocating %i bytes\n",numcomponents*numVoxel*sizeof(std::complex<T>));

        hanalysis_cuda::gpu_malloc<T>(this->data,numcomponents*numVoxel*2*sizeof(T));
        //cudaMalloc((void **) &this->data, numcomponents*numVoxel*2*sizeof(float));
        //this->data=new std::complex<T>[numcomponents*numVoxel];
    }


    /*! creates a spherical tensor field of order \f$ L \in \mathbb N \f$ \n
      * based on existing data. No memory is allocated yet.
     *   \param shape
     *  \param L \f$ L \in \mathbb N \f$, the tensor rank
     * \param data pointer to existing memory
     * */
    stafieldGPU(const std::size_t shape[],
                int L,
                hanalysis::STA_FIELD_STORAGE field_storage,
                hanalysis::STA_FIELD_TYPE field_type,
                T  * data) : _stafield<T>() , data(NULL)
    {
        if (field_storage==hanalysis::STA_FIELD_STORAGE_C)
            throw  (hanalysis::STAError("error STA_FIELD_STORAGE_C is not supported by stafieldGPU!"));

        if (L<0)
            throw  (hanalysis::STAError("L must be >= 0"));
        if (data==NULL)
            throw ( hanalysis::STAError("data is NULL-pointer"));
        this->field_storage=field_storage;
        this->field_type=field_type;
        this->shape[0]=shape[0];
        this->shape[1]=shape[1];
        this->shape[2]=shape[2];
        this->data=data;
        this->own_memory=false;
        this->L=L;
    }




    stafieldGPU & operator=(const stafieldGPU & f)
    {
        if (this==&f)
            return *this;

        if ((this->L==-1)&&(f.L==-1))
            return *this;

        if ((f.object_is_dead_soon==1)
                &&(this->own_memory)
           )
        {
            if (f.object_is_dead_soon>1)
                throw STAError("error: something went wrong with the memory managemant \n");

            if (this->own_memory && (this->data!=NULL) && (this->object_is_dead_soon<2))
                delete [] this->data;

            this->set_death(&f);
            this->data=f.data;

            this->field_storage=f.field_storage;
            this->field_type=f.field_type;

            this->shape[0]=f.shape[0];
            this->shape[1]=f.shape[1];
            this->shape[2]=f.shape[2];

            this->setElementSize(f.getElementSize());

            this->L=f.L;

            this->stride=f.stride;
            this->own_memory=f.own_memory;
            return *this;
        }


        int check=0;

        if ((*this)!=f)
        {
            if (this->stride!=0)
                throw STAError("warning: operator=  (stride!=0) shared memory block but alocating new (own) memory would be nrequired \n");

            if (!this->own_memory)
                throw STAError("warning: operator=  (!own_memory): shared memory block but alocating new (own) memory would be nrequired \n");



            if (this->own_memory && (this->data!=NULL))
                cudaFree(this->data);

            this->field_storage=f.field_storage;
            this->field_type=f.field_type;
            this->shape[0]=f.shape[0];
            this->shape[1]=f.shape[1];
            this->shape[2]=f.shape[2];

            this->setElementSize(f.getElementSize());

            this->data=NULL;
            this->stride=0;
            this->own_memory=true;
            this->L=f.L;
            int numcomponents=hanalysis::order2numComponents(this->field_storage,this->field_type,this->L);
            int numVoxel=this->shape[0]*this->shape[1]*this->shape[2];
            hanalysis_cuda::gpu_malloc(this->data,numcomponents*numVoxel*sizeof(std::complex<T>));
            check=1;
        }

        if ((f.stride==0)&&(this->stride==0))
        {
            int numcomponents=hanalysis::order2numComponents(this->field_storage,this->field_type,this->L);
            this->setElementSize(f.getElementSize());
            hanalysis_cuda::gpu_memcpy_d2d(f.data,this->data,this->shape[0]*this->shape[1]*this->shape[2]*numcomponents*sizeof(std::complex<T>));
        }
        else
        {
            this->setElementSize(f.getElementSize());
            if (check==1)
                throw STAError("BullShit");

            if ((f.field_type!=hanalysis::STA_OFIELD_SINGLE)||
                    (this->field_type!=hanalysis::STA_OFIELD_SINGLE))
                throw STAError("operator= this cannot happen ! the input field must be  STA_OFIELD_SINGLE");
            // view -> copy
            int numcomponents_new=this->L+1;
            if (this->field_storage==STA_FIELD_STORAGE_C)
                numcomponents_new=2*this->L+1;
            numcomponents_new*=2;


            try
            {
                hanalysis_cuda::copySubfield2Subfield(f.data,
                                                      this->data,
                                                      this->getNumVoxel(),
                                                      2*f.getStride(),
                                                      2*this->getStride(),
                                                      numcomponents_new);
            } catch (hanalysis::STAError & error)
            {
                throw error;
            }
        }

        return *this;
    }


    stafield<T> gpu2cpu() const
    {
        try
        {
            stafield<T> result;
            this->set_death(&result);
            result=(*this);
            return  result;
        }
        catch (hanalysis::STAError & error)
        {
            throw error;
        }
    }

    stafieldGPU & operator=(const stafield<T> & f)
    {

      
        if (f.getStorage()==hanalysis::STA_FIELD_STORAGE_C)
            throw  (hanalysis::STAError("error STA_FIELD_STORAGE_C is not supported by stafieldGPU!"));

        if ((this->L==-1)&&(f.getRank()==-1))
            return *this;
        if (!f.oneBlockMem())
            throw  (hanalysis::STAError("error copying host memory to device memory, the host memory must be alignd in one single block!"));

        if ((*this)!=f)
        {
            if (this->stride!=0)
                throw STAError("warning: operator=  (stride!=0) shared memory block but alocating new (own) memory would be nrequired \n");

            if (!this->own_memory)
                throw STAError("warning: operator=  (!own_memory): shared memory block but alocating new (own) memory would be nrequired \n");

            if (this->own_memory && (this->data!=NULL))
                cudaFree(this->data);

            this->field_storage=f.getStorage();
            this->field_type=f.getType();
            this->shape[0]=f.getShape()[0];
            this->shape[1]=f.getShape()[1];
            this->shape[2]=f.getShape()[2];

            this->setElementSize(f.getElementSize());

            this->data=NULL;
            this->stride=0;
            this->own_memory=true;
            this->L=f.getRank();
            int numcomponents=hanalysis::order2numComponents(this->field_storage,this->field_type,this->L);
            hanalysis_cuda::gpu_malloc(this->data,numcomponents*this->getNumVoxel()*sizeof(std::complex<T>));
        }
 	    
        if (this->stride==0)
        {
            int numcomponents=hanalysis::order2numComponents(this->field_storage,this->field_type,this->L);
            this->setElementSize(f.getElementSize());
            T * const  src=( T* const)(f.getDataConst());
            hanalysis_cuda::gpu_memcpy_h2d(src,this->data,this->getNumVoxel()*numcomponents*sizeof(std::complex<T>));
        } else
            throw  (hanalysis::STAError("error copying host memory to device memory, the (existing) device memory must be alignd in one single block!"));
        return *this;
    }









    /*!
       \returns  a view on the (sub) component \f$ l \f$ with the   \n
       tensor rank  \f$ l \in \mathbb N \f$  of an orientation tensor field
     */
    stafieldGPU get(int l) const
    {
        if (l<0)
            throw  (hanalysis::STAError("error retrieving (sub) field l, l must be >= 0"));
        if (l>this->L)
            throw  (hanalysis::STAError("error retrieving (sub) field l, l must be <= L"));

        if ((this->field_type==STA_OFIELD_ODD)&&(l%2==0))
            throw  (hanalysis::STAError("error retrieving (sub) field l, l must be odd"));

        if ((this->field_type==STA_OFIELD_EVEN)&&(l%2!=0))
            throw  (hanalysis::STAError("error retrieving (sub) field l, l must be even"));

 	if ((this->field_type==STA_OFIELD_SINGLE)&&(l!=this->L))
             throw  (hanalysis::STAError("error retrieving (sub) field l, l must be equal to L"));


        T * component_data;
        int offset=hanalysis::getComponentOffset(this->field_storage,this->field_type,l);
        int numcomponents=hanalysis::order2numComponents(this->field_storage,this->field_type,this->L);

        component_data=this->data+2*offset;

        stafieldGPU  view(this->shape,
                          l,
                          this->field_storage,
                          hanalysis::STA_OFIELD_SINGLE,
                          component_data);
        this->set_death(&view);
        view.stride=numcomponents;

        view.setElementSize(this->getElementSize());
        return view;
    }


    /*!
      \returns  a view on the (sub) component \f$ l \f$ with the   \n
      tensor rank  \f$ l \in \mathbb N \f$  of an orientation tensor field
    */
    stafieldGPU operator[](int l) const
    {
        try
        {
            return this->get(l);
        }
        catch (hanalysis::STAError & error)
        {
            throw error;
        }
    }



    ~stafieldGPU()
    {
        if (this->own_memory && (this->data!=NULL))
        {
            if (do_classcount)
                printf("destroying stafieldGPU %i / remaining:  %i  [own]",this->classcount_id,--classcount);

            if (this->object_is_dead_soon<2)
            {
                if (do_classcount)
                    printf(" (deleting data)\n");

                if (hanalysis::verbose>0)
                    printf("field destrucor -> deleting data\n");
                cudaFree(this->data);
            }
            else
                if (do_classcount)
                    printf(" (not deleting data, still having references)\n");
        } else
        {
            if (do_classcount)
            {
	        if (this->L==-1)
		  printf("destroying stafieldGPU %i / remaining:  %i  [empty]\n",this->classcount_id,--classcount);
		else
		  printf("destroying stafieldGPU %i / remaining:  %i  [view]\n",this->classcount_id,--classcount);
            }
            if (hanalysis::verbose>0)
                printf("field destrucor -> --\n");
        }
    }




    static void Prod(const stafieldGPU & stIn1,
                     const stafieldGPU & stIn2,
                     stafieldGPU & stOut,
                     int J,
                     bool normalize=false,
                     std::complex<T> alpha= T( 1 ),
                     bool clear_result = false)
    {
        if ( ( std::abs ( stIn1.getRank()-stIn2.getRank() ) >J ) ||
                ( J>std::abs ( stIn1.getRank()+stIn2.getRank() ) ) )
            throw  (hanalysis::STAError("Prod: ensure that |l1-l2|<=J && |l1+l2|<=J"));
        if ( ( ( stIn1.getRank()+stIn2.getRank()+J ) %2!=0 ) && ( normalize ) )
            throw  (hanalysis::STAError("Prod: ensure that l1+l2+J even"));

        if (stOut.getRank()!=J)
            throw  (hanalysis::STAError("Prod: ensure that stOut has wrong Rank!"));


        if ((!stafieldGPU::equalShape(stIn1,stOut))||(!stafieldGPU::equalShape(stIn2,stOut)))
            throw  (hanalysis::STAError("Prod: shapes differ!"));
        if ((stIn1.getStorage()!=stOut.getStorage())||(stIn2.getStorage()!=stOut.getStorage()))
            throw  (hanalysis::STAError("Prod: storage type must be the same"));
        if (stIn1.getType()!=hanalysis::STA_OFIELD_SINGLE)
            throw  (hanalysis::STAError("Prod: first input field type must be STA_OFIELD_SINGLE"));
        if (stIn1.getType()!=stIn2.getType())
            throw  (hanalysis::STAError("Prod: first input field type must be STA_OFIELD_SINGLE"));
        if (stOut.getType()!=stIn1.getType())
            throw  (hanalysis::STAError("Prod: stOut field type must be STA_OFIELD_SINGLE"));

        int  stride_in1 = stIn1.getStride();
        int  stride_in2 = stIn2.getStride();
        int  stride_out = stOut.getStride();

        if (hanalysis::verbose>0)
            printf("Prod: stride_in1: %i,stride_in2: %i stride_out %i\n",stride_in1,stride_in2,stride_out);
        try
        {
            hanalysis_cuda::sta_product(
                stIn1.getDataConst(),
                stIn2.getDataConst(),
                stOut.getData(),
                stIn1.getShape(),
                stIn1.getRank(),
                stIn2.getRank(),
                stOut.getRank(),
                alpha,
                normalize,
                stIn1.getStorage(),
                2*stride_in1,
                2*stride_in2,
                2*stride_out,
                clear_result);
        } catch (hanalysis::STAError & error)
        {
            throw error;
        }
//         int err=hanalysis::sta_product(
//                     stIn1.getDataConst(),
//                     stIn2.getDataConst(),
//                     stOut.getData(),
//                     stIn1.getShape(),
//                     stIn1.getRank(),
//                     stIn2.getRank(),
//                     stOut.getRank(),
//                     alpha,
//                     normalize,
//                     stIn1.getStorage(),
//                     stride_in1,
//                     stride_in2,
//                     stride_out,
//                     clear_result);
        /*
                if (err==-1)
        	{
                    throw  (hanalysis::STAError("Prod: error!"));
        	}*/
    }


    static void Deriv(const stafieldGPU & stIn,
                      stafieldGPU & stOut,
                      int Jupdown,
                      bool conjugate=false,
                      std::complex<T> alpha= T( 1 ),
                      bool clear_result = false,
                      int accuracy=0
                     )
    {
        if (!stafieldGPU::equalShape(stIn,stOut))
            throw  (hanalysis::STAError("Deriv: shapes differ!"));
        if (stIn.getStorage()!=stOut.getStorage())
            throw  (hanalysis::STAError("Deriv: storage type must be the same"));
        if (stIn.getType()!=hanalysis::STA_OFIELD_SINGLE)
            throw  (hanalysis::STAError("Deriv: first input field type must be STA_OFIELD_SINGLE"));
        if (stOut.getType()!=stIn.getType())
            throw  (hanalysis::STAError("Deriv: stOut field type must be STA_OFIELD_SINGLE"));
        if (stOut.getRank()!=stIn.getRank()+Jupdown)
            throw  (hanalysis::STAError("Deriv: stOut rank must be input rank+Jupdown"));

        int  stride_in = stIn.getStride();
        int  stride_out = stOut.getStride();

        if (hanalysis::verbose>0)
            printf("Deriv: stride_in: %i stride_out %i\n",stride_in,stride_out);

        try {
            hanalysis_cuda::sta_derivatives(
                stIn.getDataConst(),
                stOut.getData(),
                stIn.getShape(),
                stIn.getRank(),
                Jupdown,
                conjugate,
                alpha,
                stIn.getStorage(),
                stIn.getElementSize(),
                2*stride_in,
                2*stride_out,
                clear_result,
                accuracy);
        } catch (hanalysis::STAError & error)
        {
            throw error;
        }

//         int err=hanalysis::sta_derivatives(
//                     stIn.getDataConst(),
//                     stOut.getData(),
//                     stIn.getShape(),
//                     stIn.getRank(),
//                     Jupdown,
//                     conjugate,
//                     alpha,
//                     stIn.getStorage(),
//                     stIn.getElementSize(),
//                     stride_in,
//                     stride_out,
//                     clear_result,
// 		    accuracy);
        /*
                if (err==-1)
                    throw  (hanalysis::STAError("Deriv: error!"));*/
    }



    static void FFT(const stafieldGPU & stIn,
                    stafieldGPU & stOut,
                    bool forward,
                    bool conjugate=false,
                    T alpha= T( 1 ))
    {
        if     ((!equalShape(stIn,stOut))||
                (stIn.field_type!=stOut.field_type)||
                (stIn.L!=stOut.L)||
                (((stIn.field_storage==STA_FIELD_STORAGE_C)||
                  (stOut.field_storage==STA_FIELD_STORAGE_C))&&
                 (stIn.field_storage!=stOut.field_storage)))
            throw  (hanalysis::STAError("FFT: shapes differ!"));

        std::size_t ncomponents_in=hanalysis::order2numComponents(stIn.getStorage(),stIn.getType(),stIn.L);
        std::size_t ncomponents_out=hanalysis::order2numComponents(stOut.getStorage(),stOut.getType(),stOut.L);
        if (((stIn.stride!=0)&&(ncomponents_in!=stIn.stride))||((stOut.stride!=0)&&(ncomponents_out!=stOut.stride)))
            throw  (hanalysis::STAError("FFT: doesn't work on this kind of views!"));
        if ((stIn.data==stOut.data))
            throw  (hanalysis::STAError("FFT: inplace not supported!"));


        hanalysis::STA_FIELD_STORAGE new_field_storage=stIn.getStorage();
        if (new_field_storage==hanalysis::STA_FIELD_STORAGE_R)
            new_field_storage=hanalysis::STA_FIELD_STORAGE_RF;
        else
            if (new_field_storage==hanalysis::STA_FIELD_STORAGE_RF)
                new_field_storage=hanalysis::STA_FIELD_STORAGE_R;
        stOut.field_storage=new_field_storage;

        if ((stIn.getStorage()==STA_FIELD_STORAGE_C)&&(stOut.getStorage()!=STA_FIELD_STORAGE_C))
            throw  (hanalysis::STAError("FFT: storage type must be the same"));




        int  stride_in = stIn.getStride();
        int  stride_out = stOut.getStride();
        int ncomponents=hanalysis::order2numComponents(stIn.getStorage(),stIn.getType(),stIn.L);

        if (hanalysis::verbose>0)
            printf("FFT: stride_in: %i stride_out %i , ncomp: %i\n",stride_in,stride_out,ncomponents);
        try {
            hanalysis_cuda::sta_fft(
                stIn.getDataConst(),
                stOut.getData(),
                stIn.getShape(),
                ncomponents,
                forward,
                conjugate,
                alpha);
        } catch (hanalysis::STAError & error)
        {
            throw error;
        }
    }



    static void Lap(const stafieldGPU & stIn,
                    stafieldGPU & stOut,
                    T alpha= T( 1 ),
                    bool clear_result = false)
    {
        if (stIn!=stOut)
            throw  (hanalysis::STAError("Lap: shapes differ!"));
        if (stIn.getStorage()!=stOut.getStorage())
            throw  (hanalysis::STAError("Lap: storage type must be the same"));
        int  stride_in = stIn.getStride();
        int  stride_out = stOut.getStride();
        int ncomponents=hanalysis::order2numComponents(stIn.getStorage(),stIn.getType(),stIn.L);


        if (hanalysis::verbose>0)
            printf("Lap: stride_in: %i stride_out %i , ncomp: %i\n",stride_in,stride_out,ncomponents);


        try {
            hanalysis_cuda::sta_laplace (
                stIn.getDataConst(),
                stOut.getData(),
                stIn.getShape(),
                ncomponents,
                alpha,
                stride_in,
                stride_out,
                clear_result);
        } catch (hanalysis::STAError & error)
        {
            throw error;
        }


    }


    /// see \ref Lap
    stafieldGPU    lap(T alpha= T( 1 )) const
    {

        try
        {
            stafieldGPU result(this->shape,this->L,this->field_storage,this->field_type);
            this->set_death(&result);
            Lap(*this,result,alpha,true);
            return  result;
        }
        catch (hanalysis::STAError & error)
        {
            throw error;
        }
    }


    ///  see \ref FFT
    stafieldGPU fft(bool forward,
                    bool conjugate=false,
                    T alpha= T( 1 )) const
    {
        try
        {
            stafieldGPU result(this->shape,this->L,this->field_storage,this->field_type);
            this->set_death(&result);
            FFT(*this,result,forward,conjugate,alpha);
            return  result;
        }
        catch (hanalysis::STAError & error)
        {
            throw error;
        }
    }


    ///  see \ref Deriv
    stafieldGPU  deriv(int J,
                       bool conjugate=false,
                       std::complex<T> alpha= T( 1 ),
                       int accuracy=0
                      ) const
    {
        try
        {
            stafieldGPU result(this->shape,this->L+J,this->field_storage,this->field_type);
            this->set_death(&result);
            Deriv(*this,result,J,conjugate,alpha,true,accuracy);
	    
            return  result;
        }
        catch (hanalysis::STAError & error)
        {
	  	    
            throw error;
        }

    }

    ///  see \ref Prod
    stafieldGPU  prod(const stafieldGPU & b,
                      int J,
                      bool normalize=false,
                      std::complex<T> alpha= T( 1 )) const
    {
        try
        {
            stafieldGPU result(this->shape,J,this->field_storage,this->field_type);
            this->set_death(&result);
            //printf("prod prod\n");
            //printf("%d %d %d \n",this->getRank(),b.getRank(),result.getRank());
            Prod(*this,b,result,J,normalize,alpha,true);
            return  result;
        }
        catch (hanalysis::STAError & error)
        {
            throw error;
        }
    }
    
    
    //void printf(const char* arg1, int arg2);

};


}



#endif

