function sta_setPaths
%maxNumCompThreads(6);
[pathstr, name, ext]=fileparts([mfilename('fullpath'),'.m']);
addpath(pathstr);
addpath([pathstr,'/matlab_help']);
addpath([pathstr,'/sta_toolbox_matlab']);
addpath([pathstr,'/applications/shfilter']);
%addpath([pathstr,'/applications/hfilter']);
%addpath([pathstr,'/applications/invariants']);
addpath([pathstr,'/applications/anisoDiffusion']);
%addpath([pathstr,'/applications/features']);
%addpath([pathstr,'/applications/shog-filter']);
%addpath([pathstr,'/applications/simu']);
%addpath([pathstr,'/applications/brainpar']);
%addpath([pathstr,'/applications/SIFT']);
%addpath([pathstr,'/applications/rf_matlab']);
addpath([pathstr,'/applications/trainable_filters']);
addpath([pathstr,'/docs/']);
addpath(['/usr/local/include'])