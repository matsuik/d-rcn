function sta_setup_data=my_sta_mex_setup

sta_setPaths;
[pathstr, name, ext]=fileparts([mfilename('fullpath'),'.m']);



%header_sta=['-largeArrayDims -I',pathstr,'/sta_toolbox/ '];  %Large Array 
header_sta=['-I',pathstr,'/sta_toolbox/ '];

header_sta=[header_sta,' -I',pathstr,'/sta_toolbox_matlab/ '];
header_fftw=['-I',pathstr,'/fftw/ '];

is_matlab=(exist('mkoctfile')==0);

sta_setup_data.is_matlab=is_matlab;


OS=-1;
OS_WIN=1;
OS_UNIX=2;
OS_MAC=3;
if ispc
    OS=OS_WIN;
    sta_setup_data.OS='W';    
elseif isunix
    OS=OS_UNIX;
    sta_setup_data.OS='L';    
elseif ismac
    OS=OS_MAC;
    sta_setup_data.OS='M';    
end;





%######################################
%
%   MATLAB
%
%###################################### 
if is_matlab
    sta_setup_data.mex_cmd_prefix=['mex '];
    sta_setup_data.mex_cmd_postfix=[''];
    
    
    % the resulting command will be 
    % mex PREFIX [filename.cc] POSTFIX


    switch OS
        %######################################
        %
        %   setup win
        %
        %######################################
        case OS_WIN
            
            % extern fft might be preferable. However,
            % I only tested it using the windows 7 SDK (VC 10 compiler)
            % so default is setting='intern_fftw';
            
            setting='intern_fftw';
            %setting='extern_fftw';
            
            switch (setting)
                case 'intern_fftw'
                    PREFIX={...
                        ' COMPFLAGS="$COMPFLAGS -Ot  -Wall "',...
                        ' -D_STA_NOGSL ',...
                    };

                    POSTFIX={...
                        header_sta,...
                    };

                case 'extern_fftw'
                    PREFIX={...
                        ' COMPFLAGS="$COMPFLAGS -Ot  -Wall "',...
                        ' -D_STA_NOGSL ',...
                        ' -D_STA_LINK_FFTW ',...
                    };

                    POSTFIX={...
                        ' -lfftw3-3 -lfftw3f-3 ',...    %FFTW
                        header_sta,...
                        header_fftw,...        
                        '-L\projects\sta-toolbox/fftw/VC10/win64/'...
                    };
            end;
            
            sta_setup_data.mex_cmd_prefix=[sta_setup_data.mex_cmd_prefix,PREFIX{:}];
            sta_setup_data.mex_cmd_postfix=[sta_setup_data.mex_cmd_postfix,POSTFIX{:}];
    
        %######################################
        %
        %   setup linux
        %
        %######################################
        case OS_UNIX
            
            % for the default settings 
            % please install the dev-files for the
            % GSL http://www.gnu.org/software/gsl/
            % and
            % http://www.fftw.org/
            % in ubuntu e.g:
            % sudo apt-get install  libgsl0-dev  libfftw3-dev
            %
            % otherwise choose setting='intern_fftw_nogsl';
            
            
            % if you use g++>=4.8 and get an error like 
            % "`GLIBCXX_3.4.11' not found" try adding
            %'LDOPTIMFLAGS="-O -static-libgcc -static-libstdc++"',...   
            % to PREFIX
            
            %setting='intern_fftw';
            setting='extern_fftw';
            %setting='extern_fftw_omp';
            %setting='intern_fftw_nogsl';
            %setting='clang';
            
            switch (setting)
                case 'intern_fftw'
                    
                       PREFIX={...
                            header_sta,...
                            ' CXXFLAGS="${CXXFLAGS} -fPIC -fexceptions -Wno-unused-function -Wno-unknown-pragmas -O2 -Wall -march=native -fopenmp" ',...
                            ' CFLAGS="${CFLAGS} -fPIC -fexceptions -Wno-unused-function -Wno-unknown-pragmas -O2 -Wall -march=native -fopenmp" ',...
                        };

                       POSTFIX={...
                           ' -lgsl ',...               %GSL
                           ' -lgomp ',...              %OPEN MP
                           ' -lgslcblas ',...          %GSLBLAS
                           ' -D_GNU_SOURCE ',...
                           ' -D_STA_MULTI_THREAD ',...
                       };        

                case 'extern_fftw'
                    
                        PREFIX={...
                            header_sta,...
                            header_fftw,...
                            ' CXXFLAGS="${CXXFLAGS} -fPIC -fexceptions -Wno-unused-function -Wno-unknown-pragmas -O2 -Wall -march=native -fopenmp" ',...
                            ' CFLAGS="${CFLAGS} -fPIC -fexceptions -Wno-unused-function -Wno-unknown-pragmas -O2 -Wall -march=native -fopenmp" ',...
                        };

                       POSTFIX={...
                           ' -lgsl ',...               %GSL
                           ' -lgomp ',...              %OPEN MP
                           ' -lfftw3 -lfftw3f ',...    %FFTW
                           ' -lgslcblas ',...          %GSLBLAS
                           ' -D_GNU_SOURCE ',...
                           ' -D_STA_MULTI_THREAD ',...
                           ' -D_STA_LINK_FFTW ',...
                       };   
                   
                case 'extern_fftw_omp'
                    
                        PREFIX={...
                            header_sta,...
                            header_fftw,...
                            ' CXXFLAGS="${CXXFLAGS} -fPIC -fexceptions -Wno-unused-function -Wno-unknown-pragmas -O2 -Wall -march=native -fopenmp" ',...
                            ' CFLAGS="${CFLAGS} -fPIC -fexceptions -Wno-unused-function -Wno-unknown-pragmas -O2 -Wall -march=native -fopenmp" ',...
                        };

                       POSTFIX={...
                           ' -lgsl ',...               %GSL
                           ' -lgomp ',...              %OPEN MP
                           ' -lfftw3 -lfftw3f -lfftw3_omp -lfftw3f_omp ',...    %FFTW
                           ' -lgslcblas ',...          %GSLBLAS
                           ' -D_GNU_SOURCE ',...
                           ' -D_STA_MULTI_THREAD ',...
                           ' -D_STA_LINK_FFTW ',...
                           ' -D_STA_FFT_MULTI_THREAD ',...
                       };                

                case 'intern_fftw_nogsl'
                    
                        PREFIX={...
                            header_sta,...
                            ' CXXFLAGS="${CXXFLAGS} -fPIC -fexceptions -Wno-unused-function -Wno-unknown-pragmas -O2 -Wall -march=native -fopenmp" ',...
                            ' CFLAGS="${CFLAGS} -fPIC -fexceptions -Wno-unused-function -Wno-unknown-pragmas -O2 -Wall -march=native -fopenmp" ',...
                        };

                       POSTFIX={...
                           ' -lgomp ',...              %OPEN MP
                           ' -D_STA_NOGSL ',...
                           ' -D_STA_MULTI_THREAD ',...
                       }; 
                   
                case 'clang'
                    
                        PREFIX={...
                            header_sta,...
                            ' CXXFLAGS="${CXXFLAGS} -fPIC -fexceptions -Wno-unused-function -Wno-unknown-pragmas -O2 -Wall -march=native" ',...
                            ' CFLAGS="${CFLAGS} -fPIC -fexceptions -Wno-unused-function -Wno-unknown-pragmas -O2 -Wall -march=native" ',...
                        };
                    
                       POSTFIX={...
                           ' -lgsl ',...               %GSL
                           ' -lfftw3 -lfftw3f ',...    %FFTW
                           ' -lgslcblas ',...          %GSLBLAS
                           ' -D_GNU_SOURCE ',...
                           ' -D_STA_LINK_FFTW ',...
                       };                     

%                        POSTFIX={...
%                            ' -D_STA_NOGSL ',...
%                        };          
             end;
    

                       

            %            POSTFIX={...
            %                ' -lgsl ',...               %GSL
            %                ' -lgomp ',...              %OPEN MP
            %                ' -lgslcblas ',...          %GSLBLAS
            %                ' -D_GNU_SOURCE ',...
            %            };            
            
            sta_setup_data.mex_cmd_prefix=[sta_setup_data.mex_cmd_prefix,PREFIX{:}];
            sta_setup_data.mex_cmd_postfix=[sta_setup_data.mex_cmd_postfix,POSTFIX{:}];
    
        %######################################
        %
        %   setup mac
        %
        %######################################            
        case OS_MAC
            
            
    end;
    
%######################################
%
%   OCTAVE
%
%###################################### 
else
    sta_setup_data.mex_cmd_prefix=['mkoctfile --mex '];
    sta_setup_data.mex_cmd_postfix=[''];   
    
    
    switch OS
        %######################################
        %
        %   setup win
        %
        %######################################
        case OS_WIN

            
        %######################################
        %
        %   setup linux
        %
        %######################################
        case OS_UNIX
            PREFIX={...
            };
        
            POSTFIX={...
                ' -lgsl ',...               %GSL
                ' -lgomp ',...              %OPEN MP
                ' -lfftw3 -lfftw3f ',...    %FFTW
                ' -lgslcblas ',...          %GSLBLAS
                    header_sta,...
                    header_fftw,...                
                ' -D_GNU_SOURCE ',...
                ' -D_STA_MULTI_THREAD ',...
                ' -D_STA_LINK_FFTW ',...
                ' -DDEF ',...
                ' -Wno-format -Wno-unknown-pragmas -Wno-unused-function',...
            };
            
            sta_setup_data.mex_cmd_prefix=[sta_setup_data.mex_cmd_prefix,PREFIX{:}];
            sta_setup_data.mex_cmd_postfix=[sta_setup_data.mex_cmd_postfix,POSTFIX{:}];
    
        %######################################
        %
        %   setup mac
        %
        %######################################            
        case OS_MAC
            
            
    end;
        
    
    
    
end;



