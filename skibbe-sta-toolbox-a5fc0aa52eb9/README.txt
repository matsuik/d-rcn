  The most current version  can be found at
  https://bitbucket.org/skibbe/sta-toolbox/

    Note that this software is still under development. 

  requires:  
  --------
    matlab or octave 
      - http://www.gnu.org/software/octave/
      
  optional:    
  --------
    GNU Scientific Library (GSL) -- development package
      - http://www.gnu.org/software/gsl/
    FFTW library for computing Fast Fourier Transforms
      - http://www.fftw.org/


  installation:
  -------------
    - [Ubuntu: install the packages listed in "installation (Ubuntu) Linux"]
    - start matlab or octave 
    - In the root directory of the toolbox is a script called
      > sta_setPaths 
      which will TEMPORARILY register all required paths in
      matlab by calling "addpath". Note that if you want to use 
      the toolbox you have to call this script every time after 
      starting matlab.
    - then run
      > sta_mexme
      if you run it the first time it will ask for creating a settings
      file called "my_sta_mex_setup.m" for you. Within this file you
      may change the compiler settings used by the sta_mexme script.
      If you are done with editing the file run
      > sta_mexme
      again to start the generation of the mex files.
      
      generation of doc help (matlab only)
      -----------
      - enter the docs directory
	> cd docs
	and run the script 
	> sta_updatehelp
        This script calls some demo scripts and TEMPORARILY generates a
        new tab in your matlab html-help. You will find the docs
        in the supplemental-software section of 
        > doc
        note that they only (temporarily) appear after running sta_setPaths
     
	  
    
  installation (Ubuntu) Linux:
  ---------------------------
    - install the following Ubuntu packages: 
		     libgsl0-dev  
		     libfftw3-dev 
		     (octave-headers)



  licence:
  -------
    GPL 3.0
    see http://www.gnu.org/licenses/


  successfully compiled on
  ----------------------

    - ubuntu linux 14.04, 64-bit, gcc-4.9.1 and matlab R2013b 
    - ubuntu linux 13.10, 64-bit, gcc-4.8 and matlab R2013b
    - ubuntu linux 13.10, 64-bit, clang++ 3.4 and matlab R2013b
    - windows 7, 64-bit, matlab R2012b, VC 10 compiler 
    - ubuntu linux 13.10, 64-bit, gcc-4.8 and GNU Octave, version 3.8.1
