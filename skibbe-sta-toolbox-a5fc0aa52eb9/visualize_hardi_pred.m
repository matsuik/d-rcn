ax = [37 101 27 83]
sta_setPaths 
order_dict = containers.Map
order_dict('0') = 1
order_dict('2') = 2
order_dict('4') = 3

result_dir = 'July10_2_024'
for number = {'1506', '1507'}
    number = char(number)
    data=load(['../data/hardi_in_ex/20160407/', number, '_exvivo_hardi.mat']);
    tdata=data.tensor_features;
    if strcmp(number, '1506')
        gamma = -0.3;
        setting = 'train';
    elseif strcmp(number, '1507')
        gamma = -0.2;
        setting = 'test'
    end
    for order = {'2'}
        order = char(order)
        load(['../results/hardi_in_ex/', result_dir '/a', order, '_all_', setting, '_', number, '.mat'])
        l=order_dict(order);shape=size(tdata{l});
        for slice = [14, 20]
            if (l==1),ofieldF=stafieldStruct(shape(3:5),0,'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');ofieldF.data(1,1,:,:,:)=pred; else ofieldF=stafieldStruct(reshape(pred,[2,4*(l-1)+1,shape(3:5)]),'STA_OFIELD_SINGLE','STA_FIELD_STORAGE_C');end;
            sta_glyph(ofieldF,'gamma',-0.3,'mask',data.wm>0.5,'init_view',3,'init_slice',slice,'backgound',data.b0);axis(ax);
            saveas(gcf, ['../results/hardi_in_ex/', result_dir, '/', number, setting, num2str(l), num2str(slice), '.png'])
        end
    end
end

        
        
        
        